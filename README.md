Capnet: An SDN Controller and Tools for Capability-based Networks
================================================================================

Capnet is a family of SDN-based tools that allow you to build networks
in which edge hosts are granted least-privilege authority, instead of
them having more traditional ambient authority.  Capnet hosts build
network communication paths by exchanging capabilities, especially flow
capabilities.  If a host `n` holds a flow capability to another host
`m`, `n` can send traffic to `m`.  `n` would have received that
capability either via grant from `m`, or from an administrative
capability grant.  Capnet network participants exchange capabilities
using a custom control plane protocol, via the SDN controller.  The
controller arbitrates and validates the exchange of capabilities.
Capnet provides a Python binding to this control plane protocol, so it
is easy to write programs that construct Capnet networks.


Supported Platforms
-------------------

Capnet builds and runs on Linux.  It should run on other platforms, but
we haven't tested anything other than Linux+OpenVSwitch yet.


Obtaining the Software
----------------------

You can download or browse the Capnet source repository at
<https://gitlab.flux.utah.edu/tcloud/capnet>.


Building and Installing the Software
------------------------------------

You can find build requirements and instructions in the source
repository in `INSTALL.md`.


Experimenting with Capnet on your Desktop
-----------------------------------------

There are two ways to experiment with Capnet.  First, if you want to
experiment in the context of Linux network namespaces on your desktop,
without working too hard to set anything up, you can refer to and use
the `etc/ovs-ns-net.sh` shell script.  This constructs simple
combinations of network namespaces and openvswitch bridges; and runs
openmul and the capnet controller for you.  You could consider it to be
a much-slimmed-down, fully-integrated version of mininet.  It is
self-documenting; just run it without arguments to see its USAGE message.
For instance, you could run

    $ etc/ovs-net-ns.sh create capnet1 3 1

to obtain a simple three-namespace network connected by an OVS bridge
named `capnet`.  You would then start openmul and the controller:

    $ etc/ovs-net-ns.sh mul_start capnet1

And finally, you could also run 

    $ etc/ovs-net-ns.sh portshell capnet1 2

to get a shell inside the Linux network namespace corresponding to port
2.  If you want one of the ports to host a DHCP server, there is
documentation in the USAGE message to help guide you.  If you want a
multi-switch topology, there is also guidance to set that up (although
for now each switch can only have one uplink port through which all
other switches are reachable).

Of course, you'll actually want to run some workflow agents to create
custom connectivity between your nodes.  You can find some example
agents in `scripts/`; for instance, you might look at `ping_master.py`
and `ping_sub.py` to setup bidirectional connectivity between a pair of
nodes.


Experimenting with Capnet in OpenStack
--------------------------------------

If you want to try out Capnet "in the cloud", we've written an OpenStack
Neutron plugin to allow you to do exactly that.  You can find that
plugin, and its installation, configuration, and usage instructions, at
https://gitlab.flux.utah.edu/tcloud/networking-capnet .

To make it easier, you can also sign up for a Cloudlab account, and
create an experiment using `OpenStack-Capnet` profile
(https://www.cloudlab.us/p/TCloud/OpenStack-Capnet).  This profile will
create a personal OpenStack cloud for you on the Cloudlab cluster of
your choice, fully configured; and it will install the entire Capnet
software stack and set up some basic demo-ready features.  There are
instructions in that repository that show you how to run an example
Capnet cooperative, multi-party network configuration where one
OpenStack project provides Hadoop software and network configuration as
a service to other tenants on-demand.


Authors
-------

Capnet is developed at the [University of Utah], in the [Flux Research
Group], by [Josh Kunz] {josh@kunz.xyz} and [David Johnson]
{<johnsond@cs.utah.edu>}.  The project and its design originated with
and continue under [Anton Burtsev] {<aburtsev@uci.edu>}, [Kobus Van
der Merwe] {<kobus@cs.utah.edu>}, and [Eric Eide] {<eeide@cs.utah.edu>}.
An early version of Capnet was demonstrated as part of [Jithu Joseph]'s
[Master's thesis].


[University of Utah]: http://www.utah.edu
[Flux Research Group]: http://www.flux.utah.edu
[David Johnson]: http://www.flux.utah.edu/profile/johnsond
[Josh Kunz]: http://www.flux.utah.edu/profile/josh
[Kobus Van der Merwe]: http://www.flux.utah.edu/profile/kobus
[Anton Burtsev]: http://www.flux.utah.edu/profile/aburtsev
[Eric Eide]: http://www.flux.utah.edu/profile/eeide
[Jithu Joseph]: http://www.flux.utah.edu/profile/jithu
