#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <pthread.h>

#include "obj.h"
#include "capnet_log.h"
#include "controller.h"
#include "metadata.h"

extern struct cnc_opts cnc_opts;

struct metadata_file_state {
    struct stat last_sbuf;
    GHashTable *ports;
    pthread_t thread;
    GHashTable *pending_adds;
    GHashTable *pending_dels;
    int thread_stop;
    cn_switch_t *csw;
    pthread_rwlock_t lock;
};

struct metadata_file_port {
    int64_t portno;
    char *domainid;
    char *ownerid;
    char *id;
    char *name;
    struct ether_addr mac;
    cn_node_service_t services;
    int addr_valid:1,
        mask_valid:1,
        addr6_valid:1,
        isnode:1,
        isuplink:1,
        ismaster:1;
    struct in_addr addr;
    struct in_addr mask;
    struct in6_addr addr6;
};

static void __metadata_file_port_free(void *rock) {
    struct metadata_file_port *port = (struct metadata_file_port *)rock;

    if (port->name)
        free(port->name);
    if (port->id)
        free(port->id);
    if (port->domainid)
        free(port->domainid);
    if (port->ownerid)
        free(port->ownerid);

    free(port);
}

/**
 ** A metadata service that gets its data from files.
 **/

int metadata_file_has_switch(uint64_t dpid) {
    char namebuf[PATH_MAX];
    struct stat sbuf;

    if (!cnc_opts.switch_metadata_dir) {
        cncerr("no metadata dir!\n");
        return -1;
    }

    snprintf(namebuf,sizeof(namebuf),"%s/cnc.metadata.%.16"PRIx64,
             cnc_opts.switch_metadata_dir,dpid);

    if (stat(namebuf,&sbuf)) {
        cncwarn("could not find metadata file %s\n",namebuf);
        return -1;
    }

    c_log_debug("Found metadata file: %s\n", namebuf);

    return 1;
}

static int __metadata_file_update_switch_info(cn_switch_t *csw) {
    struct metadata_file_state *state;
    char namebuf[PATH_MAX];
    struct stat sbuf;
    char *buf = NULL;
    int rc;
    FILE *file;
    int port;
    char *domainid,*ownerid,*role,*id,*name,*smac,*ipaddr,*smask,*ipaddr6;
    int lines = 0;
    GHashTable *newports;
    struct metadata_file_port *nport;
    struct ether_addr mac;
    struct in_addr addr;
    struct in_addr mask;
    struct in6_addr addr6;
    int isnode,isuplink,ismaster;
    char *service_idx;
    cn_node_service_t services = 0;

    state = (struct metadata_file_state *)csw->metadata_priv;

    snprintf(namebuf,sizeof(namebuf),"%s/cnc.metadata.%.16"PRIx64,
             cnc_opts.switch_metadata_dir,csw->mul_switch->dpid);

    if (stat(namebuf,&sbuf)) {
        cncerr("could not find metadata file %s\n",namebuf);
        return -1;
    }

    /* If not modified, no worries. */
    if (sbuf.st_mtim.tv_sec == state->last_sbuf.st_mtim.tv_sec
        && sbuf.st_mtim.tv_nsec == state->last_sbuf.st_mtim.tv_nsec)
        return 0;

    cncdbg(1,LA_CTL,LF_MD,"Updating metadata for switch %lx from %s...\n",
           csw->mul_switch->dpid,namebuf);

    file = fopen(namebuf,"r");
    if (!file)
        return -1;

    newports = g_hash_table_new_full(g_direct_hash,g_direct_equal,
                                     NULL,__metadata_file_port_free);

    /* Load in the new ports */
    buf = malloc(1024);
    while (fgets(buf,1024,file)) {
        ++lines;

        if (buf[0] == '#')
            continue;

        isnode = isuplink = ismaster = 0;
        services = 0;
	service_idx = NULL;
        domainid = ownerid = role = id = name = smac = ipaddr = smask = ipaddr6 = NULL;
        rc = sscanf(buf,"node %d %ms %ms %ms %ms %ms %ms %ms %ms %ms",
                    &port,&domainid,&ownerid,&role,&id,&name,&smac,&ipaddr,&smask,
                    &ipaddr6);
        if (rc > 0 && rc != 7 && rc != 9 && rc != 10) {
            cncerr("bad line %d in %s, (rc=%d)\n",lines,namebuf, rc);
            rc = -10;
            goto badline;
        }
        else if (rc > 0) {
            /* Good port line */
            isnode = 1;

            /* Maybe detect a node service or two */
            service_idx = index(role,':');
	    if (service_idx) {
		*service_idx = '\0';
		++service_idx;
		if (strstr(service_idx,"dhcp") != NULL)
		    services |= CN_NODE_SERVICE_DHCP;
		if (strstr(service_idx,"osmetadata") != NULL)
		    services |= CN_NODE_SERVICE_OSMETA;
	    }

            if (strcmp("master",role) == 0)
                ismaster = 1;
            /* Free up role right away; we're done with it. */
            free(role);
            role = NULL;
        }
        else if (rc == 0) {
            /* Check for uplink line */
            rc = sscanf(buf,"uplink %d %ms %ms %ms",&port,&id,&name,&smac);
            if (rc != 4) {
                cncerr("bad line %d in %s, (rc=%d)\n",lines,namebuf,rc);
                rc = -11;
                goto badline;
            }
            isuplink = 1;
        }
        
        if (g_hash_table_lookup(newports,GINT_TO_POINTER(port))) {
            cncerr("duplicate port %d in line %d in %s\n",port,lines,namebuf);
            rc = -11;
            goto badline;
        }
        if (!ether_aton_r(smac,&mac)) {
            cncerr("bad mac addr %s in line %d in %s\n",smac,lines,namebuf);
            rc = -12;
            goto badline;
        }
        if (isnode) {
            if (ipaddr && inet_pton(AF_INET,ipaddr,&addr) != 1) {
                cncerr("bad inet addr %s in line %d in %s\n",
                       ipaddr,lines,namebuf);
                rc = -12;
                goto badline;
            }
            if (smask) {
                if (index(smask,'.') && inet_pton(AF_INET,smask,&mask) != 1) {
                    cncerr("bad inet mask %s in line %d in %s\n",
                           smask,lines,namebuf);
                    rc = -12;
                    goto badline;
                }
                else {
                    uint32_t cmask = 0x80000000U;
                    int nbits = atoi(smask);
                    int i;
                    for (i = 0; i < nbits; ++i) {
                        cmask >>= 1;
                        cmask |= 0x80000000U;
                    }
                    mask.s_addr = htonl(cmask);
                }
            }
            if (ipaddr6 && inet_pton(AF_INET6,ipaddr6,&addr6) != 1) {
                cncerr("bad inet6 addr %s in line %d in %s\n",
                       ipaddr6,lines,namebuf);
                rc = -12;
                goto badline;
            }
        }

        /* Otherwise, save the port */
        nport = calloc(1,sizeof(*nport));
        if (isnode)
            nport->isnode = 1;
        else if (isuplink)
            nport->isuplink = 1;
        if (ismaster)
            nport->ismaster = 1;
	nport->services = services;
        nport->portno = port;
        nport->domainid = domainid;
	nport->ownerid = ownerid;
        nport->id = id;
        nport->name = name;
        nport->mac = mac;
        if (ipaddr) {
            nport->addr_valid = 1;
            nport->addr = addr;
        }
        if (smask) {
            nport->mask_valid = 1;
            nport->mask = mask;
        }
        if (ipaddr6) {
            nport->addr6_valid = 1;
            nport->addr6 = addr6;
        }

        g_hash_table_insert(newports,GINT_TO_POINTER(port),nport);

        cncdbg(1,LA_CTL,LF_MD,"Inserted new port %d, mac %s, switch %ull\n",
               port,smac,csw->mul_switch->dpid);

        continue;

    badline:
        if (role)
            free(role);
        if (domainid)
            free(domainid);
        if (ownerid)
            free(ownerid);
        if (name)
            free(name);
        if (id)
            free(id);
        if (smac)
            free(smac);
        if (ipaddr)
            free(ipaddr);
        if (smask)
            free(smask);
        if (ipaddr6)
            free(ipaddr6);

        goto badfile;
    }
    fclose(file);

    /* Blow away all port data if we already have some */
    if (state->ports)
        g_hash_table_destroy(state->ports);
    state->ports = newports;
    state->last_sbuf = sbuf;

    free(buf);
    return 0;

 badfile:
    if (newports)
        g_hash_table_destroy(newports);
    return rc;
}

static int metadata_file_update_switch_info(cn_switch_t *csw) {
    int rc;
    struct metadata_file_state *state;

    state = (struct metadata_file_state *)csw->metadata_priv;
    if (!state)
        return -1;

    pthread_rwlock_wrlock(&state->lock);
    rc = __metadata_file_update_switch_info(csw);
    pthread_rwlock_unlock(&state->lock);

    return rc;
}

int metadata_file_init_switch(cn_switch_t *csw) {
    char namebuf[PATH_MAX];
    struct stat sbuf;
    struct metadata_file_state *state;
    int rc;

    state = calloc(1,sizeof(struct metadata_file_state));
    memset(&state->last_sbuf,0,sizeof(state->last_sbuf));
    state->ports = g_hash_table_new_full(g_direct_hash,g_direct_equal,
                                         NULL,__metadata_file_port_free);
    state->pending_adds = g_hash_table_new_full(g_direct_hash,g_direct_equal,
                                                NULL,NULL);
    state->pending_dels = g_hash_table_new_full(g_direct_hash,g_direct_equal,
                                                NULL,NULL);
    pthread_rwlock_init(&state->lock,NULL);
    /* XXX: this will need to be ref counted and liveness-tracked. */
    state->csw = csw;

    csw->metadata_priv = state;

    pthread_rwlock_wrlock(&state->lock);
    rc = __metadata_file_update_switch_info(csw);
    pthread_rwlock_unlock(&state->lock);

    return rc;
}

void metadata_file_fini_switch(cn_switch_t *csw) {
    struct metadata_file_state *state;

    state = (struct metadata_file_state *)csw->metadata_priv;
    if (!state)
        return;

    pthread_rwlock_wrlock(&state->lock);

    if (state->ports) {
        g_hash_table_destroy(state->ports);
        state->ports = NULL;
    }
    if (state->pending_adds) {
        g_hash_table_destroy(state->pending_adds);
        state->pending_adds = NULL;
    }
    if (state->pending_dels) {
        g_hash_table_destroy(state->pending_dels);
        state->pending_dels = NULL;
    }
    state->csw = NULL;

    free(state);
}

void *metadata_file_watcher(void *arg) {
    cn_switch_t *csw;
    struct metadata_file_state *state;
    uint64_t dpid;
    uint16_t port_no;
    gpointer gp;
    GHashTableIter iter;
    struct metadata_file_port *_port;
    GList *cur;
    GList *addlist;

    csw = (cn_switch_t *)arg;
    state = (struct metadata_file_state *)csw->metadata_priv;
    dpid = csw->mul_switch->dpid;

    while (!state->thread_stop) {
        usleep(1000);

        pthread_rwlock_wrlock(&state->lock);

        __metadata_file_update_switch_info(csw);

        /* Check if any pending ports. */
        addlist = NULL;
        if (g_hash_table_size(state->pending_adds) > 0) {
            g_hash_table_iter_init(&iter,state->pending_adds);
            while (g_hash_table_iter_next(&iter,&gp,NULL)) {
                port_no = (uint16_t)(unsigned long)gp;
                if ((_port = g_hash_table_lookup(state->ports,
                                                 GINT_TO_POINTER(port_no)))) {
                    addlist = g_list_append(addlist,GINT_TO_POINTER(port_no));
                    g_hash_table_iter_remove(&iter);
                }
            }
        }

        pthread_rwlock_unlock(&state->lock);

        cur = NULL;
        if (addlist) {
            gw_list_foreach(addlist,cur,gp) {
                port_no = (uint16_t)(unsigned long)gp;
                cncwarn("notifying on switch %lx port %u\n",dpid,port_no);
                cnc_port_pending_notify(dpid,port_no);
                break;
            }
            g_list_free(addlist);
        }
    }

    return NULL;
}

int metadata_file_set_port_pending(cn_switch_t *csw,uint16_t port_no) {
    struct metadata_file_state *state;

    state = (struct metadata_file_state *)csw->metadata_priv;
    if (!state)
        return -1;

    pthread_rwlock_wrlock(&state->lock);
    g_hash_table_insert(state->pending_adds,GINT_TO_POINTER(port_no),NULL);
    pthread_rwlock_unlock(&state->lock);

    return 0;
}

int metadata_file_start_thread(cn_switch_t *csw) {
    struct metadata_file_state *state;
    int rc;

    state = (struct metadata_file_state *)csw->metadata_priv;
    if (!state)
        return -1;

    rc = pthread_create(&state->thread,NULL,metadata_file_watcher,csw);
    if (rc) {
        cncerr("failed to spawn pthread to watch metadata file; switch %lx!\n",
               csw->mul_switch->dpid);
        return -1;
    }
    else {
        return 0;
    }
}

int metadata_file_stop_thread(cn_switch_t *csw) {
    struct metadata_file_state *state;

    state = (struct metadata_file_state *)csw->metadata_priv;
    if (!state)
        return -1;

    state->thread_stop = 1;

    pthread_join(state->thread,NULL);

    return 0;

}

int metadata_file_get_switch_info(cn_switch_t *csw) {
    int rc;

    if ((rc = metadata_file_update_switch_info(csw)))
        return rc;

    return 0;
}

int metadata_file_get_port_info(cn_switch_t *csw,cn_port_t *port) {
    struct metadata_file_state *state;
    int rc;
    struct metadata_file_port *_port;
    cn_node_t * node;
    char buf[32];
    uint32_t port_no;
    
    if ((rc = metadata_file_update_switch_info(csw)))
        return rc;

    state = (struct metadata_file_state *)csw->metadata_priv;

    pthread_rwlock_rdlock(&state->lock);

    port_no = port->mul_port->port_no;
    if (!(_port = g_hash_table_lookup(state->ports,
                                      GINT_TO_POINTER(port_no)))) {
        cncwarn("unknown port %d\n",port_no);
        pthread_rwlock_unlock(&state->lock);
        return -1;
    }

    if (_port->isnode)
        port->role = CN_PORT_ROLE_NODE;
    else if (_port->isuplink)
        port->role = CN_PORT_ROLE_UPLINK;
    else {
        cncerr("unknown port role for port %d; aborting!\n",port_no);
        pthread_rwlock_unlock(&state->lock);
        return -1;
    }

    port->id = strdup(_port->id);

    ether_ntoa_r(&_port->mac, buf);

    cncdbg(1,LA_CTL,LF_MD,"got/set port info for port %d, mac %s, switch %"PRIx64"\n",
           port_no,buf,csw->mul_switch->dpid);

    pthread_rwlock_unlock(&state->lock);

    return 0;
}

int metadata_file_get_node_info(cn_switch_t *sw, cn_port_t *port, cn_node_t *node) {
    struct metadata_file_state *state;
    int rc;
    struct metadata_file_port *_port;
    char buf[32];
    uint32_t port_no;

    if ((rc = metadata_file_update_switch_info(sw)))
        return rc;

    state = (struct metadata_file_state *)sw->metadata_priv;
    
    pthread_rwlock_rdlock(&state->lock);

    port_no = port->mul_port->port_no;
    if (!(_port = g_hash_table_lookup(state->ports,
                                      GINT_TO_POINTER(port_no)))) {
        cncwarn("unknown port %d\n",port_no);
        pthread_rwlock_unlock(&state->lock);
        return -1;
    }

    ether_ntoa_r(&_port->mac, buf);

    node->services = _port->services;
    node->domainid = strdup(_port->domainid);
    node->ownerid = strdup(_port->ownerid);
    node->id = strdup(_port->id);
    if (_port->name)
        node->name = strdup(_port->name);
    else
        node->name = strdup(_port->id);
    if (_port->ismaster)
        node->role = CN_NODE_ROLE_MASTER;
    else
        node->role = CN_NODE_ROLE_NODE;
    memcpy(&node->mac, &_port->mac, ETH_ALEN);
    if (_port->addr_valid) {
        node->ipv4.valid = 1;
        node->ipv4.addr = _port->addr;
        node->ipv4.mask = _port->mask;
    }
    if (_port->addr6_valid) {
        node->ipv6.valid = 1;
        node->ipv6.addr = _port->addr6;
    }

    pthread_rwlock_unlock(&state->lock);
    return 0;
}

int metadata_file_get_lan_info(cn_switch_t *csw,cn_port_t *port) {
    int rc;

    if ((rc = metadata_file_update_switch_info(csw)))
        return rc;

    return 0;
}

struct metadata_ops metadata_file_ops  = {
    .has_switch = metadata_file_has_switch,
    .init_switch = metadata_file_init_switch,
    .start_thread = metadata_file_start_thread,
    .stop_thread = metadata_file_stop_thread,
    .get_port_info = metadata_file_get_port_info,
    .get_node_info = metadata_file_get_node_info,
    .get_lan_info = metadata_file_get_lan_info,
    .get_switch_info = metadata_file_get_switch_info,
    .set_port_pending = metadata_file_set_port_pending,
};
