#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <libcap.h>
#include <glib.h>

#include "metadata.h"
#include "obj.h"
#include "obj_waiting.h"
#include "util.h"
#include "errors.h"
#include "dispatch.h"
#include "controller.h"

#include "capnet_ref.h"
#include "capnet_log.h"

/* Used as the declassifier on annotations that were added nominally by
 * the system itself. This is special cased in a bunch of cases in the code
 * below. */
static const void * OBJ_SYSTEM_DECLASSIFIER = (void *) 0xdeadc0deUL;

cap_type_t TYPE_MAP[__CN_MAX_TYPE];

/* objtype free function adapters */ 

#define OBJ_FREE(_type,objtype) \
    (cn_obj_t *p) { \
        assert(p->type == objtype && "Type is not a " #_type " type"); \
        cn_ ## _type ## _free((cn_ ## _type ## _t *) p); \
    }

/* Generic free function for all cn_obj_t objects */
REFCNT __OBJ_free(cn_obj_t *obj, int i) {
    obj->free(obj); 
    return 0; }

/* Return the cn_objtype_t for the given cap_type_t or abort */
static inline cn_objtype_t _captype_to_objtype(cap_type_t t) {
    for (int i = 0; i < __CN_MAX_TYPE; i++) {
        if (TYPE_MAP[i] == t) { return i; }
    }
    cn_abort("No obj_type_t equivalent to cap_type_t %#0x", t);
}

static const char * TYPE_NAMES[__CN_MAX_TYPE] = {
    "switch", "port", "principal", "node",
    "flow", "rp", "membrane", "broker", 
    "node-grant", "sealer-unsealer",
};

const char * cn_objtype_name(cn_objtype_t type) { return TYPE_NAMES[type]; }

/* typeops for each type */

#define DEFAULT_TYPE_OPS(typename)      \
    { .name = # typename,               \
      .insert = cn_default_insert_cb,   \
      .delete = cn_default_delete_cb,   \
      .grant = cn_default_grant_cb,     \
      .derive_src = NULL,               \
      .derive_dst = cn_default_derive_dst_cb }

static const struct cap_type_ops _flow_ops = {
    .name = "flow",
    .insert = cn_insert_flow_cb,
    .delete = cn_delete_flow_cb,
    .grant = cn_grant_flow_cb,
    .derive_src = NULL, 
    .derive_dst = cn_default_derive_dst_cb
};

static const struct cap_type_ops _node_ops = DEFAULT_TYPE_OPS(node);
static const struct cap_type_ops _rp_ops = DEFAULT_TYPE_OPS(rp);
static const struct cap_type_ops _membrane_ops = DEFAULT_TYPE_OPS(membrane);
static const struct cap_type_ops _broker_ops = DEFAULT_TYPE_OPS(broker);
static const struct cap_type_ops _node_grant_ops = DEFAULT_TYPE_OPS(node_grant);
static const struct cap_type_ops _sealer_unsealer_ops = DEFAULT_TYPE_OPS(sealer_unsealer);

int cn_init(void) {
#ifdef HAVE_CPTR_INIT
    cptr_init();
#endif
    if (cap_init() != 0) { return -1; }
    TYPE_MAP[CN_FLOW] = cap_register_type(0, &_flow_ops);
    TYPE_MAP[CN_NODE] = cap_register_type(0, &_node_ops);
    TYPE_MAP[CN_RP] = cap_register_type(0, &_rp_ops);
    TYPE_MAP[CN_MEMBRANE] = cap_register_type(0, &_membrane_ops);
    TYPE_MAP[CN_BROKER] = cap_register_type(0, &_broker_ops);
    TYPE_MAP[CN_NODE_GRANT] = cap_register_type(0, &_node_grant_ops);
    TYPE_MAP[CN_SEALER_UNSEALER] = cap_register_type(0, &_sealer_unsealer_ops);
    return 0;
}

void cn_fini(void) {
    cap_fini();
}

/* Initialize the given object. The given type and free method will
 * be used as the object's type and free method */
void _obj_init(cn_obj_t *obj, cn_objtype_t type, cn_obj_free_f free) {
    c_log_debug("Initing with type: %d", type);
    obj->type = type;
    OBJSNEW(obj);
    obj->refcnt = 0;

    (void) pthread_mutexattr_init(&obj->lock_attr);
    (void) pthread_mutexattr_settype(&obj->lock_attr, PTHREAD_MUTEX_RECURSIVE_NP);
    (void) pthread_mutex_init(&obj->lock, &obj->lock_attr);

    obj->free = free;
}

/* sec: Principals */

static void _cn_principal_free OBJ_FREE(principal, CN_PRINCIPAL)

int cn_principal_init(cn_principal_t *p) {
    struct cspace *cspace = cap_alloc_cspace();
    if (cspace == NULL) { return -1; }

    if (cap_init_cspace(cspace) != 0) { goto fail; }
    if (cptr_cache_alloc(&p->cache) != 0) { goto fail; }
    if (cptr_cache_init(p->cache) != 0) { goto fail; }

    p->cspace = cspace;
    cap_cspace_set_owner(p->cspace, (void *) p);

    p->owner = NULL;

    return 0;
fail:
    cap_free_cspace(cspace);
    return -1;
}

int cn_principal_new(cn_principal_t **out) {
    cn_principal_t *principal = calloc(1,sizeof(*principal));
    if (principal == NULL) { goto fail; }

    _obj_init(&principal->obj, CN_PRINCIPAL, _cn_principal_free);

    if (cn_principal_init(principal) != 0) { goto fail; }
    *out = principal;

    return 0;
fail:
    free(principal);
    return -1;
}

void cn_principal_free(cn_principal_t *p) {
    cap_destroy_cspace(p->cspace);
    cptr_cache_destroy(p->cache);
    free(p->cspace);
    free(p);
}

/* Re-initialize the given principal, i.e., clear all of its cptrs and cnodes. */
int cn_principal_clear(cn_principal_t *p) {
    cap_destroy_cspace(p->cspace);
    cptr_cache_destroy(p->cache);
    return cn_principal_init(p);
}

/* Insert the given object into the given principal. The resulting cptr
 * is stored in o_cptr. */
int cn_principal_insert(cn_principal_t *p, cn_obj_t *o, void * payload, cptr_t *o_cptr) {
    if (cptr_alloc(p->cache, o_cptr) != 0) { return -1; }
    if (cap_insert(p->cspace, *o_cptr, o, TYPE_MAP[o->type], payload) != 0) { goto fail; }
    /* Make either the owner of the principal, or the principal if it has no owner
     * hold a reference to the inserted object. */
    RHOLD_OBJ(o, p->owner != NULL ? p->owner : p);
    return 0;
fail:
    cptr_free(p->cache, *o_cptr);
    return -1;
}

void * cn_principal_owner(cn_principal_t *p) { return p->owner; }

/* sec: Objects */

/* subsec: Switch Objects */

static void _cn_switch_free OBJ_FREE(switch, CN_SWITCH)

int cn_switch_new(cn_switch_t **o_csw) {
    cn_switch_t *csw;

    csw = calloc(1,sizeof(cn_switch_t));
    if (csw == NULL) { return -1; }
    csw->uplink_port_no = -1;
    //csw->lan_to_ports = g_hash_table_new_full(g_str_hash,g_str_equal,
    //                                          NULL,NULL);
    //csw->port_to_lan = g_hash_table_new_full();
    csw->mac_to_node = g_hash_table_new_full(__mac_hash,__mac_equal,NULL,NULL);
    csw->ipv4_to_node = g_hash_table_new_full(g_direct_hash,g_direct_equal,
                                              NULL,NULL);

    _obj_init(&csw->obj, CN_SWITCH, _cn_switch_free);

    *o_csw = csw;
    return 0;
}

void cn_switch_free(cn_switch_t *csw) {
    metadata_fini_switch(csw);
    g_hash_table_destroy(csw->mac_to_node);
    g_hash_table_destroy(csw->ipv4_to_node);
    //c_app_switch_put(csw->mul_switch);
    csw->mul_switch = NULL;
    free(csw);
}

/* subsec: Port Objects */

static void _cn_port_free OBJ_FREE(port, CN_PORT)

int cn_port_new(cn_port_t **o_cport) {
    cn_port_t *cport;

    cport = calloc(1,sizeof(cn_port_t));
    if (cport == NULL) { return -1; }

    _obj_init(&cport->obj, CN_PORT, _cn_port_free);

    cport->nodes = g_list_alloc();
    if (cport->nodes == NULL) { goto fail; }

    *o_cport = cport;
    return -1;

fail:
    free(cport);
    return -1;
}


void cn_port_free(cn_port_t *cport) {
    GList *current = cport->nodes;
    while (current != NULL) {
        GList *next = g_list_next(current);
        RPUTs_OBJ(current->data, cport);
        cport->nodes = g_list_delete_link(cport->nodes, current);
        current = next;
    }
    free(cport);
}

static void _cn_flow_free OBJ_FREE(flow, CN_FLOW)

/* Make a new flow from the 'to' node with the given flow. contents of the passed
 * flow object are copied out of the flow into a local copy, so no refernce
 * is held. */
int cn_flow_new(cn_node_t *to, struct flow flow_spec, struct flow mask, 
                cn_flow_t **o_flow) {
    cn_flow_t *flow = calloc(1,sizeof(*flow));
    if (flow == NULL) { return -1; }

    flow->flow = flow_spec;
    flow->mask = mask;

    RHOLD_OBJ(to, flow);
    flow->node = to;

    _obj_init(&flow->obj, CN_FLOW, _cn_flow_free);

    cnc_init_flow(flow);

    *o_flow = flow;

    return 0;
}

void cn_flow_free(cn_flow_t *flow) {
    RPUTs_OBJ(flow->node, flow);
    free(flow);
}

/* subsec: Node Objects */

static void _cn_node_free OBJ_FREE(node, CN_NODE)

int cn_node_new(cn_node_t **o_node) {
    cn_node_t *node = calloc(1,sizeof(*node));
    if (node == NULL) { return -1; }
    node->role = CN_NODE_ROLE_UNKNOWN;
    if (cn_principal_new(&node->principal) != 0) { goto fail; }
    RHOLD_OBJ(node->principal, node);
    /* XXX: Should the principal increment the reference count of the node
     * at this point? The principal is 1-1 with the node, so it should be OK
     * that we don't, and make our lives a little easier. */
    node->principal->owner = node;

    if (cn_principal_new(&node->secret) != 0) {
        goto fail1;
    }
    RHOLD_OBJ(node->secret, node);
    node->secret->owner = NULL;

    _obj_init(&node->obj, CN_NODE, _cn_node_free);

    cn_dispatch_result_cache_init(&node->cache);

    cn_node_grant_t * grant = NULL;
    if (cn_node_grant_new(node, &grant) != 0) {
        cncerr("Couldn't create node grant");
        goto fail2;
    }

    if (cn_principal_insert(node->secret, (cn_obj_t *) grant, NULL, &node->grant) != 0) {
        cncerr("Couldn't insert node grant into node's secret cspace");
        goto fail3;
    }

    /* Create the default rp0 and insert it into the node's cspace */
    cn_rp_t *rp0;
    if (cn_rp_new(&rp0) != 0) { goto fail3; }
    if (cn_principal_insert(node->secret, (cn_obj_t *) rp0, NULL, &node->rp0) != 0) { 
        goto fail4; 
    }

    node->name = NULL;
    node->flow_table = g_hash_table_new(g_direct_hash, g_direct_equal);
    node->node_table = g_hash_table_new(g_direct_hash, g_direct_equal);
    node->notify_wait = g_hash_table_new(g_int64_hash, g_int64_equal);

    *o_node = node;
    return 0;

fail4:
    cn_rp_free(rp0);
fail3:
    cn_node_grant_free(grant);
fail2:
    cn_principal_free(node->secret);
fail1:
    cn_principal_free(node->principal);
fail:
    free(node);
    return -1;
}

int cn_unordered_acquire_port_switch_lock(cn_port_t *port, cn_switch_t **o_sw) {
    cn_switch_t *sw = NULL;

    RHOLD_OBJ(port, OBJ_OWNER);

    cn_obj_lock(port);
    sw = port->csw;
    cn_obj_unlock(port);
    RHOLD_OBJ(sw, OBJ_OWNER);

    cn_obj_lock(sw);
    cn_obj_lock(port);

    int res = -1;
    if (port->csw != sw) {
        cncerr("port -- sw linkage modified while locking\n");
        cn_obj_unlock(port);
        cn_obj_unlock(sw);
        res = -1;
    } else {
        *o_sw = sw;
        res = 0;
    }
    RPUTs_OBJ(sw, OBJ_OWNER);
    RPUTs_OBJ(port, OBJ_OWNER);

    return res;
}

int cn_unordered_acquire_node_switch_lock(cn_node_t *node, cn_port_t **o_port, cn_switch_t **o_sw) {
    cn_switch_t *sw = NULL;
    cn_port_t *port = NULL;

    RHOLD_OBJ(node, OBJ_OWNER);

    cn_obj_lock(node);
    port = node->cn_port;
    cn_obj_unlock(node);
    RHOLD_OBJ(port, OBJ_OWNER);

    int res = cn_unordered_acquire_port_switch_lock(port, &sw);
    if (res != 0) { 
        res = -1;
        goto finish;
    }

    cn_obj_lock(node);

    if (node->cn_port != port) {
        cncerr("node -- port linkage modified while locking\n");
        cn_obj_unlock(node);
        res = -1;
    } else {
        *o_port = port;
        *o_sw = sw;
        res = 0;
    }

finish:
    RPUTs_OBJ(port, OBJ_OWNER);
    RPUTs_OBJ(node, OBJ_OWNER);
    return res;
}

int cn_unordered_acquire_flow_switch_lock(cn_flow_t *flow, cn_node_t **o_node, 
                                          cn_port_t **o_port, cn_switch_t **o_sw) {
    cn_switch_t *sw = NULL;
    cn_port_t *port = NULL;
    cn_node_t *node = NULL;

    RHOLD_OBJ(flow, OBJ_OWNER);

    cn_obj_lock(flow);
    node = flow->node;
    cn_obj_unlock(flow);
    RHOLD_OBJ(node, OBJ_OWNER);

    int res = cn_unordered_acquire_node_switch_lock(node, &port, &sw);
    if (res != 0) {
        res = -1;
        goto finish;
    }

    cn_obj_lock(flow);

    if (flow->node != node) {
        cncerr("flow -- node linkage modified while locking\n");
        cn_obj_unlock(flow);
        res = -1;
    } else {
        *o_node = node;
        *o_port = port;
        *o_sw = sw;
        res = 0;
    }

finish:
    RPUTs_OBJ(node, OBJ_OWNER);
    RPUTs_OBJ(flow, OBJ_OWNER);
    return res;
}

void cn_port_add_node(cn_port_t *port, cn_node_t *node) {
    RHOLD_OBJ(node, port);
    cn_obj_lock(port);
    port->nodes = g_list_prepend(port->nodes, node);
    cn_obj_unlock(port);
}

void cn_node_free(cn_node_t *node) {

    if (node->name)
        free(node->name);

    if (node->id)
        free(node->id);

    if (node->name) { free(node->name); }

    /* This should clean up any left-over flows when the principal gets
     * deleted. */
    RPUTs_OBJ(node->principal, node);

    /* Free the actual hash table. */
    g_hash_table_destroy(node->flow_table);

    free(node);
}

/* subsec: RP Objects */

enum cn_rp_elem_source {
    CN_RP_ELEM_SOURCE_INJECTED,
    CN_RP_ELEM_SOURCE_NORMAL,
    CN_RP_ELEM_SOURCE_MEMBRANE,
    CN_RP_ELEM_SOURCE_MESSAGE
};

enum cn_membrane_type cn_membrane_type_opposite(enum cn_membrane_type type) {
    switch (type) {
        case CN_MEMBRANE_TYPE_EXTERNAL:
            return CN_MEMBRANE_TYPE_INTERNAL;
        case CN_MEMBRANE_TYPE_INTERNAL:
            return CN_MEMBRANE_TYPE_EXTERNAL;
        default: break;
    }
    cn_abort("unreachable");
}

struct cn_rp_elem {
    enum cn_rp_elem_source source;
    bool has_msg;
    struct cn_rp_elem_message msg;
    union {
        struct {
            cn_principal_t *cap_owner;
            cptr_t cap;
        } normal;
        struct {
            cn_obj_t * object;
        } injected;
        struct {
            cn_principal_t *cap_owner;
            cptr_t cap;
            cn_cnode_meta_t * sender_meta;
        } membrane;
    };
};

static inline bool cn_rp_elem_is_normal(struct cn_rp_elem * elem) {
    return elem->source == CN_RP_ELEM_SOURCE_NORMAL;
}

static inline bool cn_rp_elem_is_injected(struct cn_rp_elem * elem) {
    return elem->source == CN_RP_ELEM_SOURCE_INJECTED;
}

static inline bool cn_rp_elem_is_membrane(struct cn_rp_elem * elem) {
    return elem->source == CN_RP_ELEM_SOURCE_MEMBRANE;
}

static void __cn_rp_elem_release_refs(void * who, struct cn_rp_elem *elem);
static void __cn_rp_elem_hold_refs(void * who, struct cn_rp_elem *elem);

void cn_rp_elem_free(struct cn_rp_elem * elem) { 
    if (elem->has_msg) { 
        free(elem->msg.data);
    }
    if (cn_rp_elem_is_membrane(elem)) {
        cn_cnode_meta_free(elem->membrane.sender_meta);
    }
    g_slice_free1(sizeof(struct cn_rp_elem), elem); 
}

static void __cn_rp_queue_free(cn_rp_t *rp) {
    while (! g_queue_is_empty(rp->queue)) {
        struct cn_rp_elem *elem = (struct cn_rp_elem *) g_queue_pop_head(rp->queue);
        __cn_rp_elem_release_refs(rp, elem);
        cn_rp_elem_free(elem);
    }
    g_queue_free(rp->queue);
    rp->queue = NULL;
}

void cn_rp_free(cn_rp_t *rp) {
    __cn_rp_queue_free(rp);
    free(rp);
}

static void _cn_rp_free OBJ_FREE(rp, CN_RP)

int cn_rp_new(cn_rp_t **o_rp) {
    cn_rp_t * rp = calloc(1,sizeof(*rp));
    if (rp == NULL) { return -1; }
    rp->queue = g_queue_new();
    if (rp->queue == NULL) { goto fail; }
    _obj_init(&rp->obj, CN_RP, _cn_rp_free);

    *o_rp = rp;
    return 0;
fail:
    free(rp);
    return -1;
}

/* subsec: Broker Objects */

struct cn_broker_elem {
    cn_principal_t *cap_owner;
    cptr_t cap;
};

static void __cn_broker_elem_hold_refs(const void *who, struct cn_broker_elem *elem) {
    RHOLD_OBJ(elem->cap_owner, who);
}

static void __cn_broker_elem_release_refs(const void *who, struct cn_broker_elem *elem) {
    RPUTs_OBJ(elem->cap_owner, who);
}

void cn_broker_elem_free(struct cn_broker_elem *elem) { 
    g_slice_free1(sizeof(*elem), elem); 
}

static void __cn_broker_elem_free_gpointer(gpointer elem) {
    __cn_broker_elem_release_refs(OBJ_OWNER, (struct cn_broker_elem *) elem);
    cn_broker_elem_free((struct cn_broker_elem *)elem);
}

int cn_broker_elem_new(cn_principal_t *p, cptr_t cap,
                       struct cn_broker_elem **o_elem) {
    struct cn_broker_elem *elem = g_slice_alloc(sizeof(*elem));
    if (elem == NULL) {
        return -1;
    }
    elem->cap_owner = p;
    elem->cap = cap;
    *o_elem = elem;

    return 0;
}

void cn_broker_free(cn_broker_t *broker) {
    /* XXX: This should clean up any left-over broker RP caps when the
     * principal gets deleted.
     */
    //RPUTs_OBJ(broker->principal, broker);
    g_hash_table_destroy(broker->registry);
    free(broker);
}

static void _cn_broker_free OBJ_FREE(broker, CN_BROKER)

int cn_broker_new(cn_broker_t **o_broker) {
    cn_broker_t * broker = calloc(1,sizeof(*broker));

    if (broker == NULL)
        return -1;

    /* XXX: how do we safely free the RP caps in the registry? */
    broker->registry = g_hash_table_new_full(g_str_hash, g_str_equal,
                                             free, __cn_broker_elem_free_gpointer);
    broker->waiting = g_hash_table_new_full(g_str_hash, g_str_equal, free, NULL);

    _obj_init(&broker->obj, CN_BROKER, _cn_broker_free);

    *o_broker = broker;
    return 0;
}

/* subsec: Membranes */

static void _cn_membrane_free OBJ_FREE(membrane, CN_MEMBRANE)

int cn_membrane_new(cn_membrane_t **membrane) {
    cn_membrane_t *m = calloc(1,sizeof(*m));
    if (m == NULL) { return -1; }
    if (cn_rp_new(&m->rp) != 0) { goto fail; }
    if (cn_principal_new(&m->secret) != 0) { goto fail1; }

    m->wrapped = NULL;

    _obj_init(&m->obj, CN_MEMBRANE, _cn_membrane_free);

    *membrane = m;
    return 0;

fail1:
    cn_rp_free(m->rp);
fail:
    free(m);
    return -1;
}

void cn_membrane_free(cn_membrane_t *m) {
    cn_rp_free(m->rp);
    free(m);
}

/* private membrane interaface */

static void __cn_membrane_log(cn_membrane_t *m, cptr_t cptr) {
    c_log_debug("wrapped: %#0lx", cptr2ul(cptr));
    m->wrapped = g_list_append(m->wrapped, cptr2gptr(cptr));
}

static void __cn_membrane_log_free(cn_membrane_t *m) {
    c_log_debug("Free-ing: %p", m);
    g_list_free(m->wrapped);
    m->wrapped = NULL;
}

static bool __cn_cnode_is_not_annotated_by(struct cnode *c, void * _obj) {
    cn_obj_t * obj = (cn_obj_t *) _obj;
    bool wrapped = cn_cnode_is_annotated_by(c, obj);
    c_log_debug("[membrane] cptr %#lx is wrapped? %d", 
                cptr2ul(cap_cnode_cptr(c)), wrapped);
    return ! wrapped;
}

static void __cn_membrane_log_clear(cn_membrane_t *m) {
    GList * cur = g_list_first(m->wrapped);
    for (; cur; cur = g_list_next(cur)) {
        cptr_t cptr = gptr2cptr(cur->data);
        /* XXX: Does this fail? */
        c_log_debug("revoking %#0lx", cptr2ul(cptr));
        cn_revoke_till(m->secret, cptr, __cn_cnode_is_not_annotated_by, 
                       NULL, (void *) m);
    }
    c_log_debug("freeing the log %p", m);
    __cn_membrane_log_free(m);
}

struct cn_membrane_recv_grant_callback_payload {
    cn_membrane_t * membrane;
    enum cn_membrane_type send_type;
    enum cn_membrane_type recv_type;
};

STATIC_ASSERT(sizeof(enum cn_membrane_type) <= sizeof(void *), 
              cn_membrane_type_fits_in_ptr);
static inline void * cn_membrane_type_to_ptr(enum cn_membrane_type t) {
    return (void *) (unsigned long) t;
}

static inline enum cn_membrane_type cn_membrane_type_from_ptr(void * ptr) {
    return (enum cn_membrane_type) (unsigned long) ptr;
}

int cn_membrane_annotation_new(cn_membrane_t * membrane,
                               enum cn_membrane_type side,
                               cn_annotation_t **a_out) {
    cn_annotation_t * a;
    if (cn_annotation_new(&a) != 0) {
        return -1;
    }
    a->declassifier = (cn_obj_t *) membrane;
    RHOLD_OBJ(membrane, a);
    a->data.val = cn_membrane_type_to_ptr(side);
    cn_method_mask_permission_all(&a->method_mask);
    *a_out = a;
    return 0;
}

enum cn_membrane_type cn_membrane_annotation_type(cn_annotation_t *a) {
    assert(a->declassifier->type == CN_MEMBRANE 
            && "Can only find the side of a membrane annotation");
    return cn_membrane_type_from_ptr(a->data.val);
}

static const char * cn_membrane_type_name(enum cn_membrane_type t) {
    switch (t) {
        case CN_MEMBRANE_TYPE_INTERNAL:
            return "INTERNAL";
        case CN_MEMBRANE_TYPE_EXTERNAL:
            return "EXTERNAL";
        default:
            cn_abort("not a valid membrane type: %d", t);
    }
    return NULL;
}

static inline bool
cn_membrane_recv_grant_crosses_membrane(enum cn_membrane_type send_type, 
                                        enum cn_membrane_type recv_type) {
    return (send_type == CN_MEMBRANE_TYPE_INTERNAL
            && recv_type == CN_MEMBRANE_TYPE_EXTERNAL)
        || (send_type == CN_MEMBRANE_TYPE_EXTERNAL
            && recv_type == CN_MEMBRANE_TYPE_INTERNAL);
}

static int __cn_membrane_recv_grant_callback(struct cnode * ca, struct cnode *cb, 
                                             void * payload) {
    struct cn_membrane_recv_grant_callback_payload * args = 
        (struct cn_membrane_recv_grant_callback_payload *) payload;

    if (cn_ensure_target_meta(ca, cb) != 0) {
        cncerr("Couldn't ensure target metadata\n");
    }

    cn_cnode_meta_t * grantee = (cn_cnode_meta_t *) cap_cnode_metadata(ca);
    cn_cnode_meta_t * granted = (cn_cnode_meta_t *) cap_cnode_metadata(cb);

    /* crossing from inside -> outside */
    if (cn_membrane_recv_grant_crosses_membrane(args->send_type, args->recv_type)) {
        c_log_debug("[membrane single] Grant Crosses Membrane %p", args->membrane);

        cn_annotation_t *annotation = NULL;
        if (grantee != NULL) {
            if (cn_cnode_meta_get_annotation_by(grantee, 
                                                (cn_obj_t *) args->membrane, 
                                                &annotation) != 0) {
                // Need to re-set annotation 'cause it could've gotten messed
                // with during the call.
                annotation = NULL;
            }
        }

        /* If this is wrapped with this membrane, and the type of the wrap
         * mathces the type of the destination, unwrap. */
        if (annotation != NULL && 
                /* since send_type != recv_type, and there are only two types
                 * this is equivalent to annotation_type(...) != recv_type */
                cn_membrane_annotation_type(annotation) == args->send_type) {
            char * _cnode_str = cn_cnode_string(cb);
            c_log_debug("[membrane] UNwrapping %s", _cnode_str);
            free(_cnode_str);

            cn_cnode_meta_remove_annotation_by(granted, (cn_obj_t *) args->membrane);
        /* If the cptr is already wrapped by the membrane, but with the same
         * side that we're receiving, just let it pass. */
        } else if (annotation != NULL) {
        /* Otherwise, wrap. */
        } else {
            char * _cnode_str = cn_cnode_string(cb);
            c_log_debug("[membrane] wrapping %s", _cnode_str);
            free(_cnode_str);

            cn_annotation_t * wrap_annotation;
            if (cn_membrane_annotation_new(args->membrane, args->recv_type, 
                                           &wrap_annotation) != 0) {
                cncerr("Failed to create new annotation for the wrap\n");
                goto fail;
            }
            (void) cn_cnode_meta_add_annotation(granted, wrap_annotation);
            __cn_membrane_log(args->membrane, cap_cnode_cptr(cb));
        }
    }
    /* Otherise, we just treat it like a normal receive and perform no
     * special actions. */

    return 0;

fail:
    cn_ensure_target_meta_cleanup(ca, cb);
    return -1;
}

/* This function performs a single grant from the perspective of the
 * given membrane `membrane`. `send_type` is the type of the sending
 * membrane, and `recv_type` is the type of the recving membrane.
 *
 * If the send type and the recv type do not match, then the resulting
 * capability will have an annotation added or removed depending on the
 * current annotation state of the capability. If the capbility being
 * granted already has an annotation that *matches* the recving side,
 * the annotation will be removed. If it has no annotations that match the
 * recving side, an annotation of the *sending* side will be added.
 */
static int cn_membrane_grant_single(cn_membrane_t * membrane,
                                    enum cn_membrane_type send_type,
                                    enum cn_membrane_type recv_type,
                                    cn_principal_t * granter, cptr_t granted,
                                    cptr_t *o_cap) {

    struct cn_membrane_recv_grant_callback_payload recv_payload = {
        .membrane = membrane,
        .send_type = send_type,
        .recv_type = recv_type,
    };

    /* XXX: Most of the magic happens in this callback, look there. */
    struct cn_cb_payload cb_payload = {
        .binop = __cn_membrane_recv_grant_callback,
        .payload = (void *) &recv_payload 
    };

    int res = -1;

    cn_obj_lock(membrane);

    struct cnode * granted_cnode = NULL;
    if (cn_principal_get(granter, granted, &granted_cnode) != 0) {
        cncerr("Couldn't resolve cptr\n");
        goto finish;
    }

    /* Special case for sealers/unsealers, they skip the normal wrapping logic */
    /* TODO: Move check to cn_membrane_grant instead. */
    if (cn_cnode_type(granted_cnode) == CN_SEALER_UNSEALER) {
        cncwarn("skipping wrapping for sealer/unsealer\n");
        res = ESKIPPED;
        goto finish1;
    }

    c_log_debug("[membrane single] granting cnode of type %s", 
                cn_objtype_name(cn_cnode_type(granted_cnode)));

    c_log_debug("[membrane single] recv grant...");
    /* First we grant into a secret cspace specific to the membrane so
     * the capability can't be deleted out from under us. */
    cn_objtype_t type;
    if (cn_grant_cnode_extended(granted_cnode, membrane->secret, 
                                (void *) &cb_payload, o_cap, &type) != 0) {
        goto finish1;
    }

    c_log_debug("[membrane single] granted type: %s", cn_objtype_name(type));

    res = 0;

finish1:
    cap_cnode_put(granted_cnode);
finish:
    cn_obj_unlock(membrane);
    return res;
}

int cn_membrane_grant(cn_cnode_meta_t * send_meta,
                      cn_cnode_meta_t * recv_meta,
                      cn_principal_t *granter, cptr_t granted,
                      cn_principal_t *receiver, 
                      cptr_t *o_cap, cn_objtype_t *type) {


    char * _send_meta_str = send_meta ? cn_cnode_meta_string(send_meta) 
                                      : "(null)";
    char * _recv_meta_str = recv_meta ? cn_cnode_meta_string(recv_meta) 
                                      : "(null)";
    char * _send_cap = cn_cptr_string(granter, granted);
    c_log_debug("[membrane] send meta: %s", _send_meta_str);
    c_log_debug("[membrane] recv meta: %s", _recv_meta_str);
    c_log_debug("[membrane] send cap: %s", _send_cap);
    free(_send_cap);
    if (send_meta) { free(_send_meta_str); }
    if (recv_meta) { free(_recv_meta_str); }

    /* If there are actually no annotations at play, just do a normal grant. */
    if (! cn_cnode_meta_is_annotated(send_meta)
        && ! cn_cnode_meta_is_annotated(recv_meta)) {
        c_log_debug("[membrane] Fallback to grant");
        return cn_grant_extended(granter, granted, receiver, NULL, o_cap, type);
    }

    int res = -1;

    cn_cnode_meta_t * _allocated_meta = NULL;
    if (send_meta == NULL || recv_meta == NULL) {
        assert((send_meta != NULL || recv_meta != NULL) &&
                "Either sender meta or recv meta must not be null");
        if (cn_cnode_meta_new(&_allocated_meta) != 0) {
            cncerr("send or recv meta not given and allocating meta failed\n");
            return -1;
        }
        if (send_meta == NULL) {
            c_log_debug("[membrane] send meta set to null meta");
            send_meta = _allocated_meta;
        } else if (recv_meta == NULL) {
            c_log_debug("[membrane] recv meta set to null meta");
            recv_meta = _allocated_meta;
        }
    }

    /* There are three types of annotations:
     *
     * 1. Annotations that exist on the sender side, but not on 
     *    the receiver side.
     * 2. Annotations that exist on the receiver side, but not on the
     *    sender side.
     * 3. Annotations that exist on both sides.
     *
     * In the first loop we handle the first and third categories, and track
     * which annotations we handled in the hash table called 'handled'. Then,
     * in the second loop, we deal with annotations in the second group that
     * have not yet been handled. */

    cn_principal_t *prev_owner = granter;
    cptr_t prev_cap = granted;

    GHashTable * handled = g_hash_table_new(g_direct_hash, g_direct_equal);

    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, send_meta->annotations);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        cn_annotation_t * send_annotation = (cn_annotation_t *) value;
        if (send_annotation->declassifier->type != CN_MEMBRANE) {
            continue;
        }
        cn_annotation_t * recv_annotation = NULL;
        int ret = cn_cnode_meta_get_annotation_by(recv_meta, (cn_obj_t *) key, 
                                                  &recv_annotation);
        if (ret != 0 && ret == ENOTEXISTS) {
            recv_annotation = NULL;
        } else if (ret != 0) {
            cn_abort("unreachable");
        }

        cn_membrane_t * membrane = (cn_membrane_t *) key;
        assert(((cn_obj_t *) membrane)->type == CN_MEMBRANE
                && "Key should point to a membrane");

        enum cn_membrane_type send_type = cn_membrane_annotation_type(send_annotation);
        enum cn_membrane_type recv_type;

        if (recv_annotation == NULL) {
            recv_type = cn_membrane_type_opposite(send_type);
        } else {
            recv_type = cn_membrane_annotation_type(recv_annotation);
        }

#ifndef NDEBUG
        cptr_t _prev_prev_cap = prev_cap;
        cn_principal_t * _prev_prev_owner = prev_owner;
#endif

        res = cn_membrane_grant_single(membrane, send_type, recv_type,
                                       prev_owner, prev_cap,
                                       &prev_cap);
        if (res == 0) {
            cn_obj_lock(membrane);
            prev_owner = membrane->secret;
            cn_obj_unlock(membrane);
        } else if (res == ESKIPPED) {
            // pass
        } else {
            cncerr("Failed to do single grant with membrane %p\n", membrane);
            res = -1;
            goto fail;
        }

#ifndef NDEBUG
        char * _grant_source_str = cn_cptr_string(_prev_prev_owner, 
                                                  _prev_prev_cap);
        char * _grant_dest_str = cn_cptr_string(prev_owner, prev_cap);
        c_log_debug("[membrane] Did Grant %s -> %s", _grant_source_str, 
                                                     _grant_dest_str);
        free(_grant_source_str);
        free(_grant_dest_str);

        char * _annotation_string = cn_annotation_string(send_annotation);
        c_log_debug("[membrane]   handled annotation: %s", _annotation_string);
        free(_annotation_string);
#endif

        g_hash_table_insert(handled, key, NULL);
    }

    g_hash_table_iter_init(&iter, recv_meta->annotations);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        if (g_hash_table_contains(handled, key)) {
            continue;
        }
        cn_annotation_t * recv_annotation = (cn_annotation_t *) value;

        cn_membrane_t * membrane = (cn_membrane_t *) key;
        assert(((cn_obj_t *) membrane)->type == CN_MEMBRANE
                && "Key should point to a membrane");

        enum cn_membrane_type recv_type = cn_membrane_annotation_type(recv_annotation);
        enum cn_membrane_type send_type = cn_membrane_type_opposite(recv_type);

#ifndef NDEBUG
        cptr_t _prev_prev_cap = prev_cap;
        cn_principal_t * _prev_prev_owner = prev_owner;
#endif

        int res = cn_membrane_grant_single(membrane, send_type, recv_type, 
                                           prev_owner, prev_cap,
                                           &prev_cap);
        if (res == 0) {
            cn_obj_lock(membrane);
            prev_owner = membrane->secret;
            cn_obj_unlock(membrane);
        } else if (res == ESKIPPED) {
            // just pass
        } else {
            cncerr("Failed to add membrane wrap for membrane %p\n", membrane);
            goto fail;
        }

                    
        /* Don't need to track handled state because the recv-ers hash table
         * guarantees uniqueness. */

#ifndef NDEBUG
        char * _grant_source_str = cn_cptr_string(_prev_prev_owner, 
                                                  _prev_prev_cap);
        char * _grant_dest_str = cn_cptr_string(prev_owner, prev_cap);
        c_log_debug("[membrane] Did Grant (pass 2) %s -> %s", _grant_source_str, 
                                                     _grant_dest_str);
        free(_grant_source_str);
        free(_grant_dest_str);

        char * _annotation_string = cn_annotation_string(recv_annotation);
        c_log_debug("[membrane]   handled annotation (pass 2): %s", _annotation_string);
        free(_annotation_string);
#endif
    }

    g_hash_table_destroy(handled);

    /* XXX: WARNING! THIS DOES'T GUARANTEE ATOMICITY.
     * To do so, we'd have to put both grants in a single transaction. Currently,
     * we assume that cptrs are unguessable, so that there's no chance someone can
     * execute an operation on this cnode before we give them the cptr for it. 
     *
     * I think worst-case scenario, we'd just fail the grant, but I'm not sure.
     * Since we're granting out of the membrane's secret c-space (and the membrane
     * is locked) we don't have to worry about the source, only the destination. */

    /* Then we do the normal grant to the actual destination. Its safe to do
     * this in two operations since we're the only ones that see the internal
     * c-space. */
    if (cn_grant_extended(prev_owner, prev_cap,
                          receiver, NULL, o_cap, type) != 0) {
        cncerr("Failed to do final grant out of membrane c-space\n");
        goto fail;
    }

    char * _final_source_str = cn_cptr_string(prev_owner, prev_cap);
    char * _final_dest_str = cn_cptr_string(receiver, *o_cap);
    c_log_debug("[membrane] Final Grant %s -> %s", _final_source_str, 
                                                   _final_dest_str);
    free(_final_source_str);
    free(_final_dest_str);

    c_log_debug("[membrane] Final Grant type: %s", cn_objtype_name(*type));

    if (_allocated_meta != NULL) { cn_cnode_meta_free(_allocated_meta); }
    return 0;

fail:
    cn_revoke(granter, granted, NULL);
    if (_allocated_meta != NULL) { cn_cnode_meta_free(_allocated_meta); }
    return -1;
}

/* subsec: node grants */

static void _cn_node_grant_free OBJ_FREE(node_grant, CN_NODE_GRANT)

int cn_node_grant_new(cn_node_t *node, cn_node_grant_t ** o_grant) {
    cn_node_grant_t * grant = calloc(1,sizeof(*grant));
    if (grant == NULL) { return -1; }
    RHOLD_OBJ(node, grant);
    grant->node = node;

    _obj_init(&grant->obj, CN_NODE_GRANT, _cn_node_grant_free);

    *o_grant = grant;
    return 0;
}

/* Free the node grant. */
void cn_node_grant_free(cn_node_grant_t * grant) {
    RPUTs_OBJ(grant->node, grant);
    free(grant);
}


/* subsec: Sealer Unsealers */

void _cn_sealer_unsealer_free OBJ_FREE(sealer_unsealer, CN_SEALER_UNSEALER)

int cn_sealer_unsealer_new(cn_sealer_unsealer_t **su) {
    cn_sealer_unsealer_t * sealer_unsealer = calloc(1,sizeof(*sealer_unsealer));
    if (sealer_unsealer == NULL) {
        cncerr("Could not allocate sealer/unsealer");
        return -1;
    }
    _obj_init(&sealer_unsealer->obj, CN_SEALER_UNSEALER, _cn_sealer_unsealer_free);

    *su = sealer_unsealer;
    return 0;
}

void cn_sealer_unsealer_free(cn_sealer_unsealer_t * su) {
    free(su);
}

/* sec: cnode metadata */

static void __cn_cnode_meta_compute_cache(cn_cnode_meta_t *m) {
    method_mask_t method_mask_cache = METHOD_MASK_ALL;
    GHashTableIter iter;
    gpointer key, value;
    g_hash_table_iter_init(&iter, m->annotations);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        cn_annotation_t *a = (cn_annotation_t *) value;
        assert(a != NULL && "Null value for an annotation");
        method_mask_cache &= a->method_mask;
    }
    m->method_mask_cache = method_mask_cache;
}

static void __cn_cnode_meta_destroy_annotation_value(gpointer v) {
    cn_annotation_free((cn_annotation_t *) v);
}

/* Create a new meta object */
static int __cn_cnode_meta_alloc(cn_cnode_meta_t **m_o) {
    cn_cnode_meta_t *m = calloc(1, sizeof(cn_cnode_meta_t));
    if (m == NULL) { 
        cncerr("Couldn't allocate new cnode meta struct, calloc failed\n");
        return -1; 
    }

    *m_o = m;

    return 0;
}

static void __cn_cnode_meta_init_annotations(cn_cnode_meta_t *m) {
    m->annotations = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL,
                                           __cn_cnode_meta_destroy_annotation_value);

    __cn_cnode_meta_compute_cache(m);
}

int cn_cnode_meta_new(cn_cnode_meta_t **m_o) {
    int res = __cn_cnode_meta_alloc(m_o);
    if (res != 0) { 
        return res; 
    }
    __cn_cnode_meta_init_annotations(*m_o);
    return 0;
}

void cn_cnode_meta_free(cn_cnode_meta_t *m) {
    g_hash_table_destroy(m->annotations);
    free(m);
}

/* Create a new meta object that is the clone of an existing meta object. Also
 * copies all annotations on the cnode_meta struct into the copied cnode_meta
 * struct. */
int cn_cnode_meta_copy(cn_cnode_meta_t *m, cn_cnode_meta_t **m_copy) {
    cn_cnode_meta_t *new;
    if (__cn_cnode_meta_alloc(&new) != 0) { 
        return -1; 
    }

    /* Copy the other fields */
    *new = *m;

    /* Re-init the annotations */
    __cn_cnode_meta_init_annotations(new);

    /* Copy over the annotations */
    GHashTableIter iter;
    gpointer key, value;

    g_hash_table_iter_init(&iter, m->annotations);
    while(g_hash_table_iter_next(&iter, &key, &value)) {
        cn_annotation_t * orig = (cn_annotation_t *) value;
        cn_annotation_t * copy;
        if (cn_annotation_copy(orig, &copy) != 0) {
            cncerr("failed to copy annotation from cnode_meta\n");
            cn_cnode_meta_free(new);
            return -1;
        }
        assert(key == copy->declassifier 
                && "The old key of the copy should always be the annotation"
                   " declassifier");
        /* should be OK to keep the same key, since it's just a const value */
        g_hash_table_insert(new->annotations, key, (gpointer) copy);
    }

    /* copy over the cache, should be the same since they both have 
     * the same annotations */
    new->method_mask_cache = m->method_mask_cache;

    *m_copy = new;
    return 0;
}

/* Returns true if the given meta object is annotated*/
bool cn_cnode_meta_is_annotated(cn_cnode_meta_t *m) {
    return m != NULL && g_hash_table_size(m->annotations) > 0;
}

/* Returns true if the given cnode is annotated*/
bool cn_cnode_is_annotated(struct cnode *c) {
    cn_cnode_meta_t *m = (cn_cnode_meta_t *) cap_cnode_metadata(c);
    return cn_cnode_meta_is_annotated(m);
}

/* Returns true if the given cnode meta object has an annotation from the given
 * object */
bool cn_cnode_meta_is_annotated_by(cn_cnode_meta_t *m, const cn_obj_t *obj) {
    return g_hash_table_contains(m->annotations, (gconstpointer) obj);
}

bool cn_cnode_is_annotated_by(struct cnode *c, const cn_obj_t *obj) {
    cn_cnode_meta_t *m = (cn_cnode_meta_t *) cap_cnode_metadata(c);
    if (m == NULL) { return false; }
    return cn_cnode_meta_is_annotated_by(m, obj);
}

/* Add the annotation the set of annotations on the metadata. */
int cn_cnode_meta_add_annotation(cn_cnode_meta_t *m,  cn_annotation_t * a) {
    if (g_hash_table_contains(m->annotations, a->declassifier)) {
        return EEXISTS;
    }
    assert(a->declassifier != NULL && "Can't have a null declassifier");
    g_hash_table_insert(m->annotations, (gpointer) a->declassifier, a);
    m->method_mask_cache &= a->method_mask;
    return 0;
}

/* Get the annotation that can be declassified using the given object. */
int cn_cnode_meta_get_annotation_by(cn_cnode_meta_t *m, cn_obj_t *obj, 
                                    cn_annotation_t **a_out) {
    gpointer result = g_hash_table_lookup(m->annotations, (gpointer) obj);
    if (result == NULL) {
        return ENOTEXISTS;
    }
    *a_out = (cn_annotation_t *) result;
    return 0;
}

/* Remove the given annotation from the set of annotations on the metadata */
int cn_cnode_meta_remove_annotation_by(cn_cnode_meta_t *m, const cn_obj_t *obj) {
    gboolean result = g_hash_table_remove(m->annotations, (gpointer) obj);
    if (! result) {
        return ENOTEXISTS;
    }
    __cn_cnode_meta_compute_cache(m);
    return 0;
}

char * cn_cnode_meta_string(cn_cnode_meta_t *m) {
    unsigned int annotation_count = g_hash_table_size(m->annotations);
    gchar ** annotation_strs;
    annotation_strs = malloc(sizeof(gchar *) * (annotation_count + 1));
    if (annotation_strs == NULL) { cn_abort("couldn't allocate temp string buffer"); }

    GHashTableIter it;
    gpointer key, value;
    g_hash_table_iter_init(&it, m->annotations);
    unsigned int i = 0;
    while (g_hash_table_iter_next(&it, &key, &value)) {
        char * annotation = cn_annotation_string((cn_annotation_t *) value);
        annotation_strs[i] = g_strdup_printf("(%p %s)", key, annotation);
        free(annotation);
        i++;
    }
    annotation_strs[annotation_count] = NULL;

    char * annotations = g_strjoinv(" ", annotation_strs);
    for (unsigned int i = 0; i < annotation_count; i++) {
        free(annotation_strs[i]);
    }
    free(annotation_strs);

    char * result = g_strdup_printf("(cnode-meta (annotations %s) "
                                                "(method-mask-cache %#0lx))",
                                    annotations, m->method_mask_cache);
    free(annotations);
    return result;
}

/* Annotations */

/* Create a new annotation */
int cn_annotation_new(cn_annotation_t **o_annotation) {
    cn_annotation_t * a = calloc(1, sizeof(*a));
    if (a == NULL) { 
        cncerr("Can't allocate new annotation\n");
        return -1;
    }
    a->declassifier = NULL;
    a->method_mask = 0x0;
    a->data.val = NULL;
    a->data.copy_f = NULL;
    a->data.free_f = NULL;

    *o_annotation = a;
    return 0;
}

/* Copy the annotation */
int cn_annotation_copy(cn_annotation_t *a, cn_annotation_t **a_copy) {
    cn_annotation_t * new = NULL;
    if (cn_annotation_new(&new) != 0) { return -1; }
    *new = *a;
    if (new->data.copy_f != NULL) { 
        new->data.copy_f(new->data.val);
    }

    if (new->declassifier != OBJ_SYSTEM_DECLASSIFIER) {
        RHOLD_OBJ(new->declassifier, new);
    }

    *a_copy = new;

    return 0;
}

/* Free an annotation created with cn_annotation_new, or cn_annotation_copy */
void cn_annotation_free(cn_annotation_t *a) {
    if (a->data.free_f != NULL) {
        a->data.free_f(a->data.val);
    }
    if (a->declassifier != NULL && a->declassifier != OBJ_SYSTEM_DECLASSIFIER) {
        RPUTs_OBJ(a->declassifier, a);
    }
    free(a);
}

char *cn_annotation_string(cn_annotation_t *a) {
    return g_strdup_printf(
            "(annotation (declassifier %p (%s %d)) (method-mask %#0lx) "
                        "(data (val %p) (copy_f %p) (free_f %p)))",
            a->declassifier,
            (a->declassifier == OBJ_SYSTEM_DECLASSIFIER)
                ? "obj-system-declassifier"
                : cn_objtype_name(a->declassifier->type),
            (a->declassifier == OBJ_SYSTEM_DECLASSIFIER)
                ? -1 
                : (int) a->declassifier->type,
            a->method_mask,
            a->data.val, a->data.copy_f, a->data.free_f);
}

/* Allow all operations */
void cn_method_mask_permission_all(method_mask_t * mask) {
    *mask = METHOD_MASK_ALL;
}

/* Prevent all operations */
void cn_method_mask_permission_none(method_mask_t * mask) {
    *mask = METHOD_MASK_NONE;
}

/* Set this given permission to allow */
void cn_method_mask_permission_set(method_mask_t * mask, cn_method_t m) {
    CN_SET_BIT(*mask, m);
}

/* Set the given permission to deny */
void cn_method_mask_permission_clear(method_mask_t * mask, cn_method_t m) {
    CN_UNSET_BIT(*mask, m);
}

/* Returns true if the given permission is set, returns false otherwise */
bool cn_method_mask_permission_is_set(method_mask_t * mask, cn_method_t m) {
    return CN_BIT_IS_SET(*mask, m);
}

/* sec: Normal Capability Operations */

char * cn_cnode_string(struct cnode * c) {
    cn_obj_t * obj = cn_cnode_object(c);
    cn_cnode_meta_t * meta = cap_cnode_metadata(c);
    char * meta_str = meta ? cn_cnode_meta_string(meta) : "(null)";
    char * res = g_strdup_printf("(cnode (cptr %p %#lx) (type %s) (obj %p %s) %s)",
                                 cn_cnode_principal(c), cptr2ul(cap_cnode_cptr(c)),
                                 cn_objtype_name(cn_cnode_type(c)),
                                 obj, (obj ? cn_objtype_name(obj->type) : NULL),
                                 meta_str);
    if (meta) { free(meta_str); }
    return res;
}

char * cn_cptr_string(cn_principal_t *p, cptr_t c) {
    return g_strdup_printf("(cptr %p %#lx)", p, cptr2ul(c));
}

int cn_grant_cnode_extended(struct cnode * ca,
                            cn_principal_t *dest, void * payload,
                            cptr_t *out, cn_objtype_t *o_type) {
    cptr_t cptr;
    cap_type_t type;
    cn_obj_lock(dest);
    if (cptr_alloc(dest->cache, &cptr) != 0) { goto fail; }
    if (cap_grant_cnode(ca, dest->cspace, cptr, payload, &type)) { goto fail1; }

    *out = cptr;
    *o_type = _captype_to_objtype(type);
    cn_obj_unlock(dest);
    return 0;

fail1:
    cptr_free(dest->cache, cptr);
fail:
    cn_obj_unlock(dest);
    return -1;
}

int cn_grant_extended(cn_principal_t *src, cptr_t csrc, 
                      cn_principal_t *dest, void * payload, 
                      cptr_t *out, cn_objtype_t *o_type) {
    cptr_t cptr;
    cap_type_t type;
    cn_obj_lock(src);
    cn_obj_lock(dest);
    if (cptr_alloc(dest->cache, &cptr) != 0) { goto fail; }
    c_log_debug("invoking cap_grant with payload %#0lx", (unsigned long) payload);
    if (cap_grant(src->cspace, csrc, dest->cspace, cptr, payload, &type) != 0) { goto fail1; }

    *out = cptr;
    c_log_debug("!!! converting cap_type: %d to obj_type...", type);
    *o_type = _captype_to_objtype(type);
    cn_obj_unlock(dest);
    cn_obj_unlock(src);
    return 0;

fail1:
    cptr_free(dest->cache, cptr);
fail:
    cn_obj_unlock(dest);
    cn_obj_unlock(src);
    return -1;
}

int cn_grant(cn_principal_t *src, cptr_t csrc,
             cn_principal_t *dest, void * payload, 
             cptr_t *out) {
    cn_objtype_t type;
    return cn_grant_extended(src, csrc, dest, payload, out, &type);
}

int cn_mint(cn_principal_t *p, cptr_t cptr, void * payload, 
            cptr_t *out, cn_objtype_t *type) {
    return cn_grant_extended(p, cptr, p, payload, out, type);
}

int cn_derive(cn_principal_t *src_p, cptr_t src_cptr,
              cn_principal_t *dst_p, cptr_t dst_cptr,
              void * payload) {
    cn_obj_lock(src_p);
    cn_obj_lock(dst_p);
    int res = cap_derive(src_p->cspace, src_cptr, dst_p->cspace, dst_cptr, payload);
    cn_obj_unlock(dst_p);
    cn_obj_unlock(src_p);
    return res;
}

void cn_delete(cn_principal_t *p, cptr_t cptr, void * payload) {
    cn_obj_lock(p);
    cap_delete(p->cspace, cptr, payload);
    cptr_free(p->cache, cptr);
    cn_obj_unlock(p);
}

int cn_revoke(cn_principal_t *p, cptr_t cptr, void * payload) {
    cn_obj_lock(p);
    int res = cap_revoke(p->cspace, cptr, payload);
    cn_obj_unlock(p);
    return res;
}

int cn_revoke_till(cn_principal_t *p, cptr_t cptr, 
                   bool (*pred)(struct cnode *, void * payload),
                   void * payload, void * pred_payload) {
    cn_obj_lock(p);
    int res = cap_revoke_till(p->cspace, cptr, pred, payload, pred_payload);
    cn_obj_unlock(p);
    return res;
}

/* sec: cnode helpers */

/* Get the principal associated with the given cnode. Should never fail. */
cn_principal_t * cn_cnode_principal(struct cnode * c) {
    return (cn_principal_t *) cap_cspace_owner(cap_cnode_cspace(c));
}

/* Get the cn_objtype_t type of this cnode. Note: This is a different than
 * the cn_objtype_t of the object inside the cnode, but they *should* always
 * match */
inline cn_objtype_t cn_cnode_type(struct cnode * c) {
    return _captype_to_objtype(cap_cnode_type(c));
}

/* Get the object contained in the given cnode. If asserts are enabled, it does
 * some type checking */
inline cn_obj_t * cn_cnode_object(struct cnode * c) {
#ifndef NDEBUG
    cn_obj_t * obj = (cn_obj_t *) cap_cnode_object(c);
    cn_obj_lock(obj);
    assert(obj->type == cn_cnode_type(c)
           && "cnode type does not match contained object type");
    cn_obj_unlock(obj);
    return obj;
#else
    return (cn_obj_t *) cap_cnode_object(c);
#endif
}

/* If two cnodes have the same principal, and are pointed to by the same cptr
 * then they are the same cnode. So, a principal and a cptr uniquely describe
 * a cnode */
bool cn_is_referenced_cnode(cn_principal_t * p, cptr_t c, struct cnode * cnode) {
    return (cn_cnode_principal(cnode) == p &&
            cptr2ul(cap_cnode_cptr(cnode)) == cptr2ul(c));
}

/* Get the cnode that the given cptr points to in the given principal. */
int cn_principal_get(cn_principal_t *p, cptr_t cap, struct cnode **o_cnode) {
    cn_obj_lock(p);
    int res = -1;
    if (cap_cnode_get(p->cspace, cap, o_cnode) != 0) { goto finish; } 
    res = 0;
finish:
    cn_obj_unlock(p);
    return res;
}

/* sec: Node Manipulation */

int cn_node_reset(struct cnode * node, cptr_t *o_grant) {
    int res = -1;
    assert(cn_cnode_type(node) == CN_NODE && "Tried to invoke reset on non-node");
    cn_node_t * n = (cn_node_t *) cn_cnode_object(node);

    cn_obj_lock(n);

    cn_node_grant_t * grant;
    if (cn_node_grant_new(n, &grant) != 0) {
        cncerr("failed to create new node grant\n");
        res = -1;
        goto finish;
    }

    RHOLD_OBJ(grant, OBJ_OWNER);

    cn_rp_t * rp0;
    if (cn_rp_new(&rp0) != 0) {
        cncerr("failed to create new rp\n");
        res = -1;
        goto finish1;
    }

    RHOLD_OBJ(rp0, OBJ_OWNER);

    cncinfo("Revoking grant chain (cap: %#0lx)\n", cptr2ul(n->grant));
    cn_revoke(n->secret, n->grant, NULL);
    cncinfo("Deleting grant\n");
    cn_delete(n->secret, n->grant, NULL);

    cncinfo("Revoking rp0 (cap: %#0lx)\n", cptr2ul(n->rp0));
    cn_revoke(n->secret, n->rp0, NULL);
    cn_delete(n->secret, n->rp0, NULL);

    if ((res = cn_principal_clear(n->principal)) != 0) { 
        cncerr("principal clear failed\n");
        goto finish2; }
    n->principal->owner = n;

    if ((res = cn_principal_insert(n->secret, (cn_obj_t *) grant, NULL, &n->grant)) != 0) {
        cncerr("Failed to insert grant into node's secret principal\n");
        goto finish2;
    }

    struct cnode * grant_cnode;
    if ((res = cn_principal_get(n->secret, n->grant, &grant_cnode)) != 0) {
        cncerr("Failed to lookup the grant object we just inserted\n");
        goto finish2;
    }

    /* insert the node grant into the CDT of the node cap from which it was
     * derived. */
    if ((res = cap_derive_cnode(node, grant_cnode, NULL)) != 0) {
        cncerr("Failed to derive the grant cnode from the node cnode\n");
        cap_delete_cnode(grant_cnode, NULL);
        goto finish2;
    }
    cap_cnode_put(grant_cnode);
    grant_cnode = NULL;

    /* give the node access to its RP0 */
    if ((res = cn_principal_insert(n->secret, cn_obj_ref(rp0), NULL, &n->rp0)) != 0) {
        cncerr("rp0 insert failed");
        cn_delete(n->secret, n->grant, NULL);
        goto finish2;
    }

    /* A wrapped node being reset should produce a wrapped node grant. Conceptually,
     * if the node capability is wrapped, then the entity performing the reset is
     * "outside" of the node with respect to the given wraps. We use the metadata of
     * the node as the "recv_meta" because those accurately reflect the position of
     * the receiver. The node itself on the other hand is always "internal" with
     * respect to its own node grant. */
    cn_objtype_t type;
    if ((res = cn_membrane_grant(/*send_meta=*/NULL, /*recv_meta=*/cap_cnode_metadata(node),
                                 n->secret, n->grant, cn_cnode_principal(node), 
                                 o_grant, &type)) != 0) {
        cncerr("Failed to grant grant obj to caller\n");
        cn_delete(n->secret, n->grant, NULL);
        goto finish2;
    }

    res = 0;
    goto finish;

finish2:
    RPUTs_OBJ(rp0, OBJ_OWNER);
finish1:
    RPUTs_OBJ(grant, OBJ_OWNER);
finish:
    cn_obj_unlock(n);

    return res;
}

/* sec: Node Grant Manipulation */

int cn_node_grant_grant(struct cnode * grant_cnode, 
                        cn_principal_t *p, cptr_t cap, 
                        cptr_t *o_cap, cn_objtype_t * o_type) {
    if (cn_cnode_type(grant_cnode) != CN_NODE_GRANT) { 
        cn_abort("Tried to grant using non-node_grant"); 
    }
    cn_node_grant_t * grant = (cn_node_grant_t *) cn_cnode_object(grant_cnode);
    cn_obj_lock(grant);
    cn_obj_lock(grant->node);

    int res = cn_membrane_grant(cap_cnode_metadata(grant_cnode), /*recv_meta=*/NULL,
                                p, cap, grant->node->principal, o_cap, o_type);

    if (res != 0) {
        cncerr("Failed membrane_grant in cn_node_grant_grant with code: %d\n", res);
    }

    cn_obj_unlock(grant->node);
    cn_obj_unlock(grant);

    return res;
}

int cn_node_grant_rp0(struct cnode * grant_cnode, cptr_t *o_rp_cap) {
    assert(cn_cnode_type(grant_cnode) == CN_NODE_GRANT);

    cn_node_grant_t * grant = (cn_node_grant_t *) cn_cnode_object(grant_cnode);
    cn_obj_lock(grant);
    cn_obj_lock(grant->node);

    cn_objtype_t type;
    int res = cn_membrane_grant(cap_cnode_metadata(grant_cnode), /*recv_meta=*/NULL,
                                grant->node->secret, grant->node->rp0,
                                cn_cnode_principal(grant_cnode), o_rp_cap, &type);

    if (res != 0) {
        cncerr("cn_membrane_grant failed for node_grant_rp0");
    }

    cn_obj_unlock(grant->node);
    cn_obj_unlock(grant);

    return res;
}

/* sec: RP Manipulation */

/* Forward declarations */
static int __cn_try_recv_waiting_all(GList **waiting, struct cnode * calling_obj);

static int __cn_rp_elem_message_set(struct cn_rp_elem *elem, 
                                    struct cn_rp_elem_message *msg) {
    if (msg == NULL) {
        elem->has_msg = false;
        return 0;
    }

    elem->has_msg = true;
    elem->msg.data = malloc(msg->len);
    if (elem->msg.data == NULL) {
        elem->has_msg = false;
        return -1;
    }
    memcpy(elem->msg.data, msg->data, msg->len);
    elem->msg.len = msg->len;
    return 0;
}

/* make a new rp_elem */
int cn_rp_elem_normal(cn_principal_t *p, cptr_t cap, 
                      struct cn_rp_elem_message * msg, 
                      struct cn_rp_elem ** o_elem) {
    struct cn_rp_elem * elem = g_slice_alloc(sizeof(struct cn_rp_elem));
    if (elem == NULL) { return -1; }
    if (__cn_rp_elem_message_set(elem, msg) != 0) {
        cn_rp_elem_free(elem);
        return -1;
    }
    elem->source = CN_RP_ELEM_SOURCE_NORMAL;
    elem->normal.cap_owner = p;
    elem->normal.cap = cap;
    *o_elem = elem;
    return 0;
}

int cn_rp_elem_injected(cn_obj_t *obj, struct cn_rp_elem_message *msg,
                        struct cn_rp_elem ** o_elem) {
    struct cn_rp_elem * elem = g_slice_alloc(sizeof(struct cn_rp_elem));
    if (elem == NULL) { return -1; }
    if (__cn_rp_elem_message_set(elem, msg) != 0) {
        cn_rp_elem_free(elem);
        return -1;
    }
    elem->source = CN_RP_ELEM_SOURCE_INJECTED;
    elem->injected.object = obj;
    *o_elem = elem;
    return 0;
}

int cn_rp_elem_membrane(cn_principal_t *p, cptr_t cap, 
                        cn_cnode_meta_t * meta,
                        struct cn_rp_elem_message *msg,
                        struct cn_rp_elem ** o_elem) {
    struct cn_rp_elem * elem = g_slice_alloc(sizeof(struct cn_rp_elem));
    if (elem == NULL) { return -1; }
    if (__cn_rp_elem_message_set(elem, msg) != 0) {
        cncerr("Failed to set membrane elem message\n");
        goto fail;
    }
    cn_cnode_meta_t *elem_meta = NULL;
    if (cn_cnode_meta_copy(meta, &elem_meta) != 0) {
        cncerr("Failed to create membrane elem cnode meta copy\n");
        goto fail;
    }
    elem->source = CN_RP_ELEM_SOURCE_MEMBRANE;
    elem->membrane.cap_owner = p;
    elem->membrane.cap = cap;
    elem->membrane.sender_meta = elem_meta;
    *o_elem = elem;
    return 0;

fail:
    cn_rp_elem_free(elem);
    return -1;
}

int cn_rp_elem_message(struct cn_rp_elem_message *msg,
                       struct cn_rp_elem **o_elem) {
    struct cn_rp_elem * elem = g_slice_alloc(sizeof(struct cn_rp_elem));
    if (elem == NULL) { return -1; }
    if (__cn_rp_elem_message_set(elem, msg) != 0) {
        cn_rp_elem_free(elem);
        return -1;
    }
    elem->source = CN_RP_ELEM_SOURCE_MESSAGE;
    *o_elem = elem;
    return 0;
}

static void __cn_rp_elem_hold_refs(void * who, struct cn_rp_elem *elem) {
    if (cn_rp_elem_is_injected(elem)) {
        RHOLD_OBJ(elem->injected.object, who);
    } else if (cn_rp_elem_is_membrane(elem)) {
        RHOLD_OBJ(elem->membrane.cap_owner, who);
    } else if (elem->source == CN_RP_ELEM_SOURCE_NORMAL) {
        RHOLD_OBJ(elem->normal.cap_owner, who);
    }
}

static void __cn_rp_elem_release_refs(void * who, struct cn_rp_elem *elem) {
    if (cn_rp_elem_is_injected(elem)) {
        RPUTs_OBJ(elem->injected.object, who);
    } else if (cn_rp_elem_is_membrane(elem)) {
        RPUTs_OBJ(elem->membrane.cap_owner, who);
    } else if (elem->source == CN_RP_ELEM_SOURCE_NORMAL) {
        RPUTs_OBJ(elem->normal.cap_owner, who);
    }
}

/* Push onto the front of the queue. I.e., the (p, cap) pair will be the next
 * thing to be popped */
int cn_rp_pushfront(cn_rp_t *rp, struct cn_rp_elem *elem) {
    cn_obj_lock(rp);
    __cn_rp_elem_hold_refs(rp, elem);
    g_queue_push_tail(rp->queue, (void *) elem);
    cn_obj_unlock(rp);
    return 0;
}

/* Push onto the back of the queue */
int cn_rp_push(cn_rp_t *rp, struct cn_rp_elem *elem) {
    cn_obj_lock(rp);
    __cn_rp_elem_hold_refs(rp, elem);
    g_queue_push_head(rp->queue, (void *) elem);
    cn_obj_unlock(rp);
    return 0;
}

/* Pop the next item from the queue. Note: DOES NOT DECREF PRINCIPAL. The caller
 * needs to decref the principal when it is appropriate. */
int cn_rp_pop_noput(cn_rp_t *rp, struct cn_rp_elem **o_elem) {
    cn_obj_lock(rp);
    struct cn_rp_elem * next_elem = (struct cn_rp_elem *) g_queue_pop_tail(rp->queue);
    cn_obj_unlock(rp);
    if (next_elem == NULL) { return -1; }
    *o_elem = next_elem;
    return 0;
}

void cn_rp_clear(cn_rp_t *rp) {
    cn_obj_lock(rp);
    __cn_rp_queue_free(rp);
    rp->queue = g_queue_new();
    cn_obj_unlock(rp);
}

int cn_rp_send(struct cnode *rp, cptr_t send_cap, struct cn_rp_elem_message *msg) {

#ifndef NDEBUG
    char * _rp_str = cn_cnode_string(rp);
    cncinfo("[rp_send] RP CNode: %s\n", _rp_str);
    free(_rp_str);
#endif

    struct cn_rp_elem * e;
    /* XXX: Special case functionality if the RP is wrapped. */
    if (cn_cnode_is_annotated(rp)) {
        cncinfo("[rp_send] membrane special case\n");
        cn_cnode_meta_t * meta = (cn_cnode_meta_t *) cap_cnode_metadata(rp);
        assert(meta != NULL && "cnode wrapped, but no meta");
        if (cn_rp_elem_membrane(cn_cnode_principal(rp), send_cap,
                                meta, msg, &e) != 0) {
            return -1;
        }
    } else { /* XXX: Normal case */
        if (cn_rp_elem_normal(cn_cnode_principal(rp), send_cap, msg, &e) != 0) { return -1; }
    }

    int res = cn_rp_send_rp_elem(rp, e);
    if (res != 0) { cn_rp_elem_free(e); }
    return res;
}

int cn_rp_send_rp_elem(struct cnode * rp, struct cn_rp_elem * e) {
    int res = -1;
    if (cn_cnode_type(rp) != CN_RP) { cn_abort("Tried to send to non-rp"); }
    cn_rp_t *_rp = (cn_rp_t *) cn_cnode_object(rp);
    cn_obj_lock(_rp);
    if (cn_rp_push(_rp, e) != 0) { 
        cn_rp_elem_free(e);
        res = -1;
        goto finish;
    }
    /* Try and recv any pending messages */
    if (__cn_try_recv_waiting_all(&_rp->waiting, rp) != 0) {
        res = -1;
        goto finish;
    }
    res = 0;

finish:
    cn_obj_unlock(_rp);
    return res;
}

// If the capability is being "injected" by the controller onto the RP
// instead of sent by another principal, there's no sending principal.
// In this case, just insert the capability into the node's CSpace.
static int cn_rp_recv_injected(struct cnode * rp, struct cn_rp_elem * e, 
                               cptr_t *recv_cap, cn_objtype_t *type) {
#ifndef NDEBUG
    cn_node_t * owner = (cn_node_t *) cn_principal_owner(cn_cnode_principal(rp));
    if (owner == NULL) {
        cncerr("Trying to recv injected capability via principal that is not"
               " associated with a node.\n");
        return -1;
    }
#endif

    if (cn_principal_insert(cn_cnode_principal(rp), e->injected.object, NULL, recv_cap) != 0) {
        cncerr("Couldn't insert injected object into principal\n");
        return -1;
    }
    // succeed injected reception
    cn_obj_lock(e->injected.object);
    *type = e->injected.object->type;
    cn_obj_unlock(e->injected.object);
    return 0;
}

static int cn_rp_recv_membrane(struct cnode *rp, struct cn_rp_elem *e,
                               cptr_t * recv_cap, cn_objtype_t *type) {
    assert((cn_rp_elem_is_membrane(e) || cn_rp_elem_is_normal(e)) &&
           "rp_elem must be normal or membrane when doing a membrane recv");

    cn_cnode_meta_t * send_meta = NULL;
    cn_cnode_meta_t * recv_meta = (cn_cnode_meta_t *) cap_cnode_metadata(rp);

    cn_principal_t * owner = NULL;
    cptr_t cap;
    if (cn_rp_elem_is_membrane(e)) {
        owner = e->membrane.cap_owner;
        cap = e->membrane.cap;
        send_meta = e->membrane.sender_meta;
    } else if (cn_rp_elem_is_normal(e)) {
        owner = e->normal.cap_owner;
        cap = e->normal.cap;
    }  else {
        cn_abort("unreachable");
    }

    /* Now we just do the normal membrane grant... */
    if (cn_membrane_grant(send_meta, recv_meta,
                          owner, cap, cn_cnode_principal(rp),
                          recv_cap, type) != 0) {
        cncerr("membrane grant failed\n");
        return -1;
    }

    return 0;
}

int cn_rp_recv(struct cnode *rp, 
               enum cn_rp_recv_result_flag *flags,
               cptr_t *recv_cap, cn_objtype_t *type,
               struct cn_rp_elem_message * o_message) {
    int result = -1;
    if (cn_cnode_type(rp) != CN_RP) { cn_abort("Tried to recv from non-rp"); }
    cn_rp_t * _rp = (cn_rp_t *) cn_cnode_object(rp);
    cn_obj_lock(_rp);

    *flags = CN_RP_RECV_RESULT_TYPE_NONE;

    struct cn_rp_elem * e;
    if (cn_rp_pop_noput(_rp, &e) != 0) { 
        cncerr("couldn't pop off of RP\n");
        cn_obj_unlock(_rp);
        return EEMPTY;
    }

    if (e->has_msg) {
        o_message->data = malloc(e->msg.len);
        if (o_message->data == NULL) {
            cncerr("couldn't allocate message data result\n");
            goto recover1;
        }
        memcpy(o_message->data, e->msg.data, e->msg.len);
        o_message->len = e->msg.len;
        *flags |= CN_RP_RECV_RESULT_TYPE_MESSAGE;
    }

    cncinfo("[rp_recv] source: %d\n", e->source);

    // XXX: No wrapping is done on injected objects, or on messages.
    if (e->source == CN_RP_ELEM_SOURCE_MESSAGE) {
        cncinfo("[rp_recv] message only elem\n");
        // if our cap contains only a message, then skip the rest of the
        // processing, it was handled above.
    } else if (cn_rp_elem_is_injected(e)) { 
        cncinfo("[rp_recv] injected elem\n");
        if (cn_rp_recv_injected(rp, e, recv_cap, type) != 0) {
            goto recover;
        }
        *flags |= CN_RP_RECV_RESULT_TYPE_CPTR;
    // XXX: If we received a capability from a wrapped RP or we are wrapped,
    //      do the special-case wrapping stuff...
    } else if (cn_rp_elem_is_membrane(e) || cn_cnode_is_annotated(rp)) {
        cncinfo("[rp_recv] membrane action\n");
        if (cn_rp_recv_membrane(rp, e, recv_cap, type) != 0) {
            goto recover;
        }
        *flags |= CN_RP_RECV_RESULT_TYPE_CPTR;
    } else if (e->source == CN_RP_ELEM_SOURCE_NORMAL) { // XXX: Normal case
        cncinfo("[rp_recv] normal grant\n");
        if (cn_grant_extended(e->normal.cap_owner, e->normal.cap, 
                              cn_cnode_principal(rp), NULL, recv_cap, type) != 0) {
            cncerr("normal grant failed\n");
            goto recover;
        }
        *flags |= CN_RP_RECV_RESULT_TYPE_CPTR;
    } else {
        cn_abort("Unrecognized rp elem source: %d", e->source);
    }

    __cn_rp_elem_release_refs(_rp, e);
    cn_rp_elem_free(e);
    cn_obj_unlock(_rp);
    return 0;

recover:
    if (CN_FLAG_SET(*flags, CN_RP_RECV_RESULT_TYPE_MESSAGE)) {
        free(o_message->data);
    }
recover1:
    // If the grant fails, put it back on the queue, in the front
    cn_rp_pushfront(_rp, e);
    __cn_rp_elem_release_refs(_rp, e);
    cn_obj_unlock(_rp);
    return -1;
}

/* calling_obj may be NULL */
static int __cn_try_recv_waiting(cn_node_t *waiting_node, cptr_t recv_obj, 
                                 uint64_t waiting_uuid,
                                 struct cnode * calling_obj) {
    int res = -1;
    cn_obj_lock(waiting_node);
    cn_obj_lock(waiting_node->principal);

    cn_dispatch_result_t result;
    cn_dispatch_result_init(&result);
    result.request.uuid = cn_gen_uuid();

    struct cnode * obj_cnode = NULL;
    /* if we are getting called in an RP callback, and the rp the principal is waiting
     * on is the rp we're in the callback of, just use that RP cnode */
    if (calling_obj != NULL && 
            cn_is_referenced_cnode(waiting_node->principal, recv_obj, calling_obj)) {
        obj_cnode = calling_obj;
    } else {
        res = cn_principal_get(waiting_node->principal, recv_obj, &obj_cnode);
        if (res != 0) {
            res = cn_dispatch_result_fail(&result, "CPTR being waited on is invalid");
            goto finish2;
        }
    }

    c_log_debug("processing wait entry");

    cptr_t cap;
    cn_objtype_t type;
    struct cn_rp_elem_message msg;
    enum cn_rp_recv_result_flag flags;

    switch (cn_cnode_type(obj_cnode)) {
        case CN_RP:
            res = cn_rp_recv(obj_cnode, &flags, &cap, &type, &msg);
            break;
        case CN_MEMBRANE:
            res = cn_membrane_recv(obj_cnode, &cap, &type);
            break;
        default:
            cn_abort("unreachable, somehow recv_wait-ing on an object that"
                     " does not support recv");
    }

    if (res == EEMPTY) {
        goto finish1;
    } else if (res != 0) {
        cncerr("recv on object of type %s failed with bad exit code: %d\n", 
               cn_objtype_name(cn_cnode_type(obj_cnode)), res);
        res = cn_dispatch_result_fail(&result, "recv on %#lx failed with exit code: %d", 
                                               cptr2ul(cap_cnode_cptr(obj_cnode)), res);
    } else {
        if (cn_cnode_type(obj_cnode) == CN_RP) {
            if (cn_dispatch_util_fill_rp_recv_result(&result, cap, type, 
                                                     &msg, flags) != 0) {
                res = cn_dispatch_result_fail(&result, "couldn't add recv results to result"); 
                goto finish2;
            } 
        } else if (cn_cnode_type(obj_cnode) == CN_MEMBRANE) {
            cn_dispatch_result_item_t * item = cn_dispatch_result_item_alloc();
            if (item == NULL) {
                res = cn_dispatch_result_fail(&result, "couldn't allocate result item");
                goto finish2;
            }
            cn_dispatch_result_item_init_cptr(item, cap, type);
            cn_dispatch_result_push_item(&result, item);
        } else {
            cn_abort("unreachable");
        }
        result.status = true;
        result.msg = strdup("success");
        res = 0;
    }

finish2:
    c_log_debug("Sending notify...");
    if (cnc_send_notify(waiting_node, waiting_uuid, &result) != 0) {
        cn_abort("Failed to notify recv_wait after capability received");
    }
    cn_dispatch_result_clear(&result);
finish1:
    if (obj_cnode) { cap_cnode_put(obj_cnode); }
//finish:
    cn_obj_unlock(waiting_node);
    cn_obj_unlock(waiting_node->principal);
    return res;
}


/* 'calling_rp' can be null */
static int __cn_try_recv_waiting_all(GList **waiting, struct cnode * calling_obj) {
    int res = -1;
    GList * head = g_list_first(*waiting);
    while (head != NULL) {
        GList *next = g_list_next(head);
        struct cn_waiting_node * wait = 
            (struct cn_waiting_node *) head->data;
        res = __cn_try_recv_waiting(wait->node, wait->obj, wait->uuid, calling_obj);
        if (res == EEMPTY) {
            break;
        } else if (res != 0) {
            return -1;
        }
        cn_waiting_node_free(wait);
        *waiting = g_list_delete_link(*waiting, head);
        head = next;
    }
    return 0;
}

int cn_rp_recv_wait(struct cnode *rp, uint64_t uuid) {
    int res = -1;
    cn_principal_t * p = cn_cnode_principal(rp);
    cn_obj_lock(p);
    cn_node_t * owner = (cn_node_t *) p->owner;
    assert(owner != NULL && "Cannot recv_wait with a principal alone");
    cn_obj_lock(owner);

    cn_rp_t *_rp = (cn_rp_t *) cn_cnode_object(rp);
    cn_obj_lock(_rp);

    struct cn_waiting_node * wait = NULL;
    if (cn_waiting_node_new(owner, cap_cnode_cptr(rp), uuid,&wait) != 0) {
        res = -1;
        goto finish;
    }

    _rp->waiting = g_list_append(_rp->waiting, wait);

    /* Try and recv any possibly pending cptrs */
    res = __cn_try_recv_waiting_all(&_rp->waiting, rp);

finish:
    cn_obj_unlock(_rp);
    cn_obj_unlock(owner);
    cn_obj_unlock(p);
    return res;
}

/* sec: Broker Manipulation */

static int __cn_broker_try_lookup_single(struct cnode *broker, 
                                         struct cn_waiting_node *w,
                                         char * name) {
    int res = -1;
    cn_obj_lock(w->node);
    cn_obj_lock(w->node->principal);

    cn_dispatch_result_t result;
    cn_dispatch_result_init(&result);
    result.request.uuid = cn_gen_uuid();

    struct cnode *obj_cnode = NULL;

    if (broker != NULL && 
            cn_is_referenced_cnode(w->node->principal, w->obj, broker)) {
        obj_cnode = broker;
    } else {
        res = cn_principal_get(w->node->principal, w->obj, &obj_cnode);
        if (res != 0) {
            res = cn_dispatch_result_fail(&result, "CPTR being waited on is invalid");
            goto finish;
        }
    }

    cptr_t rp;
    if (cn_broker_lookup(obj_cnode, name, &rp) != 0) {
        res = cn_dispatch_result_fail(&result, "Couldn't lookup from broker");
        goto finish;
    }

    cn_dispatch_result_item_t *item = cn_dispatch_result_item_alloc();
    if (item == NULL) {
        res = cn_dispatch_result_fail(&result, "couldn't allocate result item");
        goto finish;
    }

    cn_dispatch_result_item_init_cptr(item, rp, CN_RP);
    cn_dispatch_result_push_item(&result, item);

    result.status = true;
    result.msg = strdup("success");
    
    res = 0;

finish:
    if (cnc_send_notify(w->node, w->uuid, &result) != 0) {
        cn_abort("Failed to notify broker.lookup after service registered");
    }
    cn_dispatch_result_clear(&result);
    if (obj_cnode) { cap_cnode_put(obj_cnode); }
    cn_obj_unlock(w->node->principal);
    cn_obj_unlock(w->node);
    return res;
}


static int __cn_broker_try_lookup(struct cnode * broker, char * name, GList * waiting) {
    cn_broker_t * _broker = (cn_broker_t *) cn_cnode_object(broker);
    cn_obj_lock(_broker);

    if (! g_hash_table_contains(_broker->registry, name)) {
        cn_obj_unlock(_broker);
        return -1;
    }

    GList * cur = g_list_first(waiting);
    for (; cur != NULL; cur = g_list_next(cur)) {
        (void) __cn_broker_try_lookup_single(broker, 
                                             (struct cn_waiting_node *) cur->data,
                                             name);
        cn_waiting_node_free((struct cn_waiting_node *) cur->data);
    }

    return 0;
}

static void __debug_print_broker_registry(cn_broker_t * b) {
    GHashTableIter iter;
    gpointer key, value;

    cncinfo("Broker Registry:\n");
    g_hash_table_iter_init(&iter, b->registry);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        cncinfo("%s\n", (char *) key);
    }
}

int cn_broker_register(struct cnode *broker, char *service_name, cptr_t rp_cap) {
    cn_broker_t *real_broker;
    struct cn_broker_elem *e;
    int res = -1;

    if (cn_cnode_type(broker) != CN_BROKER) {
        cn_abort("Tried to register on a non-broker");
    }
    real_broker = (cn_broker_t *)cn_cnode_object(broker);
    cn_obj_lock(real_broker);
    if (g_hash_table_lookup(real_broker->registry,service_name)) {
        cncwarn("broker already has service %s registered!",service_name);
        goto finish;
    }
    if (cn_broker_elem_new(cn_cnode_principal(broker), rp_cap, &e) != 0)
        goto finish;
    g_hash_table_insert(real_broker->registry,strdup(service_name),e);

    __debug_print_broker_registry(real_broker);

    if (g_hash_table_contains(real_broker->waiting, service_name)) {
        GList * waiters = g_hash_table_lookup(real_broker->waiting, service_name);
        (void) __cn_broker_try_lookup(broker, service_name, waiters);
    }

    cncdbg(3,LA_CTL,LF_CTL,"registered service %s\n",service_name);

    res = 0;
finish:
    cn_obj_unlock(real_broker);
    return res;
}

int cn_broker_lookup(struct cnode *broker, char *service_name, cptr_t *rp_cap) {
    cn_broker_t *real_broker;
    struct cn_broker_elem *e;
    int res = -1;
    cn_objtype_t type;

    if (cn_cnode_type(broker) != CN_BROKER) {
        cn_abort("Tried to lookup on a non-broker");
    }
    real_broker = (cn_broker_t *)cn_cnode_object(broker);
    cn_obj_lock(real_broker);

    __debug_print_broker_registry(real_broker);

    e = (struct cn_broker_elem *)g_hash_table_lookup(real_broker->registry,
                                                     service_name);
    if (!e) {
        cncwarn("broker does not have service %s registered!",service_name);
        goto finish;
    }

    if (cn_grant_extended(e->cap_owner, e->cap, 
                          cn_cnode_principal(broker), NULL, rp_cap, &type) != 0) {
        cncerr("grant failed\n");
        goto finish;
    }
    assert(type == CN_RP && "broker lookup did not grant an RP cap!");

    cncdbg(3,LA_CTL,LF_CTL,
           "found service %s and granted cap to its RP\n",
           service_name);
    res = 0;

finish:
    cn_obj_unlock(real_broker);
    return res;
}

int cn_broker_lookup_wait(struct cnode *broker, char *service_name, uint64_t uuid) {
    int res = -1;
    cn_broker_t * b = (cn_broker_t *) cn_cnode_object(broker);
    cn_obj_lock(b);

    cn_principal_t * _p = cn_cnode_principal(broker);
    cn_obj_lock(_p);
    cn_node_t * node = (cn_node_t *) _p->owner;
    cn_obj_lock(node);

    struct cn_waiting_node *w;
    if (cn_waiting_node_new(node, cap_cnode_cptr(broker), uuid, &w) != 0) {
        res = -1;
        goto finish;
    }

    __debug_print_broker_registry(b);

    GList * waiting = NULL;
    if (g_hash_table_contains(b->waiting, service_name)) {
        waiting = (GList *) g_hash_table_lookup(b->waiting, service_name);
    }
    waiting = g_list_append(waiting, w);
    g_hash_table_insert(b->waiting, service_name, waiting);

    (void) __cn_broker_try_lookup(broker, service_name, waiting);

    res = 0;
finish:
    cn_obj_unlock(node);
    cn_obj_unlock(_p);
    cn_obj_unlock(b);
    return res;
}

/* sec: Membrane Manipulation */

int cn_membrane_send(struct cnode *m, cptr_t send_cap) {
    int res = -1;
    assert(cn_cnode_type(m) == CN_MEMBRANE && "Tried to send on non-membrane");

    cn_membrane_t * membrane = (cn_membrane_t *) cn_cnode_object(m);
    cn_obj_lock(membrane);
    cn_cnode_meta_t * meta = (cn_cnode_meta_t *) cap_cnode_metadata(m);
    assert(meta != NULL && "membrane must have metadata");

    struct cn_rp_elem * e;
    if (cn_rp_elem_membrane(cn_cnode_principal(m), send_cap, 
                            meta, NULL, &e) != 0) { return -1; }
    if (cn_rp_push(membrane->rp, e) != 0) { goto fail; }

    /* Try and recv any pending messages */
    if (__cn_try_recv_waiting_all(&membrane->waiting, m) != 0) {
        res = -1;
        goto finish;
    }

    c_log_debug("[membrane] sent %#lx", cptr2ul(send_cap));

    res = 0;
    goto finish;

fail:
    cn_rp_elem_free(e);
finish:
    cn_obj_unlock(membrane);
    return res;
}

                                  
/* Basically the same code as cn_rp_recv, but have to de encapsulate the
 * rp first. */
int cn_membrane_recv(struct cnode *m, cptr_t *o_cap, cn_objtype_t *type) {
    int res = -1;
    assert(cn_cnode_type(m) == CN_MEMBRANE && "Tried to send on non-membrane");
    cn_membrane_t * membrane = (cn_membrane_t *) cn_cnode_object(m);
    cn_obj_lock(membrane);
    cn_cnode_meta_t * meta = (cn_cnode_meta_t *) cap_cnode_metadata(m);
    assert(meta != NULL && "membrane meta must not be null");

    struct cn_rp_elem *e; 
    if (cn_rp_pop_noput(membrane->rp, &e) != 0) { 
        res = EEMPTY;
        goto recover; 
    }
    
    assert(cn_rp_elem_is_membrane(e) && "membranes only deal with membrane rp elems");

    if (cn_membrane_grant(e->membrane.sender_meta, meta,
                          e->membrane.cap_owner, e->membrane.cap,
                          cn_cnode_principal(m),
                          o_cap, type) != 0) {
        cncerr("cn_membrane_grant returned non-zero exit code\n");
        res = -1;
        goto recover1;
    }

    __cn_rp_elem_release_refs(membrane->rp, e);
    cn_rp_elem_free(e);
    cn_obj_unlock(membrane);
    return 0;

recover1:
    /* Push first in case these are the last references to this object */
    cn_rp_pushfront(membrane->rp, e);
    __cn_rp_elem_release_refs(membrane->rp, e);
recover:
    cn_obj_unlock(membrane);
    return res;
}

int cn_membrane_clear(struct cnode *m) {
    assert(cn_cnode_type(m) == CN_MEMBRANE && "tried to clear non-membrane");
    cn_membrane_t * membrane = (cn_membrane_t *) cn_cnode_object(m);
    cn_obj_lock(membrane);

    __cn_membrane_log_clear(membrane);
    /* have to clear the queued capabilities or the receiver may be able to get
     * wrapped references after the sender thought they isolated the membrane. */
    cn_rp_clear(membrane->rp);
    cn_obj_unlock(membrane);
    return 0;
}

static int __cn_membrane_external_grant_callback(struct cnode * ca, 
                                                 struct cnode * cb, 
                                                 void * payload) {
    (void) payload;
    assert(cn_cnode_type(ca) == CN_MEMBRANE && 
           "tried to call membrane_external on non membrane");
    cn_cnode_meta_t * ca_meta = (cn_cnode_meta_t *) cap_cnode_metadata(ca);
    assert(cn_cnode_is_annotated(ca) &&
            "All membranes must be annotated");
    cn_annotation_t * internal;
    if (cn_cnode_meta_get_annotation_by(ca_meta, (cn_obj_t *) cn_cnode_object(ca), 
                                        &internal)) {
        cn_abort("Membrane not annotated with its own annotation\n");
    }

    if (cn_membrane_annotation_type(internal) != CN_MEMBRANE_TYPE_INTERNAL) {
        cncerr("Can't create external membrane from non-internal membrane\n");
        return -1;
    }

    cn_cnode_meta_t * cb_meta = (cn_cnode_meta_t *) cap_cnode_metadata(cb);
    assert(cb_meta != NULL && "If ca meta is set, cb meta cannot be null");
    if (cn_cnode_meta_remove_annotation_by(cb_meta, internal->declassifier) != 0) {
        cn_abort("cb_meta should internal type annotation\n");
    }

    cn_annotation_t * external;
    if (cn_annotation_copy(internal, &external) != 0) {
        cncerr("Failed to copy internal -> external annotation\n");
        return -1;
    }

    external->data.val = cn_membrane_type_to_ptr(CN_MEMBRANE_TYPE_EXTERNAL);

    if (cn_cnode_meta_add_annotation(cb_meta, external) != 0) {
        cncerr("Failed to add external annotation to new cap's meta\n");
        cn_annotation_free(external);
        return -1;
    }

    return 0;
}

int cn_membrane_external(struct cnode * membrane, cptr_t *o_cap) {
    printf("pre-struct\n");
    struct cn_cb_payload cb_payload = {
        .binop = __cn_membrane_external_grant_callback,
        .payload = NULL
    };
    printf("post-struct\n");

    cn_objtype_t type;
    return cn_grant_cnode_extended(membrane, cn_cnode_principal(membrane), 
                                   (void *) &cb_payload, o_cap, &type);
}

int cn_membrane_recv_wait(struct cnode *membrane, uint64_t uuid) {
    int res = -1;
    cn_principal_t * p = cn_cnode_principal(membrane);
    cn_obj_lock(p);
    cn_node_t * owner = (cn_node_t *) p->owner;
    assert(owner != NULL && "Cannot recv_wait with a principal alone");
    cn_obj_lock(owner);

    cn_membrane_t *m = (cn_membrane_t *) cn_cnode_object(membrane);
    cn_obj_lock(m);

    struct cn_waiting_node * wait = NULL;
    if (cn_waiting_node_new(owner, cap_cnode_cptr(membrane), uuid, &wait) != 0) {
        res = -1;
        goto finish;
    }

    m->waiting = g_list_append(m->waiting, wait);

    /* Try and recv any possibly pending cptrs */
    res = __cn_try_recv_waiting_all(&m->waiting, membrane);

finish:
    cn_obj_unlock(m);
    cn_obj_unlock(owner);
    cn_obj_unlock(p);
    return res;
}

/* sec: Sealer-Unsealer Manipulation */

static int __cn_sealer_unsealer_seal_callback(struct cnode * ca,
                                              struct cnode * cb,
                                              void * payload) {
    cn_sealer_unsealer_t * su = (cn_sealer_unsealer_t *) payload;

    cn_cnode_meta_t * ca_meta = cap_cnode_metadata(ca);
    if (ca_meta != NULL && cn_cnode_meta_is_annotated_by(ca_meta, (cn_obj_t *) su)) {
        c_log_debug("[seal callback] already annotated, aborting");
        return 0;
    }

    if (cn_ensure_target_meta(ca, cb) != 0) {
        cncerr("Couldn't ensure cb meta\n");
        return -1;
    }

    cn_cnode_meta_t * cb_meta = cap_cnode_metadata(cb);

    cn_annotation_t * a;
    if (cn_annotation_new(&a) != 0) {
        cncerr("Couldn't create annotation\n");
        goto fail;
    }

    a->declassifier = (cn_obj_t *) su;
    cn_method_mask_permission_none(&a->method_mask);

    assert(a->declassifier->type == CN_SEALER_UNSEALER && "type must be sealer unsealer");
    c_log_debug("[seal callback] %p", a->declassifier);


    if (cn_cnode_meta_add_annotation(cb_meta, a) != 0) {
        cncerr("Couldn't add seal annotation to cnode meta");
        goto fail;
    }

#ifndef NDEBUG
    char * meta_str = cn_cnode_meta_string(cap_cnode_metadata(cb));
    c_log_debug("[seal callback] added annotation: %s", meta_str);
    free(meta_str);
#endif

    return 0;

fail:
    cn_ensure_target_meta_cleanup(ca, cb);
    return -1;
}

int cn_sealer_unsealer_seal(struct cnode * su, cptr_t to_seal, 
                            cptr_t *sealed, cn_objtype_t * type) {
    int res = -1;
    assert(cn_cnode_type(su) == CN_SEALER_UNSEALER 
            && "Invoked seal on non-seal-unsealer");
    cn_sealer_unsealer_t * _su = (cn_sealer_unsealer_t *) cn_cnode_object(su);
    cn_cnode_meta_t * meta = cap_cnode_metadata(su);

    cn_obj_lock(_su);

    struct cn_cb_payload cb_payload = {
        .binop = __cn_sealer_unsealer_seal_callback,
        .payload = (void *) _su,
    };

    if (cn_grant_extended(cn_cnode_principal(su), to_seal,
                          cn_cnode_principal(su), (void *) &cb_payload,
                          sealed, type) != 0) {
        cncerr("Couldn't do sealing grant\n");
        res = -1;
        goto finish;
    }

    res = 0;
finish:
    cn_obj_unlock(_su);
    return res;
}

static int __cn_sealer_unsealer_unseal_callback(struct cnode *ca, struct cnode *cb, 
                                                void * payload) {

    cn_sealer_unsealer_t * su = (cn_sealer_unsealer_t *) payload;

    cn_cnode_meta_t * cb_meta = cap_cnode_metadata(cb);
    if (cb_meta == NULL) {
        c_log_debug("[unseal callback] cb meta is null");
    } else {
        char * str = cn_cnode_meta_string(cb_meta);
        c_log_debug("[unseal callback] cb_meta: %s", str);
        free(str);
    }

    c_log_debug("[unseal callback] is sealed by: %p", su);

    cn_annotation_t * a;
    if (cb_meta == NULL || ! cn_cnode_meta_is_annotated_by(cb_meta, (cn_obj_t *) su)) {
        cncerr("Cannot invoke unseal on non-sealed object\n");
        return -1;
    }

    if (cn_cnode_meta_remove_annotation_by(cb_meta, (cn_obj_t *) su) != 0) {
        cncerr("Cannot remove annotation\n");
        return -1;
    }

    return 0;
}

int cn_sealer_unsealer_unseal(struct cnode * su, cptr_t sealed, 
                              cptr_t *unsealed, cn_objtype_t *type) {
    int res = -1;

    assert(cn_cnode_type(su) == CN_SEALER_UNSEALER && "Tried to call unseal on non sealer");
    cn_sealer_unsealer_t *_su = (cn_sealer_unsealer_t *) cn_cnode_object(su);

    cn_obj_lock(_su);

    struct cn_cb_payload cb_payload = {
        .binop = __cn_sealer_unsealer_unseal_callback,
        .payload = (void *) _su,
    };

    /* Sealer/unsealers are never wrapped, so no need to do a membrane grant
     * on unseal. Any additional annotations on the capability itself will
     * persist. */
    if (cn_grant_extended(cn_cnode_principal(su), sealed,
                          cn_cnode_principal(su), (void *) &cb_payload,
                          unsealed, type) != 0) {
        cncerr("Couldn't do sealing grant\n");
        res = -1;
        goto finish;
    }

    res = 0;

finish:
    cn_obj_unlock(_su);
    return res;
}

static int __cn_sealer_unsealer_restrict_cb(struct cnode *ca, struct cnode *cb, void *payload) {
    assert(payload != NULL && "restrict_cb should always get payload");

    cn_method_t restricted_to = *((cn_method_t *) payload);

    if (cn_ensure_target_meta(ca, cb) != 0) {
        cncerr("Couldn't allocate meta for target cnode\n");
        return -1;
    }

    cn_cnode_meta_t * b_meta = cap_cnode_metadata(cb);

    assert((! cn_cnode_meta_is_annotated_by(b_meta, (const cn_obj_t *) OBJ_SYSTEM_DECLASSIFIER))
            && "shouldn't be able to invoke restrictions after we're already restricted");

    cn_annotation_t *restriction;
    if (cn_annotation_new(&restriction) != 0) {
        cncerr("Failed to allocate annotation for restriction\n");
        goto fail;
    }

    restriction->declassifier = (const cn_obj_t *) OBJ_SYSTEM_DECLASSIFIER;

    cn_method_mask_permission_all(&restriction->method_mask);
    cn_method_mask_permission_clear(&restriction->method_mask, CN_SEALER_UNSEALER_RESTRICT_TO_SEAL);
    cn_method_mask_permission_clear(&restriction->method_mask, CN_SEALER_UNSEALER_RESTRICT_TO_UNSEAL);
    if (restricted_to == CN_SEALER_UNSEALER_SEAL) {
        cn_method_mask_permission_clear(&restriction->method_mask, CN_SEALER_UNSEALER_UNSEAL);
    } else if (restricted_to == CN_SEALER_UNSEALER_UNSEAL) {
        cn_method_mask_permission_clear(&restriction->method_mask, CN_SEALER_UNSEALER_SEAL);
    } else {
        cn_abort("unreachable");
    }

    if (cn_cnode_meta_add_annotation(b_meta, restriction) != 0) {
        cncerr("Failed to add restricted annotation to sealer/unsealer");
        goto fail1;
    }

    return 0;

fail1:
    cn_annotation_free(restriction);
fail:
    cn_ensure_target_meta_cleanup(ca, cb);
    return -1;
}

static int __cn_sealer_unsealer_restrict_to(struct cnode *su, 
                                            cn_method_t method,
                                            cptr_t *restricted) {
    assert(cn_cnode_type(su) == CN_SEALER_UNSEALER 
            && "must be sealer/unsealer to restrict rights");

    struct cn_cb_payload payload = {
        .binop = __cn_sealer_unsealer_restrict_cb,
        .payload = (void *) &method,
    };

    cn_objtype_t type;
    if (cn_grant_cnode_extended(su, cn_cnode_principal(su), (void *) &payload,
                                restricted, &type) != 0) {
        cncerr("Restriction grant failed\n");
        return -1;
    }
    assert(type == CN_SEALER_UNSEALER && "should always be granting sealer/unsealer");

    return 0;
}

int cn_sealer_unsealer_restrict_to_seal(struct cnode * su, cptr_t *seal_only) {
    return __cn_sealer_unsealer_restrict_to(su, CN_SEALER_UNSEALER_SEAL, seal_only);
}

int cn_sealer_unsealer_restrict_to_unseal(struct cnode * su, cptr_t *unseal_only) {
    return __cn_sealer_unsealer_restrict_to(su, CN_SEALER_UNSEALER_UNSEAL, unseal_only);
}

/* sec: Generic Helper */

int cnc_deliver_obj(cn_node_t *node, cn_obj_t *obj) {
    int result = -1;
    struct cnode * rp0;
    cn_obj_lock(node);
    if (cn_principal_get(node->secret, node->rp0, &rp0) != 0) {
        cncerr("Can't get RP0 for node to deliver cptr\n");
        goto fail1;
    }
    struct cn_rp_elem * e;
    if (cn_rp_elem_injected(obj, NULL, &e) != 0) {
        cncerr("couldn't create injected rp_elem\n");
        goto fail;
    }
    result = cn_rp_send_rp_elem(rp0, e);
    if (result != 0) { cn_rp_elem_free(e); goto fail; }

fail:
    cap_cnode_put(rp0);
fail1:
    cn_obj_unlock(node);
    return result;
}

/* Sec: Notify Wait */

void cn_notify_wait_init(struct cn_notify_wait * w) {
    if (pthread_mutex_init(&w->cond_mutex, NULL) != 0) {
        cn_abort("Could not init mutex");
    }
    if (pthread_cond_init(&w->cond, NULL) != 0) {
        cn_abort("Could not init condition variable");
    }
}

void cn_notify_wait_destroy(struct cn_notify_wait *w) {
    if (pthread_cond_destroy(&w->cond) != 0) {
        cn_abort("Could not destory condition variable");
    }
    if (pthread_mutex_destroy(&w->cond_mutex) != 0) {
        cn_abort("Could not destory condition variable's mutex");
    }
}
