#ifndef __OBJ_WAITING_H__
#define __OBJ_WAITING_H__

#include <stdint.h>

#include <libcap.h>

#include "obj_internal.h"
#include "obj_decl.h"

struct cn_waiting_node {
    cn_node_t *node;
    cptr_t obj;
    uint64_t uuid;
};

/* Allocate and initialize a new cn_waiting_node struct */
int cn_waiting_node_new(cn_node_t *node, cptr_t obj, uint64_t uuid,
                        struct cn_waiting_node ** o_waiting);

/* Free a cn_waiting_node struct. */
void cn_waiting_node_free(struct cn_waiting_node * waiting);

#endif
