#ifndef __OBJ_H__
#define __OBJ_H__

#include <libcap.h>
#include <glib.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>

#include <netinet/ether.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* For gettid */
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
/* end gettid */

#include "mul_common.h"
#include "util.h"

#include "glib_wrapper.h"
#include "capnet_ref.h"
#include "capnet_ooutil.h"

#include "obj_decl.h"
#include "dispatch.h"

#include "obj_internal.h"

const char * cn_objtype_name(cn_objtype_t type);

typedef void (*cn_obj_free_f)(cn_obj_t *);

struct cn_obj {
    cn_objtype_t type;
    obj_flags_t obj_flags;
    REFCNT refcnt;
    /* Object-Specific Locking */
    pthread_mutexattr_t lock_attr;
    pthread_mutex_t lock;
    unsigned int lock_count;
    /* Generic Methods */
    cn_obj_free_f free;
};


/* XXX: At this point, I should probably just change/ask david to change
 * the ref counting library. */
REFCNT __OBJ_free(cn_obj_t *obj, int i);

#define cn_obj_ref(o) ((cn_obj_t *) (o))

#ifndef __DEBUG_OBJ
#define __DEBUG_OBJ
static bool __debug_obj_locks = false;
static bool __debug_obj_holds = false;
#endif

static pid_t gettid(void) {
    return (pid_t) syscall(SYS_gettid);
}

#define cn_obj_lock(o) \
    do { \
         int res = pthread_mutex_lock(&cn_obj_ref((o))->lock); \
         if (__debug_obj_locks) { \
             printf("OBJDEBUG,lock,%6$d,%2$u,%1$p,%3$s,%4$s,%5$d\n", o, ++(cn_obj_ref((o))->lock_count), cn_objtype_name(cn_obj_ref((o))->type), __FILE__, __LINE__, gettid()); \
         } \
         if (res != 0) { \
             cn_abort("failed to lock mutex with code: %d", res); \
         } \
    } while (0)

#define cn_obj_unlock(o) \
    do { \
         if (__debug_obj_locks) { \
             printf("OBJDEBUG,unlock,%6$d,%2$u,%1$p,%3$s,%4$s,%5$d\n", o, --(cn_obj_ref((o))->lock_count), cn_objtype_name(cn_obj_ref((o))->type), __FILE__, __LINE__, gettid()); \
         } \
         int res = pthread_mutex_unlock(&cn_obj_ref((o))->lock); \
         if (res != 0) { \
             cn_abort("failed to unlock mutex with node: %d", res); \
         } \
    } while (0)

/* Acquire a reference to the object 'o' */
#define RHOLD_OBJ(o,acc) \
    do { cn_obj_lock(cn_obj_ref((o))); \
         RHOLD(cn_obj_ref((o)), (acc)); \
         if (__debug_obj_holds) { \
             printf("OBJHOLD,hold,%s,%p,%u,%p,%s,%d\n", \
                    cn_objtype_name(cn_obj_ref((o))->type), \
                    cn_obj_ref((o)), \
                    cn_obj_ref((o))->refcnt, \
                    acc, \
                    __FILE__,__LINE__); } \
         cn_obj_unlock(cn_obj_ref((o))); } while (0)

/* Release a reference to the object 'o' 
 * The logging is as follows: 
 * OBJHOLD,put,typename,obj pointer, obj->refcnt - 1, acquirer, file, line*/
#define RPUT_OBJ(o,acc,rc) \
    do { cn_obj_lock(cn_obj_ref((o))); \
        if (__debug_obj_holds) { \
             printf("OBJHOLD,put,%s,%p,%u,%p,%s,%d\n", \
                    cn_objtype_name(cn_obj_ref((o))->type), \
                    cn_obj_ref((o)), \
                    (cn_obj_ref((o))->refcnt) - 1, \
                    acc, \
                    __FILE__,__LINE__); } \
         RPUT(cn_obj_ref((o)), __OBJ, (acc), (rc)); \
         if (rc > 0) { cn_obj_unlock(cn_obj_ref((o))); } \
       } while (0);

/* Simplified version of RPUT_OBJ */
#define RPUTs_OBJ(o, acc) \
    do { REFCNT _rc; RPUT_OBJ((o), (acc), _rc); } while (0)

/* Principals represent the concept of a capability holder
 * in the system. There is a 1-1 mapping between principals and
 * cspaces. */
struct cn_principal {
    cn_obj_t obj;
    struct cptr_cache *cache;
    struct cspace *cspace;

    /* This node could be owned by some other entity/object. This field
     * allows the owner to store a back-reference to itself, so the owner can
     * be found in libcap callbacks. It is initialized to NULL. */
    void * owner;
};

/* Object representing a capability that has been sealed. */
struct cn_sealed_capability {
    cn_obj_t obj;
    cn_sealer_unsealer_t *sealer;
    cn_principal_t *sealed_principal;
    cptr_t sealed_cap;
};

/* Object that can seal and unseal capabilities. */
struct cn_sealer_unsealer {
    cn_obj_t obj;
};

/*
 * Brokers allow service wfagents to register an RP, and service clients
 * to look them up and get a cap to their rp.
 */
struct cn_broker {
    cn_obj_t obj;

    GHashTable *registry;

    /* A mapping of keys to nodes that are waiting on that key. If a matching
     * key is registerd, they will be notified of the RP that was registered. */
    GHashTable *waiting;
};

/**
 * Capnet domain.  A domain is identified by a string.  Each node 
 * is a member of one domain.  Domains come into existence as nodes come
 * and go.  They're just a membership mechanism for nodes.  Each has a
 * master node.  If nodes enter the network prior to the master, the
 * master receives capabilities to all the nodes when it arrives.  If
 * the master is replaced, the same thing happens, but all capabilities
 * delegated by the first master are revoked first.  For now, the
 * binding is domain<->node.  We don't worry about the domain<->port
 * relationship; it's not clear what it is.  And the domain<->node
 * binding allows ports to host multiple nodes, which might be a virtue.
 *
 * Ok, now there's also an ownerdomain.  We need a notion of a network
 * domain, and an owner/principal/tenant domain.  Basically, on a shared
 * OS network, multiple tenants plug into the same network and use its
 * services.Basically, the domain is the L2 network domain; for instance
 * it hosts services like DHCP.  ownerdomain is what actually owns the
 * nodes, and has a master.  This supports a shared L2 network where
 * multiple tenants can each have their own master on the network, and
 * own nodes they allocate (or are allocated with their tenant privs).
 *
 * When caps are delivered, they are delivered in terms of the
 * ownerdomain; when masters are bound, they are bound to an
 * owner/domain tuple.  But when network services are consulted (dhcp,
 * arp, whatever), domain is consulted.
 */
typedef struct cn_domain {
    /**
     * Domain id.
     */
    char *id;

    /**
     * Owner "sub"domains.  Nodes and masters are bound to ownerdomains,
     * but they may be present in the same network domain.
     */
    GHashTable *ownerdomains;

    /**
     * The DHCP server node for this domain.  Right now, we just support
     * one; so no redundant DHCP.
     */
    cn_node_t *dhcp_server;

    /**
     * The OpenStack metadata server node for this domain that serves
     * metadata on a special 169.254.169.254 address.  In OpenStack,
     * this may be either the dhcp port or the router (l3) port for a
     * subnet/network.
     *
     * XXX: obviously this shouldn't be here, but until we have both
     * fine-grained flow caps *and* IP aliasing support, it has to be
     * here.
     */
    cn_node_t *osmeta_server;

    /**
     * For now, each network domain gets its own broker.  This is sent
     * to every node in the network domain on its RP0, right after its
     * self-node capability.
     */
    cn_broker_t *broker;
} cn_domain_t;

typedef struct cn_ownerdomain {
    cn_domain_t *domain;

    /**
     * Owner id.
     */
    char *ownerid;

    /**
     * A GList of cn_node_t * that are in this domain.  Note that for
     * now, these are nodes, not ports.
     */
    GList *nodes;

    /**
     * The master node for this domain.  All node capabilities for nodes
     * in this domain are given to the master.
     */
    cn_node_t *master;
} cn_ownerdomain_t;

/**
 * Capnet switch state.
 *
 * We keep our own structs so we can maintain capability state
 * regardless of the connection to the switch(es) flapping up and down.
 * When switches and ports come and go, we match them with our
 * structures.
 *
 * Well, that was the early idea, but that is initially too
 * time-consuming and it's a questionable feature anyway.  So for now,
 * if the switch/port gets "deleted", we will have to rebuild the
 * capability network from scratch.  That's ok from my point of view for
 * now...
 */
struct cn_switch {
    cn_obj_t obj;
    //c_rw_lock_t lock;
    //c_atomic_t  ref;
    //uint64_t    swid;

    /**
     * For now, we allow a max of one uplink port per switch, to ensure
     * we don't loop.
     */
    cn_port_t *uplink;
    uint32_t uplink_port_no;
    
    /**
     * The metadata service ops for this switch, and its private state.
     */
    struct metadata_ops *mops;
    void *metadata_priv;

    /**
     * A map of mac addrs to cn_node_t objects.  The keys are the
     * node->mac array pointers; so don't delete a node before removing
     * it from this table!
     */
    GHashTable *mac_to_node;

    /**
     * A map of ipv4 addrs to cn_node_t objects.  The keys are the
     * node->ipv4.addr.s_addr (a uint32_t), if node->ipv4.valid == 1.
     * Don't delete a node before removing it from this table!
     */
    GHashTable *ipv4_to_node;

    /**
     * A backref to our MUL switch.
     */
    mul_switch_t *mul_switch;
};

/**
 * Ports also have roles.  For now, either a port is a node port, or an
 * uplink port.
 */
typedef enum cn_port_role {
    CN_PORT_ROLE_UNKNOWN = 0,
    /**
     * This is the role that node ports get.
     */
    CN_PORT_ROLE_NODE = 1,
    /**
     * This is the role that switch uplink nodes get.  Any non-local
     * flows will get routed out one of these nodes from the switch they
     * originate on.
     */
    CN_PORT_ROLE_UPLINK = 2,
} cn_port_role_t;

/**
 * Capnet port state.
 */
struct cn_port {
    cn_obj_t obj;

    /**
     * The role this port plays.
     */
    cn_port_role_t role;

    /**
     * An identifier for this node, used by higher-level entities.
     */
    char *id;

    /**
     * A GList of struct cn_node.  Each port can have multiple nodes.
     */
    GList *nodes;

    /* XXX: Everything below can be NULL if nobody owns this port yet. */
    
    /**
     * The cn_switch that owns us.
     */
    cn_switch_t *csw;

    /**
     * The mul port that owns us.
     */
    mul_port_t *mul_port;

    /**
     * A backref to the switch that owns us.
     */
    mul_switch_t *mul_switch;
};

/**
 * Nodes have roles, and these roles determine what flows are written to
 * switches for forwarding to these ports.
 */
typedef enum cn_node_role {
    /**
     * If we don't have any metadata about a particular node, we will
     * never allow traffic to/from it -- it gets an "unknown" role.
     */
    CN_NODE_ROLE_UNKNOWN = 0,
    /**
     * Regular nodes whose communication is governed by capabilities get
     * this role.
     */
    CN_NODE_ROLE_NODE = 1,
    /**
     * If the master workflow app is not attached to the controller over
     * an out-of-band control net, this is the role that the node that
     * hosts master workflow app gets.
     */
    CN_NODE_ROLE_MASTER = 2,
    /**
     * If there are non-master workflow apps that are not attached
     * directly to the controller over an out-of-band control net, this
     * is the role that the node that hosts said workflow app gets.
     */
    /* XXX: No need for a difference between non-master workflow node roles
     * and normal node roles */
    //CN_NODE_ROLE_WORKFLOW = 3,
} cn_node_role_t;

typedef enum cn_node_service {
    CN_NODE_SERVICE_NONE = 1 << 0,
    CN_NODE_SERVICE_DHCP = 1 << 1,
    CN_NODE_SERVICE_OSMETA = 1 << 2,
} cn_node_service_t;

struct cn_notify_wait {
    pthread_cond_t cond;
    pthread_mutex_t cond_mutex;
};

void cn_notify_wait_init(struct cn_notify_wait *w);
void cn_notify_wait_destroy(struct cn_notify_wait *w);

/* Nodes correspond to physical nodes in the network, but are also
 * principals in their own right. */
struct cn_node {
    cn_obj_t obj;
    cn_principal_t *principal;

    /* Our RP0, set at initialization. This capability is owned by the node's
     * secret c-space */
    cptr_t rp0;

    /* the secret cspace for local caps like the grant object */
    cn_principal_t * secret;

    /* Our current grant. */
    bool has_grant;
    cptr_t grant;

    /**
     * The role this node plays.
     */
    cn_node_role_t role;

    /**
     * The special services this node provides.
     */
    cn_node_service_t services;

    /**
     * The additional, special services this port provides.
     */

    /**
     * A name for the node, useful for debugging.
     */
    char *name;
    /**
     * An identifier for this node, used by higher-level entities.
     */
    char *id;

    /**
     * This node's netdomain.
     */
    char *domainid;

    /**
     * This node's ownerdomain.
     */
    char *ownerid;
    
    /**
     * Some basic address stuff.
     */
    uint8_t mac[OFP_ETH_ALEN];

    struct {
        struct in_addr addr;
        struct in_addr mask;
        int valid:1;
    } ipv4;

    struct {
        struct in6_addr addr;
        int valid:1;
    } ipv6;

    /* Mapping of flow -> # of capabilites to flow. If a flow is granted to
     * this node and does not exist in the flow table, it's installed on the
     * node's first-hop switch. Every delete the # of capabilites is decremented
     * and if it reaches 0, the flows are removed. */
    GHashTable *flow_table;

    /* Mapping of nodes -> # of capabilities to flows that have that nodes as
     * a destination. */
    GHashTable *node_table;

    /**
     * The request cache for this particular node.
     */
    cn_dispatch_result_cache_t cache;

    /**
     * Mapping of message UUIDs -> struct cn_notify_wait structures.
     */
    GHashTable *notify_wait;

    /**
     * A backref to the cn_port that owns us. May be NULL if no port owns this
     * node yet.
     */
    cn_port_t *cn_port;
};

/**
 * This is a capability-enabled flow, bound to the cptr that gives some
 * node that holds the cptr permission to send packets from itself
 * according to the flow.
 *
 * (The reason the @head member is there is to optimize its removal from
 * the owning cn_node_t object.)
 */
struct cn_flow {
    cn_obj_t obj;
    /**
     * The flow
     */
    struct flow flow;
    struct flow mask;

    /* The node this flow is "towards". Owners of the capability will
     * be given the right to send packets matching the above flow to this
     * node. */
    cn_node_t *node;
    GListHead *head;

};

/* Rendezvous points allow capabilities to be sent between principals. */
struct cn_rp {
    cn_obj_t obj;
    GQueue *queue;

    /* A list of nodes waiting on this RP */
    GList *waiting;
};

struct cn_membrane {
    cn_obj_t obj;
    cn_rp_t *rp;

    /* A list of nodes waiting on this membrane */
    GList *waiting;

    /* A cspace to hold the capabilities at the top of the wrapped hierarchy.
     * This is to ensure that a node can't prevent us from removing a membrane
     * by minting a capability and then deleting the one they received from
     * the membrane. */
    cn_principal_t * secret;

    /* A list of cptrs to wrapped objects that have been created by this membrane */
    GList * wrapped;
}; 

enum cn_membrane_type {
    CN_MEMBRANE_TYPE_INTERNAL,
    CN_MEMBRANE_TYPE_EXTERNAL
};

typedef uint64_t method_mask_t;
static const method_mask_t METHOD_MASK_ALL = UINT64_MAX;
static const method_mask_t METHOD_MASK_NONE = 0x0;


typedef struct {
    const cn_obj_t * declassifier;
    method_mask_t method_mask;
    /* Arbitrairy data field for the party adding the annotation to store
     * information in */
    struct {
        void * val;
        /* If not NULL, it is invoked on 'val' when the annotation is copied. */
        void (*copy_f)(void *);
        /* If not NULL, it is invoked on 'val' when the annotation is freed */
        void (*free_f)(void *);
    } data;
} cn_annotation_t;

/* All locking and refcounting of this object is done via the libcap library
 * itself. Be careful you understand the semantic guarantees of those locks
 * when working with instances of these objects. */
typedef struct {
    /* The set of annotations currently applied to this object. The keys are
     * the declassifiers of the annotations. The values are the actual annotations.
     * As a side-effect of this, only one annotation can be added for each
     * declassifier. If changes are needed, the annotation must be removed
     * and then re-added. */
    GHashTable * annotations;

    /* Cache that is the logical AND of all of the method masks in
     * the annotations. It is re-calculated when the annotations change. */
    method_mask_t method_mask_cache;
} cn_cnode_meta_t;

struct cn_node_grant {
    cn_obj_t obj;

    cn_node_t * node;
};

/* -------------- Setup ----------------- */

/* Initialize the object/capability system */
int cn_init(void);
/* Teardown the object/capability system */
void cn_fini(void);

/* ------------ Principals ------------- */

/* Allocate and initialize a new principal. A pointer to the principal is
 * stored in 'out'. A status code of zero is returned on success. */
int cn_principal_new(cn_principal_t **out);

/* Clear the principal's cspace. This is equivalent to calling cn_delete on
 * every object this principal holds a capability to. A status code of zero
 * is returned on success. */
int cn_principal_clear(cn_principal_t *p);

/* Clear principal and free the memory associated with it. A status code
 * of zero is returned on success. */
void cn_principal_free(cn_principal_t *p);

void * cn_principal_owner(cn_principal_t *p);

/* ---------- Basic Capability Ops ----------- */

/* Insert the given object into the given principal's cspace. The cptr that
 * points to this object is returned via 'o_cptr'. */
int cn_principal_insert(cn_principal_t *p, cn_obj_t *o, void * payload, 
                        cptr_t *o_cptr);

/* Grant the cptr csrc from principal src's cspace into principal dest's
 * cspace. The resulting cptr is stored in 'out'. 
 * Returns zero on success. */
int cn_grant(cn_principal_t *src, cptr_t csrc, 
             cn_principal_t *dest, void * payload, 
             cptr_t *out);

/* Same as cn_grant, but returns the type of the granted object in the
 * 'type' parameter. */
int cn_grant_extended(cn_principal_t *src, cptr_t csrc, 
                      cn_principal_t *dest, void * payload, 
                      cptr_t *out, cn_objtype_t *type);

int cn_grant_cnode_extended(struct cnode * ca,
                            cn_principal_t *dest, void * payload,
                            cptr_t *out, cn_objtype_t *type);

/* Create a new capability in principal 'p' that points to the same object
 * as the cptr 'cptr'. The type of the minted cptr is stored in 'type'.
 * Returns zero on success */
int cn_mint(cn_principal_t *p, cptr_t cptr, void * payload, 
            cptr_t *out, cn_objtype_t *type);

/* Add 'dst_cptr' to 'src_cptr's CDT. Returns 0 on success. See cap_derive */
int cn_derive(cn_principal_t *src_p, cptr_t src_cptr,
              cn_principal_t *dst_p, cptr_t dst_cptr,
              void * payload);

/* see also: cap_derive_cnode(struct cnode *ca, struct cnode *cb, void * payload);
 *
 * Note: Use cap_derive_cnode if you want/need to supply cnodes as parameters.
 *       There is no convenience function for cap_derive_cnode becase it doesn't
 *       rely on any of the CapNetModel */

/* Delete the given cptr. */
void cn_delete(cn_principal_t *p, cptr_t cptr, void * payload);

/* Revoke the given cptr (delete all of it's children). */
int cn_revoke(cn_principal_t *p, cptr_t cptr, void * payload);

int cn_revoke_till(cn_principal_t *p, cptr_t cptr, 
                   bool (*pred)(struct cnode *, void *payload),
                   void * payload, void * pred_payload);

/* ---------- Object Creation ----------- */

int cn_switch_new(cn_switch_t **o_csw);
void cn_switch_free(cn_switch_t *csw);

int cn_port_new(cn_port_t **o_port);
void cn_port_free(cn_port_t *port);

void cn_port_add_node(cn_port_t *port, cn_node_t *node);

/* Make a new flow from the 'to' node with the given flow. contents of the passed
 * flow object are copied out of the flow into a local copy, so no refernce
 * is held. */
int cn_flow_new(cn_node_t *to, struct flow flow, struct flow mask, 
                cn_flow_t **o_flow);
void cn_flow_free(cn_flow_t *flow);

/* Allocate and initialize a new node object. A status code of zero is returned 
 * on success. */
int cn_node_new(cn_node_t **node);

/* Free the given node */
void cn_node_free(cn_node_t *n);

/* These functions are used to properly (sequentially speaking) acquire the locks
 * of the given object's parent objects safely. On output, all objects given as
 * parameters (including input and output objects) are locked, the must be released
 * by the caller. They may return -1 if any object's reference to a parent object
 * is changed to a different parent during lock aquisition. */
int cn_unordered_acquire_port_switch_lock(cn_port_t *p, cn_switch_t **o_sw);
int cn_unordered_acquire_node_switch_lock(cn_node_t *n, cn_port_t **o_port, cn_switch_t **o_sw);
int cn_unordered_acquire_flow_switch_lock(cn_flow_t *flow, cn_node_t **o_node, 
                                          cn_port_t **o_port, cn_switch_t **o_sw);

/* Allocate and initialize a new rendezvous point object.
 * A status code of zero is returned on success. */
int cn_rp_new(cn_rp_t **rp);

/* Free the RP. */
void cn_rp_free(cn_rp_t *rp);

/* Allocate and initialize a new broker object.
 * A status code of zero is returned on success. */
int cn_broker_new(cn_broker_t **broker);

/* Free the broker. */
void cn_broker_free(cn_broker_t *broker);

/* Allocate and intialize a new membrane object. Returns cptr_t in 'out' for
 * the create membrane object */
int cn_membrane_new(cn_membrane_t **membrane);

/* Free the membrane */
void cn_membrane_free(cn_membrane_t *m);

/* Create and initialize a new grant object for the given node.
 * Note: This *does not* perform the reset for the node. */
int cn_node_grant_new(cn_node_t *node, cn_node_grant_t ** grant);

/* Free the node grant. */
void cn_node_grant_free(cn_node_grant_t * grant);

/* Create a new sealer unsealer */
int cn_sealer_unsealer_new(cn_sealer_unsealer_t **su);

/* Free a previously created sealer/unsealer. */
void cn_sealer_unsealer_free(cn_sealer_unsealer_t *su);

/* --------- cnode metadata interface ---------- */

/* Create a new meta object */
int cn_cnode_meta_new(cn_cnode_meta_t **m);

void cn_cnode_meta_free(cn_cnode_meta_t *m);

/* Create a new meta object that is the clone of an existing meta object */
int cn_cnode_meta_copy(cn_cnode_meta_t *m, cn_cnode_meta_t **copy);

/* Returns true if the given meta object is wrapped */
bool cn_cnode_meta_is_annotated(cn_cnode_meta_t *m);

/* Returns true if the given cnode meta object has an annotation from the given
 * object */
bool cn_cnode_meta_is_annotated_by(cn_cnode_meta_t *m, const cn_obj_t *obj);

/* Returns true if the given cnode is annotated */
bool cn_cnode_is_annotated(struct cnode *c);

/* Returns true if the given cnode is annotated by the given object. */
bool cn_cnode_is_annotated_by(struct cnode *c, const cn_obj_t *obj);

/* Add the annotation the set of annotations on the metadata. */
int cn_cnode_meta_add_annotation(cn_cnode_meta_t *m,  cn_annotation_t * a);

/* Get the annotation that can be declassified using the given object. Returns
 * 0 on success, ENOTEXISTS on failure. */
int cn_cnode_meta_get_annotation_by(cn_cnode_meta_t *m, cn_obj_t *obj, 
                                    cn_annotation_t **a_out);

/* Remove the annotation with a declassifier matching 'obj' from the set of 
 * annotations for the metadata. 
 * Frees the memory associated with that annotation. */
int cn_cnode_meta_remove_annotation_by(cn_cnode_meta_t *m, const cn_obj_t *obj);

/* Return an s-expression representation of the given cnode meta. The string
 * returned must be free()d. */
char * cn_cnode_meta_string(cn_cnode_meta_t *m);

/* --------- annotation interface ---------- */

/* Annotations */

/* Create a new annotation */
int cn_annotation_new(cn_annotation_t **a);

/* Copy the annotation */
int cn_annotation_copy(cn_annotation_t *a, cn_annotation_t **a_copy);

/* Free an annotation created with cn_annotation_new, or cn_annotation_copy */
void cn_annotation_free(cn_annotation_t *a);

/* Return an s-expression representation of the given annotation. The string
 * returned must be free()d. */
char * cn_cnode_meta_string(cn_cnode_meta_t *m);
char * cn_annotation_string(cn_annotation_t *a);

/* method_masks */

/* Allow all operations */
void cn_method_mask_permission_all(method_mask_t * mask);

/* Prevent all operations */
void cn_method_mask_permission_none(method_mask_t * mask);

/* Set this given permission to allow */
void cn_method_mask_permission_set(method_mask_t * mask, cn_method_t m);

/* Set the given permission to deny */
void cn_method_mask_permission_clear(method_mask_t * mask, cn_method_t m);

/* Returns true if the given permission is set, returns false otherwise */
bool cn_method_mask_permission_is_set(method_mask_t * mask, cn_method_t m);

/* --------- libcap conversions ---------- */

STATIC_ASSERT(sizeof(cptr_t) == sizeof(gpointer), 
              size_of_cptr_and_gptr_must_be_same);

#define cptr2gptr(cptr_) ((gpointer) (cptr_.cptr))
#define cptr2gptr2(cptr_) ((gpointer *) &(cptr_.cptr))

/* need to do an explicity copy since gpointers are not compatible with
 * cptr_t structs, and therefore a naieve dereference violates strict-aliasing. */
static inline cptr_t gptr2cptr(gpointer gptr) {
    cptr_t res;
    memcpy(&res, &gptr, sizeof(res));
    return res;
}

STATIC_ASSERT(sizeof(cptr_t) == sizeof(unsigned long), 
              size_of_cptr_and_unsigned_long_must_be_same);

#define cptr2ul(cptr_) (cptr_.cptr)
#define ul2cptr(cptr_) (*((cptr_t *) &(cptr_)))

/* --------- cnode extensions --------- */

/* Get the cnode to the obj pointed to by the cptr 'cptr' in the cspace of
 * the given principal. This acquires a lock to the cnode. Returns -1 when
 * there is no object pointed to by the given cptr. Always follow a call to 
 * cn_principal_get with a call to cap_cnode_put on the returned cnode. */
int cn_principal_get(cn_principal_t *p, cptr_t cap, struct cnode **o_cnode);

/* Return the objtype of the given __cnode__. This should should always match
 * the type of the object stored within, but the object type and the
 * cnode type are distinct values. */
cn_objtype_t cn_cnode_type(struct cnode * c);

/* Get the object pointed to by the given cnode. A pointer to the object
 * this cnode references is returned. */
cn_obj_t * cn_cnode_object(struct cnode *c);

/* Return a pointer to the principal that owns the capability this particular
 * cnode corresponds to. */
cn_principal_t * cn_cnode_principal(struct cnode * c);

/* Returns 'true' if the given pricipal 'p' and the cptr 'c' reference the
 * given cnode 'cnode'. This function can be used to check if a new reference to
 * a cnode needs to be obtained, or if the existing reference is sufficient. */
bool cn_is_referenced_cnode(cn_principal_t * p, cptr_t c, struct cnode * cnode);

/* Returns an s-expression representing this cnode. It must be free()d. */
char * cn_cnode_string(struct cnode * c);

/* Returns an s-expression representing the capabilitily `c` in provider
 * `p`. The returned string must be free()d */
char * cn_cptr_string(cn_principal_t *p, cptr_t c);

/* ---------- Object-Specific Callbacks ----------- */

/* A set of additional per-caller callbacks that can be passed to the callback
 * via the payload mechanism */
struct cn_cb_payload {
    union {
        int(*unop)(struct cnode * c, void * payload);
        int(*binop)(struct cnode * ca, struct cnode * cb, void * payload);
    };
    void * payload;
};

int cn_default_insert_cb(struct cnode *c, void * payload);

/* The default grant callback. This callback makes sure that the cnode type
 * matches the object type, and retains a reference to the object. */
int cn_default_grant_cb(struct cnode *ca, struct cnode *cb, void * payload);

/* The default delete callback. This callback makes sure that the cnode type
 * matches the object type, and /releases/ an object reference. */
int cn_default_delete_cb(struct cnode *cnode, void * payload);

/* The default derive callback */
int cn_default_derive_dst_cb(struct cnode *ca, struct cnode *cb, void * payload);

/* Invoked as the grant callback for a node by libcap. Set statically as the
 * callback in obj.c. */
//int cn_grant_node_cb(struct cnode *node_a, struct cnode *node_b);
//int cn_insert_node_cb(struct cnode *node);

/* the node handling callbacks for flows */
int cn_grant_flow_cb(struct cnode *flow_a, struct cnode *flow_b, void * payload);
int cn_insert_flow_cb(struct cnode *flow_cnode, void * payload);
int cn_delete_flow_cb(struct cnode *flow_cnode, void * payload);

/* --------- Node Manipulation --------- */

/* Reset the node's c-space and set it's RP0 to the rendezvous point pointed to
 * by the given cptr owned by the given principal. Additionally, it revokes the
 * current grant object for this node (if it exists) and creates a new one. A
 * capability to the grant object for the given principal (`p`) 
 * is stored in `o_grant`. Returns 0 on success, and a negative value on error. */
int cn_node_reset(struct cnode * node, cptr_t *o_grant);

/* --------- Node Grant Manipulation --------- */

/* Perform a grant operation granting the capability `cap` owned by `p` to
 * the node whose grant object is supplied as the first argument. Returns 0
 * on success. */
int cn_node_grant_grant(struct cnode * grant_cnode, 
                        cn_principal_t *p, cptr_t cap, 
                        cptr_t *o_cap, cn_objtype_t * o_type);

/* Fetch the RP0 of the node this grant is for. A capability to the node's
 * RP0 is stored in `o_rp_cap`. Returns 0 on success. */
int cn_node_grant_rp0(struct cnode * grant_cnode, cptr_t *o_rp_cap);

/* --------- RP Manipulation --------- */

/* Creates a "normal" rp_elem struct that consistes of a principal and a 
 * cptr. When this elem is recevied, it will trigger a "grant" operation
 * from the "owner" of the given "cap" to the receiver. Returns 0 on success. */
int cn_rp_elem_normal(cn_principal_t *owner, cptr_t cap,
                      struct cn_rp_elem_message * msg,
                      struct cn_rp_elem **o_elem);

/* Creates an "injected" rp_elem struct that consists of a single cn_obj_t object.
 * Instead of being granted, these objects are inserted into the receiver when
 * a 'recv' is performed on the rp and one of these is the next element in the
 * queue. This allows objects from outside the capability system to enter. 
 * Returns 0 on success. */
int cn_rp_elem_injected(cn_obj_t *obj, 
                        struct cn_rp_elem_message * msg,
                        struct cn_rp_elem **o_elem);

/* Creates a "membrane" element for a rendezvous point. */
int cn_rp_elem_membrane(cn_principal_t *p, cptr_t cap,
                        cn_cnode_meta_t *meta,
                        struct cn_rp_elem_message *msg,
                        struct cn_rp_elem ** o_elem);

/* Create a new rp_elem that just contains a message and no cptr itself. This
 * allows Rendezvous points to be used as a general messaging system */
int cn_rp_elem_message(struct cn_rp_elem_message *msg,
                       struct cn_rp_elem **o_elem);

/* Free a given rp_elem. */
void cn_rp_elem_free(struct cn_rp_elem *elem);

/* Push the (principal, cptr) pair onto the back of the rp's queue. The given
 * principal should be the the owner of the given cptr. */
int cn_rp_push(cn_rp_t *rp, struct cn_rp_elem *elem);

/* Same as cn_rp_push, but push the (principal, cptr) pair onto the front of
 * the queue. Useful for error recovery. */
int cn_rp_pushfront(cn_rp_t *rp, struct cn_rp_elem *elem);

/* Pop the next (principal, cptr) pair off the rp's queue */
int cn_rp_pop(cn_rp_t *rp, struct cn_rp_elem *elem);

/* Delete every entry enqueued in the given rp */
void cn_rp_clear(cn_rp_t *rp);

/* Send the given cptr on the rp pointed to by the given cnode. Assumes that
 * the cptr has the same owner as the cnode. */
int cn_rp_send(struct cnode *rp, cptr_t send_cap, struct cn_rp_elem_message *msg);

/* Same as cn_rp_send but with an rp_elem directly. cn_rp_send uses this
 * method internally, but it is used externally to inject objects. Returns
 * 0 on success. */
int cn_rp_send_rp_elem(struct cnode *rp, struct cn_rp_elem * elem);

/* Receive the next capability off of the rp contained in the given cnode.
 * The resulting capability is stored in `recv_cap`. Returns EEMPTY when
 * there are no capabilities to recv. Always returns a negative value when
 * an exception occurs. 
 *
 * The result may be either a message, a received capability, or a combination
 * of the two. What is being returned will be signaled via the 'flags' field. Note:
 * the value of the supplied 'flags' field is undefined if cn_rp_recv returns
 * a non-zero exit code. 
 *   - If CN_FLAG_SET(*o_flags, CN_RP_RECV_RESULT_TYPE_CPTR) is true, then
 *     `o_cap` and `o_type` will be set appropriately according to the capability
 *     and type that were received from the rp.
 *   - if CN_FLAG_SET(*o_flags, CN_RP_RECV_RESULT_TYPE_MESSAGE) is true, then
 *     `o_message` will be set according to the message that was part of the last
 *     element popped off the rp's queue. o_message->data should be freed once you're
 *     done using it.
 *
 * At least one of those flags should be set if a zero exit code is returned.
 * (but both may be set).
 * See also: `dispatch.c:cn_dispatch_util_fill_rp_recv_result`
 */
int cn_rp_recv(struct cnode *rp, 
               enum cn_rp_recv_result_flag *o_flags,
               cptr_t *o_cap, cn_objtype_t *o_type,
               struct cn_rp_elem_message * o_message);

/* Register to asyncronusly receive the next capability off of this RP as
 * soon as it is added via cn_rp_send. If multiple principals call this function
 * they'll be queued, and will receive capabilities off of the RP in the order
 * they called recv_wait. */
int cn_rp_recv_wait(struct cnode *rp, uint64_t uuid);

/* --------- Broker Manipulation --------- */

/*
 * Register a cap to an RP, @rp_cptr, with the service named
 * @service_name.  This capability can be passed to clients who call the
 * lookup function on the broker with the same @service_name string.
 */
int cn_broker_register(struct cnode *broker, char *service_name, cptr_t rp_cptr);

/*
 * If a service named @service_name has been registered at this broker,
 * return a cap to the RP associated with it in @rp_cptr.
 */
int cn_broker_lookup(struct cnode *broker, char *service_name, cptr_t *rp_cptr);

/* Register to be notified when an rp is registerd for the service with the given
 * service name. 'uuid' will be used as the uuid of the notification. */
int cn_broker_lookup_wait(struct cnode *broker, char *service_name, uint64_t uuid);

/* --------- Membrane Manipulation --------- */

/* Like cn_annotation_new, but create's a new membrane wrapping annotation
 * for the given membrane side. Returns 0 on success. The resultant annotation
 * is stored in `a_out`. */
int cn_membrane_annotation_new(cn_membrane_t * membrane,
                               enum cn_membrane_type side,
                               cn_annotation_t **a_out);

/* Return the type of membrane annotation. A must be an annotation created
 * using `cn_membrane_annotation_new`. */
enum cn_membrane_type cn_membrane_annotation_type(cn_annotation_t *a);

/* Return the opposite membrane type of the given membrane type
 * CN_MEMBRANE_TYPE_INTERNAL yields CN_MEMBRANE_TYPE_EXTERNAL and vice
 * versa. */
enum cn_membrane_type cn_membrane_type_opposite(enum cn_membrane_type type);

/* This function is the heart of a membrane, it is responsible for performing
 * the actual wrapping or unwrapping action between a pair of membranes when
 * a capability is recieved on one end of that membrane. 
 *
 * The `send_meta` is the cn_cnode_meta_t of capability used to send the capability
 * and then `recv_meta` is the cn_cnode_meta_t of the capability used to recv
 * the capability. Instead of a pointer to a valid cn_cnode_meta_t, NULL can
 * be supplied for ONE of these paramters, in which case a new cn_cnode_meta_t
 * object is assumed.
 *
 * This function grants the capability `granted` owned by principal `granter` 
 * to the principal `receiver` and wraps or unwraps it as implied by the
 * send and recv meta objects.
 *
 * The resulting capability is stored in `o_cap`, and the type of the granted
 * object is stored in `type`.
 *
 * Returns 0 on success.
 * */
int cn_membrane_grant(cn_cnode_meta_t *send_meta,
                      cn_cnode_meta_t *recv_meta,
                      cn_principal_t *granter, cptr_t granted,
                      cn_principal_t *receiver, 
                      cptr_t *o_cap, cn_objtype_t *type);

/* Send a capability using the given membrane. Creates a shadow object
 * for the object referenced by the capability 'cap', and then stores a capability
 * for that object in the principal of the given cnode. The cptr is returned in
 * 'shadow_cap'. */
int cn_membrane_send(struct cnode *m, cptr_t send_cap);

/* Receives a capability from this membrane. See cn_rp_recv. This cptr should
 * always point to a shadow object. */
int cn_membrane_recv(struct cnode *m, cptr_t *o_cap, cn_objtype_t *o_type);

/* Destroys all shadow references to objects that have been sent through
 * this membrane. This does not actually delete the membrane, it can still be used
 * afterwards, it is just reset to a clean state.
 * XXX: May or may not be safe... */
int cn_membrane_clear(struct cnode *m);

/* Get the "external" membrane for the membrane reference by the given cnode.
 * A capability to the external membrane is stored in 'o_cap'. Returns 0 on
 * success. */
int cn_membrane_external(struct cnode *membrane, cptr_t *o_cap);

/* Same as cn_rp_recv_wait, but for membranes. */
int cn_membrane_recv_wait(struct cnode *membrane, uint64_t uuid);

/* --------- Sealed Capability Manipulation --------- */

/* --------- Sealer-Unsealer Manipulation --------- */

int cn_sealer_unsealer_seal(struct cnode *, cptr_t cap, cptr_t *sealed, cn_objtype_t *type);
int cn_sealer_unsealer_unseal(struct cnode *, cptr_t cap, cptr_t *unsealed, 
                              cn_objtype_t *type);
int cn_sealer_unsealer_restrict_to_seal(struct cnode *, cptr_t *seal_only);
int cn_sealer_unsealer_restrict_to_unseal(struct cnode *, cptr_t *unseal_only);

/* --------- Generic Helper function --------- */

/* A generic helper function to deliver a capability to a node. */
int cnc_deliver_obj(cn_node_t * node, cn_obj_t *obj);

#endif /* __OBJ_H__ */
