#include "obj_waiting.h"
#include "obj.h"

int cn_waiting_node_new(cn_node_t *node, cptr_t obj, uint64_t uuid,
                        struct cn_waiting_node ** o_waiting) {
    struct cn_waiting_node * wait = malloc(sizeof(*wait));
    if (wait == NULL) {
        return -1;
    }

    RHOLD_OBJ(node, OBJ_OWNER);
    wait->node = node;
    wait->obj = obj;
    wait->uuid = uuid;

    *o_waiting = wait;
    return 0;
}

void cn_waiting_node_free(struct cn_waiting_node * waiting) {
    RPUTs_OBJ(waiting->node, OBJ_OWNER);
    free(waiting);
}
