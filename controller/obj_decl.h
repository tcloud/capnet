#ifndef __OBJ_DECL_H__
#define __OBJ_DECL_H__

typedef enum {
    CN_SWITCH,
    CN_PORT,
    CN_PRINCIPAL,
    CN_NODE,
    CN_FLOW,
    CN_RP,
    CN_MEMBRANE,
    CN_BROKER,
    CN_NODE_GRANT,
    CN_SEALER_UNSEALER,
    __CN_MAX_TYPE
} cn_objtype_t;

/* Pre-Declarations */
typedef struct cn_obj cn_obj_t;
typedef struct cn_switch cn_switch_t;
typedef struct cn_port cn_port_t;
typedef struct cn_principal cn_principal_t;
typedef struct cn_node cn_node_t;
typedef struct cn_flow cn_flow_t;
typedef struct cn_rp cn_rp_t;
typedef struct cn_membrane cn_membrane_t;
typedef struct cn_broker cn_broker_t;
typedef struct cn_node_grant cn_node_grant_t;
typedef struct cn_sealer_unsealer cn_sealer_unsealer_t;

/* RP elem stuff */

struct cn_rp_elem;

struct cn_rp_elem_message {
    uint8_t * data;
    size_t len;
};

enum cn_rp_recv_result_flag {
    CN_RP_RECV_RESULT_TYPE_NONE = 0x0,
    CN_RP_RECV_RESULT_TYPE_CPTR = 0x1,
    CN_RP_RECV_RESULT_TYPE_MESSAGE = 0x2,
    // 0x3 (the low two bits reserved)
};

/* Utilities */

char * cn_cnode_string(struct cnode * c);
char * cn_cptr_string(cn_principal_t *p, cptr_t c);

#endif
