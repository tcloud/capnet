#ifndef CAPNET_UTIL_H
#define CAPNET_UTIL_H

#include "capnet_log.h"

// Taken From: http://stackoverflow.com/a/3385694/188792
#define STATIC_ASSERT(COND,MSG) typedef char static_assertion_##MSG[(!!(COND))*2-1]

/* Abort the program with the given error message */
#define cn_abort(format,...) do { cncerr(format, ## __VA_ARGS__); \
                                  abort(); } while (0)

unsigned int __mac_hash(const void *p);

int __mac_equal(const void *p1,const void *p2);

/* Check if the given flag is unset */
#define CN_FLAG_UNSET(flags, flag) (!((flags) & (flag)))

/* Check if the given flag is set */
#define CN_FLAG_SET(flags, flag) (!(CN_FLAG_UNSET((flags), (flag))))

#define CN_SET_BIT(field, index) \
    do { (field) |= 0x1 << (index); } while (0)

#define CN_UNSET_BIT(field, index) \
    do { (field) &= ~(0x1 << (index)); } while (0)

#define CN_BIT_IS_SET(field, index) \
    (!!((field) & (0x1 << (index))))

#endif
