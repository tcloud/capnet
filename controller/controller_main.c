
#include "capnet_config.h"
#include "capnet_log.h"
#include "mul_common.h"
#include "mul_vty.h"
#include "controller.h"

/* Global Opts */
struct cnc_opts cnc_opts = {
    .switch_metadata_dir = "/var/tmp",
    /* XXX: id of the port that should be the master. This should come from the
     * metadata service, but for now, just hardcode it. */
    //.MASTER_node_id = "port-1",
    .node_flow_caps = NULL,
};

/*
 * Callback setup.
 */
struct mul_app_client_cb cnc_app_cbs = {
    .switch_priv_alloc = cnc_switch_priv_alloc,
    .switch_priv_free = cnc_switch_priv_free,
    .switch_add_cb = cnc_switch_add,
    .switch_del_cb = cnc_switch_del,
    .switch_priv_port_alloc = cnc_priv_port_alloc,
    .switch_priv_port_free = cnc_priv_port_free,
    .switch_port_add_cb = cnc_port_add,
    .switch_port_del_cb = cnc_port_del,
    .switch_port_link_chg = NULL,
    .switch_port_adm_chg = NULL,
    .switch_packet_in = cnc_packet_in,
    .core_conn_closed = cnc_core_closed,
    .core_conn_reconn = cnc_core_reconn 
};

/**
 * A few generic MUL callbacks that don't involve our core controller
 * code, for now.
 */
void cnc_core_closed(void) {
    c_log_info("%s: ",FN);
    return;
}

void cnc_core_reconn(void) {
    c_log_info("%s: ",FN);
    mul_register_app_cb(NULL,CNC_APP_NAME,C_APP_ALL_SW,C_APP_ALL_EVENTS,
                        0,NULL,&cnc_app_cbs);
}

/**
 ** Module core stuff.
 **/
void cnc_module_init(void *base_arg) {
    struct event_base *base = base_arg;

    c_log_debug("%s: switch metadata dir is %s",FN,cnc_opts.switch_metadata_dir);

    cnc_init();

    mul_register_app_cb(NULL,CNC_APP_NAME,C_APP_ALL_SW,C_APP_ALL_EVENTS,
                        0,NULL,&cnc_app_cbs);

    cn_init();

    return;
}

/**
 ** getopt options support.
 **/
int cnc_getopt_helper(void *priv,int opt,char *optarg) {
    struct cnc_opts *opts = (struct cnc_opts *)priv;
    char *tok,*tok2;
    char *saveptr,*saveptr2;
    char *sp,*sp2,*cp;
    GList *list;

    c_log_debug("%s: opt is %c, optarg is %s",FN,(char)opt,optarg);

    switch (opt) {
        case 'S':
            opts->switch_metadata_dir = strdup(optarg);
            break;
        case 'W':
            cnc_set_warn_level(atoi(optarg));
            break;
        case 'Z':
            cnc_set_log_level(atoi(optarg));
            break;
        case 'L':
            if (cnc_add_log_area_flaglist(optarg,NULL)) {
                c_log_err("%s: bad log level flag in '%s'!\n",FN,optarg);
                return -1;
            }
            break;
        case 'F':
            if (!opts->node_flow_caps)
                opts->node_flow_caps =
                g_hash_table_new_full(g_str_hash,g_str_equal,NULL,NULL);
            sp = optarg;
            saveptr = NULL;
            while ((tok = strtok_r(sp,";",&saveptr)) != NULL) {
                sp = NULL;
                cp = index(tok,':');
                if (!cp) {
                    c_log_err("%s: bad node flow cap item '%s'!",FN,tok);
                    return -1;
                }
                *cp = '\0';
                ++cp;
                if (*cp == '\0') {
                    c_log_err("%s: bad node flow cap item at '%s'!",FN,cp - 1);
                    return -1;
                }
                sp2 = cp;
                saveptr2 = NULL;
                while ((tok2 = strtok_r(sp2,",",&saveptr2)) != NULL) {
                    sp2 = NULL;
                    list = (GList *)g_hash_table_lookup(opts->node_flow_caps,tok);
                    list = g_list_append(list,tok2);
                    g_hash_table_steal(opts->node_flow_caps,list);
                    g_hash_table_insert(opts->node_flow_caps,tok,list);
                }
            }
            break;
        default:
            return -1;
    }

    return 0;
}

static struct option cnc_longopts[] = {
    { "switch-metadata-dir",required_argument,NULL,'S' },
    { "debug-level",required_argument,NULL,'Z' },
    { "warn-level",required_argument,NULL,'W' },
    { "log-flags",required_argument,NULL,'L', },
    { "node-flow-caps",required_argument,NULL,'F', },
};

struct mul_getopt_helper_info cnc_getopt_helper_info = {
    .module_init_fn = cnc_module_init,
    .module_getopt_helper_fn = cnc_getopt_helper,
    .priv = (void *)&cnc_opts,
    .opts = "S:Z:W:L:F:",
    .longopts = cnc_longopts,
    .longopts_len = sizeof(cnc_longopts) / sizeof(struct option),
    .usage = \
        CNC_APP_NAME " Options:\n" \
        "\t-S, --switch-metadata-dir <DIRECTORY>\n" \
        "\t\tThe DIRECTORY containing per-datapath-id files describing each\n" \
        "\t\tswitch.  Each file contains port descriptions.\n" \
        "\t-Z, --debug-level <LEVEL>\n" \
        "\t\tSet the debug msg level.\n" \
        "\t-W, --warn-level <LEVEL>\n" \
        "\t\tSet the optional warn msg level.\n" \
        "\t-L, --log-flags <FLAGS>\n" \
        "\t\tA comma-separated list of debug flags (set to ALL to see all).\n"
        "\t-F, --node-flow-caps <src1:dst1,dst2,dst3,...;src2:dst4,dst5,dst6,...;...\n" \
        "\t\tA comma-separated list of debug flags (set to ALL to see all).\n",
};

module_getopt_helper(cnc_getopt_helper_info);

/**
 ** Terminal support.
 **/
#ifdef MUL_APP_VTY
void cnc_module_vty_init(void *arg UNUSED) {
    c_log_debug("%s:",FN);
}

module_vty_init(cnc_module_vty_init);
#endif

module_init(cnc_module_init);
