
#ifndef __CAPNET_CONTROLLER_H__
#define __CAPNET_CONTROLLER_H__

#include <netinet/ether.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <glib.h>
#include "glib_wrapper.h"
#include "mul_common.h"
#include "obj.h"

#define CNC_APP_NAME "capnet-controller"

#define __UNK_BUFFER_ID (0xffffffff)

/**
 * Capnet getopt helper stuff.
 */
struct cnc_opts {
    char *switch_metadata_dir;
    char *MASTER_node_id;
    GHashTable *node_flow_caps;
};

/*
typedef struct cnc_lan {
    char *id;
    uint64_t lanid;
} cnc_lan_t;
*/

void cnc_init(void);

void cnc_module_init(void *ctx);
void cnc_module_vty_init(void *arg);

/* Callback Interface */

//int cnc_node_flow(cn_switch_t *csw, cn_node_t *srcnode, 
//                                    cn_node_t *dstnode,
//                  int bidirectional);

/* Initialize the given flow with default values. */
void cnc_init_flow(cn_flow_t *flow);

/* Actually install the flow assuming a source of 'src_node' */
int cnc_install_flow(cn_node_t *src_node, cn_flow_t *flow);
/* Remove the flow assuming a source of 'src_node' */
int cnc_remove_flow(cn_node_t *src_node, cn_flow_t *flow);

/* Send a CPNotify packet with the given result to the given 'dest' node. The
 * 'for_uuid' argument is used to fill the 'reply_for' filed in the CPNotify
 * message */
int cnc_send_notify(cn_node_t *dest,
                    uint64_t for_uuid,
                    cn_dispatch_result_t *result);

/**
 * The metadata service for a switch calls this function to notify us
 * when information about a port has become available.  This function
 * grabs the switch lock and adds the port and its node(s).
 *
 * The metadata service must call this function without any of its locks
 * held -- and it must remove the dpid/port_no from its pending list --
 * why?  Because if we can't find it, we might put it right back on the
 * pending list :).
 */
int cnc_port_pending_notify(uint64_t dpid,uint16_t port_no);

/**
 * MUL callbacks, defined in controller.c and sent to MUL in controller_main.c .
 */
int cnc_switch_priv_alloc(void **priv);
void cnc_switch_priv_free(void *priv);
void cnc_switch_add(mul_switch_t *sw);
void cnc_switch_del(mul_switch_t *sw);
void cnc_port_add(mul_switch_t *sw, mul_port_t *port);
void cnc_port_del(mul_switch_t *sw,mul_port_t *port);
int cnc_priv_port_alloc(void **port_ptr);
void cnc_priv_port_free(void *priv);
void cnc_packet_in(mul_switch_t *sw,struct flow *fl,uint32_t inport,
		   uint32_t buffer_id,uint8_t *raw,size_t pkt_len);
void cnc_core_closed(void);
void cnc_core_reconn(void);

#endif /* __CAPNET_CONTROLLER_H__ */
