
#include "controller.h"
#include "metadata.h"

/**
 ** A metadata service that gets its data from openstack.
 **
 ** This is just a special port that gets CNP messages from
 ** openstack... i.e. when network, port, node metadata changes.
 **/

/*
 * There's only one of these per controller.  All metadata for any
 * network comes down to this, where it is demultiplexed.
 */
//struct lock *__openstack_lock;
//static thread *openstack_thread = NULL;

int metadata_openstack_has_switch(uint64_t dpid) {
    return 0;
}

int metadata_openstack_init_switch(cn_switch_t *sw) {
    return -1;
}

void metadata_openstack_fini_switch(cn_switch_t *sw) {
    return;
}

int metadata_openstack_start_thread(cn_switch_t *sw) {
    /*
    lock(__openstack_lock);
    if (openstack_thread) {
	unlock(__openstack_lock);
	return;
    */
    return -1;
}

int metadata_openstack_get_switch_info(cn_switch_t *sw) {
    return -1;
}

int metadata_openstack_get_port_info(cn_switch_t *sw,cn_port_t *port) {
    return -1;
}

int metadata_openstack_get_lan_info(cn_switch_t *sw,cn_port_t *port) {
    return -1;
}

struct metadata_ops metadata_openstack_ops  = {
    .has_switch = metadata_openstack_has_switch,
    .init_switch = metadata_openstack_init_switch,
    .fini_switch = metadata_openstack_fini_switch,
    .start_thread = metadata_openstack_start_thread,
    .get_port_info = metadata_openstack_get_port_info,
    .get_lan_info = metadata_openstack_get_lan_info,
    .get_switch_info = metadata_openstack_get_switch_info,
};
