#ifndef __OBJ_INTERNAL_H__
#define __OBJ_INTERNAL_H__

#include <libcap.h>

static const void * OBJ_OWNER = (void *) 0xdeadbeefUL;

int cn_ensure_target_meta(struct cnode *ca, struct cnode *cb);

void cn_ensure_target_meta_cleanup(struct cnode *ca, struct cnode *cb);

#endif
