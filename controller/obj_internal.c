#include <libcap.h>

#include "obj_internal.h"
#include "obj.h"

int cn_ensure_target_meta(struct cnode *ca, struct cnode *cb) {
    if (cap_cnode_metadata(ca) == NULL) {
        assert(cap_cnode_metadata(cb) == NULL && "ca meta is null, but cb meta is not");
        cn_cnode_meta_t * meta = NULL;
        if (cn_cnode_meta_new(&meta) != 0) {
            return -1;
        }
        cap_cnode_set_metadata(cb, meta);
    }
    return 0;
}

void cn_ensure_target_meta_cleanup(struct cnode *ca, struct cnode *cb) {
    if (cap_cnode_metadata(ca) == NULL) {
        cn_cnode_meta_free((cn_cnode_meta_t *) cap_cnode_metadata(cb));
        cap_cnode_set_metadata(cb, NULL);
    }
}
