
#include "capnet_config.h"
#include "capnet_log.h"
#include "mul_common.h"
#include "controller.h"
#include "metadata.h"

#include "obj.h"
#include "dispatch.h"
#include "errors.h"

#include "crc.h"

#include <libcap.h>
#include <linux/if_ether.h>
#include <netinet/if_ether.h>
#include <net/ethernet.h>

#include <time.h>

#define CNCPRIxMAC "%.2hhx:%.2hhx:%.2hhx:%.2hhx:%.2hhx:%.2hhx"
#define CNCPRIxMACARGS(mac) mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]

#define MUL_OWNER ((void *) 0xdeadc0deUL)

/* An officially and unofficially unused ethernet protocol number for 
 * the capnet capability protocol. */
#define ETH_P_CAP 0x0809

/* Global Opts */
struct cnc_opts cnc_opts = {
    .switch_metadata_dir = "/var/tmp",
    /* XXX: id of the port that should be the master. This should come from the
     * metadata service, but for now, just hardcode it. */
    //.MASTER_node_id = "port-1",
    .node_flow_caps = NULL,
};

/*
 * Capability "domains".  Nodes on different switches may be part of a
 * domain, so we must store domain data globally.  This is a map between
 * domain string and cn_domain_t *.
 *
 * (NOTE: as far as locking order, always take the domain_lock *after*
 * taking a switch and/or port lock.  We do one big lock for now; until
 * we have thousands of flapping ports, this won't be a big problem.)
 */
static c_rw_lock_t domain_lock;
static GHashTable *domains = NULL;

void cnc_init(void) {
    c_rw_lock_init(&domain_lock);
    domains = g_hash_table_new_full(g_str_hash,g_str_equal,NULL,NULL);
}

/**
 ** Prototypes.
 **/
static void cnc_install_dfl_flows(uint64_t dpid);
static int __cnc_domain_setup_node_dhcp_flows(cn_node_t *dhcpnode,
                                              cn_node_t *node);
static int __cnc_domain_setup_node_osmeta_flows(cn_node_t *osmetanode,
                                                cn_node_t *node);
/**
 ** Util functions.
 **/
/* This is a MUL-internal function, in the common lib. */
uint32_t hash_bytes(const void *p_, size_t n, uint32_t basis);

unsigned int __mac_hash(const void *p) {   
    return hash_bytes(p,OFP_ETH_ALEN,1);
}

int __mac_equal(const void *p1,const void *p2) {
    return !memcmp(p1,p2,OFP_ETH_ALEN);
}

/**
 ** Domain helpers.
 **/
static cn_domain_t *__cnc_domain_new(char *domainid) {
    cn_domain_t *domain;

    domain = calloc(1,sizeof(*domain));
    domain->ownerdomains = g_hash_table_new_full(g_str_hash,g_str_equal,
                                                 NULL,NULL);
    domain->id = strdup(domainid);

    if (cn_broker_new(&domain->broker) != 0) {
        cncerr("couldn't alloc broker for domain %s; continuing anyway!\n",
               domain->id);
        domain->broker = NULL;
    }

    RHOLD_OBJ(domain->broker, domain);

    return domain;
}

static cn_ownerdomain_t *__cnc_ownerdomain_new(char *ownerid) {
    cn_ownerdomain_t *odomain;

    odomain = calloc(1,sizeof(*odomain));
    odomain->ownerid = strdup(ownerid);

    return odomain;
}

static void __cnc_ownerdomain_free(cn_ownerdomain_t *odomain) {
    if (odomain->nodes) {
        if (g_list_length(odomain->nodes) > 0) {
            cncwarn("freeing ownerdomain %s with nodes remaining; BUG!\n",
                    odomain->ownerid);
        }
        g_list_free(odomain->nodes);
        odomain->nodes = NULL;
    }
    odomain->master = NULL;
    if (odomain->ownerid) {
        free(odomain->ownerid);
        odomain->ownerid = NULL;
    }
}

static void __cnc_domain_free(cn_domain_t *domain) {
    gpointer gp;
    GHashTableIter iter;
    cn_ownerdomain_t *odomain;

    if (domain->broker) {
        RPUTs_OBJ(domain->broker, domain);
        domain->broker = NULL;
    }

    g_hash_table_remove(domains,domain->id);

    if (g_hash_table_size(domain->ownerdomains) > 0) {
        cncwarn("freeing domain %s with owners/nodes remaining; BUG!\n",
                domain->id);
        g_hash_table_iter_init(&iter,domain->ownerdomains);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            odomain = (cn_ownerdomain_t *)gp;
            __cnc_ownerdomain_free(odomain);
        }
        g_hash_table_destroy(domain->ownerdomains);
        domain->ownerdomains = NULL;
    }
    free(domain->id);
    domain->id = NULL;
    free(domain);
}

static cn_domain_t *__cnc_domain_lookup(char *domainid) {
    return (cn_domain_t *)g_hash_table_lookup(domains,domainid);
}

static int __cnc_domain_owner_lookup(char *domainid,char *ownerid,
                                     cn_domain_t **domain,
                                     cn_ownerdomain_t **odomain) {
    cn_domain_t *d;
    cn_ownerdomain_t *od;

    d = (cn_domain_t *)g_hash_table_lookup(domains,domainid);
    if (!d)
        return -1;

    od = (cn_ownerdomain_t *)g_hash_table_lookup(d->ownerdomains,ownerid);
    if (!od)
        return -2;

    if (domain)
        *domain = d;
    if (odomain)
        *odomain = od;

    return 0;
}

int __cnc_domain_remove_node(cn_node_t *node) {
    cn_domain_t *domain;
    cn_ownerdomain_t *odomain;
    int rc;

    cn_obj_lock(node);
    
    rc = __cnc_domain_owner_lookup(node->domainid,node->ownerid,
                                   &domain,&odomain);
    if (rc) {
        cncerr("no domain %s owner %s when removing node %s; BUG!\n",
               node->domainid,node->ownerid,node->id);
        rc = -1;
        goto finish;
    }

    if (odomain->master == node) {
        cncdbg(1,LA_CTL,LF_CTL,"removing master from ownerdomain %s\n",
               odomain->ownerid);
        odomain->master = NULL;
    }
    if (domain->dhcp_server == node) {
        cncdbg(1,LA_CTL,LF_CTL,"removing DHCP server from domain %s\n",domain->id);
        domain->dhcp_server = NULL;
    }
    if (domain->osmeta_server == node) {
        cncdbg(1,LA_CTL,LF_CTL,
               "removing OpenStack metadata server from domain %s\n",
               domain->id);
        domain->osmeta_server = NULL;
    }
    odomain->nodes = g_list_remove(odomain->nodes,node);

    rc = 0;
finish:
    cn_obj_unlock(node);

    return rc;
}

int cnc_domain_remove_node(cn_node_t *node) {
    int ret;
    
    c_wr_lock(&domain_lock);
    ret = __cnc_domain_remove_node(node);
    c_wr_unlock(&domain_lock);

    return ret;
}

int __cnc_domain_deliver_node(cn_node_t *node) {
    cn_domain_t *domain = NULL;
    cn_ownerdomain_t *odomain = NULL;
    int rc;

    cn_obj_lock(node);

    rc = __cnc_domain_owner_lookup(node->domainid,node->ownerid,&domain,&odomain);
    if (rc) {
        rc = -1;
        goto finish;
    }

    if (!odomain->master) {
        rc = 0;
        goto finish;
    }
    
    rc = cnc_deliver_obj(odomain->master,(cn_obj_t *)node);
    if (rc != 0) {
        cncerr("couldn't deliver node to domain %s owner %s master's RP0\n",
               domain->id,odomain->ownerid);
        rc = -1;
        goto finish;
    }
    else {
        c_log_debug("Sent node %s to domain %s owner %s master's rp0!",
                    node->id,domain->id,odomain->ownerid);
        rc = 0;
        goto finish;
    }

finish:
    cn_obj_unlock(node);
    return rc;
}

int cnc_domain_deliver_node(cn_node_t *node) {
    int ret;
    
    c_rd_lock(&domain_lock);
    ret = __cnc_domain_deliver_node(node);
    c_rd_unlock(&domain_lock);

    return ret;
}

int __cnc_domain_insert_node(cn_node_t *node) {
    cn_domain_t *domain;
    cn_ownerdomain_t *odomain;
    GList *cur;
    cn_node_t *lnode;
    int rc;

    cn_obj_lock(node);

    domain = __cnc_domain_lookup(node->domainid);
    if (!domain) {
        domain = __cnc_domain_new(node->domainid);
        g_hash_table_insert(domains,domain->id,domain);
    }
    odomain = (cn_ownerdomain_t *)g_hash_table_lookup(domain->ownerdomains,
                                                      node->ownerid);
    if (!odomain) {
        odomain = __cnc_ownerdomain_new(node->ownerid);
        odomain->domain = domain;
        g_hash_table_insert(domain->ownerdomains,odomain->ownerid,odomain);
    }

    if (node->role == CN_NODE_ROLE_MASTER && odomain->master) {
        cncerr("ownerdomain %s already has a master (%s); not adding node %s!\n",
               odomain->ownerid,odomain->master->id,node->id);
        rc = -1;
        goto finish;
    }
    if (node->services & CN_NODE_SERVICE_DHCP && domain->dhcp_server) {
        cncerr("domain %s already has a DHCP server (%s); not adding node %s!\n",
               domain->id,domain->dhcp_server->id,node->id);
        rc = -2;
        goto finish;
    }
    if (node->services & CN_NODE_SERVICE_OSMETA && domain->osmeta_server) {
        cncerr("domain %s already has an OpenStack etadata server (%s);"
               " not adding node %s!\n",
               domain->id,domain->osmeta_server->id,node->id);
        rc = -3;
        goto finish;
    }

    odomain->nodes = g_list_append(odomain->nodes,node);
    if (node->role == CN_NODE_ROLE_MASTER) {
        odomain->master = node;

        c_log_debug("node %s is master of domain %s ownerdomain %s",
                    node->id,node->domainid,node->ownerid);
        RHOLD_OBJ(node, MUL_OWNER);
        /*
        if (MASTER != NULL) { 
            c_log_debug("Port Add: removing old master (%s)", MASTER->id);
            RPUTs_OBJ(MASTER, MUL_OWNER); 
        }
        MASTER = node;
        */

        /* Send caps for all nodes in this domain to this master.  Do
         * the master first!
         */
        __cnc_domain_deliver_node(node);

        /* Ok, also deliver the broker cap to the master next. */
        rc = cnc_deliver_obj(node,(cn_obj_t *)domain->broker);
        if (rc) {
            cncerr("failed to deliver broker cap to ownerdomain %s master %s!\n",
                   odomain->ownerid,node->id);
        }
        else {
            cncdbg(5,LA_CTL,LF_CTL,
                   "sent broker cap to ownerdomain %s master %s\n",
                   odomain->ownerid,node->id);
        }

        /* There, now finish sending caps for all the rest of the nodes. */
        gw_list_foreach(odomain->nodes,cur,lnode) {
            if (lnode == node)
                continue;
            __cnc_domain_deliver_node(lnode);
        }
    }
    else {
        if (odomain->master)
            __cnc_domain_deliver_node(node);
    }

    if (node->services & CN_NODE_SERVICE_DHCP)
        domain->dhcp_server = node;
    if (node->services & CN_NODE_SERVICE_OSMETA)
        domain->osmeta_server = node;

    rc = 0;

finish:
    cn_obj_unlock(node);

    return rc;
}

int cnc_domain_insert_node(cn_node_t *node) {
    int rc;

    c_wr_lock(&domain_lock);
    rc = __cnc_domain_insert_node(node);
    c_wr_unlock(&domain_lock);

    return rc;
}

cn_node_t *__cnc_domain_owner_lookup_master(cn_node_t *node) {
    cn_domain_t *domain = NULL;
    cn_ownerdomain_t *odomain = NULL;
    int rc;

    cn_obj_lock(node);

    rc = __cnc_domain_owner_lookup(node->domainid,node->ownerid,
                                   &domain,&odomain);
    if (rc) {
        cncerr("no such domain %s owner %s for node %s; BUG!\n",
               node->domainid,node->ownerid,node->id);
        cn_obj_unlock(node);
        return NULL;
    }

    cn_obj_unlock(node);
    return odomain->master;
}

cn_node_t *cnc_domain_owner_lookup_master(cn_node_t *node) {
    cn_node_t *ret;

    c_rd_lock(&domain_lock);
    ret = __cnc_domain_owner_lookup_master(node);
    c_rd_unlock(&domain_lock);

    return ret;
}

cn_node_t *cnc_domain_lookup_dhcp_server(char *domainid) {
    cn_domain_t *domain;
    cn_node_t *dhcp_server;

    c_rd_lock(&domain_lock);

    domain = __cnc_domain_lookup(domainid);
    if (!domain) {
        cncerr("no such domain %s; BUG!\n",domainid);
        c_rd_unlock(&domain_lock);
        return NULL;
    }

    dhcp_server = domain->dhcp_server;

    c_rd_unlock(&domain_lock);

    return dhcp_server;
}

cn_node_t *cnc_domain_lookup_osmeta_server(char *domainid) {
    cn_domain_t *domain;
    cn_node_t *osmeta_server;

    c_rd_lock(&domain_lock);

    domain = __cnc_domain_lookup(domainid);
    if (!domain) {
        cncerr("no such domain %s; BUG!\n",domainid);
        c_rd_unlock(&domain_lock);
        return NULL;
    }

    osmeta_server = domain->osmeta_server;

    c_rd_unlock(&domain_lock);

    return osmeta_server;
}

void cnc_switch_insert_node(cn_switch_t *csw, cn_node_t *node) {
    cn_obj_lock(csw);
    cn_obj_lock(node);
    g_hash_table_insert(csw->mac_to_node, node->mac,node);
    cncdbg(5, LA_CTL, LF_CTL, "inserted mac "CNCPRIxMAC" to node id %s\n",
           CNCPRIxMACARGS(node->mac), node->id);

    if (node->ipv4.valid) {
        gpointer gp = (gpointer)(unsigned long)node->ipv4.addr.s_addr;
        g_hash_table_insert(csw->ipv4_to_node, gp, node);
        cncdbg(5, LA_CTL, LF_CTL, "inserted ipv4 %s to node id %s\n",
               inet_ntoa(node->ipv4.addr), node->id);
    }

    cn_obj_unlock(node);
    cn_obj_unlock(csw);
}

// XXX: Again, probably not 2pl
cn_node_t *cnc_switch_lookup_node(cn_switch_t *csw,char *node_id) {
    GHashTableIter iter;
    cn_node_t *node = NULL;

    cn_obj_lock(csw);

    g_hash_table_iter_init(&iter,csw->mac_to_node);
    while (g_hash_table_iter_next(&iter,NULL,(gpointer *)&node)) {
        cn_obj_lock(node);
        if (strcmp(node_id,node->id) == 0)
            goto finish;
        cn_obj_unlock(node);
    }

finish:
    if (node) { cn_obj_unlock(node); }
    cn_obj_unlock(csw);

    return NULL;
}

cn_node_t *cnc_switch_lookup_node_by_mac(cn_switch_t *csw,
                      uint8_t mac[ETH_ALEN]) {
    cn_node_t *node;

    cn_obj_lock(csw);

    node = (cn_node_t *)g_hash_table_lookup(csw->mac_to_node,mac);

    if (node) { 
        cn_obj_lock(node); 
    }
    cncdbg(5,LA_CTL,LF_CTL,"looked up mac "CNCPRIxMAC" -> %s\n",
           CNCPRIxMACARGS(mac),node ? node->id : "?");
    if (node) { 
        cn_obj_unlock(node); 
    }

    cn_obj_unlock(csw);

    return node;
}

cn_node_t *cnc_switch_lookup_node_by_ipv4(cn_switch_t *csw,uint32_t addr) {
    cn_node_t *node;
    gpointer gp;
    struct in_addr ia;

    ia.s_addr = addr;

    cn_obj_lock(csw);

    gp = (gpointer)(unsigned long)addr;
    node = (cn_node_t *)g_hash_table_lookup(csw->ipv4_to_node,gp);
    if (node) { 
        cn_obj_lock(node);
    }
    cncdbg(5,LA_CTL,LF_CTL,"looked up ipv4 %s -> %s\n",
       inet_ntoa(ia),node ? node->id : "?");
    if (node) { 
        cn_obj_unlock(node);
    }

    cn_obj_unlock(csw);

    return node;
}

void cnc_switch_remove_node(cn_switch_t *csw,cn_node_t *node) {
    cn_obj_lock(csw);
    cn_obj_lock(node);

    g_hash_table_remove(csw->mac_to_node,node->mac);
    if (node->ipv4.valid)
        g_hash_table_remove(csw->mac_to_node,
                            (gpointer) (unsigned long) node->ipv4.addr.s_addr);

    cn_obj_lock(node);
    cn_obj_lock(csw);
}

/**
 ** Capability functions.
 **/

void cnc_init_flow(cn_flow_t *flow) {
    cn_obj_lock(flow);
    of_mask_clr_in_port(&flow->mask);
    of_mask_set_dl_dst(&flow->mask);
    of_mask_set_dl_src(&flow->mask);
    memcpy(&flow->flow.dl_dst, flow->node->mac, OFP_ETH_ALEN);
    cn_obj_unlock(flow);
}

int cnc_install_flow(cn_node_t *src_node, cn_flow_t *flow) {
    int res = -1;
    uint64_t sdpid;
    cn_port_t *scpo, *dcpo;
    cn_switch_t *scsw,*dcsw;
    uint32_t outport;
    cn_port_t *outport_obj = NULL;

    if (cn_unordered_acquire_node_switch_lock(src_node, &scpo, &scsw) != 0) {
        cncerr("Couldn't lock src_node\n");
        return -1;
    }

    cn_node_t *flow_node;

    if (cn_unordered_acquire_flow_switch_lock(flow, &flow_node, &dcpo, &dcsw) != 0) {
        cncerr("Couldn't lock flow parents\n");
        goto finish1;
    }

    sdpid = scsw->mul_switch->dpid;

    if (scsw == dcsw) {
        /* Flow is contained on switch; output direct to dst port. */
        outport_obj = flow->node->cn_port;
    }
    else {
        /* Must find the uplink port for this source switch. */
        outport_obj = scsw->uplink;
        if (!outport_obj) {
            cncerr("no uplink port for switch %lx; cannot install flow"
                   " from %s to %s!\n",
                   sdpid,src_node->id,flow->node->id);
            goto finish;
        }
    }

    cn_obj_lock(outport_obj);
    outport = outport_obj->mul_port->port_no;

    struct flow inst_flow = flow->flow;
    memcpy(&inst_flow.dl_src, src_node->mac, OFP_ETH_ALEN);

    struct mul_act_mdata mdata;
    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata, sdpid);
    mul_app_action_output(&mdata, outport);

    res = mul_app_send_flow_add(CNC_APP_NAME, NULL, sdpid, &inst_flow, &flow->mask,
                                0xffffffff, mdata.act_base, mul_app_act_len(&mdata),
                                0, 0, C_FL_PRIO_DFL+1, 0);
    if (res != 0) {
        cncerr("mul_app_send_flow_add failed with code: %d\n", res);
        res = -1;
    } else {
        res = 0;
    }

    mul_app_act_free(&mdata);

    cn_obj_unlock(outport_obj);
finish:
    cn_obj_unlock(flow);
    cn_obj_unlock(flow->node);
    cn_obj_unlock(dcpo);
    cn_obj_unlock(dcsw);
finish1:
    cn_obj_unlock(src_node);
    cn_obj_unlock(scpo);
    cn_obj_unlock(scsw);

    return res;
}

int cnc_remove_flow(cn_node_t *src_node, cn_flow_t *flow) {
    int res = -1;
    uint64_t sdpid;
    cn_port_t *scpo, *dcpo;
    cn_switch_t *scsw,*dcsw;
    cn_node_t *flow_node;

    if (cn_unordered_acquire_node_switch_lock(src_node, &scpo, &scsw) != 0) {
        cncerr("unable to lock src_node switch\n");
        return -1;
    }

    if (cn_unordered_acquire_flow_switch_lock(flow, &flow_node, &dcpo, &dcsw) != 0) {
        cncerr("unable to lock flow switch\n");
        res = -1;
        goto finish1;
    }


    sdpid = scsw->mul_switch->dpid;

    if (scsw != dcsw && !scsw->uplink) {
        cncerr("no uplink port for switch %lx; cannot remove flow"
               " from %s to %s; BUG?\n",
               sdpid,src_node->id,flow->node->id);
        goto finish;
    }

    struct flow inst_flow = flow->flow;
    memcpy(&inst_flow.dl_src, src_node->mac, OFP_ETH_ALEN);

    res = mul_app_send_flow_del(CNC_APP_NAME, NULL, sdpid, &inst_flow, 
                                &flow->mask, OFPP_NONE, C_FL_PRIO_DFL+1, 0, 
                                OFPG_ANY);

    if (res != 0) {
        cncerr("mul_app_send_flow_del failed with code: %d\n", res);
        res = -1;
    } else {
        res = 0;
    }

finish:
    cn_obj_unlock(flow);
    cn_obj_unlock(flow->node);
    cn_obj_unlock(dcpo);
    cn_obj_unlock(dcsw);
finish1:
    cn_obj_unlock(src_node);
    cn_obj_unlock(scpo);
    cn_obj_unlock(scsw);

    return res;
}

/**
 ** MUL Callback functions.
 **/
int cnc_switch_priv_alloc(void **priv) {
    cn_switch_t **csw = (cn_switch_t **)priv;
    int ret = cn_switch_new(csw);
    if (ret != 0) { return ret; }
    /* MUL itself needs to hold a reference to this object */
    RHOLD_OBJ(*csw, MUL_OWNER);
    return ret;
}

void cnc_switch_priv_free(void *priv) {
    assert(priv != NULL && "MUL asked to free a NULL pointer");
    RPUTs_OBJ(priv, MUL_OWNER);
}

void cnc_switch_add(mul_switch_t *sw) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);

    /*
     * Hold a ref to the switch to prevent the switch from being deleted
     * if it goes away.  XXX: is this correct?  Do we have to be careful
     * later to reset state when it comes back?
     * 
     * Ok, don't do this for now...
     */
    //csw->mul_switch = c_app_switch_get_with_id(sw->dpid);
    csw->mul_switch = sw;

    /* Set up switch metadata stuff */
    csw->mops = metadata_get_ops_for_switch(sw->dpid);
    if (! csw->mops) {
        cncerr("No mops for switch %#0llx\n", (unsigned long long) sw->dpid);
        return;
    }
    metadata_init_switch(csw);
    metadata_start_thread(csw);
    metadata_get_switch_info(csw);

    cnc_install_dfl_flows(sw->dpid);

    c_log_debug("Capnet Switch 0x%llx added",(unsigned long long)(sw->dpid));
}

void cnc_switch_del(mul_switch_t *sw) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
 
    c_log_debug("Capnet Switch 0x%llx removed",(unsigned long long)(sw->dpid));

    /*
     * XXX:
     *
     * All the ports would have been removed by now, so all that's left
     * to do is remove workflow apps, or other privileged "ports"...
     */

    //metadata_stop_thread(csw);
    //metadata_fini_switch(csw);
}

// XXX: Non-2phase locking? May not guarantee atomicity.
static void cnc_port_setup_uplink_flows(cn_switch_t *csw,cn_port_t *cport,
                                        cn_node_t *cnode) {
    cn_obj_lock(csw);
    cn_obj_lock(cport);
    cn_obj_lock(cnode);
    
    mul_switch_t *sw = csw->mul_switch;
    mul_port_t *port = cport->mul_port;
    GHashTableIter iter;
    gpointer gp = NULL;
    cn_node_t *tnode;
    struct flow flow;
    struct flow mask;
    struct mul_act_mdata mdata;
    uint64_t dpid = sw->dpid;
    uint32_t inport,outport;
    cn_domain_t *domain;

    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_in_port(&mask);

    /*
     * If this port is an uplink port, any packets coming into it, that
     * are destined for local ports, are trusted and go straight to
     * those ports.
     */
    if (cport->role == CN_PORT_ROLE_UPLINK) {
        inport = port->port_no;
        g_hash_table_iter_init(&iter,csw->mac_to_node);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            tnode = (cn_node_t *)gp;
            cn_obj_lock(tnode);
            outport = tnode->cn_port->mul_port->port_no;

            flow.in_port = htonl(inport);
            memcpy(&flow.dl_dst,tnode->mac,OFP_ETH_ALEN);

            mul_app_act_alloc(&mdata);
            mul_app_act_set_ctors(&mdata,dpid);
            mul_app_action_output(&mdata,outport);

            int res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&flow,&mask,
                                            0xffffffff,mdata.act_base,
                                            mul_app_act_len(&mdata),
                                            0,0,C_FL_PRIO_DFL+2,0);
            if (res != 0) {
                cn_abort("mul_app_send_flow_add failed with code: %d", res);
            }

            cn_obj_unlock(tnode);

            mul_app_act_free(&mdata);
            memset(&mdata,0,sizeof(mdata));

            cncdbg(3,LA_CTL,LF_CTL,
                   "added uplink %d to port %d flow for node %s\n",
                   inport,outport,tnode->id);
        }
    }
    else if (cport->role == CN_PORT_ROLE_NODE && csw->uplink) {
        /*
         * If it's a node port, and if we have an uplink port already,
         * add a flow from the uplink to this port for packets destined
         * to this port's mac.
         */
        tnode = cnode;
        inport = csw->uplink->mul_port->port_no;
        outport = tnode->cn_port->mul_port->port_no;

        flow.in_port = htonl(inport);
        memcpy(&flow.dl_dst,tnode->mac,OFP_ETH_ALEN);

        mul_app_act_alloc(&mdata);
        mul_app_act_set_ctors(&mdata,dpid);
        mul_app_action_output(&mdata,outport);

        int res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&flow,&mask,
                                        0xffffffff,mdata.act_base,
                                        mul_app_act_len(&mdata),
                                        0,0,C_FL_PRIO_DFL,0);

        if (res != 0) {
            cn_abort("mul_app_send_flow_add failed with code: %d", res);
        }

        mul_app_act_free(&mdata);

        memset(&mdata,0,sizeof(mdata));

        cncdbg(3,LA_CTL,LF_CTL,
               "added uplink %d to port %d flow for node %s\n",
               inport,outport,tnode->id);
    }


    if (cport->role == CN_PORT_ROLE_UPLINK) {
        /**
         * Lock for domain service section below.
         */
        c_wr_lock(&domain_lock);

        /*
         * If there is a dhcp server for this domain, and it's not on this
         * switch, then all the existing nodes on this switch don't have
         * paths to the dhcp server (because there was no uplink before).
         * So, fix that up.
         */
        // XXX: Deadlock Risk, via node lock aquisition order?
        g_hash_table_iter_init(&iter,csw->mac_to_node);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            tnode = (cn_node_t *)gp;
            cn_obj_lock(tnode);
            domain = __cnc_domain_lookup(tnode->domainid);
            if (!domain->dhcp_server) {
                cn_obj_unlock(tnode);
                continue;
            }
            __cnc_domain_setup_node_dhcp_flows(domain->dhcp_server,tnode);
            cn_obj_unlock(tnode);
        }

        /*
         * Then, if if there is a dhcp server *on this switch*, we may
         * have tried to add dhcp flows before, but the outbound ones
         * from the server to the uplink to the non-local nodes would
         * have failed.  So set those all up.
         */
        g_hash_table_iter_init(&iter,domains);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            domain = (cn_domain_t *)gp;
            if (domain->dhcp_server
                && domain->dhcp_server->cn_port->csw == csw) {
                GHashTableIter iter2;
                gpointer gp2;
                cn_ownerdomain_t *odomain;
                GList *cur = NULL;
                g_hash_table_iter_init(&iter2,domain->ownerdomains);
                while (g_hash_table_iter_next(&iter2,NULL,&gp2)) {
                    odomain = (cn_ownerdomain_t *)gp2;
                    gw_list_foreach(odomain->nodes,cur,tnode) {
                        __cnc_domain_setup_node_dhcp_flows(domain->dhcp_server,
                                                           tnode);
                    }
                }
            }
        }

        /*
         * If there is an OpenStack metadata server for this domain, and
         * it's not on this switch, then all the existing nodes on this
         * switch don't have paths to the OpenStack metadata server (because
         * there was no uplink before).  So, fix that up.
         */

        // XXX: Deadlock Risk, via node lock aquisition order?
        g_hash_table_iter_init(&iter,csw->mac_to_node);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            tnode = (cn_node_t *)gp;
            cn_obj_lock(tnode);
            domain = __cnc_domain_lookup(tnode->domainid);
            if (!domain->osmeta_server) {
                cn_obj_lock(tnode);
                continue;
            }
            __cnc_domain_setup_node_osmeta_flows(domain->osmeta_server,tnode);
            cn_obj_lock(tnode);
        }

        /*
         * Then, if if there is a OpenStack metadata server *on this
         * switch*, we may have tried to add osmeta flows before, but the
         * outbound ones from the server to the uplink to the non-local
         * nodes would have failed.  So set those all up.
         */
        g_hash_table_iter_init(&iter,domains); while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            domain = (cn_domain_t *)gp;
            if (domain->osmeta_server
                && domain->osmeta_server->cn_port->csw == csw) {
                GHashTableIter iter2;
                gpointer gp2;
                cn_ownerdomain_t *odomain;
                GList *cur = NULL;
                g_hash_table_iter_init(&iter2,domain->ownerdomains);
                while (g_hash_table_iter_next(&iter2,NULL,&gp2)) {
                    odomain = (cn_ownerdomain_t *)gp2;
                    gw_list_foreach(odomain->nodes,cur,tnode) {
                        __cnc_domain_setup_node_osmeta_flows(domain->osmeta_server,
                                                             tnode);
                    }
                }
            }
        }

        /**
         * Unlock from service section above.
         */
        c_wr_unlock(&domain_lock);
    }

    cn_obj_unlock(cnode);
    cn_obj_unlock(cport);
    cn_obj_unlock(csw);
}

static int __cnc_domain_setup_node_dhcp_flows(cn_node_t *dhcpnode,
                                              cn_node_t *node) {

    cn_port_t *cportn = NULL;
    cn_port_t *cportd = NULL;
    cn_switch_t *cswn = NULL;
    cn_switch_t *cswd = NULL;

    int ret = -1;

    if (cn_unordered_acquire_node_switch_lock(node, &cportn, &cswn) != 0) {
        cncerr("Unable to acquire switch locks\n");
        return -1;
    }

    if (cn_unordered_acquire_node_switch_lock(dhcpnode, &cportd, &cswd) != 0) {
        cncerr("unable to acquire switch lock for dhcpnode\n");
        ret = -1;
        goto finish1;
    }

    mul_port_t *portd = cportd->mul_port;
    mul_port_t *portn = cportn->mul_port;
    mul_switch_t *swn = cswn->mul_switch;
    mul_switch_t *swd = cswd->mul_switch;

    GHashTableIter iter;
    gpointer gp = NULL;
    cn_node_t *tnode;
    struct flow flow;
    struct flow mask;
    struct mul_act_mdata mdata;
    int32_t inport,outport;

    /*
     * Make sure both dhcpnode and node are colocated, or that we have
     * uplinks on both switches; otherwise we will wait til the uplinks
     * are in place before we install.
     */
    if (!(cswn == cswd || (cswn->uplink && cswn->uplink_port_no > 0))) {
        cncwarn("cannot not setup DHCP flow from node %s to server %s;"
                " node switch has no uplink\n",node->name,dhcpnode->name);
        ret = -2;
        goto finish;
    }
    if (!(cswn == cswd || (cswd->uplink && cswd->uplink_port_no > 0))) {
        cncwarn("cannot not setup DHCP flow from dhcpnode %s to node %s;"
                " node switch has no uplink\n",dhcpnode->name,node->name);
        ret = -2;
        goto finish;
    }

    /*
     * First, setup the flow from the source to the dhcp server.
     */
    if (cswn == cswd)
        outport = portd->port_no;
    else if (cswn->uplink && cswn->uplink_port_no > 0)
        outport = cswn->uplink_port_no;
    else {
        cncerr("could not setup DHCP flow from node %s to server %s;"
               " node switch has no uplink\n",node->name,dhcpnode->name);
        ret = -2;
        goto finish;
    }
    inport = portn->port_no;

    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_proto(&mask);
    of_mask_set_tp_src(&mask);
    of_mask_set_tp_dst(&mask);
    of_mask_set_in_port(&mask);

    /* A DHCP flow from the node to the dhcp server. */
    memcpy(&flow.dl_src,node->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,dhcpnode->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    flow.nw_proto = 17;
    flow.tp_src = htons(68);
    flow.tp_dst = htons(67);
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swn->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swn->dpid,&flow,&mask,
                                0xffffffff,mdata.act_base,
                                mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    if (ret != 0) {
        cncerr("mul_app_send_flow_add failed with code: %d\n", ret);
        ret = -2;
        goto finish;
    }

    cncdbg(2,LA_CTL,LF_CTL,
           "added DHCP flow from node %s to dhcp server %s\n",
           node->name,dhcpnode->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));

    /*
     * Also do DNS from node to dhcp server :(
     */
    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_proto(&mask);
    of_mask_set_tp_dst(&mask);
    of_mask_set_in_port(&mask);

    /* A DNS flow from the node to the dhcp server. */
    memcpy(&flow.dl_src,node->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,dhcpnode->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    flow.nw_proto = 17;
    flow.tp_dst = htons(53);
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swn->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swn->dpid,&flow,&mask,
                                0xffffffff,mdata.act_base,
                                mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    /* XXX: This should probably do a rollback, but just abort for 
     * now instead. */
    if (ret != 0) {
        cn_abort("mul_app_send_flow_add failed with code: %d\n", ret);
    }


    cncdbg(2,LA_CTL,LF_CTL,
           "added DNS flow from node %s to dhcp server %s\n",
           node->name,dhcpnode->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));

    if (ret != 0) {
        cncerr("mul_app_send_flow_add failed with code: %d\n", ret);
        ret = -2;
        goto finish;
    }

    /*
     * Second, setup the flow from the dhcp server to the node.
     */
    if (cswn == cswd)
        outport = portn->port_no;
    else if (cswd->uplink && cswd->uplink_port_no > 0)
        outport = cswd->uplink_port_no;
    else {
        cncerr("could not setup DHCP flow from dhcpnode %s to node %s;"
               " node switch has no uplink\n",dhcpnode->name,node->name);
        ret = -2;
        goto finish;
    }
    inport = portd->port_no;

    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_proto(&mask);
    of_mask_set_tp_src(&mask);
    of_mask_set_tp_dst(&mask);
    of_mask_set_in_port(&mask);

    /* A DHCP flow from the dhcp server to the node. */
    memcpy(&flow.dl_src,dhcpnode->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,node->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    flow.nw_proto = 17;
    flow.tp_src = htons(67);
    flow.tp_dst = htons(68);
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swd->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swd->dpid,&flow,&mask,
                                0xffffffff,mdata.act_base,
                                mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    if (ret != 0) {
        cncerr("mul_app_send_flow_add failed with code: %d\n", ret);
        ret = -2;
        goto finish;
    }

    cncdbg(2,LA_CTL,LF_CTL,
           "added DHCP flow from dhcp server %s to node %s\n",
           dhcpnode->name,node->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));


    /*
     * Also do DNS from the dhcp server to the node :(
     */
    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_proto(&mask);
    of_mask_set_tp_src(&mask);
    of_mask_set_in_port(&mask);

    /* A DNS flow from the dhcp server to the node. */
    memcpy(&flow.dl_src,dhcpnode->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,node->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    flow.nw_proto = 17;
    flow.tp_src = htons(53);
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swd->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swd->dpid,&flow,&mask,
                          0xffffffff,mdata.act_base,
                          mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    /* XXX: Should probably do a rollback if we fail at this point,
     * instead just abort. */
    if (ret != 0) {
        cn_abort("mul_app_send_flow_add failed with code: %d", ret);
    }

    cncdbg(2,LA_CTL,LF_CTL,
           "added DNS flow from dhcp server %s to node %s\n",
           dhcpnode->name,node->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));

    ret = 0;

finish:
    cn_obj_unlock(cswd);
    cn_obj_unlock(cportd);
    cn_obj_unlock(dhcpnode);

finish1:
    cn_obj_unlock(cswn);
    cn_obj_unlock(cportn);
    cn_obj_unlock(node);

    return ret;
}

static int __cnc_domain_setup_node_osmeta_flows(cn_node_t *osmetanode,
                                                cn_node_t *node) {

    int ret = -1;

    cn_port_t *cportn = NULL;
    cn_port_t *cportd = NULL;
    cn_switch_t *cswn = NULL;
    cn_switch_t *cswd = NULL;

    if (cn_unordered_acquire_node_switch_lock(node, &cportn, &cswn) != 0) {
        cncerr("Failed to acquire switch lock\n");
        return -1;
    }
    if (cn_unordered_acquire_node_switch_lock(osmetanode, &cportd, &cswd) != 0) {
        cncerr("Failed to acquire osmeta switch lock\n");
        goto finish1;
    }

    mul_port_t *portd = cportd->mul_port;
    mul_port_t *portn = cportn->mul_port;
    mul_switch_t *swn = cswn->mul_switch;
    mul_switch_t *swd = cswd->mul_switch;

    GHashTableIter iter;
    gpointer gp = NULL;
    cn_node_t *tnode;
    struct flow flow;
    struct flow mask;
    struct mul_act_mdata mdata;
    int32_t inport,outport;


    /* First, setup the flow from the source to the metadata server. */
    if (cswn == cswd)
        outport = portd->port_no;
    else if (cswn->uplink && cswn->uplink_port_no > 0)
        outport = cswn->uplink_port_no;
    else {
        cncerr("could not setup OpenStack metadata flow from node %s to server %s;"
               " node switch has no uplink\n",node->name,osmetanode->name);
        ret = -2;
        goto finish;
    }
    inport = portn->port_no;

    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_dst(&mask,32);
    of_mask_set_in_port(&mask);

    /* An OpenStack metadata flow from the node to the metadata server. */
    memcpy(&flow.dl_src,node->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,osmetanode->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    // 169.254.169.254, htonl() == 0xfea9fea9
    flow.ip.nw_dst = 0xfea9fea9;
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swn->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swn->dpid,&flow,&mask,
                                0xffffffff,mdata.act_base,
                                mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    if (ret != 0) {
        cncerr("mul_app_send_flow_add failed with code: %d", ret);
        ret = -2;
        goto finish;
    }

    cncdbg(2,LA_CTL,LF_CTL,
           "added OpenStack metadata flow from node %s to server %s\n",
           node->name,osmetanode->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));

    /* Second, setup the flow from the metadata server to the node. */
    if (cswn == cswd)
        outport = portn->port_no;
    else if (cswd->uplink && cswd->uplink_port_no > 0)
        outport = cswd->uplink_port_no;
    else {
        cncerr("could not setup OpenStack metadata flow from metadata server %s"
               " to node %s; node switch has no uplink\n",
               osmetanode->name,node->name);
        ret = -2;
        goto finish;
    }
    inport = portd->port_no;

    memset(&flow,0,sizeof(flow));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_src(&mask,32);
    of_mask_set_in_port(&mask);

    /* An OpenStack metadata flow from the metadata server to the node. */
    memcpy(&flow.dl_src,osmetanode->mac,OFP_ETH_ALEN);
    memcpy(&flow.dl_dst,node->mac,OFP_ETH_ALEN);
    flow.dl_type = htons(0x0800);
    flow.ip.nw_src = 0xfea9fea9;
    flow.in_port = htonl(inport);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata,swd->dpid);
    mul_app_action_output(&mdata,outport);

    ret = mul_app_send_flow_add(CNC_APP_NAME,NULL,swd->dpid,&flow,&mask,
                                0xffffffff,mdata.act_base,
                                mul_app_act_len(&mdata),0,0,C_FL_PRIO_DFL+1,0);

    /* XXX: Should probably rollback instead of abort, but for now, just
     * abort */
    if (ret != 0) {
        cn_abort("mul_app_send_flow_add failed with code: %d\n", ret);
    }

    cncdbg(2,LA_CTL,LF_CTL,
           "added OpenStack metadata flow from metadata server %s to node %s\n",
           osmetanode->name,node->name);

    mul_app_act_free(&mdata);
    memset(&mdata,0,sizeof(mdata));

    ret = 0;

finish:
    cn_obj_unlock(cswd);
    cn_obj_unlock(cportd);
    cn_obj_unlock(osmetanode);
finish1:
    cn_obj_unlock(cswn);
    cn_obj_unlock(cportn);
    cn_obj_unlock(node);

    return ret;
}

/**
 * This function sets up DHCP flows.  @cnode may be either the
 * dhcp_server node itself, or a new node.  If the former, any existing
 * nodes will get flows to and from the DHCP server; if the latter, and
 * if there's a dhcp_server already, we'll just install the to/from
 * flows for that node.
 *
 * See also the comments inside the function that discuss our strategy
 * for minimizing flows despite the broadcast nature of DHCP.
 */
static void __cnc_domain_setup_dhcp_flows(cn_switch_t *csw,cn_port_t *cport,
                                          cn_node_t *cnode) {
    cn_obj_lock(csw);
    cn_obj_lock(cport);
    cn_obj_lock(cnode);

    cn_domain_t *domain;
    cn_node_t *dhcpnode;
    GList *cur;
    cn_node_t *tnode;
    GHashTableIter iter;
    gpointer gp;
    cn_ownerdomain_t *odomain;
    cn_port_t *dport = NULL;
    cn_switch_t *dsw = NULL;

    domain = __cnc_domain_lookup(cnode->domainid);
    if (!domain) {
        cncerr("BUG -- domain is no longer here somehow!\n");
        goto finish;
    }

    dhcpnode = domain->dhcp_server;
    if (!dhcpnode) {
        goto finish;
    }

    if (cn_unordered_acquire_node_switch_lock(dhcpnode,&dport,&dsw) != 0) {
        cncerr("could not lock dhcpnode!");
	goto finish;
    }

    /*
     * XXX: really need to hold a tmp ref to dhcpnode!
     * XXX: also, the because the dhcpnode and cnode will probably be on
     * separate switches, we have to deal with multi-switch locking.
     * This is doomed to fail eventually -- someday new nodes will
     * arrive on both switches simultaneously and there will be a
     * conflict trying to lock the second (non-local) switch.  So some
     * of these operations really, really need to be spun off into
     * separate threads like a continuation, where the thread can just
     * block forever until it gets a switch lock.
     *
     * Ok, for now, we get around this by taking the big domain lock and
     * holding it til we're done.
     */

    /*
     * If this node is the DHCP server for this domain, it has to
     * receive unicast DHCP and send unicast DHCP to any other node in
     * the domain -- as well as receive "broadcast" requests from any
     * node in the domain.
     *
     * So, to limit the number of flows we need to setup, we do
     * something simple: we rewrite the dest mac address of the DHCP
     * discovery from the client from ff:ff:ff:ff:ff:ff to be the
     * destination of the DHCP server we know about, and send it either
     * locally or to the uplink.  Then we also add a unicast flow from
     * the client to the server from udp 68 to the server mac, udp 67
     * (again to either the uplink or to the local port.  The server is
     * the same way, but in reverse.  So each node requires 2 flow
     * entries, one in the dhcp server switch, and another in the node's
     * switch; and manual handling of all DHCP discovery packets.
     */

    if (dhcpnode && dhcpnode == cnode) {
        g_hash_table_iter_init(&iter,domain->ownerdomains);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            odomain = (cn_ownerdomain_t *)gp;
            gw_list_foreach(odomain->nodes,cur,tnode) {
                __cnc_domain_setup_node_dhcp_flows(dhcpnode,tnode);
            }
        }
    }
    else if (dhcpnode) {
        __cnc_domain_setup_node_dhcp_flows(dhcpnode,cnode);
    }


    cn_obj_unlock(dhcpnode);
    cn_obj_unlock(dport);
    cn_obj_unlock(dsw);
finish:
    cn_obj_unlock(cnode);
    cn_obj_unlock(cport);
    cn_obj_unlock(csw);
}

/**
 * This function sets up OpenStack metadata flows (nearly identical to DHCP).
 */
static void __cnc_domain_setup_osmeta_flows(cn_switch_t *csw,cn_port_t *cport,
                                            cn_node_t *cnode) {
    cn_obj_lock(csw);
    cn_obj_lock(cport);
    cn_obj_lock(cnode);

    cn_domain_t *domain;
    cn_node_t *osmetanode;
    GList *cur;
    cn_node_t *tnode;
    GHashTableIter iter;
    gpointer gp;
    cn_ownerdomain_t *odomain;
    cn_port_t *omport = NULL;
    cn_switch_t *omsw = NULL;

    domain = __cnc_domain_lookup(cnode->domainid);
    if (!domain) {
        cncerr("BUG -- domain is no longer here somehow!\n");
        goto finish;
    }

    osmetanode = domain->osmeta_server;
    if (!osmetanode)
        goto finish;

    if (cn_unordered_acquire_node_switch_lock(osmetanode,&omport,&omsw) != 0) {
        cncerr("could not lock osmetanode!");
	goto finish;
    }

    if (osmetanode && osmetanode == cnode) {
        g_hash_table_iter_init(&iter,domain->ownerdomains);
        while (g_hash_table_iter_next(&iter,NULL,&gp)) {
            odomain = (cn_ownerdomain_t *)gp;
            gw_list_foreach(odomain->nodes,cur,tnode) {
                __cnc_domain_setup_node_osmeta_flows(osmetanode,tnode);
            }
        }
    }
    else if (osmetanode) {
        __cnc_domain_setup_node_osmeta_flows(osmetanode,cnode);
    }

    cn_obj_unlock(osmetanode);
    cn_obj_unlock(omport);
    cn_obj_unlock(omsw);
finish:
    cn_obj_unlock(cnode);
    cn_obj_unlock(cport);
    cn_obj_unlock(csw);
}

void cnc_port_add(mul_switch_t *sw, mul_port_t *port) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
    cn_port_t *cport = MUL_PRIV_PORT(port);
    cn_obj_lock(csw);
    cn_obj_lock(cport);
    int rc;
    cn_node_t *node = NULL;

    c_log_debug("Capnet Switch 0x%llx port add %hd",
        (unsigned long long)(sw->dpid),port->port_no);

    cport->mul_switch = sw;
    c_log_debug("Setting mul switch to: %#0lx", (unsigned long) sw);
    cport->mul_port = port;

    // We get the node + port metadata from the metadata service
    rc = metadata_get_port_info(csw, cport);
    if (rc != 0) { 
        cncwarn("couldn't get metadata for port %d, switch 0x%lx"
                " (adding to pending list)\n",
                port->port_no,sw->dpid);
        rc = metadata_set_port_pending(csw,port->port_no);
        if (rc != 0) {
            cncerr("couldn't add port %d, switch %lx to pending list port!\n",
                   port->port_no,sw->dpid);
        }
        goto finish;
    }

    cport->csw = csw;
    RHOLD_OBJ(csw, cport);

    if (cport->role == CN_PORT_ROLE_NODE) {
        if (cn_node_new(&node) != 0) {
            cncerr("couldn't alloc node");
            goto finish;
        }
        cn_obj_lock(node);
        rc = metadata_get_node_info(csw, cport, node);
        if (rc != 0) {
            cncerr("couldn't get metatdata for node");
            goto finish;
        }

        cport->nodes = g_list_prepend(cport->nodes, node);
        RHOLD_OBJ(node, cport);

        node->cn_port = cport;
        RHOLD_OBJ(cport, node);

        cnc_switch_insert_node(csw, node);
        RHOLD_OBJ(node, csw);
        
        /*
         * NB: for now, assume all this domain stuff is done with the
         * domain_lock held!  It is sort of too bad, but it is the
         * easiest way to guarantee lock safety.
         *
         * (XXX: the part where it is still unsafe is if the uplink
         * ports change on any switch involved, because those don't go
         * through the domain_lock to get removed... only their switch
         * lock.  We make this safer by at least having an uplink port
         * number... but of course the switch could go out from under
         * us, too!  However, before the switch data structures are
         * destroyed, the domain_lock will have to be taken... so I
         * guess maybe it's all good.  Sigh.)
         */
        c_wr_lock(&domain_lock);

    dom_insert_again:
        rc = __cnc_domain_insert_node(node);
        if (rc) {
            if (node->role == CN_NODE_ROLE_MASTER) {
                cncwarn("could not add node %s to domain %s; demoting it to"
                        " regular node and trying again!\n",
                        node->id,node->domainid);
                node->role = CN_NODE_ROLE_NODE;
                goto dom_insert_again;
            }
            else {
                cncerr("could not add node %s to domain %s; leaving port"
                       " info here; but this node cannot be used for flows!\n",
                        node->id,node->domainid);
                //cn_node_free(node);
                node->role = CN_NODE_ROLE_UNKNOWN;
            }
        }

        /* Handle DHCP. */
        __cnc_domain_setup_dhcp_flows(csw,cport,node);

        /* Handle OpenStack metadata service. */
        __cnc_domain_setup_osmeta_flows(csw,cport,node);

        /* Release the domain_lock. */
        c_wr_unlock(&domain_lock);

        /* Set up uplink->port flows. */
        cnc_port_setup_uplink_flows(csw,cport,node);
    }
    else if (cport->role == CN_PORT_ROLE_UPLINK) {
        csw->uplink = cport;
        csw->uplink_port_no = cport->mul_port->port_no;
        RHOLD_OBJ(cport,csw);
    }

finish:
    if (node)
	cn_obj_unlock(node);
    cn_obj_unlock(cport);
    cn_obj_unlock(csw);

    return;
}

/**
 * The metadata service for a switch calls this function to notify us
 * when information about a port has become available.  This function
 * grabs the switch lock and adds the port and its node(s).
 *
 * The metadata service must call this function without any of its locks
 * held -- and it must remove the dpid/port_no from its pending list --
 * why?  Because if we can't find it, we might put it right back on the
 * pending list :).
 */
int cnc_port_pending_notify(uint64_t dpid,uint16_t port_no) {
    mul_switch_t *mul_switch;
    mul_port_t *port,*fport = NULL;
    GSList *cur = NULL;
    int rc = -1;

    mul_switch = c_app_switch_get_with_id(dpid);
    if (!mul_switch) {
        cncwarn("switch %lx must have disappeared!\n",dpid);
        c_app_switch_put(mul_switch);
        return -1;
    }

    /* Hate to do this, but we have to lock the switch, because that's
     * what mul does before calling the port_add callback.  Maybe I
     * don't have a good reason for hating, but since I don't grok their
     * locking much, I'm a bit loath...
     */
    c_wr_lock(&mul_switch->lock);

    /* Lookup the port. MUL doesn't expose this as a public function. */
    gw_slist_foreach(mul_switch->port_list,cur,port) {
        if (port->port_no == port_no) {
            fport = port;
            break;
        }
    }
    if (!fport) {
        cncwarn("switch %lx port %u must have disappeared!\n",dpid,port_no);
        rc = -1;
        goto out;
    }

    /* Ok, we have the switch, port, and locks -- hit the callback. */
    cnc_port_add(mul_switch,fport);

    rc = 0;

 out:
    c_wr_unlock(&mul_switch->lock);
    c_app_switch_put(mul_switch);

    return rc;
}

void cnc_port_del(mul_switch_t *sw,mul_port_t *port) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
    cn_port_t *cport = MUL_PRIV_PORT(port);

    c_log_debug("Capnet Switch 0x%llx port del %hd",
                (unsigned long long)(sw->dpid),port->port_no);

    /* XXX: delete all capability flows associated with this port */
    /* For now, this is done in cnc_priv_port_free -> cnc_port_free. */
    ;

    /* XXX: is this even remotely right? */
    //if (!cptr_is_null(cport->node->cptr))
    //    cn_delete(csw->principal,cport->node->cptr);

    /* Remove the node. */
    //cnc_switch_remove_node(csw,cport->node);
}

int cnc_priv_port_alloc(void **port_ptr) {
    cn_port_t **port = (cn_port_t **) port_ptr;
    int ret = cn_port_new(port);
    RHOLD_OBJ(*port, MUL_OWNER);
    return ret;
}

void cnc_priv_port_free(void *priv) {
    cn_port_t *cport;
    
    assert(priv != NULL && "MUL asked us to free a NULL port!");

    cport = (cn_port_t *)priv;

    if (cport->role == CN_PORT_ROLE_UPLINK) {
        cport->csw->uplink = NULL;
        cport->csw->uplink_port_no = 0;
        RPUTs_OBJ(cport,cport->csw);
    }

    RPUTs_OBJ(priv, MUL_OWNER);
}

static void cnc_arp_responder(mul_switch_t *sw, struct flow *fl, uint32_t inport,
                              uint32_t buffer_id, uint8_t *raw, size_t pkt_len) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
    cn_obj_lock(csw);
    struct of_pkt_out_params outparams;
    struct mul_act_mdata mdata;
    struct ether_header *ehdr,*o_ehdr;
    struct ether_arp *ahdr,*o_ahdr;
    uint8_t *outbuf = NULL;
    cn_node_t *node,*srcnode;
    size_t o_pktlen;
    uint32_t addr;
    uint32_t outport;
    struct in_addr ia;
    int inport_is_uplink = 0;

    ehdr = (struct ether_header *)raw;
    ahdr = (struct ether_arp *)(raw + sizeof(*ehdr));

    /* We only answer ipv4 requests, obviously. */
    if (ahdr->ea_hdr.ar_op != htons(ARPOP_REQUEST) 
        && ahdr->ea_hdr.ar_pro != ETHERTYPE_IP)
        goto finish;

    /*
     * NB: if the ARP request came from an uplink port; just answer it
     * -- IFF there's a valid flow from request src mac to one of our
     * local nodes.
     */
    inport_is_uplink = (csw->uplink && csw->uplink_port_no == inport);

    /*
     * Find at least one flow from this dl_src to dl_dst; if that
     * exists, we can reply.
     */
    srcnode = cnc_switch_lookup_node_by_mac(csw,fl->dl_src);
    if (!srcnode && !inport_is_uplink) {
        cncdbg(6,LA_CTL,LF_CTL,
               "no node with mac "CNCPRIxMAC"; ignoring arp request\n",
               CNCPRIxMACARGS(fl->dl_src));
        goto finish;
    }

    /* If we can't find the dstip, we're done. */
    memcpy(&addr,ahdr->arp_tpa,sizeof(addr));
    node = cnc_switch_lookup_node_by_ipv4(csw,addr);
    if (!node) {
        if (csw->uplink && !inport_is_uplink) {
            /*
             * If we have an uplink port (and if this packet didn't come
             * on an uplink!), send the packet on that.
             */
            cn_obj_lock(csw->uplink);
            outport = csw->uplink->mul_port->port_no;
            cn_obj_unlock(csw->uplink);
            
            mul_app_act_alloc(&mdata);
            mdata.only_acts = true;
            mul_app_act_set_ctors(&mdata,sw->dpid);
            mul_app_action_output(&mdata,outport);
            outparams.buffer_id = buffer_id;
            outparams.in_port = inport;
            outparams.action_list = mdata.act_base;
            outparams.action_len = mul_app_act_len(&mdata);
            outparams.data_len = pkt_len;
            outparams.data = raw;

            mul_app_send_pkt_out(CNC_APP_NAME,sw->dpid,&outparams);

            mul_app_act_free(&mdata);

            cncdbg(8,LA_CTL,LF_CTL,
                   "forwarded non-local ARP request to uplink %d\n",outport);

            goto finish;
        }
        else {
            ia.s_addr = addr;
            cncdbg(8,LA_CTL,LF_CTL,
                   "failed to resolve IP %s locally, and no uplink\n",
                   inet_ntoa(ia));
            goto finish;
        }
    }

    cn_obj_lock(node);

    if (srcnode) {
        cn_obj_lock(srcnode);
        if (!g_hash_table_lookup(srcnode->node_table, node)) {
            cncerr("no flow caps from %s/"CNCPRIxMAC" to %s/"CNCPRIxMAC";"
                   " ignoring arp request\n",
                   srcnode->id,CNCPRIxMACARGS(srcnode->mac),
                   node->id,CNCPRIxMACARGS(node->mac));
            cn_obj_unlock(srcnode);
            goto finish1;
        }
        cn_obj_unlock(srcnode);
    }

    cncdbg(8,LA_CTL,LF_CTL,"resolved IP %s to mac "CNCPRIxMAC" node %s\n",
       inet_ntoa(node->ipv4.addr),CNCPRIxMACARGS(node->mac),node->id);

    o_pktlen = sizeof(*o_ehdr) + sizeof(*o_ahdr);
    outbuf = calloc(1,o_pktlen);
    o_ehdr = (struct ether_header *)outbuf;
    o_ahdr = (struct ether_arp *)(outbuf + sizeof(*o_ehdr));

    memcpy(o_ehdr->ether_shost,node->mac,ETH_ALEN);
    memcpy(o_ehdr->ether_dhost,fl->dl_src,ETH_ALEN);
    o_ehdr->ether_type = ehdr->ether_type;

    memcpy(&o_ahdr->ea_hdr,&ahdr->ea_hdr,sizeof(o_ahdr->ea_hdr));
    o_ahdr->ea_hdr.ar_op = htons(ARPOP_REPLY);
    memcpy(o_ahdr->arp_sha,node->mac,sizeof(o_ahdr->arp_sha));
    memcpy(o_ahdr->arp_spa,&addr,sizeof(addr));
    memcpy(o_ahdr->arp_tha,ahdr->arp_sha,sizeof(o_ahdr->arp_tha));
    memcpy(o_ahdr->arp_tpa,ahdr->arp_spa,sizeof(o_ahdr->arp_tpa));

    mul_app_act_alloc(&mdata);
    mdata.only_acts = true;
    mul_app_act_set_ctors(&mdata,sw->dpid);
    mul_app_action_output(&mdata,inport);
    outparams.buffer_id = buffer_id;
    outparams.in_port = node->cn_port->mul_port->port_no; //inport;
    outparams.action_list = mdata.act_base;
    outparams.action_len = mul_app_act_len(&mdata);
    outparams.data_len = o_pktlen;
    outparams.data = outbuf;

    mul_app_send_pkt_out(CNC_APP_NAME,sw->dpid,&outparams);

    mul_app_act_free(&mdata);
    free(outbuf);

finish1:
    cn_obj_unlock(node);
finish:
    cn_obj_unlock(csw);

    return;
}

static void cnc_dhcp_discovery_handler(mul_switch_t *sw,struct flow *fl,
                                       uint32_t inport,cn_node_t *in_node,
                                       uint32_t buffer_id,uint8_t *raw,
                                       size_t pkt_len) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
    cn_obj_lock(csw);
    cn_obj_lock(in_node);

    struct of_pkt_out_params outparams;
    struct mul_act_mdata mdata;
    struct ether_header *o_ehdr;
    uint8_t *outbuf = NULL;
    cn_node_t *node,*srcnode;
    int32_t outport;
    int inport_is_uplink = 0;
    cn_node_t *dhcpnode;

    /* Ignore DHCP discovery messages on the uplink. */
    if (csw->uplink && csw->uplink_port_no == inport)
        goto finish;

    /*
     * Find the dhcp server for in_node's domain.  If it is on this
     * switch, send the packet out directly to that node.  If it is on
     * another switch, change the dst mac and send on the uplink port.
     */
    dhcpnode = cnc_domain_lookup_dhcp_server(in_node->domainid);
    if (!dhcpnode) {
        cncwarn("no dhcp server for domain %s; cannot answer node %s DISCOVERY\n",
                in_node->domainid,in_node->name);
        goto finish;
    }

    cn_obj_lock(dhcpnode);
    cn_obj_lock(dhcpnode->cn_port);

    if (dhcpnode->cn_port->csw == csw) {
        outport = dhcpnode->cn_port->mul_port->port_no;
    }
    else if (csw->uplink && csw->uplink_port_no > 0)
        outport = csw->uplink_port_no;
    else {
        cncwarn("could not forward DHCP discovery from node %s to server %s;"
                " node switch has no uplink\n",in_node->name,dhcpnode->name);
        goto finish1;
    }

    cncdbg(8,LA_CTL,LF_CTL,
           "forwarding dhcp discovery from node %s to domain %s dhcp server %s\n",
           in_node->name,in_node->domainid,dhcpnode->name);

    outbuf = calloc(1,pkt_len);
    o_ehdr = (struct ether_header *)outbuf;

    memcpy(outbuf,raw,pkt_len);
    memcpy(o_ehdr->ether_dhost,dhcpnode->mac,ETH_ALEN);

    mul_app_act_alloc(&mdata);
    mdata.only_acts = true;
    mul_app_act_set_ctors(&mdata,sw->dpid);
    mul_app_action_output(&mdata,outport);
    outparams.buffer_id = buffer_id;
    outparams.in_port = inport; //node->cn_port->mul_port->port_no;
    outparams.action_list = mdata.act_base;
    outparams.action_len = mul_app_act_len(&mdata);
    outparams.data_len = pkt_len;
    outparams.data = outbuf;

    mul_app_send_pkt_out(CNC_APP_NAME,sw->dpid,&outparams);

    mul_app_act_free(&mdata);
    free(outbuf);

finish1:
    cn_obj_unlock(dhcpnode);
    cn_obj_unlock(dhcpnode->cn_port);
finish:
    cn_obj_unlock(csw);
    cn_obj_unlock(in_node);

    return;
}

/* The length of the size field in a capability packet */
#define CAP_SIZE_LEN 2
STATIC_ASSERT(sizeof(uint16_t) == CAP_SIZE_LEN, cap_size_fits_in_uint16_t);
STATIC_ASSERT(CAP_SIZE_LEN <= (ETH_ZLEN - ETHER_HDR_LEN), 
              cap_size_always_fits_in_payload);

static const size_t MUL_ETHER_MIN_LEN = ETHER_MIN_LEN - ETHER_CRC_LEN;
static const size_t MUL_ETHER_MAX_LEN = ETHER_MAX_LEN - ETHER_CRC_LEN;

static inline bool ETH_VALID(size_t len) {
    return !((len < MUL_ETHER_MIN_LEN) || (len > MUL_ETHER_MAX_LEN));
}

static inline uint8_t * ETH_PAYLOAD(uint8_t *raw) { return raw + ETHER_HDR_LEN; }
static inline size_t ETH_PAYLOAD_SIZE(size_t len) { return len - ETHER_HDR_LEN; }

static inline uint8_t * CAP_PAYLOAD(uint8_t *raw) { 
    return ETH_PAYLOAD(raw) + CAP_SIZE_LEN; 
}
static inline size_t CAP_PAYLOAD_SIZE(uint8_t *eth_payload) {
    return ntohs(*((uint16_t *) eth_payload));
}

static int cnc_cap_responder_dispatch(cn_node_t *sender, 
                                       uint8_t *msg, size_t msg_len,
                                       uint8_t **out_payload, size_t *out_payload_len) {
    cn_dispatch_result_t * result = NULL;
    cn_obj_lock(sender);
    int res = cn_dispatch(sender->principal, &result, msg, msg_len, &sender->cache);
    if (res == EWASREPLY) {
        cncwarn("[cap] No actual dispatch result message was a CPReply\n");
        goto finish;
    } else if (res != 0) {
        cncerr("[cap] cn_dispatch failed with code: %d\n", res);
        res = -1;
        goto finish;
    }
    assert(result != NULL && "cn_dispatch succeeded, but no result returned.");

    *out_payload = NULL;
    res = cn_dispatch_result_reply_bytes(result, out_payload, out_payload_len);
    if (res != 0) {
        cncerr("[cap] couldn't serialize result to bytes: %d\n", res);
        res = -1;
        goto finish;
    }
    assert(*out_payload != NULL 
            && "cn_dispatch_result_bytes code OK, but no output");

    res = 0;

finish:
    cn_obj_unlock(sender);
    return res;
}

static int __cn_build_cap_packet(uint8_t *payload_bytes, size_t payload_len,
                                 uint8_t src_addr[ETHER_ADDR_LEN],
                                 uint8_t dst_addr[ETHER_ADDR_LEN],
                                 uint8_t **o_packet_bytes, size_t *o_packet_len) {
    if (payload_len > (ETHERMTU - CAP_SIZE_LEN)) {
        cncerr("[cap] !!! payload too large to fit in mtu: %zu\n", payload_len);
        return -1;
    }

    /* Check if we need to pad the result packet's payload */
    size_t padding = 0;
    if (payload_len < ETH_ZLEN - CAP_SIZE_LEN - ETHER_HDR_LEN) {
        padding = ETH_ZLEN - CAP_SIZE_LEN - ETHER_HDR_LEN - payload_len;
    }
#ifndef DEBUG
    if (padding > 0) { assert(payload_len < ETH_ZLEN
                              && "Adding padding when not needed."); }
#endif

    c_log_debug("Result payload: %zu", payload_len);
    size_t out_pkt_len = ( ETHER_HDR_LEN 
                         + CAP_SIZE_LEN + payload_len + padding 
    //                     + ETHER_CRC_LEN
                         );
    c_log_debug("Result payload: %zu, padding: %zu, out: %zu", 
                payload_len, padding, out_pkt_len);
    assert(ETH_VALID(out_pkt_len) && "Bad out_pkt_len");
    uint8_t * out_pkt = malloc(out_pkt_len);
    if (out_pkt == NULL) {
        cncerr("[cap] Couldn't alloc out_pkt\n");
        return -1;
    }

    c_log_debug("[cap] building packet");

    struct ether_header * out_pkt_hdr = (struct ether_header *) out_pkt;
    memcpy(out_pkt_hdr->ether_dhost, src_addr, ETHER_ADDR_LEN);
    memcpy(out_pkt_hdr->ether_shost, dst_addr, ETHER_ADDR_LEN);
    out_pkt_hdr->ether_type = htons(ETH_P_CAP);

    /* At this point, we know that result_payload fits in an MTU - CAP_SIZE_LEN, 
     * so this is OK. */
    uint16_t network_result_payload_len = htons((uint16_t) payload_len);
    memcpy(out_pkt + ETHER_HDR_LEN, &network_result_payload_len, CAP_SIZE_LEN);

    memcpy(out_pkt + ETHER_HDR_LEN + CAP_SIZE_LEN, payload_bytes, payload_len);

    /* If needed, pad the frame. */
    if (padding) {
        memset(out_pkt + ETHER_HDR_LEN + CAP_SIZE_LEN + payload_len, 
               0x0, padding);
    }

    /* Calculate the CRC of the new ethernet frame */
    //crc_t crc = crc_init();
    //crc = crc_update(crc, out_pkt, out_pkt_len - ETHER_CRC_LEN);
    //crc = crc_finalize(crc);

    //STATIC_ASSERT(sizeof(uint32_t) == ETHER_CRC_LEN, uint32_t_is_ether_crc_len);
    //uint32_t network_crc = htonl((uint32_t) crc);
    //memcpy(out_pkt + (out_pkt_len - ETHER_CRC_LEN), &network_crc, ETHER_CRC_LEN);

    *o_packet_bytes = out_pkt;
    *o_packet_len = out_pkt_len;
    return 0;
}

/* If 'buffer_id' is NULL, the OFP_NO_BUFFER will be used */
static void __cnc_send_packet(uint64_t sw_dpid, uint32_t port, 
                              uint32_t * buffer_id,
                              uint8_t *packet, size_t packet_len) {
    struct of_pkt_out_params pkt_out_params;
    pkt_out_params.buffer_id = buffer_id ? *buffer_id : OFP_NO_BUFFER;
    pkt_out_params.in_port = OFPP131_CONTROLLER;

    mul_act_mdata_t act;
    mul_app_act_alloc(&act);
    act.only_acts = true;
    mul_app_act_set_ctors(&act, sw_dpid);
    mul_app_action_output(&act, port);

    pkt_out_params.action_len = mul_app_act_len(&act);
    pkt_out_params.action_list = act.act_base;
    pkt_out_params.data_len = packet_len;
    pkt_out_params.data = packet;

    mul_app_send_pkt_out(CNC_APP_NAME, sw_dpid, &pkt_out_params);

    mul_app_act_free(&act);
}

struct cnc_send_notify_thread_args {
    uint64_t uuid;
    cn_node_t *dest;
    uint8_t *packet; 
    size_t packet_len;
};

static void * __cnc_send_notify_thread(void * __args) {
    struct cnc_send_notify_thread_args * args = 
        (struct cnc_send_notify_thread_args *) __args;

    cn_obj_lock(args->dest);
    struct cn_notify_wait * w = g_hash_table_lookup(args->dest->notify_wait, &args->uuid);
    cn_obj_unlock(args->dest);
    if (w == NULL) {
        cn_abort("Could not find notify_wait given uuid %#lx", args->uuid);
    }

    // In seconds
    long sleep_duration = 1;

    struct timespec abs_sleep = {
        .tv_sec = 0,
        .tv_nsec = 0,
    };

    pthread_mutex_lock(&w->cond_mutex);

    while (true) {
        cn_obj_lock(args->dest);
        cn_port_t * node_port = args->dest->cn_port;
        cn_obj_lock(node_port);
        c_wr_lock(&node_port->mul_switch->lock);
        // XXX: Port locking? They don't have a ->lock member. I'm assuming
        // locking just the switch is OK.
        c_log_debug("Sending notification");
        __cnc_send_packet(node_port->mul_switch->dpid, 
                          node_port->mul_port->port_no, NULL,
                          args->packet, args->packet_len);
        c_log_debug("notification sent");
        c_wr_unlock(&node_port->mul_switch->lock);
        cn_obj_unlock(node_port);
        cn_obj_unlock(args->dest);

        abs_sleep.tv_sec = time(NULL);
        if (abs_sleep.tv_sec == (time_t) -1) {
            cn_abort("Coudln't get current time");
        }
        abs_sleep.tv_sec += sleep_duration;
        int res = pthread_cond_timedwait(&w->cond, &w->cond_mutex, &abs_sleep);
        if (res == 0) {
            break;
        } else if (res == ETIMEDOUT) {
            cncwarn("Timed out waiting for notify CPReply for %#lx. Resending.\n",
                    args->uuid);
            sleep_duration *= 2;
            cncwarn("New timeout: %ld\n", sleep_duration);
            continue;
        } else {
            cn_abort("pthread_cond_timedwait threw error: %d", res);
        }
    }

    c_log_debug("got CPReply for notification!");

    pthread_mutex_unlock(&w->cond_mutex);

    cn_obj_lock(args->dest);
    g_hash_table_remove(args->dest->notify_wait, &args->uuid);
    cn_obj_unlock(args->dest);

    cn_notify_wait_destroy(w);
    free(w);
    free(args);

    return 0;
}


int cnc_send_notify(cn_node_t *dest,
                    uint64_t for_uuid,
                    cn_dispatch_result_t *result) {

    int res = -1;

    uint8_t *msg = NULL;
    size_t msg_len = 0;
    if (cn_dispatch_result_notify_bytes(for_uuid, result, &msg, &msg_len) != 0) {
        cncerr("Couldn't generate byte representation of message\n");
        return -1;
    }

    uint8_t src_addr[ETHER_ADDR_LEN] = {0};

    cn_obj_lock(dest);

    uint8_t * msg_packet = NULL;
    size_t msg_packet_len;
    if (__cn_build_cap_packet(msg, msg_len, src_addr, dest->mac, 
                              &msg_packet, &msg_packet_len) != 0) {
        res = -1;
        free(msg);
        goto finish;
    }

    free(msg);

    struct cn_notify_wait *wait = malloc(sizeof(*wait));
    if (wait == NULL) {
        res = -1;
        goto finish;
    }
    
    cn_notify_wait_init(wait);

    struct cnc_send_notify_thread_args *notify_thread_args = 
        malloc(sizeof(*notify_thread_args));
    if (notify_thread_args == NULL) {
        cn_notify_wait_destroy(wait);
        free(wait);
        res = -1;
        goto finish;
    }

    notify_thread_args->uuid = result->request.uuid;
    notify_thread_args->dest = dest;
    notify_thread_args->packet = msg_packet; 
    notify_thread_args->packet_len = msg_packet_len; 

    c_log_debug("inserting with uuid: %#lx", result->request.uuid);
    g_hash_table_insert(dest->notify_wait, &notify_thread_args->uuid, wait);

    /* Unlock the node so we can receive the node reply. */
    cn_obj_unlock(dest);

    pthread_t thread;
    res = pthread_create(&thread, NULL, __cnc_send_notify_thread, notify_thread_args);
    if (res != 0) {
        cncerr("Couldn't spawn cnc_send_notify thread\n");
        return -1;
    }

    return 0;

finish:
    cn_obj_unlock(dest);
    return res;
}

static void cnc_cap_responder(mul_switch_t *sw, cn_switch_t *csw,
                              struct flow *fl, uint32_t inport,
                              cn_node_t *sending_node,
                              uint32_t buffer_id, 
                              uint8_t *raw, size_t pkt_len) {
    if (!ETH_VALID(pkt_len)) {
        cncerr("[cap] packet is not a valid ethernet frame: size %zu\n", pkt_len);
        return;
    }
    struct ether_header * ehdr = (struct ether_header *) raw;
    if (ehdr->ether_type != htons(ETH_P_CAP)) {
        cncerr("[cap] packet has wrong type: %#0hx\n", ntohs(ehdr->ether_type));
        return;
    }

    /* protobuf-c needs the exact buffer size, so extract the size field of
     * the capability message so we know how much padding there is. */
    size_t payload_size = ETH_PAYLOAD_SIZE(pkt_len);
    size_t cap_size = CAP_PAYLOAD_SIZE(ETH_PAYLOAD(raw));

    c_log_debug("Payload: %zu, Cap Payload: %zu", payload_size, cap_size);

    if (cap_size > (payload_size - CAP_SIZE_LEN)) {
        cncerr("[cap] reported message size greater than payload size\n");
        return;
    }

    uint8_t * cap_payload = CAP_PAYLOAD(raw);


#ifndef NDEBUG
    char __buf[(ETHERMTU * 2)+ 3];
    __buf[0] = '|';
    for (size_t q = 0; q < cap_size; q++) {
        sprintf(__buf + 1 + (2 * q), "%02hhx", cap_payload[q]);
    }
    __buf[1 + (cap_size * 2)] = '|';
    __buf[1 + (cap_size * 2) + 1] = '\0';
    c_log_debug("Payload: %s", __buf);
#endif

    uint8_t * result_payload = NULL;
    size_t result_payload_len;

    cn_obj_lock(sending_node);

    int res = cnc_cap_responder_dispatch(sending_node, 
                                         cap_payload, cap_size,
                                         &result_payload, &result_payload_len);
    if (res != 0) {
        cncerr("[cap] Couldn't dispatch request\n");
        goto finish;
    }

    uint8_t * out_packet = NULL;
    size_t out_packet_len;

    if (__cn_build_cap_packet(result_payload, result_payload_len,
                              ehdr->ether_dhost, ehdr->ether_shost,
                              &out_packet, &out_packet_len) != 0) {
        free(result_payload);
        res = -1;
        goto finish;
    }

    c_log_debug("[cap] building packet send");

    cn_obj_lock(sending_node->cn_port);
    __cnc_send_packet(sw->dpid, sending_node->cn_port->mul_port->port_no,
                      &buffer_id,
                      out_packet, out_packet_len);
    cn_obj_unlock(sending_node->cn_port);

    free(out_packet);

finish:
    cn_obj_unlock(sending_node);
    return;
}

void cnc_packet_in(mul_switch_t *sw,struct flow *fl,uint32_t inport,
		   uint32_t buffer_id,uint8_t *raw,size_t pkt_len) {
    cn_switch_t *csw = MUL_PRIV_SWITCH(sw);
    char *buf;
    cn_node_t *in_node;
    cn_node_t *out_node;

    if (is_zero_ether_addr(fl->dl_src) || 
        is_zero_ether_addr(fl->dl_dst) ||
        is_multicast_ether_addr(fl->dl_src) || 
        is_broadcast_ether_addr(fl->dl_src)) {
        c_log_err("%s: Invalid src/dst mac addr",FN);
        return;
    }

    /* Fire off our ARP responder... */
    if (is_broadcast_ether_addr(fl->dl_dst) && fl->dl_type == htons(ETHERTYPE_ARP))
        return cnc_arp_responder(sw,fl,inport,buffer_id,raw,pkt_len);

    in_node = cnc_switch_lookup_node_by_mac(csw,fl->dl_src);

    /* Fire off cap responder */
    if (in_node && fl->dl_type == htons(ETH_P_CAP)) {
        return cnc_cap_responder(sw, csw, fl, inport, in_node, 
                                 buffer_id, raw, pkt_len);
    }

    /* Fire off our DHCP discovery handler... */
    if (in_node && is_broadcast_ether_addr(fl->dl_dst)
        && fl->dl_type == htons(0x0800) && fl->nw_proto == 17
        && fl->tp_src == htons(68) && fl->tp_dst == htons(67))
        return cnc_dhcp_discovery_handler(sw,fl,inport,in_node, 
                                          buffer_id,raw,pkt_len);

    out_node = cnc_switch_lookup_node_by_mac(csw,fl->dl_dst);

    if (in_node) { cn_obj_lock(in_node); }
    if (out_node) { cn_obj_lock(out_node); }

    buf = of_dump_flow(fl,0);
    if (buf) {
        c_log_info("Packet in from 0x%lx (%s -> %s):\n  %s",sw->dpid,
               (in_node) ? in_node->cn_port->id : "?",
               (out_node) ? out_node->cn_port->id : "?",buf);
        free(buf);
    }
    else
        c_log_err("Packet in from 0x%lx (%s -> %s): unprintable!",sw->dpid,
              (in_node) ? in_node->cn_port->id : "?",
              (out_node) ? out_node->cn_port->id : "?");

    if (!in_node) {
        c_log_err("unknown src mac addr!");
        goto finish;
    }
    cn_obj_lock(in_node->cn_port);
    if (in_node->cn_port->mul_port->port_no != inport) {
        c_log_err("spoofed mac on in_port %u does not match its mac's port %hu",
              inport,in_node->cn_port->mul_port->port_no);
        goto finish1;
    }
    if (!out_node) {
        c_log_err("unknown dst mac addr!");
        goto finish1;
    }

finish1:
    cn_obj_unlock(in_node->cn_port);
finish:
    if (out_node) { cn_obj_unlock(out_node); }
    if (in_node) { cn_obj_unlock(in_node); }

    return;
}

/**
 ** Flow logic.
 **/

/* Default flows: */
static void cnc_install_dfl_flows(uint64_t dpid) {
    struct flow fl;
    struct flow mask;
    struct mul_act_mdata mdata;

    int res = -1;

    memset(&fl, 0, sizeof(fl));
    of_mask_set_dc_all(&mask);

    /* Clear all entries for this switch */
    res = mul_app_send_flow_del(CNC_APP_NAME, NULL, dpid, &fl, &mask, 
                                0, 0, C_FL_ENT_NOCACHE, OFPG_ANY);

    if (res != 0) {
        cn_abort("mul_app_send_flow_del failed with error code: %d\n", res);
    }

    /* Zero DST MAC Drop */
    of_mask_set_dl_dst(&mask);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,NULL,0,0,0,
                                C_FL_PRIO_DRP,C_FL_ENT_NOCACHE);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Zero SRC MAC Drop */
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_src(&mask);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,NULL,0,0,0, 
                                C_FL_PRIO_DRP,C_FL_ENT_NOCACHE);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Broadcast SRC MAC Drop */
    memset(&fl.dl_src,0xff,OFP_ETH_ALEN);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,NULL,0,0,0,
                                C_FL_PRIO_DRP,C_FL_ENT_NOCACHE);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Send any unknown flow to app */
    memset(&fl,0,sizeof(fl));
    of_mask_set_dc_all(&mask);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,NULL,0,0,0,C_FL_PRIO_LDFL,
                                C_FL_ENT_LOCAL);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Send all DHCP broadcasts to controller. */
    memset(&fl, 0, sizeof(fl));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    of_mask_set_nw_proto(&mask);
    of_mask_set_tp_src(&mask);
    of_mask_set_tp_dst(&mask);
    memset(&fl.dl_dst,0xff,OFP_ETH_ALEN);
    fl.dl_type = htons(0x0800);
    fl.nw_proto = 17;
    fl.tp_src = htons(68);
    fl.tp_dst = htons(67);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata, dpid);
    mul_app_action_output(&mdata, 0);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,mdata.act_base,mul_app_act_len(&mdata),
                                0,0,C_FL_PRIO_EXM,
                                C_FL_ENT_GSTATS | C_FL_ENT_CTRL_LOCAL);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Send all ARP requests to controller. */
    memset(&fl, 0, sizeof(fl));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_dst(&mask);
    of_mask_set_dl_type(&mask);
    memset(&fl.dl_dst,0xff,OFP_ETH_ALEN);
    fl.dl_type = htons(ETHERTYPE_ARP);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata, dpid);
    mul_app_action_output(&mdata, 0);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,mdata.act_base,mul_app_act_len(&mdata),
                                0,0,C_FL_PRIO_EXM,
                                C_FL_ENT_GSTATS | C_FL_ENT_CTRL_LOCAL);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    /* Send all Capability Protocol packets to controller. */
    memset(&fl, 0, sizeof(fl));
    of_mask_set_dc_all(&mask);
    of_mask_set_dl_type(&mask);
    fl.dl_type = htons(ETH_P_CAP);

    mul_app_act_alloc(&mdata);
    mul_app_act_set_ctors(&mdata, dpid);
    mul_app_action_output(&mdata, 0);
    res = mul_app_send_flow_add(CNC_APP_NAME,NULL,dpid,&fl,&mask,
                                __UNK_BUFFER_ID,mdata.act_base,mul_app_act_len(&mdata),
                                0,0,C_FL_PRIO_EXM,
                                C_FL_ENT_GSTATS | C_FL_ENT_CTRL_LOCAL);

    if (res != 0) {
        cn_abort("mul_app_send_flow_add failed with error code: %d\n", res);
    }

    //memset(&fl, 0, sizeof(fl));
    //of_mask_set_dc_all(&mask);

    //of_mask_set_dl_type(&mask);
    //fl.dl_type = htons(ETH_P_CAP);

    //of_mask_set_in_port(&mask);
    //fl.in_port = 

    //struct mul_act_mdata mdata;

    //mul_app_act_alloc(&mdata);
    ////mdata.only_acts = true;
    //mul_app_act_set_ctors(&mdata, dpid);
    //mul_app_action_output(&mdata, OF_SEND_IN_PORT);

    //c_log_debug("Pushed protocol flow");
    //mul_app_send_flow_add(CNC_APP_NAME, NULL, dpid, &fl, &mask,
    //                      __UNK_BUFFER_ID, 
    //                      mdata.act_base, mul_app_act_len(&mdata),
    //                      0, 0, C_FL_PRIO_DFL+1, 0);

    //mul_app_act_free(&mdata);

    return;
}
