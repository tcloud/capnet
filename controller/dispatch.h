#ifndef __DISPATCH_H__
#define __DISPATCH_H__

#include <libcap.h>
#include <glib.h>

#include "obj_decl.h"

/* Dispatch Layer interface:
 *
 * There is a cn_dispatch_* procedure below for each message in the
 * capability protocol. Each procedure takes two arguments in addition to
 * the fields specfied in the protocol: the principal the message is associated
 * with, and a pointer to a cn_dispatch_result_t pointer. As a result of
 * the dispatched message, the result pointer will be filled with a pointer
 * to an allocated cn_dispatch_result_t struct with the result of the dispatched
 * message. When the caller is done using this structure, it should be freed
 * using the provided 'cn_dispatch_result_free(...)' function. */

typedef enum {
    CN_RP_SEND,
    CN_RP_RECV,
    CN_RP_RECV_WAIT,
    CN_NODE_RESET,
    CN_NODE_INFO,
    CN_BROKER_REGISTER,
    CN_BROKER_LOOKUP,
    CN_BROKER_LOOKUP_WAIT,
    CN_MEMBRANE_SEND,
    CN_MEMBRANE_RECV,
    CN_MEMBRANE_EXTERNAL,
    CN_MEMBRANE_CLEAR,
    CN_MEMBRANE_RECV_WAIT,
    CN_NODE_GRANT_GRANT,
    CN_NODE_GRANT_RP0,
    CN_SEALER_UNSEALER_SEAL,
    CN_SEALER_UNSEALER_UNSEAL,
    CN_SEALER_UNSEALER_RESTRICT_TO_SEAL,
    CN_SEALER_UNSEALER_RESTRICT_TO_UNSEAL,
} cn_method_t;

static const char * METHOD_NAMES[] = {
    "rp_send", 
    "rp_recv", 
    "rp_recv_wait", 
    "node_reset", 
    "node_info",
    "broker_register", 
    "broker_lookup",
    "broker_lookup_wait",
    "membrane_send",
    "membrane_recv",
    "membrane_external",
    "membrane_clear",
    "membrane_recv_wait",
    "node_grant_grant",
    "node_grant_rp0",
    "sealer_unsealer_seal",
    "sealer_unsealer_unseal",
    "sealer_unsealer_restrict_to_seal",
    "sealer_unsealer_restrict_to_unseal",
};

typedef enum {
    CN_RESULT_ITEM_CPTR,
    CN_RESULT_ITEM_INFO,
    CN_RESULT_ITEM_MSG,
} cn_dispatch_result_item_type_t;

typedef struct {
    uint8_t * data;
    size_t len;
} cn_dispatch_result_item_msg_t;

/* resulting/created cptr, and its type */
typedef struct {
    cptr_t cptr;
    cn_objtype_t type;
} cn_dispatch_result_item_cptr_t;

/* node info fields */
typedef struct {
    char * name;
    uint8_t ipv4[4];
    uint8_t mac[6];
    char * id;
    char * domain;
    char * owner;
} cn_dispatch_result_item_info_t;

typedef struct {
    cn_dispatch_result_item_type_t type;
    union {
        cn_dispatch_result_item_cptr_t cptr;
        cn_dispatch_result_item_info_t info;
        cn_dispatch_result_item_msg_t msg;
    };
} cn_dispatch_result_item_t;

typedef struct {
    struct {
        uint64_t uuid;
    } request;

    /* True -> succes, false -> failure */
    bool status;
    /* Debugging message, may be NULL if no such message */
    char * msg;
    /* result items */
    GList * items;
} cn_dispatch_result_t;

typedef struct {
    uint64_t uuid;
    cn_dispatch_result_t * result;
} cn_dispatch_result_cache_t;

/* Alloc a new dispatch result */
int cn_dispatch_result_alloc(cn_dispatch_result_t **result);

/* Initialize a dispatch result to the default values */
void cn_dispatch_result_init(cn_dispatch_result_t *result);

/* Frees the given dispatch result */
void cn_dispatch_result_free(cn_dispatch_result_t *result);

/* Free all dispatch internal datastructures, but don't actually free the given
 * result */
void cn_dispatch_result_clear(cn_dispatch_result_t *result);

/* Make the given dispatch result a failing dispatch result and set the message
 * to the string described by the given format string. */
int cn_dispatch_result_fail(cn_dispatch_result_t *result, char * fmt, ...);

int cn_dispatch_result_push_item(cn_dispatch_result_t *result,
                                 cn_dispatch_result_item_t *item);

cn_dispatch_result_item_t * cn_dispatch_result_item_alloc(void);

int cn_dispatch_result_item_init_info(cn_dispatch_result_item_t *item,
                                      char * name, 
                                      const uint8_t ipv4[4], 
                                      const uint8_t mac[6],
                                      char * id, char * domainid, char *ownerid);

void cn_dispatch_result_item_init_cptr(cn_dispatch_result_item_t *item,
                                       cptr_t cptr,
                                       cn_objtype_t type);

int cn_dispatch_result_item_init_msg(cn_dispatch_result_item_t *item,
                                     uint8_t * data,
                                     size_t len);

void cn_dispatch_result_item_clear(cn_dispatch_result_item_t *item);

/* Return a new random UUID. */
uint64_t cn_gen_uuid(void);

/* Get the byte representation of a cn_dispatch_result_t as a CPReply type
 * CPMessage. */
int cn_dispatch_result_reply_bytes(cn_dispatch_result_t *result, 
                                   uint8_t **o_bytes, size_t *o_len);

/* Get the byte representation of a cn_dispatch_result_t as a CPNotify type
 * CPMessage. This function uses the result->request.uuid as the CPMessage UUID.
 * Generally, cn_gen_uuid() should be called to obtain a new unique uuid. */
int cn_dispatch_result_notify_bytes(uint64_t for_uuid,
                                    cn_dispatch_result_t *result,
                                    uint8_t **o_bytes, size_t *o_len);

/* Initialize a new cn_dispatch_result_cache_t. Should be invoked before
 * the cache is used. */ 
void cn_dispatch_result_cache_init(cn_dispatch_result_cache_t * cache);

/* Dispatch the message stored in the 'bytes' parameter of size 'len' for
 * the given principal. The result is stored in 'result' whose byte
 * representation can be obtained using cn_dispatch_result_bytes. 
 * 
 * The 'cache' parameter can be set to NULL, if no result caching is desired. If
 * it's set to a pointer to a valid cn_dispatch_result_cache_t struct, the result
 * will be answered out of that cache, or the cache will be updated as needed.
 * */
int cn_dispatch(cn_principal_t * as, cn_dispatch_result_t **result, 
                const uint8_t * bytes, size_t len, 
                cn_dispatch_result_cache_t *cache);

typedef struct {
    cptr_t node_grant;
    //... flow stuff ...
} cn_dispatch_create_flow_args_t;

/* Corresponds to CPCreate message */
int cn_dispatch_create(cn_principal_t * as, cn_dispatch_result_t *result,
                       cn_objtype_t type, void * args);

typedef struct {
    //... flow stuff ...
} cn_dispatch_mint_flow_args_t;

/* Corresponds to CPMint message */
int cn_dispatch_mint(cn_principal_t * as, cn_dispatch_result_t *result,
                     cptr_t cptr, void * args);

int cn_dispatch_revoke(cn_principal_t * as, cn_dispatch_result_t *result, 
                       cptr_t cptr);

int cn_dispatch_delete(cn_principal_t * as, cn_dispatch_result_t *result,
                       cptr_t cptr);

int cn_dispatch_rp0(cn_principal_t * as, cn_dispatch_result_t *result);

typedef struct {
    bool has_cap;
    cptr_t cap;
    bool has_msg;
    struct cn_rp_elem_message msg;
} cn_dispatch_invoke_rp_send_args_t;

typedef struct {
    cptr_t cap;
} cn_dispatch_invoke_membrane_send_args_t;

typedef struct {
    char *service_name;
    cptr_t cap;
} cn_dispatch_invoke_broker_register_args_t;

typedef struct {
    char *service_name;
} cn_dispatch_invoke_broker_lookup_args_t;

typedef struct {
    char *service_name;
} cn_dispatch_invoke_broker_lookup_wait_args_t;

typedef struct {
    cptr_t cap;
} cn_dispatch_invoke_node_grant_grant_args_t;

typedef struct {
    cptr_t to_seal;
} cn_dispatch_invoke_sealer_unsealer_seal_args_t;

typedef struct {
    cptr_t sealed;
} cn_dispatch_invoke_sealer_unsealer_unseal_args_t;

int cn_dispatch_invoke(cn_principal_t * as, cn_dispatch_result_t *result,
                       cptr_t obj, cn_method_t method, void * args);

/* This is a utility function to help fill in a cn_dispatch_result_t structure
 * given the output of cn_rp_recv (given that it can now be non-trival). 
 * Returns 0 on success, -1 on failure. */
int cn_dispatch_util_fill_rp_recv_result(cn_dispatch_result_t *result,
                                         cptr_t recvd_cap,
                                         cn_objtype_t recvd_type,
                                         struct cn_rp_elem_message *msg,
                                         enum cn_rp_recv_result_flag flags);

#endif
