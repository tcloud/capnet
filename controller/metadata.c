#include "capnet_log.h"
#include "controller.h"
#include "metadata.h"

extern struct metadata_ops metadata_file_ops;
extern struct metadata_ops metadata_openstack_ops;

/**
 ** A metadata service provides info about switches and switch ports to
 ** the controller.  These is a generic wrapper around metadata.
 **/

struct metadata_ops *metadata_get_ops_for_switch(uint64_t dpid) {
    int rc;

    rc = metadata_file_ops.has_switch(dpid);
    if (rc == 1)
        return &metadata_file_ops;

    rc = metadata_openstack_ops.has_switch(dpid);
    if (rc == 1)
        return &metadata_openstack_ops;

    return NULL;
}

int metadata_init_switch(cn_switch_t *sw) {
    if (!sw->mops || !sw->mops->init_switch)
        return 0;
    else
        return sw->mops->init_switch(sw);
}

void metadata_fini_switch(cn_switch_t *sw) {
    if (sw->mops && sw->mops->fini_switch)
        sw->mops->fini_switch(sw);
    sw->metadata_priv = NULL;
}
    
int metadata_start_thread(cn_switch_t *sw) {
    if (!sw->mops || !sw->mops->start_thread)
        return 0;
    else
        return sw->mops->start_thread(sw);
}
    
int metadata_stop_thread(cn_switch_t *sw) {
    if (!sw->mops || !sw->mops->stop_thread)
        return 0;
    else
        return sw->mops->stop_thread(sw);
}

int metadata_get_switch_info(cn_switch_t *sw) {
    if (!sw->mops || !sw->mops->get_switch_info)
        return -1;
    else
        return sw->mops->get_switch_info(sw);
}

int metadata_get_port_info(cn_switch_t *sw, cn_port_t *port) {
    if (!sw->mops || !sw->mops->get_port_info)
        return -1;
    else
        return sw->mops->get_port_info(sw, port);
}

int metadata_get_node_info(cn_switch_t *sw, cn_port_t *port, cn_node_t *node) {
    if (!sw->mops || !sw->mops->get_node_info)
        return -1;
    else
        return sw->mops->get_node_info(sw, port, node);
}

int metadata_get_lan_info(cn_switch_t *sw,cn_port_t *port) {
    if (!sw->mops || !sw->mops->get_lan_info)
        return -1;
    else
        return sw->mops->get_lan_info(sw,port);
}

int metadata_set_port_pending(cn_switch_t *sw,uint16_t port_no) {
    if (!sw->mops || !sw->mops->set_port_pending)
        return -1;
    else
        return sw->mops->set_port_pending(sw,port_no);
}
