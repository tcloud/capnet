#include "errors.h"
#include "util.h"

static const char * ERROR_DESCs[] = {
    // EEMPTY
    "Couldn't perform operation because the datastructure was empty",
    // EBADCPTR
    "The CPTR provided was either NULL or could not be dereferenced",
    // EWASREPLY
    "The dispatched message was of the type CPReply, so no result was returned",
    // EBADRIGHTS
    "The capability does not have the rights to perform that operation",
    // EBADSEALER
    "The sealer provided is not the sealer used to seal this capability",
    // EEXISTS
    "Attempted to create an item when that item already exists",
    // ENOTEXISTS
    "The item does not exists",
    // ESKIPPED
    "The operation was skipped because it didn't need to be performed.",
};

static inline int error_index(int err) {
    return (-err) - __ERROR_BASE;
}

static inline void error_check(int err) {
    if (-err < __ERROR_BASE || -err > __ERROR_MAX) {
        cn_abort("Tried to get description of unknown error: %d", err);
    }
}

const char * cn_error_desc(int err) {
    error_check(err);
    return ERROR_DESCs[error_index(err)];
}
