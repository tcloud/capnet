#include <libcap.h>
#include "obj.h"
#include "controller.h"

#include "capnet_log.h"

static int cn_dispatch_caller_unop(struct cnode * c, void * payload) {
    if (payload != NULL) {
        struct cn_cb_payload * cbs = (struct cn_cb_payload *) payload;
        return cbs->unop(c, cbs->payload);
    }
    return 0;
}

static int cn_dispatch_caller_binop(struct cnode * ca, struct cnode *cb, 
                                    void * payload) {
    if (payload != NULL) {
        c_log_debug("Got binop payload: %#0lx", (unsigned long) payload);
        struct cn_cb_payload * cbs = (struct cn_cb_payload *) payload;
        return cbs->binop(ca, cb, cbs->payload);
    }
    return 0;
}

/* Generic delete callback that just decrefs the object the cnode contains */
int cn_default_delete_cb(struct cnode *cnode, void * payload) {
    cncinfo("[delete] (cap (principal %p) (cptr %#0lx) (obj-type %s))\n", 
            cn_cnode_principal(cnode),
            cptr2ul(cap_cnode_cptr(cnode)),
            cn_objtype_name(cn_cnode_type(cnode)));
    cn_obj_t *obj = (cn_obj_t *) cap_cnode_object(cnode);
    cn_obj_lock(obj);
    assert(cn_cnode_type(cnode) == obj->type
           && "type of cnode does not match type of object");
    cn_cnode_meta_t * meta = (cn_cnode_meta_t *) cap_cnode_metadata(cnode);
    int ret = cn_dispatch_caller_unop(cnode, payload);
    if (ret < 0) { cn_obj_unlock(obj); return ret; }
    if (meta != NULL) { cn_cnode_meta_free(meta); }
    cn_principal_t * p = cn_cnode_principal(cnode);
    cn_obj_lock(p);
    cptr_free(p->cache, cap_cnode_cptr(cnode));
    cn_obj_unlock(p);
    cn_obj_unlock(obj);
    RPUTs_OBJ(obj, cnode);
    return 0;
}

int cn_default_insert_cb(struct cnode *ca, void * payload) {
    cn_obj_t *obj = (cn_obj_t *) cap_cnode_object(ca);
    cn_obj_lock(obj);

    char * cnode_str = cn_cnode_string(ca);
    cncinfo("[default insert cb] INSERT %s\n", cnode_str);
    free(cnode_str);

    assert(cn_cnode_type(ca) == obj->type
           && "type of cnode does not match type of object");
    int ret = cn_dispatch_caller_unop(ca, payload);
    if (ret < 0) { 
        cncinfo("[default insert cb] unop failed with code: %d\n", ret);
        cn_obj_unlock(obj); 
        return ret;
    }
    RHOLD(obj, ca);
    cn_obj_unlock(obj);
    return 0;
}

/* Generic grant callback that just increfs the object the cnode contains */
int cn_default_grant_cb(struct cnode *ca, struct cnode *cb, void * payload) {
    int ret = -1;

#ifndef NDEBUG
    char * _send_cap_str = cn_cnode_string(ca);
    char * _recv_cap_str = cn_cnode_string(cb);
    c_log_debug("[default grant cb] %s -> %s", 
                _send_cap_str, _recv_cap_str);
    free(_send_cap_str);
    free(_recv_cap_str);
#endif

    cn_obj_t *obj = (cn_obj_t *) cap_cnode_object(ca);
    cn_obj_lock(obj);
    assert(cn_cnode_type(ca) == obj->type
           && "type of cnode does not match type of object");
    cn_cnode_meta_t * ca_meta = (cn_cnode_meta_t *) cap_cnode_metadata(ca);
    if (ca_meta != NULL) {
        cn_cnode_meta_t * cb_meta = NULL;
        if (cn_cnode_meta_copy(ca_meta, &cb_meta) != 0) {
            ret = -1;
            goto finish;
        }
        cap_cnode_set_metadata(cb, cb_meta);
    } else {
        cap_cnode_set_metadata(cb, NULL); // where ca_meta == NUL
    }

    ret = cn_dispatch_caller_binop(ca, cb, payload);
    if (ret < 0) { 
        cncinfo("[default grant cb] binop payload failed with code: %d\n", ret);
        goto finish; 
    }
    ret = cn_default_insert_cb(cb, NULL);

finish:
    cn_obj_unlock(obj);

#ifndef NDEBUG
    char * dest_meta = NULL;
    if (cap_cnode_metadata(cb)) {
        dest_meta = cn_cnode_meta_string((cn_cnode_meta_t *) cap_cnode_metadata(cb));
    }
    c_log_debug("[default grant cb] dest meta: %s", dest_meta ? dest_meta : "(nil)");
    if (dest_meta) { free(dest_meta); }
#endif

    return ret;
}

int cn_default_derive_dst_cb(struct cnode *ca, struct cnode *cb, void * payload) {
    cn_obj_t * obj = (cn_obj_t *) cap_cnode_object(cb);
    cn_obj_lock(obj);
    int ret = -1;
    assert(cn_cnode_type(cb) == obj->type
           && "type of cnode does not match type of object");
    cn_cnode_meta_t * ca_meta = (cn_cnode_meta_t *) cap_cnode_metadata(ca);
    cn_cnode_meta_t * cb_meta = (cn_cnode_meta_t *) cap_cnode_metadata(cb);
    if (ca_meta == NULL) { ret = 0; goto finish; }
    if (! cn_cnode_meta_is_annotated(ca_meta)) { ret = 0; goto finish; }
    if (cn_cnode_meta_is_annotated(ca_meta)
            && (cb_meta == NULL || (! cn_cnode_meta_is_annotated(cb_meta)))) {
        if (cb_meta != NULL) {
            cn_cnode_meta_free(cb_meta);
        }
        if (cn_cnode_meta_copy(ca_meta, &cb_meta) != 0) {
            ret = -1;
            goto finish;
        }
        cap_cnode_set_metadata(cb, cb_meta);
        ret = 0;
        goto finish;
    }

    cn_abort("not implemented");
    ret = -1;

finish:
    cn_obj_unlock(obj);
    return ret;
}

static bool __entry_incref(GHashTable * table, gpointer key) {
    gpointer out_key; /* XXX: Unused */
    uintptr_t value = 0;
    /* We're using the node as the flow, so look up the node capability value,
     * not the flow capability. */
    bool found = g_hash_table_lookup_extended(table, key, 
                                              &out_key, (gpointer *) &value);
    value += 1;
    /* XXX: don't need to hold a reference, because the cptr is already holding
     * a refernce to this flow. */
    g_hash_table_insert(table, key, (gpointer) value);
    return found;
}

/* Returns 'true' if the key has reached zero (and been removed) or false if
 * has just been decremented */
static bool __entry_decref(GHashTable * table, gpointer key) {
    gpointer out_key; /* XXX: Unused */
    uintptr_t value = 0;
    bool found = g_hash_table_lookup_extended(table, key, &out_key, (gpointer *) &value);
    if (! found) {
        cn_abort("Tried to decref a flow entry not in the table");
    }
    assert(value > 0 && "decref flow with zero value");
    value -= 1;
    if (value == 0) {
        if (g_hash_table_remove(table, key) != TRUE) {
            cn_abort("Delete flow failed");
        }
        return true;
    } else {
        g_hash_table_insert(table, key, (gpointer) value);
    }
    return false;
}

static int __handle_flow_grant(cn_node_t * receiver, cn_flow_t *flow) {
    assert(receiver != NULL && "granted to principal with no owner");

    cn_obj_lock(receiver);
    assert(receiver->cn_port != NULL && "granted to node not on a port");
    cn_obj_lock(flow);

    (void) __entry_incref(receiver->node_table, flow->node);
    bool found = __entry_incref(receiver->flow_table, flow);

    /* We've already provisioned this flow. */
    if (found) { 
        cncinfo("[flow grant cb] skipping flow create, already exists\n");
        goto finish;
    }

    cn_obj_lock(flow->node);

    /* Otherwise, go and install the flow between receiver and dst_node. */
    cncinfo("[flow grant cb] creating flow %s -> %s\n", receiver->id, flow->node->id);

    cnc_install_flow(receiver, flow);

    cn_obj_unlock(flow->node);

finish:
    cn_obj_unlock(flow);
    cn_obj_unlock(receiver);
    return 0;
}

int cn_delete_flow_cb(struct cnode *flow_cnode, void * payload) {
    int ret;
    assert(cn_cnode_type(flow_cnode) == CN_FLOW && "delete flow cb invoked on non-flow");
    cn_principal_t * principal = cn_cnode_principal(flow_cnode);
    cn_obj_lock(principal);
    cn_node_t * owner = (cn_node_t *) principal->owner;

    /* No need to worry about flow stuff if the owner is not a node */
    if (owner == NULL) { goto end; }

    cn_flow_t * flow = (cn_flow_t *) cn_cnode_object(flow_cnode);

    cn_obj_lock(owner);
    cn_obj_lock(flow);

    (void) __entry_decref(owner->node_table, flow->node);
    bool do_delete = __entry_decref(owner->flow_table, flow);

    if (! do_delete) { 
        cncinfo("[flow delete cb] skipping flow delete, not our last capability\n");
        goto end1;
    }

    cncinfo("[flow delete cb] deleteing flow %s -> %s\n", owner->id, flow->node->id);

    cnc_remove_flow(owner, flow);

end1:
    cn_obj_unlock(flow);
    cn_obj_unlock(owner);
end:
    cncinfo("[flow delete cb] calling default delete cb\n");
    ret = cn_default_delete_cb(flow_cnode, payload);
    cn_obj_unlock(principal);
    return ret;
}

int cn_insert_flow_cb(struct cnode *flow, void * payload) {
    int ret;
    assert(cn_cnode_type(flow) == CN_FLOW
            && "flow insert callback invoked for non-flow");
    if ((ret = cn_default_insert_cb(flow, payload)) != 0) { return ret; }

    /* XXX: We should have some kind of virtual method for applying a flow
     * when it's granted, but for now, if all methods are disabled on
     * a capability, then consider it sealed, and skip the flow granting. */
    cn_cnode_meta_t * meta = cap_cnode_metadata(flow);
    if (meta != NULL && meta->method_mask_cache == METHOD_MASK_NONE) {
        return 0; 
    }

    cn_node_t * cap_receiver = (cn_node_t *) cn_cnode_principal(flow)->owner;

    /* if we're not inserting into a node, then no need to need to 
     * worry about flow pushing */
    if (cap_receiver == NULL) { return 0; }

    cn_flow_t * cn_flow = (cn_flow_t *) cn_cnode_object(flow);
    return __handle_flow_grant(cap_receiver, cn_flow);
}

int cn_grant_flow_cb(struct cnode *src, struct cnode *dst, void * payload) {
    int ret;
    assert(cn_cnode_type(src) == CN_FLOW
           && "flow grant callback invoked for non-flow");
    if ((ret = cn_default_grant_cb(src, dst, payload)) != 0) { return ret; }

    /* XXX: We should have some kind of virtual method for applying a flow
     * when it's granted, but for now, if all methods are disabled on
     * a capability, then consider it sealed, and skip the flow granting. */
    cn_cnode_meta_t * meta = cap_cnode_metadata(dst);
    if (meta != NULL && meta->method_mask_cache == METHOD_MASK_NONE) {
        return 0; 
    }

    /* The node that owns the principal that the capability is being granted
     * into is the node who we need to install flows for. */
    cn_node_t * cap_receiver = (cn_node_t *) cn_cnode_principal(dst)->owner;

    /* if we're not granting to a node, then no need to need to worry about
     * flow pushing */
    if (cap_receiver == NULL) { return 0; }

    cn_flow_t * flow = (cn_flow_t *) cn_cnode_object(src);

    return __handle_flow_grant(cap_receiver, flow);
}
