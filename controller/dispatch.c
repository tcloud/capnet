/* for vasprintf */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdarg.h>

#include <libcap.h>

#include "capnet_cp.pb-c.h"

#include "dispatch.h"
#include "obj.h"
#include "errors.h"

#include "mul_common.h"

#include "capnet_log.h"

#define DISPATCH_OWNER ((void *) 0xd15A74beefUL)

static const uint32_t CP_PROTO_MAGIC = 0x434e5000U;
static const uint32_t CP_PROTO_V1 = 0x1U;

static const bool SUCCESS = true;
static const bool FAIL = false;

static cn_objtype_t __cn_objtype_for_CPObjType(Cp__CPObjType t) {
    switch (t) {
        case CP__CPOBJ_TYPE__CPOBJTYPE_NODE: return CN_NODE;
        case CP__CPOBJ_TYPE__CPOBJTYPE_RP: return CN_RP;
        case CP__CPOBJ_TYPE__CPOBJTYPE_MEMBRANE: return CN_MEMBRANE;
        case CP__CPOBJ_TYPE__CPOBJTYPE_FLOW: return CN_FLOW;
        case CP__CPOBJ_TYPE__CPOBJTYPE_BROKER: return CN_BROKER;
        case CP__CPOBJ_TYPE__CPOBJTYPE_NODE_GRANT: return CN_NODE_GRANT;
        case CP__CPOBJ_TYPE__CPOBJTYPE_SEALER_UNSEALER: return CN_SEALER_UNSEALER;
        default:
            cn_abort("Invalid CPObjType in message: %d", t);
    }
}

static Cp__CPObjType __CPObjType_for_cn_objtype(cn_objtype_t t) {
    switch (t) {
        case CN_NODE: return CP__CPOBJ_TYPE__CPOBJTYPE_NODE;
        case CN_RP: return CP__CPOBJ_TYPE__CPOBJTYPE_RP;
        case CN_MEMBRANE: return CP__CPOBJ_TYPE__CPOBJTYPE_MEMBRANE;
        case CN_FLOW: return CP__CPOBJ_TYPE__CPOBJTYPE_FLOW;
        case CN_BROKER: return CP__CPOBJ_TYPE__CPOBJTYPE_BROKER;
        case CN_NODE_GRANT: return CP__CPOBJ_TYPE__CPOBJTYPE_NODE_GRANT;
        case CN_SEALER_UNSEALER: return CP__CPOBJ_TYPE__CPOBJTYPE_SEALER_UNSEALER;
        default:
            cn_abort("Invalid cn_objtype_t in message: %d", t);
    }
}

static cn_method_t __cn_method_for_CPObjMethod(Cp__CPObjMethod m) {
    switch (m) {
        case CP__CPOBJ_METHOD__CPOBJMETHOD_RP_SEND:
            return CN_RP_SEND;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_RP_RECV:
            return CN_RP_RECV;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_RP_RECV_WAIT:
            return CN_RP_RECV_WAIT;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_NODE_RESET:
            return CN_NODE_RESET;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_NODE_INFO:
            return CN_NODE_INFO;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_MEMBRANE_SEND:
            return CN_MEMBRANE_SEND;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_MEMBRANE_RECV:
            return CN_MEMBRANE_RECV;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_BROKER_REGISTER:
            return CN_BROKER_REGISTER;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_BROKER_LOOKUP:
            return CN_BROKER_LOOKUP;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_BROKER_LOOKUP_WAIT:
            return CN_BROKER_LOOKUP_WAIT;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_MEMBRANE_EXTERNAL:
            return CN_MEMBRANE_EXTERNAL;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_MEMBRANE_CLEAR:
            return CN_MEMBRANE_CLEAR;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_MEMBRANE_RECV_WAIT:
            return CN_MEMBRANE_RECV_WAIT;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_NODE_GRANT_GRANT:
            return CN_NODE_GRANT_GRANT;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_NODE_GRANT_RP0:
            return CN_NODE_GRANT_RP0;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_SEALER_UNSEALER_SEAL:
            return CN_SEALER_UNSEALER_SEAL;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_SEALER_UNSEALER_UNSEAL:
            return CN_SEALER_UNSEALER_UNSEAL;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_SEALER_UNSEALER_RESTRICT_TO_SEAL:
            return CN_SEALER_UNSEALER_RESTRICT_TO_SEAL;
        case CP__CPOBJ_METHOD__CPOBJMETHOD_SEALER_UNSEALER_RESTRICT_TO_UNSEAL:
            return CN_SEALER_UNSEALER_RESTRICT_TO_UNSEAL;
        default:
            cn_abort("Invalid method type: %d", m);
    }
}

static cn_objtype_t __cn_supported_object_type(cn_method_t m) {
    switch (m) {
        case CN_RP_SEND:
        case CN_RP_RECV:
        case CN_RP_RECV_WAIT:
            return CN_RP;
        case CN_NODE_RESET:
        case CN_NODE_INFO:
            return CN_NODE;
        case CN_MEMBRANE_SEND:
        case CN_MEMBRANE_RECV:
        case CN_MEMBRANE_EXTERNAL:
        case CN_MEMBRANE_CLEAR:
        case CN_MEMBRANE_RECV_WAIT:
            return CN_MEMBRANE;
        case CN_BROKER_REGISTER:
        case CN_BROKER_LOOKUP:
        case CN_BROKER_LOOKUP_WAIT:
            return CN_BROKER;
        case CN_NODE_GRANT_GRANT:
        case CN_NODE_GRANT_RP0:
            return CN_NODE_GRANT;
        case CN_SEALER_UNSEALER_SEAL:
        case CN_SEALER_UNSEALER_UNSEAL:
        case CN_SEALER_UNSEALER_RESTRICT_TO_SEAL:
        case CN_SEALER_UNSEALER_RESTRICT_TO_UNSEAL:
            return CN_SEALER_UNSEALER;
        default:
            cn_abort("Unrecognized unregonized method: %d", m);
    }
}

void cn_dispatch_result_cache_init(cn_dispatch_result_cache_t * cache) {
    cache->result = NULL;
    cache->uuid = 0;
}

cn_dispatch_result_item_t * cn_dispatch_result_item_alloc(void) {
    return calloc(1,sizeof(cn_dispatch_result_item_t));
}

int cn_dispatch_result_push_item(cn_dispatch_result_t *result,
                                 cn_dispatch_result_item_t *item) {
    result->items = g_list_append(result->items, (gpointer) item);
    return 0;
}

STATIC_ASSERT(sizeof(((cn_dispatch_result_item_t *)NULL)->info.ipv4) == sizeof(uint32_t),
	      item_ipv4_field_32_bits);

int cn_dispatch_result_item_init_info(cn_dispatch_result_item_t *item,
                                      char * name, 
                                      const uint8_t ipv4[4], 
                                      const uint8_t mac[6],
                                      char * id, char * domainid, char *ownerid) {
    item->type = CN_RESULT_ITEM_INFO;
    item->info.name = name != NULL ? strdup(name) : NULL;
    if (name != NULL && item->info.name == NULL) {
        return -1;
    }

    uint32_t ipv4_nbo = *((uint32_t *) ipv4);
    ipv4_nbo = htonl(ipv4_nbo);
    memcpy(item->info.ipv4, &ipv4_nbo, sizeof(ipv4_nbo));
    memcpy(item->info.mac, mac, sizeof(item->info.mac));
    item->info.id = id != NULL ? strdup(id) : NULL;
    item->info.domain = domainid != NULL ? strdup(domainid) : NULL;
    item->info.owner = ownerid != NULL ? strdup(ownerid) : NULL;
    return 0;
}

void cn_dispatch_result_item_init_cptr(cn_dispatch_result_item_t *item,
                                       cptr_t cptr,
                                       cn_objtype_t type) {
    item->type = CN_RESULT_ITEM_CPTR;
    item->cptr.cptr = cptr;
    item->cptr.type = type;
}

int cn_dispatch_result_item_init_msg(cn_dispatch_result_item_t *item,
                                     uint8_t * data,
                                     size_t len) {
    item->type = CN_RESULT_ITEM_MSG;
    item->msg.data = malloc(len);
    if (item->msg.data == NULL) {
        return -1;
    }
    memcpy(item->msg.data, data, len);
    item->msg.len = len;
    return 0;
}

void cn_dispatch_result_item_clear(cn_dispatch_result_item_t *item) {
    switch (item->type) {
        case CN_RESULT_ITEM_INFO: {
            if (item->info.name) { free(item->info.name); }
            item->info.name = NULL;
            memset(item->info.ipv4, 0x0, 4);
            memset(item->info.mac, 0x0, 6);
            if (item->info.id) { free(item->info.id); }
            item->info.id = NULL;
            if (item->info.domain) { free(item->info.domain); }
            item->info.domain = NULL;
            if (item->info.owner) { free(item->info.owner); }
            item->info.owner = NULL;
        }
        break;
        case CN_RESULT_ITEM_CPTR: break;
        case CN_RESULT_ITEM_MSG: {
            if (item->msg.data) { free(item->msg.data); }
            item->msg.data = NULL;
            item->msg.len = 0;
        }
        break;
        default:
            cn_abort("Result item type not recognized: %d", item->type);
    }
}

void cn_dispatch_result_clear(cn_dispatch_result_t *result) {
    GList * l = g_list_first(result->items);
    for (; l != NULL; l = g_list_next(l)) {
        cn_dispatch_result_item_clear((cn_dispatch_result_item_t *) l->data);
        free(l->data);
    }
    g_list_free(result->items);
    result->items = NULL;
    if (result->msg) { free(result->msg); }
    cn_dispatch_result_init(result);
}

void cn_dispatch_result_free(cn_dispatch_result_t *result) {
    cn_dispatch_result_clear(result);
    free(result);
}

void cn_dispatch_result_init(cn_dispatch_result_t *result) {
    result->status = FAIL;
    result->msg = NULL;
    result->items = NULL;
}

int cn_dispatch_result_alloc(cn_dispatch_result_t **result) {
    cn_dispatch_result_t *new = malloc(sizeof(cn_dispatch_result_t));
    if (new == NULL) { return -1; }
    cn_dispatch_result_init(new);
    *result = new;
    return 0;
}

static void __cn_set_cpmessage_defaults(Cp__CPMessage *msg) {
    msg->magic = CP_PROTO_MAGIC;
    msg->version = CP_PROTO_V1;
}

static void __cn_dispatch_result_free_item(Cp__CPReplyItem *  item) {
    switch (item->result_case) {
        case CP__CPREPLY_ITEM__RESULT_RESULT_CPTR:
            free(item->result_cptr);
            break;
        case CP__CPREPLY_ITEM__RESULT_RESULT_NODE_INFO:
            free(item->result_node_info);
            break;
        case CP__CPREPLY_ITEM__RESULT_RESULT_MESSAGE:
            free(item->result_message);
            break;
        default:
            cn_abort("unrecognized CPReplyItem type: %d\n", item->result_case);
    }
}

static int __cn_dispatch_result_fill_item(cn_dispatch_result_item_t *item,
                                           Cp__CPReplyItem *reply_item) {
    switch (item->type) {
        case CN_RESULT_ITEM_CPTR: {
            Cp__CPReplyItemCptr * reply_cptr = malloc(sizeof(*reply_cptr));
            if (reply_cptr == NULL) { return -1; }
            cp__cpreply_item_cptr__init(reply_cptr);
            reply_item->result_case = CP__CPREPLY_ITEM__RESULT_RESULT_CPTR;
            reply_cptr->cptr = cptr2ul(item->cptr.cptr);
            reply_cptr->cptr_type = __CPObjType_for_cn_objtype(item->cptr.type);
            c_log_debug("cptr_result type: %d", reply_cptr->cptr_type);
            reply_item->result_cptr = reply_cptr;
        }
        break;
        case CN_RESULT_ITEM_INFO: {
            Cp__CPReplyItemNodeInfo * reply_info = malloc(sizeof(*reply_info));
            if (reply_info == NULL) { return -1; }
            cp__cpreply_item_node_info__init(reply_info);
            reply_item->result_case = CP__CPREPLY_ITEM__RESULT_RESULT_NODE_INFO;
            reply_info->name = item->info.name;
            reply_info->mac.len = sizeof(item->info.mac);
            reply_info->mac.data = item->info.mac;
            reply_info->ipv4.len = sizeof(item->info.ipv4);
            reply_info->ipv4.data = item->info.ipv4;
            reply_info->id = item->info.id;
            reply_info->domain = item->info.domain;
            reply_info->owner = item->info.owner;
            reply_item->result_node_info = reply_info;
        }
        break;
        case CN_RESULT_ITEM_MSG: {
            Cp__CPReplyItemMessage * reply_msg = malloc(sizeof(*reply_msg));
            if (reply_msg == NULL) { return -1; }
            cp__cpreply_item_message__init(reply_msg);
            reply_item->result_case = CP__CPREPLY_ITEM__RESULT_RESULT_MESSAGE;
            reply_msg->data.len = item->msg.len;
            reply_msg->data.data = item->msg.data;
            reply_item->result_message = reply_msg;
        }
        break;
        default:
            cncerr("unrecognized item type: %d\n", item->type);
            return -1;
    }
    return 0;
}

static void __cn_dispatch_result_free_reply(Cp__CPReply *reply) {
    reply->msg = NULL;
    reply->status = CP__CPSTATUS__CPSTATUS_FAILURE;

    /* Free the internal structure of any CPReplyItems */
    for (size_t i = 0; i < reply->n_items; i++) {
        __cn_dispatch_result_free_item(reply->items[i]);
    }

    if (reply->n_items > 0) {
        /* Free the actual list of CPReplyItems */
        free(reply->items[0]);
        /* Free the list of CPReplyItem pointers */
        free(reply->items);
    }

    reply->items = NULL;
}

static int __cn_dispatch_result_fill_reply(cn_dispatch_result_t *result,
                                           Cp__CPReply *reply) {
    reply->status = (result->status ? CP__CPSTATUS__CPSTATUS_SUCCESS 
                                    : CP__CPSTATUS__CPSTATUS_FAILURE);
    reply->msg = result->msg;

    guint reply_item_count = g_list_length(result->items);

    reply->n_items = reply_item_count;
    if (reply_item_count == 0) {
        return 0;
    }

    reply->items = malloc(sizeof(Cp__CPReplyItem *) * reply_item_count);
    if (reply->items == NULL) {
        cncerr("Couldn't allocate reply items list\n");
        return -1;
    }

    /* reply_items is an array of Cp_CPReplyItem structures */
    Cp__CPReplyItem * reply_items = malloc(sizeof(Cp__CPReplyItem) * reply_item_count);
    if (reply_items == NULL) {
        cncerr("Couldn't allocate reply items data list\n");
        return -1;
    }

    GList * cur = g_list_first(result->items);
    size_t i = 0;
    for (; cur != NULL; cur = g_list_next(cur), i++) {
        cp__cpreply_item__init(&reply_items[i]);
        int res = __cn_dispatch_result_fill_item((cn_dispatch_result_item_t *) cur->data,
                                                 &reply_items[i]);
        /* Even if the command failed, we need to set at least the first pointer
         * in the list, otherwise the structure won't be freed properly */
        reply->items[i] = &reply_items[i];
        if (res != 0) {
            goto abort;
        }
    }

    return 0;

abort:
    /* We only managed to set i+1 items (since it's indexed at zero). Update the
     * count, and free as normal. This is OK because we always treat the first
     * item in the items list as a pointer to an array of CPReplyItem structures. */
    reply->n_items = i + 1;
    __cn_dispatch_result_free_reply(reply);
    return -1;
}

static int __cn_cpmessage_to_bytes(Cp__CPMessage *msg,
                                uint8_t **o_bytes, size_t *o_len) {
    size_t len = cp__cpmessage__get_packed_size(msg);
    uint8_t * buf = malloc(len);
    if (buf == NULL) { return -1; }
    size_t _res = cp__cpmessage__pack(msg, buf);
    assert(_res == len && "packed different number of bytes than expected from"
                          " get_packed_size");

    *o_bytes = buf;
    *o_len = len;
    return 0;
}

uint64_t cn_gen_uuid(void) {
    uint64_t uuid;
    FILE * r = fopen("/dev/urandom", "r");
    if (r == NULL) { 
        cn_abort("Couldn't open /dev/urandom");
    }
    if (fread(&uuid, sizeof(uuid), 1, r) != 1) {
        cn_abort("Couldn't read uuid from /dev/urandom");
    }
    fclose(r);
    return uuid;
}

int cn_dispatch_result_notify_bytes(uint64_t for_uuid,
                                    cn_dispatch_result_t *result,
                                    uint8_t **o_bytes, size_t *o_len) {
    int res = -1;
    Cp__CPMessage msg = CP__CPMESSAGE__INIT;
    __cn_set_cpmessage_defaults(&msg);
    msg.uuid = result->request.uuid; 
    msg.type_case = CP__CPMESSAGE__TYPE_NOTIFY;
    
    Cp__CPNotify notify = CP__CPNOTIFY__INIT;
    notify.reply_for = for_uuid;

    msg.notify = &notify;

    Cp__CPReply reply = CP__CPREPLY__INIT;

    if (__cn_dispatch_result_fill_reply(result, &reply) != 0) {
        cncerr("Couldn't fill CPReply with cn_dispatch_result_t\n");
        return -1;
    }

    notify.reply = &reply;

    if (__cn_cpmessage_to_bytes(&msg, o_bytes, o_len) != 0) {
        res = -1;
        goto finish;
    }

    res = 0;

finish:
    __cn_dispatch_result_free_reply(&reply);
    return res;
}

int cn_dispatch_result_reply_bytes(cn_dispatch_result_t *result, 
                                   uint8_t **o_bytes, size_t *o_len) {
    int res = -1;
    /* XXX: Supposedly there's no need to free structures allocated with
     * their __INIT functions */
    Cp__CPMessage msg = CP__CPMESSAGE__INIT;
    __cn_set_cpmessage_defaults(&msg);
    msg.uuid = result->request.uuid;
    msg.type_case = CP__CPMESSAGE__TYPE_REPLY;

    Cp__CPReply reply = CP__CPREPLY__INIT;

    if (__cn_dispatch_result_fill_reply(result, &reply) != 0) {
        cncerr("Couldn't fill CPReply with cn_dispatch_result_t\n");
        return -1;
    }

    msg.reply = &reply;

    if (__cn_cpmessage_to_bytes(&msg, o_bytes, o_len) != 0) {
        cncerr("could not convert cpmessage to bytes\n");
        goto finish;
    }

    res = 0;

finish:
    __cn_dispatch_result_free_reply(&reply);
    return res;
}

int cn_dispatch_result_fail(cn_dispatch_result_t *result, char * fmt, ...) {
    int res;
    va_list l;
    va_start(l, fmt);

    result->status = false;

    if ((res = vasprintf(&result->msg, fmt, l)) < 0) {
        result->msg = NULL;
        goto fail;
    }

    res = 0;

fail:
    va_end(l);
    return res;
}

double __cn_timestamp(void) {
    struct timeval t;
    if (gettimeofday(&t, NULL) != 0) {
        cn_abort("Failed to get profiling information");
    } 
    unsigned long long stamp = (t.tv_sec * 1000000) + t.tv_usec;
    return ((double) stamp) / 1000000.0;
}

void __cn_print_profile(cn_principal_t *p, const char * opname, uint64_t uuid, const char * event) {
    printf("CONTROLLER,TIMESTAMP,%s,%p,%lx,%s,%.6f\n", 
           opname, p, uuid, event, __cn_timestamp());
}

void __cn_print_profile_create(cn_principal_t *p, cn_objtype_t t, uint64_t uuid, const char * event) {
    printf("CONTROLLER,TIMESTAMP,create,%p,%s,%lx,%s,%.6f",
           p, cn_objtype_name(t), 
           uuid, event, __cn_timestamp());
}

void __cn_print_profile_invoke(cn_principal_t *p, cptr_t cptr, 
                               cn_method_t method, 
                               uint64_t uuid, const char * event) {
    printf("CONTROLLER,TIMESTAMP,invoke,%p,%lx,%s,%lx,%s,%.6f\n", 
           (void *) p, cptr2ul(cptr), METHOD_NAMES[method], 
           uuid, event, __cn_timestamp());
}

static int __cn_handle_create_message(cn_principal_t *as, 
                                      cn_dispatch_result_t *result,
                                      Cp__CPCreate * msg) {
    cn_objtype_t type = __cn_objtype_for_CPObjType(msg->obj_type);
    __cn_print_profile_create(as, type, result->request.uuid, "start");
    int res = -1;
    switch (msg->args_case) {
        case CP__CPCREATE__ARGS_FLOW_ARGS:
            assert(type == CN_FLOW && "Passed flow args for non-flow type");
            cn_dispatch_create_flow_args_t flow_args;
            flow_args.node_grant = ul2cptr(msg->flow_args->node_grant_cptr);
            /* TODO: Add flowspec stuff here */
            res = cn_dispatch_create(as, result, type, &flow_args);
            break;
        default:
            assert(msg->args_case == CP__CPCREATE__ARGS__NOT_SET
                    && "Default case but unknown args set");
            res = cn_dispatch_create(as, result, type, NULL);
            break;
    }
    __cn_print_profile_create(as, type, result->request.uuid, "stop");
    return res;
}

static int __cn_handle_mint_message(cn_principal_t *as, 
                                    cn_dispatch_result_t *result,
                                    Cp__CPMint * msg) {
    switch (msg->args_case) {
        case CP__CPMINT__ARGS_FLOW_ARGS:
            cncwarn("Mint with flow args not implemented\n");
            return cn_dispatch_result_fail(result, "handle_mint_message with flow args not implemented.");
        default:
            assert(msg->args_case == CP__CPMINT__ARGS__NOT_SET
                   && "Default case for mint, but unknown args");
            return cn_dispatch_mint(as, result, ul2cptr(msg->cptr), NULL);
    }
}

static int __cn_handle_revoke_message(cn_principal_t *as, 
                                      cn_dispatch_result_t *result,
                                      Cp__CPRevoke * msg) {
    return cn_dispatch_revoke(as, result, ul2cptr(msg->cptr));
}

static int __cn_handle_rp0_message(cn_principal_t *as, 
                                   cn_dispatch_result_t *result,
                                   Cp__CPRP0 * msg) {
    return cn_dispatch_rp0(as, result);
}

static int __cn_handle_delete_message(cn_principal_t *as, 
                                      cn_dispatch_result_t *result,
                                      Cp__CPDelete * msg) {
    return cn_dispatch_delete(as, result, ul2cptr(msg->cptr));
}

STATIC_ASSERT(sizeof(uint64_t) == sizeof(cptr_t),
              cptr_t_is_exactly_64_bits);

static int __cn_handle_invoke_message(cn_principal_t *as, 
                                      cn_dispatch_result_t *result,
                                      Cp__CPInvoke * msg) {

    // XXX: Assuming this sanitizes for unknown methods 
    cn_method_t method = __cn_method_for_CPObjMethod(msg->method);
    int res = -1;

    __cn_print_profile_invoke(as, ul2cptr(msg->cptr), method,
                              result->request.uuid, "start");

#define ASSERT_INVOKE_ARGS(methodn, type) \
    if (msg->args_case != type) { \
        return cn_dispatch_result_fail(result, "Invoke: wrong arguments " \
                                               "applied to "methodn", wanted %d, got %d", \
                                               type, msg->args_case); \
    }

    switch (method) {
        case CN_RP_SEND: {
            c_log_debug("in rp_send invoke args");
            ASSERT_INVOKE_ARGS("rp.send", CP__CPINVOKE__ARGS_RP_SEND_ARGS)
            cn_dispatch_invoke_rp_send_args_t send_args;
            send_args.has_cap = false;
            send_args.has_msg = false;
            if (msg->rp_send_args->has_cptr) {
                send_args.has_cap = true;
                send_args.cap = ul2cptr(msg->rp_send_args->cptr);
            }
            if (msg->rp_send_args->has_msg) {
                send_args.has_msg = true;
                send_args.msg.data = msg->rp_send_args->msg.data;
                send_args.msg.len = msg->rp_send_args->msg.len;
            }
            if (! (msg->rp_send_args->has_cptr || msg->rp_send_args->has_msg)) {
                return cn_dispatch_result_fail(result, 
                                               "at least one of cptr or"
                                               " message args must be supplied");
            }
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, &send_args);
        }
        break;
        case CN_MEMBRANE_SEND:
            ASSERT_INVOKE_ARGS("membrane.send", CP__CPINVOKE__ARGS_MEMBRANE_SEND_ARGS)
            cn_dispatch_invoke_membrane_send_args_t membrane_send_args;
            membrane_send_args.cap = ul2cptr(msg->membrane_send_args->cptr);
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, &membrane_send_args);
            break;
        case CN_BROKER_REGISTER:
            ASSERT_INVOKE_ARGS("broker.register", CP__CPINVOKE__ARGS_BROKER_REGISTER_ARGS)
            cn_dispatch_invoke_broker_register_args_t broker_register_args;
            broker_register_args.service_name =
                strdup(msg->broker_register_args->service_name);
            broker_register_args.cap = ul2cptr(msg->broker_register_args->rp_cptr);
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method,
                                    &broker_register_args);
            break;
        case CN_BROKER_LOOKUP:
            ASSERT_INVOKE_ARGS("broker.lookup", CP__CPINVOKE__ARGS_BROKER_LOOKUP_ARGS)
            cn_dispatch_invoke_broker_lookup_args_t broker_lookup_args;
            broker_lookup_args.service_name = msg->broker_lookup_args->service_name;
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method,
                                     &broker_lookup_args);
            break;
        case CN_BROKER_LOOKUP_WAIT:
            ASSERT_INVOKE_ARGS("broker.lookup_wait", CP__CPINVOKE__ARGS_BROKER_LOOKUP_WAIT_ARGS)
            cn_dispatch_invoke_broker_lookup_wait_args_t broker_lookup_wait_args;
            broker_lookup_wait_args.service_name = 
                msg->broker_lookup_args->service_name;
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method,
                                     &broker_lookup_wait_args);
            break;
        case CN_NODE_GRANT_GRANT:
            ASSERT_INVOKE_ARGS("node_grant.grant", CP__CPINVOKE__ARGS_NODE_GRANT_GRANT_ARGS)
            cn_dispatch_invoke_node_grant_grant_args_t grant_args;
            grant_args.cap = ul2cptr(msg->node_grant_grant_args->cptr);
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, &grant_args);
            break;
        case CN_SEALER_UNSEALER_SEAL:
            ASSERT_INVOKE_ARGS("sealer_unsealer.seal", CP__CPINVOKE__ARGS_SEALER_UNSEALER_SEAL_ARGS)
            cn_dispatch_invoke_sealer_unsealer_seal_args_t seal_args;
            seal_args.to_seal = ul2cptr(msg->sealer_unsealer_seal_args->to_seal);
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, &seal_args);
            break;
        case CN_SEALER_UNSEALER_UNSEAL:
            ASSERT_INVOKE_ARGS("sealer_unsealer.unseal", CP__CPINVOKE__ARGS_SEALER_UNSEALER_UNSEAL_ARGS)
            cn_dispatch_invoke_sealer_unsealer_unseal_args_t unseal_args;
            unseal_args.sealed = ul2cptr(msg->sealer_unsealer_unseal_args->sealed);
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, &unseal_args);
            break;
        default: 
            ASSERT_INVOKE_ARGS("<no-arg-method>", CP__CPINVOKE__ARGS__NOT_SET)
            res = cn_dispatch_invoke(as, result, ul2cptr(msg->cptr), method, NULL);
            break;
    }
#undef ASSERT_INVOKE_ARGS

    __cn_print_profile_invoke(as, ul2cptr(msg->cptr), method,
                              result->request.uuid, "stop");
    return res;
}

static int __cn_handle_debug_grant_message(cn_principal_t *as, 
                                           cn_dispatch_result_t *result,
                                           Cp__CPDebugGrant * msg) {

    int res;

    struct cnode * cnode = NULL;
    cptr_t receiver = ul2cptr(msg->receiver);
    cptr_t cap = ul2cptr(msg->cptr);

    if (cn_principal_get(as, receiver, &cnode) != 0) {
        return cn_dispatch_result_fail(result, "Couldn't resolve capability receiver");
    }

    if (cn_cnode_type(cnode) != CN_NODE) {
        cap_cnode_put(cnode);
        return cn_dispatch_result_fail(result, "Trying to grant to non-node");
    }

    cn_node_t * receiver_node = (cn_node_t *) cap_cnode_object(cnode);
    RHOLD_OBJ(receiver_node, DISPATCH_OWNER);
    cap_cnode_put(cnode);
    cnode = NULL;

    cptr_t out;
    cn_objtype_t type;
    if (cn_grant_extended(as, cap, receiver_node->principal, NULL, &out, &type) != 0) {
        RPUTs_OBJ(receiver_node, DISPATCH_OWNER);
        return cn_dispatch_result_fail(result, "Couldn't perform grant");
    }
    RPUTs_OBJ(receiver_node, DISPATCH_OWNER);

    cn_dispatch_result_item_t * ritem = cn_dispatch_result_item_alloc();
    if (ritem == NULL) {
        return cn_dispatch_result_fail(result, "Couldn't allocate result item");
    }
    cn_dispatch_result_item_init_cptr(ritem, out, type);
    cn_dispatch_result_push_item(result, ritem);
    result->status = SUCCESS;
    result->msg = strdup("success");
    return 0;
}

int __cn_handle_reply_message(cn_principal_t *as, Cp__CPMessage * msg) {
    cn_obj_lock(as);
    cn_node_t *owner = (cn_node_t *) as->owner;
    if (owner == NULL) {
        cncerr("Reply message from non-node principal (uuid: %#lx)\n", msg->uuid);
        cn_obj_unlock(as);
        return -1;
    }
    cn_obj_lock(owner);

    if (! g_hash_table_contains(owner->notify_wait, &msg->uuid)) {
        cncwarn("Got CPReply, but no matching CPNotify wait\n");
        goto finish;
    }

    struct cn_notify_wait * wait = g_hash_table_lookup(owner->notify_wait, &msg->uuid);
    if (wait == NULL) {
        cn_abort("Somehow got NULL wait even though contains check passed\n");
    }

    if (pthread_cond_broadcast(&wait->cond) != 0) {
        cn_abort("Unreachable unless the manpages are wrong\n");
    }

finish:
    cn_obj_unlock(owner);
    cn_obj_unlock(as);
    return EWASREPLY;
}

int cn_dispatch(cn_principal_t * as, cn_dispatch_result_t **o_result, 
                const uint8_t * bytes, size_t len, 
                cn_dispatch_result_cache_t *cache) {

    int res = -1;
    cn_dispatch_result_t * result = NULL;

    /* If we can't even parse the message, it doesn't make sense to try and
     * return any kind of reply message. */
    Cp__CPMessage * msg = cp__cpmessage__unpack(NULL, len, bytes);
    if (msg == NULL) {
        return -1;
    }

    /* Early abort if we have a cache, there is a cached value, and the uuid
     * of that cached values matches the uuid of the message we're dispatching */
    if (cache && cache->result && cache->uuid == msg->uuid) {
        result = cache->result;
        res = 0;
        goto finish1;
    }

    if (cn_dispatch_result_alloc(&result) != 0) {
        return -1;
    }

    result->request.uuid = msg->uuid;

    if (msg->magic != CP_PROTO_MAGIC) {
        res = cn_dispatch_result_fail(result, "Bad magic number. Got: %#0lx, wanted: %#0lx",
                                               msg->magic, CP_PROTO_MAGIC);
        goto finish;
    }
    if (msg->version != CP_PROTO_V1) { 
        res = cn_dispatch_result_fail(result, "Unsupported version: %d, only version 1 is supported",
                                              msg->version);
        goto finish;
    }

    switch (msg->type_case) {
        case CP__CPMESSAGE__TYPE_CREATE:
            /* logging is done in the message handler */
            res = __cn_handle_create_message(as, result, msg->create);
            break;
        case CP__CPMESSAGE__TYPE_MINT:
            __cn_print_profile(as, "mint", msg->uuid, "start");
            res = __cn_handle_mint_message(as, result, msg->mint);
            __cn_print_profile(as, "mint", msg->uuid, "stop");
            break;
        case CP__CPMESSAGE__TYPE_REVOKE:
            __cn_print_profile(as, "revoke", msg->uuid, "start");
            res = __cn_handle_revoke_message(as, result, msg->revoke);
            __cn_print_profile(as, "revoke", msg->uuid, "stop");
            break;
        case CP__CPMESSAGE__TYPE_RP0:
            __cn_print_profile(as, "rp0", msg->uuid, "start");
            res = __cn_handle_rp0_message(as, result, msg->rp0);
            __cn_print_profile(as, "rp0", msg->uuid, "stop");
            break;
        case CP__CPMESSAGE__TYPE_DELETE:
            __cn_print_profile(as, "delete", msg->uuid, "start");
            res = __cn_handle_delete_message(as, result, msg->delete_);
            __cn_print_profile(as, "delete", msg->uuid, "stop");
            break;
        case CP__CPMESSAGE__TYPE_INVOKE:
            /* logging for this one is handled in the invoke_message handler */
            res = __cn_handle_invoke_message(as, result, msg->invoke);
            break;
        case CP__CPMESSAGE__TYPE_DEBUG_GRANT:
            __cn_print_profile(as, "debug_grant", msg->uuid, "start");
            res = __cn_handle_debug_grant_message(as, result, msg->debug_grant);
            __cn_print_profile(as, "debug_grant", msg->uuid, "stop");
            break;
        case CP__CPMESSAGE__TYPE_REPLY:
            __cn_print_profile(as, "reply", msg->uuid, "start");
            res = __cn_handle_reply_message(as, msg);
            __cn_print_profile(as, "reply", msg->uuid, "stop");
            break;
        default:
            res = cn_dispatch_result_fail(result, "Unsupported message type: %d", msg->type_case);
            break;
    }


finish:
    if (res == 0 && cache) {
        assert(!(cache->result && cache->uuid == result->request.uuid) &&
               "Cache should not contain this UUID");
        if (cache->result) {
            cn_dispatch_result_free(cache->result);
        }
        cache->uuid = msg->uuid;
        cache->result = result;
    }
finish1:
    if (res == 0) { *o_result = result; }
    cp__cpmessage__free_unpacked(msg, NULL);
    return res;
}

static int __cn_dispatch_create_flow(cn_principal_t *as, 
                                     cn_dispatch_result_t *result,
                                     cn_dispatch_create_flow_args_t * args,
                                     cn_obj_t **obj,
                                     struct cnode **cnode) {
    int res = -1;
    struct flow flow;
    struct flow mask;
    of_mask_set_dc_all(&mask);
    of_mask_set_dc_all(&flow);

    cn_obj_lock(as);

    cn_node_t *node = NULL;

    if (args != NULL) {
        if ((res = cn_principal_get(as, args->node_grant, cnode)) != 0) {
            res = cn_dispatch_result_fail(result, "Cannot de-reference flow's destination node_grant");
            goto finish;
        }

        if (cn_cnode_type(*cnode) != CN_NODE_GRANT) {
            res = cn_dispatch_result_fail(result, "Node grant capability is not actually a node grant, it's a %s", 
                                                  cn_objtype_name(cn_cnode_type(*cnode)));
            goto finish;
        }
        node = ((cn_node_grant_t *) cn_cnode_object(*cnode))->node;
    } else {
        node = (cn_node_t *) as->owner;
        assert(node && "Tried to create flow in principal with NULL owner");
        assert(((cn_obj_t *) node)->type == CN_NODE 
                && "Tried to create flow with nodeless owner");
        *cnode = NULL;
    }
    cn_obj_lock(node);

    if ((res = cn_flow_new(node, flow, mask, (cn_flow_t **) obj)) != 0) {
        *obj = NULL;
        res = cn_dispatch_result_fail(result, "Couldn't create new flow: %d", res);
        goto finish1;
    }

    res = 0;

finish1:
    cn_obj_unlock(node);
finish:
    cn_obj_unlock(as);
    return res;
}

/* Set the cnode metadata for a newly created membrane getting inserted into
 * cnode 'c'. */
static int __cn_create_membrane_metadata(struct cnode * c, void * payload) {
    cn_cnode_meta_t * meta;
    if (cn_cnode_meta_new(&meta) != 0) {
        return -1;
    }

    cn_obj_t * membrane = cn_cnode_object(c);
    assert(membrane->type == CN_MEMBRANE && 
            "can only create membrane metadata for membranes");

    cn_annotation_t * wrap;
    if (cn_membrane_annotation_new((cn_membrane_t *) membrane,
                                   CN_MEMBRANE_TYPE_INTERNAL,
                                   &wrap) != 0) {
        cncerr("Failed to create annotation for newly created membrane\n");
        goto fail1;
    }

    if (cn_cnode_meta_add_annotation(meta, wrap) != 0) {
        cncerr("Failed to add annotation to newly created membrane's meta\n");
        goto fail2;
    }

    cap_cnode_set_metadata(c, meta);
    return 0;

fail2:
    cn_annotation_free(wrap);
fail1:
    cn_cnode_meta_free(meta);
    return -1;
}

int cn_dispatch_create(cn_principal_t *as, cn_dispatch_result_t *result,
                       cn_objtype_t type, void * args) {

    int res = -1;

    cn_obj_t *obj = NULL;
    struct cnode * obj_base = NULL;
    struct cn_cb_payload insert_payload;
    bool send_payload = false;

    cn_dispatch_result_item_t *ritem = cn_dispatch_result_item_alloc();
    if (ritem == NULL) {
        return cn_dispatch_result_fail(result, "Couldn't allocate result item");
    }

    cn_obj_lock(as);


    /* Actions before insert */
    switch (type) {
        case CN_FLOW: {
            cn_dispatch_create_flow_args_t * fargs = 
                (cn_dispatch_create_flow_args_t *) args;
            c_log_debug("creating flow!");
            res = __cn_dispatch_create_flow(as, result, args, &obj, &obj_base);
            if (obj == NULL) { goto fail1; }
        }
            break;
        case CN_RP: {
            if (cn_rp_new((cn_rp_t **) &obj) != 0) { 
                obj = NULL;
                goto fail; 
            }
        }
            break;
        case CN_BROKER: {
            goto fail; 
        }
            break;
        case CN_MEMBRANE: {
            if (cn_membrane_new((cn_membrane_t **) &obj) != 0) {
                obj = NULL;
                goto fail;
            }
            insert_payload.unop = __cn_create_membrane_metadata;
            insert_payload.payload = NULL;
            send_payload = true;
        }
            break;
        case CN_SEALER_UNSEALER: {
            if (cn_sealer_unsealer_new((cn_sealer_unsealer_t **) &obj) != 0) {
                cncerr("Couldn't allocate new sealer/unsealer");
                obj = NULL;
                goto fail;
            }
        }
            break;
        default: 
            goto fail;
    }

    /* add the insert payload if the "send_payload" flag is set. */
    void * _payload = send_payload ? (void *) &insert_payload : NULL;
    cptr_t obj_cptr;
    if (cn_principal_insert(as, obj, _payload, &obj_cptr) != 0) {
        res = cn_dispatch_result_fail(result, "Cannot create object of type '%s': couldn't"
                                              " insert object into caller's cspace",
                                              cn_objtype_name(obj->type));
        goto fail1;
    }

    /* Actions after insert */
    switch (type) {
        case CN_FLOW: {
            cn_dispatch_create_flow_args_t * fargs = 
                (cn_dispatch_create_flow_args_t *) args;
            cptr_t node_grant;
            if (args != NULL) {
                assert(obj_base != NULL && "no obj cptr, but not bare flow");
                cap_cnode_put(obj_base);
                obj_base = NULL;
                node_grant = fargs->node_grant;
            } else {
                assert(obj_base == NULL && "valid cnode but no args");
                cn_obj_lock(as->owner);
                assert(as == ((cn_node_t *) as->owner)->principal 
                       && "node principal should always be invoker");
                node_grant = ((cn_node_t *) as->owner)->grant;
                cn_obj_unlock(as->owner);
            }
            if (cn_derive(as, node_grant, as, obj_cptr, NULL) != 0) {
                cn_delete(as, obj_cptr, NULL);
                res = cn_dispatch_result_fail(result, "Cannot perform derive operation on flow");
                goto fail1;
            }
        }
        break;
        default: break;
    }

    cn_dispatch_result_item_init_cptr(ritem, obj_cptr, obj->type);
    cn_dispatch_result_push_item(result, ritem);
    result->status = SUCCESS;
    result->msg = strdup("success");
    c_log_debug("Create setting type to: %d", obj->type);

    if (obj_base) { cap_cnode_put(obj_base); }
    cn_obj_unlock(as);
    return 0;

fail:
    res = cn_dispatch_result_fail(result, "Cannot create object of type '%s'",
                                     cn_objtype_name(type));
fail1:
    if (obj_base) { cap_cnode_put(obj_base); }
    if (obj) { obj->free(obj); }
    cn_obj_unlock(as);
    return res;
}

int cn_dispatch_rp0(cn_principal_t * as, cn_dispatch_result_t *result) {
    // XXX: Assumes that A) principals do not have rp0s, and B) all invoking
    // principals are nodes. This may need to change at some point.
    
    int ret = -1;

    cn_dispatch_result_item_t * ritem = cn_dispatch_result_item_alloc();
    if (ritem == NULL) {
        return cn_dispatch_result_fail(result, "couldn't alloc result item");
    }

    cn_obj_lock(as);
    cn_node_t * node = (cn_node_t *) as->owner;
    cn_obj_lock(node);

    c_log_debug("got node: %0#lx", (unsigned long) node);

    cn_obj_lock(node->secret);

    cptr_t rp0;
    if (cn_grant(node->secret, node->rp0, as, NULL, &rp0) != 0) {
        ret = cn_dispatch_result_fail(result, "couldn't grant rp0 from secret");
        goto end;
    }

    cn_dispatch_result_item_init_cptr(ritem, rp0, CN_RP);
    cn_dispatch_result_push_item(result, ritem);
    result->status = SUCCESS;
    result->msg = strdup("success");

    ret = 0;

end:
    cn_obj_unlock(node->secret);
    cn_obj_unlock(node);
    cn_obj_unlock(as);
    return ret;
}

/* Generate the appropriate cn_dispatch_result_t items according to the output
 * variables from cn_rp_recv. Returns 0 on success, -1 on failure. */
int cn_dispatch_util_fill_rp_recv_result(cn_dispatch_result_t *result,
                                         cptr_t recvd_cap,
                                         cn_objtype_t recvd_type,
                                         struct cn_rp_elem_message *msg,
                                         enum cn_rp_recv_result_flag flags) {
    if (CN_FLAG_SET(flags, CN_RP_RECV_RESULT_TYPE_CPTR)) {
        cn_dispatch_result_item_t *item = cn_dispatch_result_item_alloc();
        if (item == NULL) {
            return -1;
        }
        cn_dispatch_result_item_init_cptr(item,recvd_cap, recvd_type);
        cn_dispatch_result_push_item(result, item);
    }
    if (CN_FLAG_SET(flags, CN_RP_RECV_RESULT_TYPE_MESSAGE)) {
        cn_dispatch_result_item_t *item = cn_dispatch_result_item_alloc();
        if (item == NULL) {
            cn_dispatch_result_clear(result);
            free(msg->data);
            return -1;
        }
        if (cn_dispatch_result_item_init_msg(item, msg->data, msg->len) != 0) {
            cn_dispatch_result_clear(result);
            free(item);
            free(msg->data);
            return -1;
        }
        cn_dispatch_result_push_item(result, item);
    }
    return 0;
}

int cn_dispatch_invoke(cn_principal_t *as, cn_dispatch_result_t *result,
                       cptr_t obj_cptr, cn_method_t method, void * args) {
    int res = -1;

    cn_obj_lock(as);

#define fail_invoke(res, fmt, ...) \
    cn_dispatch_result_fail(res, "Cannot invoke method '%s' on object '%#0lx': " fmt, \
                                 METHOD_NAMES[method], cptr2ul(obj_cptr), \
                                 ## __VA_ARGS__)

    struct cnode *obj_cnode = NULL;
    if (cn_principal_get(as, obj_cptr, &obj_cnode) != 0) {
        res = fail_invoke(result, "couldn't resolve object_cptr");
        goto fail;
    }

    cn_cnode_meta_t *meta = cap_cnode_metadata(obj_cnode);
    if (meta != NULL && ! cn_method_mask_permission_is_set(&meta->method_mask_cache, method)) {
        c_log_debug("[dispatch] Failed method check on %d, had mask: %#0lx",
                    method, meta->method_mask_cache);
        char * meta_str = cn_cnode_meta_string(meta);
        c_log_debug("meta: %s", meta_str);
        free(meta_str);
        
        res = fail_invoke(result, "One ore more annotations prevent this invocation");
        goto fail;
    }

    cn_obj_t * obj = cn_cnode_object(obj_cnode);

    cn_obj_lock(obj);

#define check_dispatch_result_item(item) \
    do { \
        if ((item) == NULL) { \
            res = fail_invoke(result, "could not allocate result item"); \
            goto fail1; \
        } \
    } while (0)

#define push_item_cptr(result, cptr, type) \
    do { \
        cn_dispatch_result_item_t * item = cn_dispatch_result_item_alloc(); \
        check_dispatch_result_item(item); \
        cn_dispatch_result_item_init_cptr(item, (cptr), (type)); \
        cn_dispatch_result_push_item((result), item); \
    } while (0)

    if (obj->type != __cn_supported_object_type(method)) {
        res = fail_invoke(result, "object of type '%s' does not support method",
                                  cn_objtype_name(obj->type));
        goto fail1;
    }

    switch (method) {
        case CN_RP_SEND: {
            cn_dispatch_invoke_rp_send_args_t * send_arg = args;
            c_log_debug("got args: %p", args);
            assert((send_arg->has_cap || send_arg->has_msg) &&
                   "cptr, msg, or both args must be set");
            if (send_arg->has_cap) {
                if (cn_rp_send(obj_cnode, send_arg->cap,
                               send_arg->has_msg ? &send_arg->msg : NULL) != 0) {
                    res = fail_invoke(result, "object of type '%s' does not support method",
                                              cn_objtype_name(obj->type));
                    goto fail1;
                }
            } else if (send_arg->has_msg && ! send_arg->has_cap) {
                struct cn_rp_elem *e = NULL;
                if (cn_rp_elem_message(&send_arg->msg, &e) != 0) {
                    res = fail_invoke(result, "couldn't allocate rp element");
                    goto fail1;
                }
                if (cn_rp_send_rp_elem(obj_cnode, e) != 0) {
                    cn_rp_elem_free(e);
                    res = fail_invoke(result, "object of type '%s' does not support method",
                                              cn_objtype_name(obj->type));
                    goto fail1;
                }
            } else {
                cn_abort("unreachable");
            }
        }
        break;
        case CN_RP_RECV: {
            cn_objtype_t recvd_type;
            cptr_t recvd_cap;
            struct cn_rp_elem_message msg;
            enum cn_rp_recv_result_flag flags;

            res = cn_rp_recv(obj_cnode, &flags, &recvd_cap, &recvd_type, &msg);
            if (res != 0) {
                res = fail_invoke(result, "cn_rp_recv failed with code: %d", res);
                goto fail1;
            }

            if (cn_dispatch_util_fill_rp_recv_result(result, recvd_cap, recvd_type,
                                                     &msg, flags) != 0) {
                res = fail_invoke(result, "couldn't allocate recv reply items");
                goto fail1;
            }
        }
            break;
        case CN_RP_RECV_WAIT: {
            res = cn_rp_recv_wait(obj_cnode, result->request.uuid);
            if (res != 0) {
                res = fail_invoke(result, "cn_rp_recv_wait exited with code: %d", res);
                goto fail1;
            }
            }
            break;
        case CN_NODE_RESET: {
            assert(obj->type == CN_NODE && "Somehow calling reset on non-node");
            //RHOLD_OBJ(obj, DISPATCH_OWNER);
            //cap_cnode_put(obj_cnode);
            //obj_cnode = NULL; // signal we don't need to put the cnode anymore
            c_log_debug("Doing reset!");
            cptr_t grant_cptr;
            if (cn_node_reset(obj_cnode, &grant_cptr) != 0) {
                res = fail_invoke(result, "cn_node_reset failed: %d", res);
                goto fail1;
            }
            c_log_debug("Did reset!");
            //RPUTs_OBJ(obj, DISPATCH_OWNER);
            push_item_cptr(result, grant_cptr, CN_NODE_GRANT);
            }
            break;
        case CN_NODE_INFO: {
            assert(obj->type == CN_NODE && "calling info on non-node");
            cn_node_t *node = (cn_node_t *) obj;
            cn_dispatch_result_item_t *item = cn_dispatch_result_item_alloc();
            check_dispatch_result_item(item);
            cn_dispatch_result_item_init_info(item, node->name, 
                                              (uint8_t *) &node->ipv4.addr.s_addr, 
                                              node->mac,node->id,node->domainid,
					      node->ownerid);
            cn_dispatch_result_push_item(result, item);
            }
            break;
        case CN_MEMBRANE_SEND: {
            assert(obj->type == CN_MEMBRANE && "calling membrane send on non-membrane");
            cn_dispatch_invoke_membrane_send_args_t * send_args = args;
            if (cn_membrane_send(obj_cnode, send_args->cap) != 0) {
                res = fail_invoke(result, "cn_membrane_send failed");
                goto fail1;
            }
        }
        break;
        case CN_MEMBRANE_RECV: {
            assert(obj->type == CN_MEMBRANE && "object must be membrane to do membrane recv");
            cptr_t recvd_cap;
            cn_objtype_t recvd_type;
            if (cn_membrane_recv(obj_cnode, &recvd_cap, &recvd_type) != 0) {
                res = fail_invoke(result, "cn_membrane_recv failed");
                goto fail1;
            }
            push_item_cptr(result, recvd_cap, recvd_type);
        }
        break;
        case CN_MEMBRANE_EXTERNAL: {
            assert(obj->type == CN_MEMBRANE && "object must be membrane to do membrane external");
            /* Drop the cnode since we're gonna use this object in a grant */
            cptr_t ext_cap;
            if (cn_membrane_external(obj_cnode, &ext_cap) != 0) {
                res = fail_invoke(result, "cn_membrane_external failed");
                goto fail1;
            }
            push_item_cptr(result, ext_cap, CN_MEMBRANE);
        }
        break;
        case CN_MEMBRANE_CLEAR: {
            assert(obj->type == CN_MEMBRANE && "object must be membrane to do membrane external");
            c_log_debug("doing membrane clear...");
            if (cn_membrane_clear(obj_cnode) != 0) {
                res = fail_invoke(result, "cn_membrane_clear failed");
                goto fail1;
            }
        }
        break;
        case CN_MEMBRANE_RECV_WAIT: {
            assert(obj->type == CN_MEMBRANE && "object must be membrane to do membrane_recv_wait");
            res = cn_membrane_recv_wait(obj_cnode, result->request.uuid);
            if (res != 0) {
                res = fail_invoke(result, "cn_membrane_recv_wait exited with code: %d", res);
                goto fail1;
            }
        }
        break;
        case CN_BROKER_REGISTER: {
            cn_dispatch_invoke_broker_register_args_t * register_args = args;
            if (cn_broker_register(obj_cnode, register_args->service_name,
                                   register_args->cap) != 0) {
                res = fail_invoke(result, "failed to register cap with broker");
                goto fail1;
            }
        }
            break;
        case CN_BROKER_LOOKUP: {
            cn_dispatch_invoke_broker_lookup_args_t * lookup_args = args;
            cptr_t looked_up_rp_cap;
            if ((res = cn_broker_lookup(obj_cnode, lookup_args->service_name,
                                        &looked_up_rp_cap)) != 0) {
                res = fail_invoke(result, "failed to lookup rp cap in broker");
                goto fail1;
            }
            c_log_debug("looked up service %s: res: %d",
                        lookup_args->service_name, res);
            push_item_cptr(result, looked_up_rp_cap, CN_RP);
        }
            break;
        case CN_BROKER_LOOKUP_WAIT: {
            cn_dispatch_invoke_broker_lookup_wait_args_t * lookup_wait_args = args;
            if (cn_broker_lookup_wait(obj_cnode, lookup_wait_args->service_name,
                                      result->request.uuid) != 0) {
                res = fail_invoke(result, "failed to register to wait on service name");
                goto fail1;
            }
        }
            break;
        case CN_NODE_GRANT_GRANT: {
            cn_dispatch_invoke_node_grant_grant_args_t * grant_args = args;
            cptr_t granted;
            cn_objtype_t granted_type;
            if (cn_node_grant_grant(obj_cnode, as, grant_args->cap, 
                                    &granted, &granted_type) != 0) {
                res = fail_invoke(result, "failed to perform grant");
                goto fail1;
            }
        }
        break;
        case CN_NODE_GRANT_RP0: {
            cptr_t rp0;
            if (cn_node_grant_rp0(obj_cnode, &rp0) != 0) {
                res = fail_invoke(result, "failed to get rp0 from node grant");
                goto fail1;
            }
            push_item_cptr(result, rp0, CN_RP);
        }
        break;
        case CN_SEALER_UNSEALER_SEAL: {
            cn_dispatch_invoke_sealer_unsealer_seal_args_t * seal_args = args;
            cn_objtype_t type;
            cptr_t sealed;
            res = cn_sealer_unsealer_seal(obj_cnode, seal_args->to_seal, &sealed, &type);
            if (res != 0) {
                res = fail_invoke(result, "failed to seal with code: %d", res);
                goto fail1;
            }
            push_item_cptr(result, sealed, type);
        }
        break;
        case CN_SEALER_UNSEALER_UNSEAL: {
            cn_dispatch_invoke_sealer_unsealer_unseal_args_t * unseal_args = args;
            cptr_t unsealed;
            cn_objtype_t unsealed_type;
            res = cn_sealer_unsealer_unseal(obj_cnode, unseal_args->sealed, 
                                            &unsealed, &unsealed_type);
            if (res != 0) {
                res = fail_invoke(result, "failed to unseal with code: %d", res);
                goto fail1;
            }
            push_item_cptr(result, unsealed, unsealed_type);
        }
        break;
        case CN_SEALER_UNSEALER_RESTRICT_TO_SEAL: {
            cptr_t seal_restricted;
            res = cn_sealer_unsealer_restrict_to_seal(obj_cnode, &seal_restricted);
            if (res != 0) {
                res = fail_invoke(result, "failed to restrict with code: %d", res);
                goto fail1;
            }
            push_item_cptr(result, seal_restricted, CN_SEALER_UNSEALER);
        }
        break;
        case CN_SEALER_UNSEALER_RESTRICT_TO_UNSEAL: {
            cptr_t unseal_restricted;
            res = cn_sealer_unsealer_restrict_to_unseal(obj_cnode, &unseal_restricted);
            if (res != 0) {
                res = fail_invoke(result, "failed to restrict with code: %d", res);
                goto fail1;
            }
            push_item_cptr(result, unseal_restricted, CN_SEALER_UNSEALER);
        }
        break;
        default:
            res = fail_invoke(result, "method not implemented");
            goto fail1;
    }

#undef fail_invoke

    result->status = SUCCESS;
    result->msg = strdup("success");
    cn_obj_unlock(obj);
    cap_cnode_put(obj_cnode);
    cn_obj_unlock(as);
    return 0;

fail1:
    cn_obj_unlock(obj);
fail:
    if (obj_cnode) { cap_cnode_put(obj_cnode); }
    cn_obj_unlock(as);
    return res;
}


int cn_dispatch_mint(cn_principal_t * as, cn_dispatch_result_t *result, 
                     cptr_t cptr, void * args) {
    assert(args == NULL && "flow args not yet implemented");

    int res = -1;

    cn_dispatch_result_item_t *item = cn_dispatch_result_item_alloc();
    if (item == NULL) {
        return cn_dispatch_result_fail(result, "Failed to allocate result item");
    }

    cptr_t minted;
    cn_objtype_t type;
    res = cn_mint(as, cptr, NULL, &minted, &type);
    if (res != 0) {
        free(item);
        return cn_dispatch_result_fail(result, "Failed to mint capability: %d", res);
    }

    cn_dispatch_result_item_init_cptr(item, minted, type);
    cn_dispatch_result_push_item(result, item);

    result->status = SUCCESS;
    result->msg = strdup("success");

    return 0;
}

int cn_dispatch_revoke(cn_principal_t * as, cn_dispatch_result_t *result, 
                       cptr_t cptr) {
    int res = -1;

    res = cn_revoke(as, cptr, NULL);
    if (res != 0) {
        return cn_dispatch_result_fail(result, "Failed to revoke capability: %d", res);
    }

    result->status = SUCCESS;
    result->msg = strdup("success");
    return 0;
}

int cn_dispatch_delete(cn_principal_t * as, cn_dispatch_result_t *result,
                       cptr_t cptr) {
    int res = -1;

    cn_delete(as, cptr, NULL);

    result->status = SUCCESS;
    result->msg = strdup("success");
    return 0;
}
