
#ifndef __CAPNET_METADATA__
#define __CAPNET_METADATA__

#include "controller.h"

/**
 ** A metadata service provides info about switches and switch ports to
 ** the controller.
 **/

struct metadata_ops {
    int (*has_switch)(uint64_t dpid);
    int (*init_switch)(cn_switch_t *sw);
    void (*fini_switch)(cn_switch_t *sw);
    int (*start_thread)(cn_switch_t *sw);
    int (*stop_thread)(cn_switch_t *sw);
    int (*get_switch_info)(cn_switch_t *sw);
    int (*get_port_info)(cn_switch_t *sw, cn_port_t *port);
    int (*get_node_info)(cn_switch_t *sw, cn_port_t *port, cn_node_t *node); 
    int (*get_lan_info)(cn_switch_t *sw, cn_port_t *port);
    int (*set_port_pending)(cn_switch_t *sw,uint16_t port_no);
};

extern struct metadata_ops metadata_file_ops;

struct metadata_ops *metadata_get_ops_for_switch(uint64_t dpid);
int metadata_init_switch(cn_switch_t *sw);
void metadata_fini_switch(cn_switch_t *sw);
int metadata_start_thread(cn_switch_t *sw);
int metadata_stop_thread(cn_switch_t *sw);
int metadata_get_switch_info(cn_switch_t *sw);
int metadata_get_port_info(cn_switch_t *sw,cn_port_t *port);
int metadata_get_node_info(cn_switch_t *sw, cn_port_t *port, cn_node_t *node);
int metadata_get_lan_info(cn_switch_t *sw,cn_port_t *port);
int metadata_set_port_pending(cn_switch_t *sw,uint16_t port_no);

#endif /* __CAPNET_METADATA__ */
