#ifndef __ERRORS_H__
#define __ERRORS_H__

// We start at 10 so that any previous error codes are not erroneusly
// categorized.
#define __ERROR_BASE    10

#define EEMPTY          -10
#define EBADCPTR        -11
#define EWASREPLY       -12
#define EBADRIGHTS      -13
#define EBADSEALER      -14
#define EEXISTS         -15
#define ENOTEXISTS      -16
#define ESKIPPED        -17

/* Update this when you add a new error */
#define __ERROR_MAX     17

const char * cn_error_desc(int err);

#endif
