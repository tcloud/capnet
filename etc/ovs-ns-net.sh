#!/bin/bash

##
## This script contains useful routines for building OpenVSwitch+Linux
## network namespace test environments for Capnet.  You should **NOT** run
## it with root privileges; it uses sudo as necessary.  Here's a summary of
## the commands:
##
usage() {
    cat <<EOF | tee
USAGE: $0 <command> [<args>]

COMMANDS:

   create <name> [number-of-ports] [master-port] [uplink-bridge] [master-switch]
     (creates an OpenVSwitch using <name> as the switch name;
      creates a network namespace for each port; creates veth device
      pairs for each port; puts one half of each pair in the
      openvswitch bridge; and moves the corresponding half to the
      appropriate namespace; and gives it an IP/subnet.

      If you specify <uplink-bridge>, this switch will be patched to a
      linux bridge that acts like a "physical" switch, thus allowing you to
      connect multiple Capnet switches in a LAN.  Right now, you must also
      specify the master-switch in order to specify the master controller.
      Furthermore, right now we only support a single controller process, so
      all Capnet switches that are uplinked will connect to this
      master-switch's controller.  So, when creating the master switch/
      controller, you do something like create sw1 3 upbr0 sw1.  When
      creating "slave" switches, create sw2 3 upbr0 sw1.  In the future, when
      we support multiple slave controllers, another argument will be added
      to help the slave controllers connect to each other.)
   destroy <name>
     (destroys the openvswitch, all veths connected to it, and the namespaces
      the veths were in.  If this switch was connected to a bridge, we remove
      the patch veth; if the bridge has no more ports, we remove it.)
   portadd <name> [port-number] [master] [services]
     (adds a network namespace and veth pair to the switch named <name>.
      If port-number is specified, we use that; else, )
   portdel <name> <port-number>
     (kills any processes associated with the port's netns; ifdown the veth
      pairs; removes the veth pair from the netns and switch; removes the
      netns.)
   portshell <name> <port-number>
     (Gets you into a shell in the netns of the specified port.)
   mul_start <name>
     (Starts up a MUL core and controller; change the MUL and MULSWITCH
      vars for different MUL binaries.)
   mul_stop <name>
   mul_restart <name>
   mul_telnet <name>
     (Telnets to the MUL controller program associated with switch <name>.)

TESTING DHCP:

   portadd now generates a dnsmasq-style database file in
     $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN each time you add a port.  This allows
     you to test automatic DHCP flow creation.  But to run a DHCP server, you
     must portadd a port that has the dhcp server service; i.e.,
       $ ovs-ns.net.sh portadd sw1 3 "''" dhcp
     Note the enclosed single quotes; those are necessary to ensure that the
     script passes an empty third argument to portadd() internally (recall
     that the third arg is 'master' or nothing).  The fourth arg is a
     comma-separated service list (although right now, dhcp is the only special
     service).  Ok, all that said, once you have a port that has the dhcp role
     (meaning it will act as a DHCP server for $DOMAIN), portshell to that port
     and fire off dnsmasq, using this command line:
       $ touch /tmp/NOTHING ; dnsmasq -C /tmp/NOTHING -h -d -k -p 0 -R -n -5 -i veth.sw1p3.in --dhcp-hostsfile=$RUNDIR/cnc.dhcp-hostsfile.mydomain -l /tmp/leases -K -F 10.0.1.200,10.0.1.250
     (these options have been carefully selected, so all you should need to do
     is change the interface as appropriate, and the dynamic range, if you need
     (the dynamic range arg is necessary to enable dnsmasq's DHCP server, but
     it's kind of weird.)).  Alrighty, once you have the server running, fire
     off a test client in another portshell (i.e. port 3):
       $ touch /tmp/NOTHING ; dhcpcd -d -f /tmp/NOTHING -1 -4 -B -A -G -L -T veth.sw1p2.in
     (Note that the -T argument enables a testing mode.  I couldn't find an
     argument to prevent it from overwriting /etc/hosts, so I had to use test
     mode.)  That's it!  You should see successful output from dhcpcd.

EOF
}

#
# Default to "debug mode".
#
set -x

#
# Some options.
#
# For now, we assume a /16 -- so 65k ports.  We leave the .0.x subnet open
# for manual use...
IPPREFIX="10.0"
NETWORKBITS=16
#STARTSUBNET=1
SPAWNSHELL=0

MUL_BIN=${MUL_BIN:-"/opt/tcloud/mul/bin"}
CAPNET_BIN=${CAPNET_BIN:-"/opt/tcloud/capnet/bin"}

#
# Programs to invoke, and permissions to invoke them with (i.e. sudo).
#
SUDO="sudo"
OVSCTL="$SUDO ovs-vsctl"
OFCTL="$SUDO ovs-ofctl"
IP="$SUDO ip"
IFCONFIG="$SUDO ifconfig"
KILL="$SUDO kill -9"
PKILL="$SUDO pkill"
MUL="$SUDO $MUL_BIN/mul -l 0"
#MULSWITCH="sudo /opt/tcloud/mul/bin/mull2sw"
MULSWITCH="$SUDO $CAPNET_BIN/capnet-controller"
MULSWITCHARGS=""
MULCLI="$SUDO $MUL_BIN/mulcli"
MULBASEPORT=6633
MULBASETTY=6933
DOSIGUPDATE=0
DOGDB=1

#
# For now, we only support a single domain (i.e., L2 VLAN).  We allow
# user to set different domain env vars for testing.
#
if [ "x$DOMAIN" = "x" ]; then
    DOMAIN="mydomain"
fi
if [ "x$ODOMAIN" = "x" ]; then
    ODOMAIN="myowner"
fi

#
# Figure out what dirs to use as our run and log dir...
#
if test -z "${RUNDIR+x}"; then
    if [ -d /run -a -w /run ]; then
        RUNDIR="/run"
    elif [ -d /var/run -a -w /var/run ]; then
        RUNDIR="/var/run"
    elif [ -d /var/tmp -a -w /var/tmp ]; then
        RUNDIR="/var/tmp"
    else
        RUNDIR="/tmp"
    fi
elif test \! -d "$RUNDIR"; then
    echo "ERROR: $RUNDIR is not a directory"
    exit 1
fi

if test -z "${LOGDIR+x}"; then
    if [ -d /var/tmp -a -w /var/tmp ]; then
        LOGDIR="/var/tmp"
    else
        LOGDIR="/tmp"
    fi
elif test \! -d "$LOGDIR"; then
    echo "ERROR: $LOGDIR is not a directory"
    exit 1
fi

#
# Figure out if we're running a capnet controller.
#
ISCAPNET=0
echo $MULSWITCH | grep -q -i capnet
if [ $? -eq 0 ]; then
    ISCAPNET=1
    MULSWITCHARGS="${MULSWITCHARGS} -S $RUNDIR -Z 16 -W 16 -L ALL"
fi

#
# Define our core functions.
#
create() {
    sw=$1
    nports=$2
    masterport=$3
    uplinkbr=$4
    mastersw=$5

    if [ -n "$uplinkbr" -a -z "$mastersw" ]; then
        echo "ERROR: if you specify an uplink bridge, you must specify the name of a switch whose controller will be the master controller we tell this new switch to connect to."
        exit 1
    fi

    if [ -n "$masterport" ]; then
        if [ $masterport -gt $nports -o $masterport -eq 0 ]; then
            echo "ERROR: masterport must be -1 (no master port) or within"
            echo " the number of ports you specified ($nports)"
            exit 1
        fi
    fi

    $OVSCTL br-exists $sw
    if [ $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw already exists!"
        exit 1
    fi
    if [ -z "$nports" ]; then
        nports=0
    fi

    $OVSCTL add-br $sw
    $IFCONFIG $sw up

    dpid=`$OFCTL show ${sw} | grep dpid: | sed -r -e 's/^.*dpid:(.*)$/\1/'`
    touch $RUNDIR/cnc.metadata.${dpid}

    lpc=1
    while [ $lpc -le $nports ]; do
        if [ -n "$masterport" -a $masterport -eq $lpc ]; then
            portadd $sw $lpc master
        else
            portadd $sw $lpc
        fi
        lpc=`expr $lpc + 1`
    done

    if [ -n "$uplinkbr" ]; then
        ip link show $uplinkbr >& /dev/null
        if [ ! $? -eq 0 ]; then
            ip link add $uplinkbr type bridge
        fi
        ifconfig $uplinkbr up

        #lpc=`expr $lpc + 1`
        pport=$lpc
        $OVSCTL list-ports $sw | grep -q "${sw}p${pport}"
        if [ $? -eq 0 ]; then
            echo "ERROR: portadd: port $pport already exists on switch ${sw}!"
            exit 1
        fi

        name="${sw}p${pport}"

        $IP link add dev veth.${name}.ex type veth peer name veth.${name}.up
        $OVSCTL add-port $sw veth.${name}.ex
        brctl addif $uplinkbr veth.${name}.up
        $IFCONFIG veth.${name}.ex up
	# veth ports in the bridge need to be promiscuous to pass/recv
	# stuff the bridge forwards them that they wouldn't otherwise forward.
        $IFCONFIG veth.${name}.up promisc up

        # Grab info and stash it in the metadata file
        portno=`$OFCTL show ${sw} | grep veth.${name}.ex | sed -r -e 's/^[^0-9]*([0-9]*)\(.*$/\1/'`
        mac=`$IP link show dev veth.${name}.up | sed -n -r -e 's/.*ether ([0-9a-fA-F:]*) .*/\1/p'`

        if [ -z "$mac" ]; then
            echo "ERROR: portadd, port veth.${name}.up has no mac addr!"
        fi

        echo "uplink $portno ${sw}-port-$portno veth.${name}.ex $mac" >> $RUNDIR/cnc.metadata.${dpid}

        # Notify the controller if it's a capnet one
        if [ $ISCAPNET -eq 1 -a $DOSIGUPDATE -eq 1 -a -f $RUNDIR/${mastersw}-mul-controller.pid ]; then
            $SUDO kill -USR1 `cat $RUNDIR/${mastersw}-mul-controller.pid`
        fi
        sleep 1

        if [ ! "$sw" = "$mastersw" ]; then
            mul_core_connect_sw $sw $mastersw
        fi
    fi
}

destroy() {
    sw=$1

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    dpid=`$OFCTL show ${sw} | grep dpid: | sed -r -e 's/^.*dpid:(.*)$/\1/'`

    # For all the ports...
    for port in `$OVSCTL list-ports $sw | sed -n -r -e 's/veth.*p([0-9]*)\.ex/\1/p'`; do
        portdel $sw $port
    done

    sleep 1

    $IFCONFIG $sw down
    $OVSCTL del-br $sw

    rm -f $RUNDIR/cnc.metadata.${dpid}
}

portadd() {
    sw=$1
    port=$2
    master=$3
    services=$4

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi
    if [ -z "$port" ]; then
        echo "ERROR: portdel: must specify a port!"
        exit 1
    fi
    $OVSCTL list-ports $sw | grep -q "${sw}p${port}"
    if [ $? -eq 0 ]; then
        echo "ERROR: portadd: port $port already exists on switch ${sw}!"
        exit 1
    fi

    dpid=`$OFCTL show ${sw} | grep dpid: | sed -r -e 's/^.*dpid:(.*)$/\1/'`

    netns="${sw}p${port}"

    $IP netns add $netns

    $IP link add dev veth.${netns}.ex type veth peer name veth.${netns}.in
    $OVSCTL add-port $sw veth.${netns}.ex

    $IP link set veth.${netns}.in netns ${netns}
    sleep 1
    $IFCONFIG veth.${netns}.ex up

    over=`expr $port / 254`
    mod=`expr $port % 254`
    if [ $mod -eq 0 ]; then
        edge=254
    else
        edge=$mod
    fi
    subnet=`echo ${sw} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    subnet=`expr $subnet + $over`
    #subnet=`expr $STARTSUBNET + $over`
    ipaddr="${IPPREFIX}.$subnet.$edge"
    $IP netns exec $netns \
        ifconfig veth.${netns}.in ${ipaddr}/$NETWORKBITS up

    # Grab info and stash it in the metadata file
    portno=`$OFCTL show ${sw} | grep veth.${netns}.ex | sed -r -e 's/^[^0-9]*([0-9]*)\(.*$/\1/'`
    mac=`$IP netns exec $netns ip link show dev veth.${netns}.in | sed -n -r -e 's/.*ether ([0-9a-fA-F:]*) .*/\1/p'`

    if [ -z "$mac" ]; then
        echo "ERROR: portadd, port veth.${netns}.in has no mac addr!"
        echo "XXX: We should back out here..."
        echo "XXX: Run 'destroy br0' to back out."
    fi

    role="node"
    if [ -n "$master" -a "$master" = "master" ]; then
        role="master"
    fi
    if [ -n "$services" ]; then
	role="${role}:${services}"
    fi

    echo "node $portno $DOMAIN $ODOMAIN $role ${sw}-port-$portno veth.${netns}.ex $mac $ipaddr $NETWORKBITS" >> $RUNDIR/cnc.metadata.${dpid}

    echo "$mac,$ipaddr" >> $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN

    # Notify the controller if it's a capnet one
    if [ $ISCAPNET -eq 1 -a $DOSIGUPDATE -eq 1 -a -f $RUNDIR/${sw}-mul-controller.pid ]; then
        $SUDO kill -USR1 `cat $RUNDIR/${sw}-mul-controller.pid`
    fi

    if [ $SPAWNSHELL -eq 1 ]; then
        # XXX: spawn shells...
        sleep 1
    fi
}

portdel() {
    sw=$1
    port=$2

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi
    if [ -z "$port" ]; then
        echo "ERROR: portdel: must specify a port!"
        exit 1
    fi
    $OVSCTL list-ports $sw | grep -q "${sw}p${port}"
    if [ ! $? -eq 0 ]; then
        echo "ERROR: portdel: port $port does not exist on switch ${sw}!"
        exit 1
    fi

    netns="${sw}p${port}"
    dpid=`$OFCTL show ${sw} | grep dpid: | sed -r -e 's/^.*dpid:(.*)$/\1/'`
    portno=`$OFCTL show ${sw} | grep veth.${netns}.ex | sed -r -e 's/^[^0-9]*([0-9]*)\(.*$/\1/'`

    # First, move the veth ex half out of the OVS bridge.
    $IFCONFIG veth.${netns}.ex down
    $OVSCTL del-port $sw veth.${netns}.ex

    # If the port doesn't have a netns, it is an uplink port.
    ip netns list | grep -q $netns
    isnetns=0
    if [ $? -eq 0 ]; then
        isnetns=1
        
        # Find all the PIDs attach to this netns; we're going to kill it
        pids=1
        while [ $pids -gt 0 ]; do
            pids=0
            for pid in `$IP netns pids $netns`; do
                $KILL $pid
                pids=`expr $pids + 1`
            done
            sleep 1
        done

        # Ok, ifdown the veth in, and move it out of the netns
        $IP netns exec $netns ifconfig veth.${netns}.in down

        # Set its netns to the same one as init, which is the root context netns
        $IP netns exec $netns ip link set veth.${netns}.in netns 1

        $IP netns delete $netns
    else
        ifconfig veth.${netns}.in down
    fi

    $IP link del dev veth.${netns}.ex

    # Remove from the metadata file.
    if [ $isnetns -eq 1 ]; then
        cat $RUNDIR/cnc.metadata.${dpid} | grep -v "node $portno " > $RUNDIR/cnc.metadata.${dpid}.NEW
    else
        cat $RUNDIR/cnc.metadata.${dpid} | grep -v "uplink $portno " > $RUNDIR/cnc.metadata.${dpid}.NEW
    fi
    mv $RUNDIR/cnc.metadata.${dpid}.NEW $RUNDIR/cnc.metadata.${dpid}

    # Remove from the dhcp-hostsfile
    #cat $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN | grep -v "$mac," > $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN.NEW
    #mv $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN.NEW $RUNDIR/cnc.dhcp-hostsfile.$DOMAIN

    # Notify the controller if it's a capnet one
    if [ $ISCAPNET -eq 1 -a $DOSIGUPDATE -eq 1 -a -f $RUNDIR/${sw}-mul-controller.pid ]; then
        $SUDO kill -USR1 `cat $RUNDIR/${sw}-mul-controller.pid`
    fi
}

portexec() {
    sw=$1
    port=$2
    shift
    shift

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi
    if [ -z "$port" ]; then
        echo "ERROR: portexec: must specify a port!"
        exit 1
    fi

    netns="${sw}p${port}"
    ip netns list | grep -q $netns
    if [ ! $? -eq 0 ]; then
        echo "ERROR: portexec: no netns $netns; maybe this is an uplink port?"
        exit 1
    fi

    if [ -z "$1" ]; then
        $IP netns exec $netns /bin/bash
    else
        $IP netns exec $netns $@
    fi
}

mul_core_stop() {
    sw=$1

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-core.pid"
    if [ -f "$file" ]; then
        $PKILL -P `cat $file`
        rm -f "$file"
    fi
}

mul_controller_stop() {
    sw=$1

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-controller.pid"
    if [ -f "$file" ]; then
        $PKILL -P `cat $file`
        rm -f "$file"
    fi
}

mul_cli_stop() {
    sw=$1

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-cli.pid"
    if [ -f "$file" ]; then
        $PKILL -P `cat $file`
        rm -f "$file"
    fi
}

mul_stop() {
    sw=$1
    #mul_telnet_stop $sw
    mul_cli_stop $sw
    mul_controller_stop $sw
    mul_core_stop $sw
}

mul_core_connect_sw() {
    # Connect a "slave" (non-master) switch to another switch's (a master
    # switch) MUL core.
    slave=$1
    master=$2

    if [ -z "$slave" ]; then
        echo "ERROR: slave switch is NULL!\n"
        exit 1
    fi
    if [ -z "$master" ]; then
        echo "ERROR: master switch is NULL (slave $slave)!\n"
        exit 1
    fi

    switchno=`echo ${master} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    if [ ! $? -eq 0 ]; then
        echo "ERROR: switch name $master does not end in integer; cannot do MUL!"
        exit 1
    fi

    $OVSCTL br-exists $master
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $master does not exist!"
        exit 1
    fi

    file="$RUNDIR/${master}-mul-core.pid"
    if [ ! -f "$file" ]; then
        echo "WARNING: ovs switch $master does not seem to have a mul core process running; pid $file does not exist!"
    fi

    port=`expr $MULBASEPORT + $switchno`
    $OVSCTL set-controller $slave tcp:127.0.0.1:${port}
}

mul_core_start() {
    sw=$1

    switchno=`echo ${sw} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    if [ ! $? -eq 0 ]; then
        echo "ERROR: switch name does not end in integer; cannot do MUL!"
        exit 1
    fi

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-core.pid"
    if [ -f "$file" ]; then
        echo "ERROR: ovs switch $sw already has a mul core process in $file !"
        exit 1
    else
        port=`expr $MULBASEPORT + $switchno`
        $MUL -P $port >& $LOGDIR/${sw}-mul-core.log &
        echo $! > $file
        sleep 1
        $OVSCTL set-controller $sw tcp:127.0.0.1:${port}
    fi
}

mul_controller_start() {
    sw=$1

    switchno=`echo ${sw} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    if [ ! $? -eq 0 ]; then
        echo "ERROR: switch name does not end in integer; cannot do MUL!"
        exit 1
    fi

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-controller.pid"
    if [ -f "$file" ]; then
        echo "ERROR: ovs switch $sw already has a mul controller process in $file !"
        exit 1
    else
        port=`expr $MULBASEPORT + $switchno`
        vty=`expr $MULBASETTY + $switchno`
        if [ $DOGDB -ne 1 ]; then
            $MULSWITCH -s 127.0.0.1 -V ${vty} ${MULSWITCHARGS} >& $LOGDIR/${sw}-mul-controller.log &
            echo $! > $file
        else
            echo "RUN THIS LINE:"
            echo "$MULSWITCH -s 127.0.0.1 -V ${vty} ${MULSWITCHARGS}"
        fi
    fi
}

mul_cli_start() {
    sw=$1

    switchno=`echo ${sw} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    if [ ! $? -eq 0 ]; then
        echo "ERROR: switch name does not end in integer; cannot do MUL!"
        exit 1
    fi

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    file="$RUNDIR/${sw}-mul-cli.pid"
    if [ -f "$file" ]; then
        echo "ERROR: ovs switch $sw already has a mul cli process in $file !"
        exit 1
    else
        $MULCLI -s 127.0.0.1 -V 10000 >& $LOGDIR/${sw}-mul-cli.log &
        echo $! > $file
    fi
}

mul_telnet() {
    sw=$1

    switchno=`echo ${sw} | sed -n -r -e 's/[^0-9]*([0-9]*)$/\1/p'`
    if [ ! $? -eq 0 ]; then
        echo "ERROR: switch name does not end in integer; cannot do MUL!"
        exit 1
    fi

    $OVSCTL br-exists $sw
    if [ ! $? -eq 0 ]; then
        echo "ERROR: ovs switch $sw does not exist!"
        exit 1
    fi

    vty=`expr $MULBASETTY + $switchno`
    exec telnet 127.0.0.1 $vty
}

mul_start() {
    sw=$1
    mul_core_start $sw
    sleep 2
    mul_cli_start $sw
    sleep 2
    mul_controller_start $sw
}

mul_restart() {
    sw=shift
    mul_stop $sw
    sleep 2
    mul_start $sw
}

##
## Do the command.
##
command=$1
shift
if [ "x$command" = "x" ]; then
    usage
    exit 1
fi
echo "Running $command..."
case $command in
create) create $@ ;;
destroy) destroy $@ ;;
portdel|delport) portdel $@ ;;
portadd|addport) portadd $@ ;;
portexec|portshell|shell) portexec $@ ;;
mul_start) mul_start $@ ;;
mul_stop) mul_stop $@ ;;
mul_telnet) mul_telnet $@ ;;
mul_restart) mul_restart $@ ;;
*) usage ; exit 1
esac

exit 0
