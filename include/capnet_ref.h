/*
 * Copyright (c) 2011, 2012, 2013, 2014, 2016 The University of Utah
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __CAPNET_REF_H__
#define __CAPNET_REF_H__

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#ifndef ptr_t
#define ptr_t unsigned long int
#endif

/*
 * This is a very simple set of reference count macros.  It supports
 * strong and weak refs (although you'll need to think carefully about
 * using those in tandem, because our weak refs are not at all complex;
 * these are only useful in strict parent-child relationships).  It is
 * not thread-safe; locking is your job.  It does do a good job helping
 * you debug ref leaks if you define REF_DEBUG; it uses global hash
 * tables to track refs and holders.
 * 
 * These macros really are intended to be used internally; if you need
 * to expose anything to library users, you'll of course want to define
 * per-object, public versions of these macros.
 */
typedef uint32_t REFCNT;
#define PRIiREFCNT PRIu32

/*
 * A few notes on references, especially weak references.  We do nothing
 * as far as cycle detection; it's too expensive.  Objects must be very
 * careful about referencing each other; such objects may not get
 * deleted since we do maintain data structures that contain any such
 * object (i.e., a referenced object may not be on some global
 * hashtable, but one it references may not be -- it may be buried in
 * some symtab tree -- and if we deleted the top of that tree, we'll be
 * left with a dangling ref).  To solve this, we would need some
 * library-wide, global, type-aware reftab -- more memory usage.
 *
 * So, unless you are careful, avoid it by by only taking top-down refs
 * (or whatever safe pattern works for you.  This means that objects
 * that hold refs to lower ones must clear the lower objects' backrefs
 * to them.  This may or not break the object model, of course.
 *
 * I applied these macros to a debugger library which read symbol files;
 * here are some notes about that.  Maybe fodder for doing cycle
 * detection.
 *
 *   In early versions of this library, we tried to get away with
 *   keeping non-cyclic, hierarchical parent->child refs only.  But
 *   unfortunately, some of our objects keep refs to the parent and
 *   operations on them end up requiring a parent deref.  So we have to
 *   make sure that the parent does not get dealloc'd until its children
 *   are gone.  But, this brings up the cyclic ref problem.  If an
 *   object gets unlinked from our global data structures, and it is no
 *   longer reachable but still need to be freed, we still have to know
 *   it needs to be freed -- but even this situation will never be
 *   arrived at for two objects that hold each other.  One must hold
 *   weakly to the other, so we know when it is safe to deallocate the
 *   first.
 *
 *   Imagine the scenario where symbols hold symbols/symtabs, are on
 *   symtabs, which are on either debugfiles or binfiles.  Symbols hold
 *   symtabs/symbols; symtabs hold debugfiles/binfiles/symbols;
 *   debugfiles hold symtabs.
 *
 *   Here's what we do.  We keep two counts for each object.  Strong
 *   refs are the parent->child refs; weak refs are the child->parent
 *   refs.  Basically, the idea is that when any object's strong ref
 *   counter becomes 0, we can start deallocating the object.
 *   Deallocation must first 1) remove itself from all global data
 *   structures, such as caches, and 2) release refs to any of its
 *   children (any objects it has strong refs to), and remove them from
 *   its structures IFF they are freed themselves.  If all children are
 *   freed, and there are no more weak refs to the object, we can finish
 *   freeing it (we need both constraints to know which children we must
 *   save and not remove from our object's data structures); else we
 *   must place the object on a global "to-free" hashtable.  Then, when
 *   weak refs are released, the object that was weakly held must be
 *   checked to see if it can be released.  That's how we will do this.
 *   This frees us from having to track ref holders.
 */

/**
 * NB for internal users that use refcnts to protect objects:
 *
 * If an object maintains its own refcnt, plus it allows other objects
 * to take weak refs to it, when it frees itself after its refcnt goes
 * to 0, it must protect itself from getting double-freed by RPUTW,
 * which will get called on it as it frees its children.  So these
 * functions just temporarily jack up its own refcnt temporarily to do
 * that.  By the time these are called, its refcnt is already 0, or
 * we're forcing a free.
 */
#define RWGUARD(x) ++((x)->refcnt)
#define RWUNGUARD(x) --((x)->refcnt)

#ifdef REF_DEBUG

/*
 * This debugging support is useful primarily for internal consumers
 * (where one object holds a ref to another).  If public, user functions
 * are added to allow library consumers to hold refs, it's unclear what
 * object holds the ref (depending on how the public function is
 * defined, of course).  This means we may not have good tracking for
 * lock owners (we might not know the address of the "owning" object).
 * So, when the user releases a lock on an object, we do not know who
 * the "owning" object was.  For these cases, we just keep a separate
 * count, so we can determine in a rough sense whether the leak was
 * internal or external.  But this is all somewhat dependent on how you
 * expose refs to users.
 */

#include <glib.h>
/*
 * NB: this table never gets freed unless you call REF_DEBUG_REPORT_FINISH().
 *
 * Oh, and this is NOT thread-safe, obviously.  Make it so if you need it...
 */

/* This one holds data for distinct obj/owner pairs. */
extern GHashTable *greftab;
/* This one holds data for self-owning objects (i.e., user-inspired refs). */
extern GHashTable *grefstab;

/* This one holds data for distinct obj/owner pairs. */
extern GHashTable *grefwtab;
/* This one holds data for self-owning objects (i.e., user-inspired refs). */
extern GHashTable *grefwstab;

#define REFCNTDECL(trefcnt) REFCNT trefcnt

#define RHOLD(x,hx)							\
    do {								\
        GHashTable *htab;						\
	char *buf;							\
	unsigned int count;						\
	void *_hx = (hx);						\
									\
        ++((x)->refcnt);						\
									\
	/* If self-ref, just save by caller function address. */	\
	if ((x) == _hx) {						\
	    _hx = (void *)__builtin_return_address(1);			\
	    fprintf(stderr,"REFDEBUG: hold %p,%p (%d) (self)\n",	\
		    (x),_hx,(x)->refcnt);				\
									\
	    if (unlikely(!grefstab)) {					\
		grefstab = g_hash_table_new(g_direct_hash,g_direct_equal); \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(grefstab,(gpointer)(x),htab);	\
	    }								\
	    else if (!(htab = (GHashTable *)g_hash_table_lookup(grefstab,(x)))) { \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(grefstab,(gpointer)(x),htab);	\
	    }								\
	    count = (unsigned int)(ptr_t)g_hash_table_lookup(htab,(gpointer)_hx); \
	    ++count;							\
	    g_hash_table_insert(htab,(gpointer)_hx,(void *)(ptr_t)count); \
	    /* XXX: we should track more than caller addr? */		\
	}								\
	else {								\
	    fprintf(stderr,"REFDEBUG: hold %p,%p (%d)\n",		\
		    (x),_hx,(x)->refcnt);				\
									\
	    if (unlikely(!greftab)) {					\
		greftab = g_hash_table_new(g_direct_hash,g_direct_equal); \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(greftab,(gpointer)(x),htab);	\
	    }								\
	    else if (!(htab = (GHashTable *)g_hash_table_lookup(greftab,(x)))) { \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,free);		\
		g_hash_table_insert(greftab,(gpointer)(x),htab);	\
	    }								\
									\
	    buf = malloc(sizeof(__FUNCTION__)+sizeof(__LINE__)+1+1);	\
	    snprintf(buf,sizeof(__FUNCTION__)+sizeof(__LINE__)+1+1,	\
		     "%s:%d",__FUNCTION__,__LINE__);			\
	    g_hash_table_insert(htab,(gpointer)_hx,buf);		\
	}								\
    } while (0);
#define RHOLDW(x,hx)							\
    do {								\
        GHashTable *htab;						\
	char *buf;							\
	unsigned int count;						\
	void *_hx = (hx);						\
									\
        ++((x)->refcntw);						\
									\
	/* If self-ref, just save by caller function address. */	\
	if ((x) == _hx) {						\
	    _hx = (void *)__builtin_return_address(1);			\
	    fprintf(stderr,"REFDEBUG: holdw %p,%p (%d) (self)\n",	\
		    (x),_hx,(x)->refcnt);				\
									\
	    if (unlikely(!grefwstab)) {					\
		grefwstab = g_hash_table_new(g_direct_hash,g_direct_equal); \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(grefwstab,(gpointer)(x),htab);	\
	    }								\
	    else if (!(htab = (GHashTable *)g_hash_table_lookup(grefwstab,(x)))) { \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(grefwstab,(gpointer)(x),htab);	\
	    }								\
	    count = (unsigned int)g_hash_table_lookup(htab,(gpointer)_hx); \
	    ++count;							\
	    g_hash_table_insert(htab,(gpointer)_hx,(void *)(ptr_t)count); \
	    /* XXX: we should track more than caller addr? */		\
	}								\
	else {								\
	    fprintf(stderr,"REFDEBUG: holdw %p,%p (%d)\n",		\
		    (x),_hx,(x)->refcnt);				\
									\
	    if (unlikely(!grefwtab)) {					\
		grefwtab = g_hash_table_new(g_direct_hash,g_direct_equal); \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,NULL);		\
		g_hash_table_insert(grefwtab,(gpointer)(x),htab);	\
	    }								\
	    else if (!(htab = (GHashTable *)g_hash_table_lookup(grefwtab,(x)))) { \
		htab = g_hash_table_new_full(g_direct_hash,g_direct_equal, \
					     NULL,free);		\
		g_hash_table_insert(grefwtab,(gpointer)(x),htab);	\
	    }								\
									\
	    buf = malloc(sizeof(__FUNCTION__)+sizeof(__LINE__)+1+1);	\
	    snprintf(buf,sizeof(__FUNCTION__)+sizeof(__LINE__)+1+1,	\
		     "%s:%d",__FUNCTION__,__LINE__);			\
	    g_hash_table_insert(htab,(gpointer)_hx,buf);		\
	}								\
    } while (0);
#define RPUT(x,objtype,hx,rc)						\
    do {								\
        typeof(x) _x = (x);						\
        void *_hx = (hx);						\
        GHashTable *htab;						\
	/*unsigned int count;					*/	\
									\
	/* if ((x)->refcnt == 0) */					\
	    /* asm("int $3"); */					\
									\
	(rc) = (--((x)->refcnt) == 0)					\
	            ? objtype ## _free(x,0) : ((x)->refcnt);		\
									\
	if (_x == _hx) {						\
	    if (!grefstab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: put %p,%p (%d) (self)\n",(x),_hx,(rc)); \
									\
	    htab = (GHashTable *)g_hash_table_lookup(grefstab,_x);	\
	    if (!htab)							\
		break;							\
	    /* Can't track holder; just nuke hashtable. */		\
	    if ((rc) == 0) {						\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(grefstab,_x);			\
	    }								\
	    /*count = (unsigned int)g_hash_table_remove(htab,_hx); */	\
	    /*if (--count == 0) {				*/	\
	    /*    if (g_hash_table_size(htab) == 0)		*/	\
	    /*	    g_hash_table_destroy(htab);			*/	\
	    /*}							*/	\
	    /*else						*/	\
	    /*	  g_hash_table_insert(htab,_hx,(gpointer)(ptr_t)count); */ \
	}								\
	else {								\
	    if (!greftab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: put %p,%p (%d)\n",(x),_hx,(rc));	\
									\
	    htab = (GHashTable *)g_hash_table_lookup(greftab,_x);	\
	    if (!htab)							\
		break;							\
	    g_hash_table_remove(htab,_hx);				\
	    if (g_hash_table_size(htab) == 0) {				\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(greftab,_x);			\
	    }								\
	}								\
    } while (0);
#define RPUTW(x,objtype,hx,rc)						\
    do {								\
        typeof(x) _x = (x);						\
        void *_hx = (hx);						\
        GHashTable *htab;						\
									\
	/* if ((x)->refcntw == 0) */					\
	    /* asm("int $3");  */					\
									\
	(rc) = ((--((x)->refcntw) + (x)->refcnt) == 0)			\
	        ? objtype ## _free(x,0) : ((x)->refcntw + (x)->refcnt);	\
									\
	if (_x == _hx) {						\
	    if (!grefwstab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putw %p,%p (%d) (self)\n",(x),_hx,(rc)); \
									\
	    htab = (GHashTable *)g_hash_table_lookup(grefwstab,_x);	\
	    if (!htab)							\
		break;							\
	    /* Can't track holder; just nuke hashtable. */		\
	    if ((rc) == 0) {						\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(grefwstab,_x);			\
	    }								\
	}								\
	else {								\
	    if (!grefwtab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putw %p,%p (%d)\n",(x),_hx,(rc));	\
									\
	    htab = (GHashTable *)g_hash_table_lookup(grefwtab,_x);	\
	    if (!htab)							\
		break;							\
	    g_hash_table_remove(htab,_hx);				\
	    if (g_hash_table_size(htab) == 0) {				\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(grefwtab,_x);			\
	    }								\
	}								\
    } while (0);
/*
 * Nobody should use RPUTNF/RPUTFF at all, ever -- it means you are not
 * using refcnts appropriately.  Do not enable them without talking to
 * David, and you better have a darn good reason.
 */
#if 0
#define RPUTFF(x,objtype,hx,rc)						\
    do {								\
        typeof(x) _x = (x);						\
        void *_hx = (hx);						\
        GHashTable *htab;						\
	unsigned int count;						\
									\
	(rc) = (--((x)->refcnt) == 0)					\
	            ? objtype ## _free(x,1) : objtype ## _free(x,1);	\
									\
	if (_x == _hx) {						\
	    if (!grefstab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putf %p,%p (%d) (self)\n",(x),_hx,(rc)); \
									\
	    htab = (GHashTable *)g_hash_table_lookup(grefstab,_x);	\
	    if (!htab)							\
		break;							\
	    count = (unsigned int)g_hash_table_remove(htab,_hx);	\
	    if (--count == 0) {						\
	        if (g_hash_table_size(htab) == 0)			\
		    g_hash_table_destroy(htab);				\
	    }								\
	    else							\
		g_hash_table_insert(htab,_hx,(gpointer)(ptr_t)count);	\
	}								\
	else {								\
	    if (!greftab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putf %p,%p (%d)\n",(x),_hx,(rc)); \
									\
	    htab = (GHashTable *)g_hash_table_lookup(greftab,_x);	\
	    if (!htab)							\
		break;							\
	    g_hash_table_remove(htab,_hx);				\
	    if (g_hash_table_size(htab) == 0) {				\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(greftab,_x);			\
	    }								\
	}								\
    } while (0);
#define RPUTNF(x,hx,rc)							\
    do {								\
        typeof(x) _x = (x);						\
        void *_hx = (hx);						\
        GHashTable *htab;						\
	unsigned int count;						\
									\
	(rc) = (--((x)->refcnt));					\
									\
	if (_x == _hx) {						\
	    if (!grefstab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putn %p,%p (%d) (self)\n",(x),_hx,(rc)); \
									\
	    htab = (GHashTable *)g_hash_table_lookup(grefstab,_x);	\
	    if (!htab)							\
		break;							\
	    count = (unsigned int)g_hash_table_remove(htab,_hx);	\
	    if (--count == 0) {						\
	        if (g_hash_table_size(htab) == 0)			\
		    g_hash_table_destroy(htab);				\
	    }								\
	    else							\
		g_hash_table_insert(htab,_hx,(gpointer)(ptr_t)count);	\
	}								\
	else {								\
	    if (!greftab)						\
		break;							\
									\
	    fprintf(stderr,"REFDEBUG: putn %p,%p (%d)\n",(x),_hx,(rc));	\
									\
	    htab = (GHashTable *)g_hash_table_lookup(greftab,_x);	\
	    if (!htab)							\
		break;							\
	    g_hash_table_remove(htab,_hx);				\
	    if (g_hash_table_size(htab) == 0) {				\
		g_hash_table_destroy(htab);				\
		g_hash_table_remove(greftab,_x);			\
	    }								\
	}								\
    } while (0);
#endif /* 0 */
/*
 * You should call this when your main program terminates.
 */
#define REF_DEBUG_REPORT_FINISH()					\
    do {								\
        GHashTableIter iter,iter2;					\
	GHashTable *htab;						\
	void *_x,*_hx;							\
	char *info;							\
	unsigned int count;						\
	if (greftab) {							\
	    g_hash_table_iter_init(&iter,greftab);			\
	    while (g_hash_table_iter_next(&iter,&_x,(gpointer)&htab)) {	\
		fprintf(stderr,"REFDEBUG: %d refs held for %p :\n",	\
			g_hash_table_size(htab),_x);			\
		g_hash_table_iter_init(&iter2,htab);			\
		while (g_hash_table_iter_next(&iter2,&_hx,(gpointer)&info)) { \
		    fprintf(stderr,"    %p held %p : %s\n",		\
			    _hx,_x,info);				\
		    g_hash_table_iter_remove(&iter2);			\
		}							\
		g_hash_table_destroy(htab);				\
									\
		/* asm("int $3"); */					\
	    }								\
	    g_hash_table_destroy(greftab);				\
	    greftab = NULL;						\
	}								\
	if (grefstab) {							\
	    g_hash_table_iter_init(&iter,grefstab);			\
	    while (g_hash_table_iter_next(&iter,&_x,(gpointer)&htab)) {	\
		fprintf(stderr,"REFDEBUG: %d refs self-held for %p :\n", \
			g_hash_table_size(htab),_x);			\
		g_hash_table_iter_init(&iter2,htab);			\
		while (g_hash_table_iter_next(&iter2,&_hx,(gpointer)&info)) { \
		    count = (unsigned int)(ptr_t)info;			\
		    fprintf(stderr,"    %p self-held %p : %d\n",	\
			    _hx,_x,count);				\
		    g_hash_table_iter_remove(&iter2);			\
		}							\
		g_hash_table_destroy(htab);				\
									\
		/* asm("int $3"); */					\
	    }								\
	    g_hash_table_destroy(grefstab);				\
	    grefstab = NULL;						\
	}								\
	if (grefwtab) {							\
	    g_hash_table_iter_init(&iter,grefwtab);			\
	    while (g_hash_table_iter_next(&iter,&_x,(gpointer)&htab)) {	\
		fprintf(stderr,"REFDEBUG: %d weak refs held for %p :\n", \
			g_hash_table_size(htab),_x);			\
		g_hash_table_iter_init(&iter2,htab);			\
		while (g_hash_table_iter_next(&iter2,&_hx,(gpointer)&info)) { \
		    fprintf(stderr,"    %p weakly held %p : %s\n",	\
			    _hx,_x,info);				\
		    g_hash_table_iter_remove(&iter2);			\
		}							\
		g_hash_table_destroy(htab);				\
									\
		/* asm("int $3"); */					\
	    }								\
	    g_hash_table_destroy(grefwtab);				\
	    grefwtab = NULL;						\
	}								\
	if (grefwstab) {						\
	    g_hash_table_iter_init(&iter,grefwstab);			\
	    while (g_hash_table_iter_next(&iter,&_x,(gpointer)&htab)) {	\
		fprintf(stderr,"REFDEBUG: %d weak refs self-held for %p :\n", \
			g_hash_table_size(htab),_x);			\
		g_hash_table_iter_init(&iter2,htab);			\
		while (g_hash_table_iter_next(&iter2,&_hx,(gpointer)&info)) { \
		    fprintf(stderr,"    %p weakly self-held %p : %s\n",	\
			    _hx,_x,info);				\
		    g_hash_table_iter_remove(&iter2);			\
		}							\
		g_hash_table_destroy(htab);				\
									\
		/* asm("int $3"); */					\
	    }								\
	    g_hash_table_destroy(grefwstab);				\
	    grefwstab = NULL;						\
	}								\
    } while (0);
#else
#define REFCNTDECL(trefcnt) 
#define RHOLD(x,hx)          ++((x)->refcnt)
#define RHOLDW(x,hx)         ++((x)->refcntw)
#define RPUT(x,objtype,hx,rc)  ((rc) = (--((x)->refcnt) == 0)	\
				           ? objtype ## _free(x,0) \
				           : (x)->refcnt); \
                               (rc) += 0
#define RPUTW(x,objtype,hx,rc)  ((rc) = ((--((x)->refcntw) + (x)->refcnt) == 0) \
				           ? objtype ## _free(x,0) \
				           : (x)->refcntw + (x)->refcnt); \
                               (rc) += 0
/*
 * Allows the caller to free the object even if the refcnt has gone
 * negative.  This is nice for the case that the caller allocates an
 * object, and passes it into the library where something in there holds
 * one or more refs to it.  But, if the caller later decides it wants to
 * "release" it because from its perspective it is no longer needed,
 * then it can call this sloppy release mechanism which allows it to
 * release an "implicit" ref.
 *
 * A cleaner way would be to hold a temp ref to it, and release that.
 * But if an algorithm is multi-part, it may be hard to follow that
 * strategy.
 */
#define RPUTIFZERO(x,objtype,rc)  ((rc) = (x)->refcnt == 0		\
				   ? objtype ## _free(x,0)		\
				   : (x)->refcnt)
#if 0
#define RPUTFF(x,objtype,hx,rc) ((rc) = (--((x)->refcnt) == 0) \
                                            ? objtype ## _free(x,1) \
                                            : objtype ## _free(x,1)
#define RPUTNF(x,hx,rc)         ((rc) = (--((x)->refcnt)))
#endif
#define REF_DEBUG_REPORT_FINISH() (void)
#endif

#endif /* __CAPNET_REF_H__ */
