/*
 * Copyright (c) 2011, 2012, 2013, 2014, 2015, 2016 The University of Utah
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __CAPNET_LOG_H__
#define __CAPNET_LOG_H__

#include <stdio.h>
#include <error.h>
#include <stdlib.h>
#include <argp.h>

#include "capnet_config.h"

extern struct argp log_argp;
#define log_argp_header "Log/Debug Options"

#define cncerr(format,...) fprintf(stdout,"CERROR:   %s:%d: "format,	\
				   __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define cncerrc(format,...) fprintf(stdout,format, ## __VA_ARGS__)
#define cncwarn(format,...)  fprintf(stdout,"CWARNING: %s:%d: "format,	\
				   __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define cncwarnc(format,...) fprintf(stdout,format, ## __VA_ARGS__)

#define cncinfo(format,...)  fprintf(stdout,"CINFO: %s:%d: "format,	\
				   __FUNCTION__, __LINE__, ## __VA_ARGS__)

#define cncwarnopt(level,area,flags,format,...) _cnc_warn(level,area,flags, \
						   "CWARNING: %s:%d: "format, \
						   __FUNCTION__, __LINE__, \
						   ## __VA_ARGS__)
#define cncwarnoptc(level,area,flags,format,...) _cnc_warn(level,area,flags,format, \
						    ## __VA_ARGS__)

/*
 * If you change these flags and bits, make really sure to update
 * lib/log.c!
 *
 * Rule for names: areas and flags must each be an alphanumeric string.
 */

/* Do something for all areas. */
#define LA_ALL        INT_MAX

typedef enum log_area_bits {
    LAB_NONE          = 0,
    LAB_LIB           = 1,
    LAB_CAP           = 2,
    LAB_CTL           = 3,
    LAB_PROTO         = 4,

    LAB_USER          = 31,
} log_area_bits_t;
typedef enum log_areas {
    LA_NONE           = 0,
    LA_LIB            = 1 << LAB_LIB,
    LA_CAP            = 1 << LAB_CAP,
    LA_CTL            = 1 << LAB_CTL,
    LA_PROTO          = 1 << LAB_PROTO,

    LA_USER           = 1 << LAB_USER,
} log_areas_t;

/*
 * Per-area flags.
 */
/* Mask all the per-area enums below :) */
typedef int log_flags_t;

typedef enum log_flag_bits_lib {
    LFB_LIB       = 0,
} log_flag_bits_lib_t;
typedef enum log_flags_lib {
    LF_LIB        = 1 << LFB_LIB,
} log_flags_lib_t;

typedef enum log_flag_bits_cap {
    LFB_CAP         = 0,
} log_flag_bits_cap_t;
typedef enum log_flags_cap {
    LF_CAP          = 1 << LFB_CAP,
} log_flags_cap_t;

typedef enum log_flag_bits_ctl {
    LFB_CTL        = 0,
    LFB_MD         = 1,
} log_flag_bits_ctl_t;
typedef enum log_flags_ctl {
    LF_CTL         = 1 << LFB_CTL,
    LF_MD          = 1 << LFB_MD,
} log_flags_ctl_t;

typedef enum log_flag_bits_proto {
    LFB_PROTO         = 0,
} log_flag_bits_proto_t;
typedef enum log_flags_proto {
    LF_PROTO          = 1 << LFB_PROTO,
} log_flags_probe_t;

/*
typedef enum log_flag_bits_ {
    LFB_         = 0,
    LFB_         = 1,
    LFB_         = 2,
} log_flag_bits__t;
typedef enum log_flags_ {
    LF_          = 1 << LFB_,
    LF_          = 1 << LFB_,
    LF_          = 1 << LFB_,
} log_flags__t;
*/

/*
 * Some special per-area masks.
 */
/* Set every last bit for a specific area. */
#define LF_ALL        INT_MAX
#define LF_L_ALL (LF_LIB)
#define LF_C_ALL (LF_CAP)
#define LF_T_ALL (LF_CTL | LF_MD)
#define LF_P_ALL (LF_PROTO)
/* Set every last bit for the user area. */
#define LF_U_ALL        INT_MAX

/*
 * Set the debug log level.
 */
void cnc_set_log_level(int level);
/*
 * Increase the debug log level by 1.
 */
void cnc_inc_log_level(void);
/*
 * Set the warn log level.
 */
void cnc_set_warn_level(int level);
/*
 * Increase the warn level by 1.
 */
void cnc_inc_warn_level(void);
/*
 * Reinitialize the flags for one or more areas.
 */
void cnc_set_log_area_flags(log_areas_t area,log_flags_t flags);
int cnc_set_log_area_flaglist(char *flaglist,char *separator);
/*
 * Additively (OR) change the flags for one or more areas.
 */
void cnc_add_log_area_flags(log_areas_t areas,log_flags_t flags);
int cnc_add_log_area_flaglist(char *flaglist,char *separator);
/*
 * Users should set this before calling any argp parsing routines we
 * provide if they want the -l standard argument to accept their debug
 * flags.
 *
 * @names must be a NULL-terminated list of names.
 */
void cnc_set_user_area_flags(char **names);

/*
 * Users should not need to call this.
 */
int cnc_log_get_flag_val(char *flag,log_areas_t *areaval,log_flags_t *flagval);

void _cnc_debug(int level,log_areas_t areas,log_flags_t flags,char *format,...);
void _cnc_warn(int level,log_areas_t areas,log_flags_t flags,char *format,...);

int cnc_debug_is_on(int level,log_areas_t areas,log_flags_t flags);
int cnc_warn_is_on(int level,log_areas_t areas,log_flags_t flags);

error_t log_argp_parse_opt(int key,char *arg,struct argp_state *state);

#ifdef CNC_DEBUG
#define cncdbg(level,areas,flags,format,...) _cnc_debug(level,areas,flags,"CDEBUG: %s:%d: "format, __FUNCTION__, __LINE__, ## __VA_ARGS__)
#define cncdbgc(level,areas,flags,format,...) _cnc_debug(level,areas,flags,format, ## __VA_ARGS__)

#else

#define cncdbg(devel,areas,flags,format,...) ((void)0)
#define cncdbgc(devel,areas,flags,format,...) ((void)0)

#endif

#endif /* __CAPNET_LOG_H__ */
