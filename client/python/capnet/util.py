import capnet
from collections import namedtuple
NodeProperties = namedtuple("NodeProperties", 
                            ["node", "grant", "info", "rp", "flow"])

def recv_preamble(p):
    rp0 = p.rp0()
    me = rp0.recv_wait()
    info = me.info()
    flow = p.create(capnet.Flow)
    me_props = NodeProperties(me, None, info, None, flow)

    broker = rp0.recv()
    assert broker.__class__ == capnet.Broker
    return rp0, me_props, broker

def recv_nodes(p, rp_iter):
    nodes = {}
    for node in rp_iter:
        grant = node.reset()
        node_rp = grant.rp0()
        info = node.info()
        flow = grant.flow()
        nodes[info.name] = NodeProperties(node, grant, info, node_rp, flow)
    return nodes

def make_safe_rps_send(p, rp):
    """Make two directional RPs from the given RP. 
    If both sides use the same RP they can experience race conditions where
    the sender receives its own capability again.
    
    One side should use the "_send" version and the other side should use the
    "_recv" version, it doesn't really matter which uses which.

    The "send" version only excutes the "send" method of the given RP, and
    the "recv" versoin executes only the "recv" method of the given rp. This
    guarantees that the capabilities will not race.
    
    see also: capnet.util.SafeRP
    """
    send_rp = p.create(capnet.RP)
    recv_rp = p.create(capnet.RP)
    rp.send(send_rp)
    rp.send(recv_rp)
    return send_rp, recv_rp

def make_safe_rps_recv(rp):
    "see make_safe_rps_send"
    recv_rp = rp.recv_wait()
    send_rp = rp.recv_wait()
    return send_rp, recv_rp

class SafeRP(object):

    @classmethod
    def from_rp_send(kls, rp):
        send_rp, recv_rp = make_safe_rps_send(rp.protocol, rp)
        return kls(send_rp, recv_rp)

    @classmethod
    def from_rp_recv(kls, rp):
        send_rp, recv_rp = make_safe_rps_recv(rp)
        return kls(send_rp, recv_rp)

    def __init__(self, send_rp, recv_rp):
        self.send_rp = send_rp
        self.recv_rp = recv_rp

    def send(self, cap=None, msg=None):
        return self.send_rp.send(cap=cap, msg=msg)

    def recv_wait(self, timeout=None):
        return self.recv_rp.recv_wait(timeout=timeout)

    def recv(self):
        return self.recv_rp.recv()
