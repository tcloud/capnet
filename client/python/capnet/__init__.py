"""
Capability protocol client interface.

Note: All objects that inherit from 'Capability' are capabilities, not actual
CapNet objects. They are however python objects, and provide the same methods
that the actual objects would provide. However, two Node capability objects can
have different capabilities that reference the same CapNet object, or even
reference the same underlying capability.

Basic Usage:

import capnet

# Create a protocol object
p = capnet.Protocol()

# Get a node capability from our RP0
n1 = p.rp0().recv()

# Mint a copy of this node capability
n1_1 = n1.mint()

# Reset the node, and set its rp0 to rp1
grant = n1.reset()

# Get the RP0 of the newly reset node
rp1 = grant.rp0()

# Send it a capability to itself
rp1.send(n1_1)

# Revoke this capability (undo the send)
n1_1.revoke()
"""

import socket
import os
import struct
import time

import logging as log
log.getLogger().setLevel(log.INFO)

from capnet.proto import capnet_cp_pb2 as cn_pb

from contextlib import contextmanager

# The ethernet mtu as defined in <net/ethernet.h>
ETHERMTU = 1500

# The minimum number of bytes that must be in an ethernet frame's payload
ETHER_PAYLOAD_MIN = 46

ETHER_BROADCAST = '\xff' * 6

#CAPNET_ADDR = '\x12\x34\x56\x78\x90\xAB'
#CAPNET_ADDR = '\x9a\x83\x2e\x8a\x26\x42'
CAPNET_ADDR = ETHER_BROADCAST

# The byte length of the capability message size indicator
CAP_SIZE_LEN = 2

CAP_MAGIC = 0x434e5000
CAP_VERSION = 0x1

CAP_UUID_LEN = 64 / 8

# The ARP hardware number for ethernet as defined in <linux/if_arp.h>
ARPHRD_ETHER = 1

## Python AF_PACKET address format:
#  (It's not documented anywhere else, this is from reading the code, specifically
#   3.4.4, so beware of other versions. They *should* work the same though)
#
# Sending Address:
# This should be a tuple with the following fields. Fields that start with 'A:'
# are required and must always be included. Fields that start with 'B:' are
# optinal, and only need to be included when required. They probably all need to
# be set though if they're included.
#   A: string - interface name
#   A: int - ethertype (in host byte order)
#   B: int - pkttype from packet(7)
#   B: int - arp hardware number (see packet(7))
#   B: bytes - destination mac address
#
# Recving Address:
# A 5 item tuple. All fields are always included.
#   string - interface name
#   py int (from signed short) - ethertype (in host byte order)
#   py int (from unsigned char) - pkttype from packet(7)
#   py int (from signed short) - arp hardware number (see packet(7))
#   bytes - sender mac address, no modifications from C struct

def hex_bytes(b):
    out = ""
    for c in b:
        hv = hex(ord(c))[2:]
        out += "0" + hv if len(hv) < 2 else hv
    return out

class _Sockaddr_ll(object):
    "A nicer representation of AF_PACKET address tuples"

    def __init__(self, addr):
        self.interface = addr[0]
        self.ethertype = addr[1]
        self.packet_type = addr[2]
        self.hardware_number = addr[3]
        self.mac_addr = MACAddress(addr[4])

    @property
    def as_tuple(self):
        return (self.interface, self.ethertype, 
                self.packet_type, self.hardware_number, 
                self.mac_addr)

    def __repr__(self):
        return "_Sockaddr_ll" + str(self.as_tuple)

    def __str__(self):
        return repr(self)

def uuid():
    return struct.unpack("@L", os.urandom(CAP_UUID_LEN))[0]

class IPv4Address(object):
    def __init__(self, addr_bytes):
        assert len(addr_bytes) == 4
        self.addr = struct.unpack("!L", addr_bytes)[0]
        self.bytes_addr = struct.pack("=L", self.addr)
        self.str_addr = socket.inet_ntoa(self.bytes_addr)

    def __repr__(self):
        return "IPv4Address(addr={0})".format(repr(self.str_addr))

    def __str__(self):
        return self.str_addr

class MACAddress(object):
    @classmethod
    def from_string(kls, mac_str):
        mac_bytes = [chr(int(x, 16)) for x in mac_str.split(":")]
        return kls(bytes("".join(mac_bytes)))

    @classmethod
    def _nibble_to_hex(kls, nib, case='a'):
        assert nib < 16
        if nib <= 9: return chr(ord('0') + nib)
        else: return chr(ord(case) + (nib - 10))

    @classmethod
    def _bytes_to_str(kls, bytes):
        rep = []
        for byte in bytes:
            byte = ord(byte)
            upper = kls._nibble_to_hex((byte >> 4) & 0xF)
            lower = kls._nibble_to_hex(byte & 0xF)
            rep.append(upper + lower)
        return ":".join(rep)

    def __init__(self, addr_bytes):
        assert len(addr_bytes) == 6
        self.addr = addr_bytes
        self.str_addr = self._bytes_to_str(self.addr)

    def __repr__(self):
        return "MACAddress(addr={0})".format(repr(self.str_addr))

    def __str__(self):
        return self.str_addr

    def __eq__(self, o):
        return self.addr == o.addr

    def __ne__(self, o):
        return not (self.__eq__(o))

class NodeInfo(object):

    def __init__(self, result):
        self.name = result.name
        self.ipv4 = IPv4Address(result.ipv4)
        self.mac = MACAddress(result.mac)
        self.id = result.id
        self.domain = result.domain
        self.owner = result.owner

    def __str__(self):
        return "NodeInfo(name={0}, ipv4={1}, mac={2}, id={3}, domain={4}, owner={5})"\
               .format(repr(self.name), self.ipv4, self.mac, self.id,
                       self.domain, self.owner)

    def __repr__(self): return str(self)

class ReplyItem(object):
    """An object representing a item in a reply. Items can be node info structures,
       a message, or a capability"""

    def __init__(self, reply_item, proto):
        self.item = reply_item
        self.proto = proto

    @property
    def is_node_info(self):
        return self.item.WhichOneof("result") == "result_node_info"

    @property
    def node_info(self):
        assert self.is_node_info, "not a node_info reply item"
        return NodeInfo(self.item.result_node_info)

    @property
    def is_capability(self):
        return self.item.WhichOneof("result") == "result_cptr"

    @property
    def capability(self):
        assert self.is_capability, "must be capability to actually get capability"
        cap_class = self.proto.class_for_objtype(self.item.result_cptr.cptr_type)
        return cap_class(self.item.result_cptr.cptr, self.proto)

    @property
    def is_msg(self):
        return self.item.WhichOneof("result") == "result_message"

    @property
    def msg(self):
        assert self.is_msg, "must be message to actually get message"
        return self.item.result_message.data.decode("utf-8")

class Reply(object):
    SUCCESS = True
    FAILURE = False

    def __init__(self, reply, proto):
        self.proto = proto
        self.reply = reply
        if reply.status == cn_pb.CPSTATUS_SUCCESS:
            self.status = Reply.SUCCESS
        else:
            self.status = Reply.FAILURE
        self.msg = reply.msg
        self.items = map(lambda ri: ReplyItem(ri, proto), reply.items);
        if len(self.items) == 1:
            self.item = self.items[0]

    @property
    def has_no_items(self):
        return not self.items

    def __len__(self):
        return len(self.items)

    def __getitem__(self, item):
        return self.items[item]

class ProtocolException(Exception):
    BAD_PAYLOAD_FORMAT = "The payload is improperly formatted"
    BAD_MAGIC_NUMBER = "The magic number is incorrect"
    BAD_VERSION = "The protocol version number is incorrect"
    BAD_REPLY_TYPE = "Message type is incorrect"

    def __init__(self, type, extra):
        self.type = type
        self.extra = extra
        super(ProtocolException, self).__init__(type)

    def __str__(self):
        if self.extra:
            return "ProtocolException: {0} ({1})".format(self.type, self.extra)
        else:
            return "ProtocolException: {0}".format(self.type)

class OperationError(Exception):

    def __init__(self, reply):
        self.reply = reply
        super(OperationError, self).__init__()

    def __str__(self):
        return "OperationError: {0}".format(self.reply.msg)

class FutureNotReceivedError(Exception): ""

class TimeoutException(Exception): ""

class Future(object):

    @classmethod
    def _recv_next_timeout(kls, protocol, timeout):
        if timeout is not None and timeout <= 0.0:
            raise TimeoutException()
        start = time.time()
        try:
            protocol._recv_next(timeout=timeout)
        except socket.timeout:
            raise TimeoutException()
        return time.time() - start

    @classmethod
    def recv_any(kls, protocol, *futures, **kwargs):
        """'timeout' can be passed as a keyword argument, but defaults to none"""
        timeout = kwargs.get("timeout", None)
        while True:
            for future in futures:
                try:
                    return future.recv(nowait=True)
                except FutureNotReceivedError:
                    continue
            waited = kls._recv_next_timeout(protocol, timeout)
            if timeout is not None:
                timeout -= waited

    def __init__(self, protocol):
        self.protocol = protocol

    def fulfil(self, value):
        raise NotImplementedError("fulfil is abstract")

    def recv_nowait(self):
        raise NotImplementedError("recv_nowait is abstract")

    def recv(self, nowait=False, timeout=None):
        while True:
            try:
                return self.recv_nowait()
            except FutureNotReceivedError:
                if nowait:
                    raise
                else:
                    pass
            waited = self._recv_next_timeout(self.protocol, timeout)
            if timeout is not None:
                timeout -= waited

class ReplyFuture(Future):

    def __init__(self, msg, protocol):
        self.uuid = msg.uuid
        self.msg = msg
        self._protocol = protocol
        self._value = None
        super(ReplyFuture, self).__init__(protocol)

    def fulfil(self, value):
        self._value = value

    def recv_nowait(self):
        if self._value is None:
            raise FutureNotReceivedError()
        if self._value.status != Reply.SUCCESS:
            raise OperationError(self._value)
        return self._value

class TransportException(Exception):
    "Raised when we receive a packet we didn't expect to receive"

    def __init__(self, sockaddr):
        self.sockaddr = sockaddr
        super(TransportException, self).__init__(sockaddr)

class Transport(object):

    "The ethertype of the capability protocol"
    ETH_P_CAP = 0x0809

    def __init__(self, send_intf, ethtype=ETH_P_CAP, mtu=ETHERMTU):
        self.intf = send_intf
        self.mtu = mtu
        self.ethtype = ethtype

        self.sock = socket.socket(socket.AF_PACKET, socket.SOCK_DGRAM, ethtype)
        self.sock.bind((self.intf, self.ETH_P_CAP))

        with open("/sys/class/net/{0}/address".format(self.intf)) as f:
            self.intf_addr = MACAddress.from_string(f.read())

    def _recv_pkt(self,timeout=None):
        self.sock.settimeout(timeout)
        data, addr = self.sock.recvfrom(self.mtu)
        log.debug("recv data: |{0}|".format(hex_bytes(data)))
        addr = _Sockaddr_ll(addr)
        if (addr.packet_type == socket.PACKET_OUTGOING 
                or addr.mac_addr != self.intf_addr):
            raise TransportException(addr)
        if addr.ethertype != self.ETH_P_CAP:
            log.error("Somehow got packet for non-cap ethertype")
            raise TransportException(addr)
        return data, addr

    def _send_pkt(self, payload, dst_addr, flags=0, timeout=None):
        self.sock.settimeout(timeout)
        addr = (self.intf, self.ethtype, socket.PACKET_OUTGOING, 
                ARPHRD_ETHER, dst_addr)
        return self.sock.sendto(payload, flags, addr)

    def _parse_cap_payload(self, payload):
        assert len(payload) >= 2
        cap_size = struct.unpack("!H", payload[0:2])[0]
        if cap_size > (len(payload) - 2):
            raise ProtocolException(ProtocolException.BAD_PAYLOAD_FORMAT,
                                    "Reported payload size larger than available payload")
        return payload[2:cap_size+2]

    def _build_cap_payload(self, msg):
        padding = ''
        total_msg_size = len(msg) + CAP_SIZE_LEN
        if total_msg_size < ETHER_PAYLOAD_MIN:
            padding = '\x00' * (ETHER_PAYLOAD_MIN - total_msg_size)
        size = struct.pack("!H", len(msg))
        log.debug("msg size: {0} |{3}|, padding-len: {1}, msg: {2}".format(
                  len(msg), len(padding), repr(msg), hex_bytes(msg)))
        return size + msg + padding

    def _send_cap_msg(self, msg, addr):
        payload = self._build_cap_payload(msg)
        return self._send_pkt(payload, addr)

    def _recv_cap_msg(self,timeout=None):
        data, addr = self._recv_pkt(timeout=timeout)
        return self._parse_cap_payload(data), addr.mac_addr

class Protocol(object):
    """Represenents the protocol/connection to the CapNet controller. An instance
    of this object is required to send any capability messages."""

    "The default message transmission retry timeout. Default = 1s"
    DEFAULT_RETRY_TIMEOUT = 1.0

    _CPObjType_class_map = {}

    @classmethod
    def register_objtype(this_kls, kls):
        this_kls._CPObjType_class_map[kls.__objtype__] = kls
        return kls

    @classmethod
    def class_for_objtype(kls, objtype):
        return kls._CPObjType_class_map[objtype]

    def __init__(self, send_intf, ethtype=Transport.ETH_P_CAP, mtu=ETHERMTU):
        self.transport = Transport(send_intf, ethtype=ethtype, mtu=mtu)
        self.pending_notifications = {}
        self.pending_replies = {}

    _CPMessage_fields = {
        cn_pb.CPReply: "reply",
        cn_pb.CPInvoke: "invoke",
        cn_pb.CPCreate: "create",
        cn_pb.CPMint: "mint",
        cn_pb.CPRevoke: "revoke",
        cn_pb.CPDelete: "delete",
        cn_pb.CPRP0: "rp0",
        cn_pb.CPDebugGrant: "debug_grant"
    }

    def _recv_msg(self, timeout=None):
        data, _ = self.transport._recv_cap_msg(timeout=timeout)
        result = cn_pb.CPMessage()
        result.ParseFromString(data)
        if not result.IsInitialized():
            raise ProtocolException(ProtocolException.BAD_PAYLOAD_FORMAT)
        if result.magic != CAP_MAGIC:
            raise ProtocolException(ProtocolException.BAD_MAGIC_NUMBER)
        if result.version != CAP_VERSION:
            raise ProtocolException(ProtocolException.BAD_VERSION)
        return result

    def _construct_message(self, pb):
        msg = cn_pb.CPMessage()
        msg.magic = CAP_MAGIC
        msg.version = CAP_VERSION
        msg.uuid = uuid()
        if pb.__class__ not in self._CPMessage_fields:
            raise Exception("Tried to send unrecognized structure")
        field = self._CPMessage_fields[pb.__class__]
        getattr(msg, field).MergeFrom(pb)
        return msg

    def _handle_reply(self, msg):
        if msg.uuid not in self.pending_replies:
            log.warn("Received non-pending reply, discarding (uuid: {0})".format(msg.uuid))
            return
        self.pending_replies[msg.uuid].fulfil(Reply(msg.reply, self))
        del self.pending_replies[msg.uuid]

    def _send_ack(self, uuid, status):
        ack = cn_pb.CPReply()
        if status == Reply.SUCCESS:
            ack.status = cn_pb.CPSTATUS_SUCCESS
            ack.msg = "success"
        elif status == Reply.FAILURE:
            ack.status = cn_pb.CPSTATUS_FAILURE
            ack.msg = "failure"
        else:
            assert False, "unreachable"
        msg = self._construct_message(ack)
        msg.uuid = uuid
        self.transport._send_cap_msg(msg.SerializeToString(), CAPNET_ADDR)

    def _handle_notify(self, msg):
        if msg.notify.reply_for not in self.pending_notifications:
            log.warn("Received non-pending notification, acking failed (reply_for: {0})"\
                     .format(msg.notify.reply_for))
            self._send_ack(msg.uuid, Reply.FAILURE)
            return
        self.pending_notifications[msg.notify.reply_for].fulfil(
            Reply(msg.notify.reply, self)
        )
        del self.pending_notifications[msg.notify.reply_for]
        self._send_ack(msg.uuid, Reply.SUCCESS)

    def _recv_next(self, timeout=None):
        msg = self._recv_msg(timeout)
        log.info("Recv MSG: {0}".format(hex(msg.uuid)))
        message_type = msg.WhichOneof("type")
        if message_type == "reply":
            return self._handle_reply(msg)
        elif message_type == "notify":
            return self._handle_notify(msg)
        else:
            raise ProtocolException(ProtocolException.BAD_REPLY_TYPE, message_type)

    def register_notify(self, msg):
        f = ReplyFuture(msg, self)
        self.pending_notifications[f.uuid] = f
        return f

    def _send_msg(self, msg):
        f = self.pending_replies.get(msg.uuid, ReplyFuture(msg, self))
        if msg.uuid not in self.pending_replies:
            self.pending_replies[msg.uuid] = f
        self.transport._send_cap_msg(msg.SerializeToString(), CAPNET_ADDR)
        log.info("Sent MSG: {0}".format(hex(msg.uuid)))
        return f

    def _request(self, sub_msg, register_notify=False, **kwargs):
        """Note: Specifiying retry_timeout=... will set the retry timeout. Valid
           values are floating point values greater than zero (in seconds) and
           "None" which means that no retries will occur (we'll wait till we 
           get a reply)."""
        retry_timeout = kwargs.get("retry_timeout", self.DEFAULT_RETRY_TIMEOUT)
        msg = self._construct_message(sub_msg)
        notification = None
        if register_notify:
            notification = self.register_notify(msg)
        reply_f = self._send_msg(msg)
        while True:
            try:
                reply_f.recv(timeout=retry_timeout)
                break
            except TimeoutException:
                pass
            self._send_msg(msg)
        if register_notify:
            return notification
        return reply_f.recv(nowait=True)

    def create(self, cap_type, *args):
        """Create a new capability of type 'cap_type' with the given arguments.
        'cap_type' must be a class that inherits from 'Capability'. There are
         currently four possible ways to invoke this function:

         .create(Node) -> returns Node object
         .create(RP) -> returns RP object
         .create(Membrane) -> returns Membrane object
         .create(Flow, <node>, <flow restrictions>)
            -> return a new flow for a node with the given flow restrictions.
               <node>.flow(<flow restrictions>) is a shortcut for this.
        """

        msg = cn_pb.CPCreate()

        if cap_type == Flow and args:
            node_grant = args[0]
            msg.obj_type = cap_type.__objtype__
            msg.flow_args.node_grant_cptr = node_grant.cptr
            msg.flow_args.spec.MergeFrom(cn_pb.CPFlowSpec())
        # The default interface
        elif hasattr(cap_type, "__objtype__"):
            msg.obj_type = cap_type.__objtype__
        else:
            raise NotImplementedError("Create: {0}".format(cap_type.__name__))

        return self._request(msg).item.capability

    def mint(self, cap, *args):
        """Mint a new capability from an existing capability. 'cap' should
        be an instance of a capability object that inherets from the
        'Capability' class.

        When minting flows, *args may be used to pass flow restrictions.

        <cap>.mint(...) is a shorthand for this call.

        Returns a new capability object that points to the same object that
        was passed to the mint call. These two objects may be deleted/revoked
        independently.
        """
        if args: raise NotImplementedError("mint not implemented with flow args")
        msg = cn_pb.CPMint()
        msg.cptr = cap.cptr
        return self._request(msg).item.capability

    def revoke(self, cap):
        """Revoke a capability to an object <cap>. <cap>.revoke() is shorthand
        for this call.
        
        Returns nothing.
        """
        msg = cn_pb.CPRevoke()
        msg.cptr = cap.cptr
        # If it fails, the error will be thrown by this function
        self._request(msg)

    def delete(self, cap):
        """Delete a capability to an object <cap>. <cap>.delete() is shorthand
        for this call.
        
        Returns nothing.
        """
        msg = cn_pb.CPDelete()
        msg.cptr = cap.cptr
        # If it fails, the error will be thrown by this function
        self._request(msg)

    def rp0(self):
        "Get the RP0 for this principal. Returns a rendezvous point."
        msg = cn_pb.CPRP0()
        return self._request(msg).item.capability

    def _invoke(self, cap, method, args, register_notify=False):
        msg = cn_pb.CPInvoke()
        msg.cptr = cap.cptr
        msg.method = method
        if args is not None:
            getattr(msg, args[0]).MergeFrom(args[1])
        return self._request(msg, register_notify=register_notify)

    def debug_grant(self, to, cap):
        msg = cn_pb.CPDebugGrant()
        msg.receiver = to.cptr
        msg.cptr = cap.cptr
        return self._request(msg).item.capability

class Capability(object):
    """The base class for all received capabilities. Has a reference to a
    protocol instance."""

    def __init__(self, cptr, protocol):
        self.protocol = protocol
        self.cptr = cptr

    def mint(self):
        "See Protocol.mint"
        return self.protocol.mint(self)

    def delete(self):
        "See Protocol.delete"
        return self.protocol.delete(self)

    def revoke(self):
        "See Protocol.revoke"
        return self.protocol.revoke(self)

    def __str__(self):
        return "{0}(cap={1})".format(self.__class__.__name__, hex(self.cptr))

    def __repr__(self): return str(self)

    def type(self):
        return self.__objtype__

    def __del__(self):
        self.delete()
        # Object has no __del__ method, otherwise, we'd have to call it
        #object.__del__(self)

@Protocol.register_objtype
class Node(Capability):
    __objtype__ = cn_pb.CPOBJTYPE_NODE

    def info(self):
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_NODE_INFO, None)
        return reply.item.node_info

    def reset(self):
        """Reset this node.
        
        Returns a node grant capability for this node.
        """
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_NODE_RESET, None)
        return reply.item.capability

# Exception raised when there is a protocol violation in the recv_iter protocol
class RecvIterException(Exception):
    ""

@Protocol.register_objtype
class RP(Capability):
    __objtype__ = cn_pb.CPOBJTYPE_RP

    def send(self, cap=None, msg=None):
        """Send the given capability on this rendezvous point.
        
        Returns nothing.
        """
        args = cn_pb.CPInvokeRPSendArgs()
        if cap is not None:
            args.cptr = cap.cptr
        if msg is not None:
            args.msg = msg.encode("utf-8")
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_RP_SEND, 
                                            ("rp_send_args", args))
        assert reply.has_no_items

    def _gen_recv_result(self, reply):
        if len(reply) == 1:
            if reply.item.is_msg: 
                return reply.item.msg
            elif reply.item.is_capability: 
                return reply.item.capability
            else:
                raise OperationError(reply)
        elif len(reply) == 2:
            return (reply[0].capability, reply[1].msg)
        else:
            raise OperationError(reply)

    def recv(self):
        """Receive the next available capability on this rendezvous point.

        Returns the received object capability.
        """
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_RP_RECV, None)
        return self._gen_recv_result(reply)

    def _recv_wait_future(self):
        """This is an internal operation intended for advanced usage. Instead
           of returning a capability result, it returns a future for the result
           that can be collected using the recv method of the future."""
        future = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_RP_RECV_WAIT, None,
                                       register_notify=True)
        return future

    def recv_wait(self, timeout=None):
        """Same as a recv operation, but it will wait until a capability is
        sent on the membrane instead of throwing an error.

        Returns the received object capability.
        """
        future = self._recv_wait_future()
        result = future.recv(timeout=timeout)
        return self._gen_recv_result(result)

    @contextmanager
    def send_iter_context(self):
        """This context can be used to iteratively send a number of capabilites
           that can be received by "recv_iter" with no timeouts"""
        iter_id = hex(uuid())
        self.send(msg="start-iter_{}".format(iter_id))
        yield
        self.send(msg="stop-iter_{}".format(iter_id))

    def recv_iter(self, timeout=None):
        """Iteratively receive capabilities that were sent in a 
           "send_iter_context" context."""
        start, iter_id = self.recv_wait(timeout=timeout).split("_")
        if start != "start-iter":
            raise RecvIterException("Expected start-iter")
        while True:
            msg = self.recv_wait(timeout=timeout)
            try:
                stop, _id = msg.split("_")
                if stop != "stop-iter":
                    raise RecvIterException("Expected stop-iter")
                if _id != iter_id:
                    raise RecvIterException("Got stop-iter for wrong id")
                return
            except AttributeError:
                yield msg

    def recv_iter_exn(self):
        """Iteritively receive rp elems until the rendezvous point throws an
           exception, presumably because it is empty"""
        yield self.recv_wait()
        while True:
            try:
                yield self.recv()
            except OperationError, e:
                log.warn(e)
                return

    def recv_iter_timeout(self, timeout):
        """Iteratively receive rp capabilities until no capabilites are received for
           'timeout' seconds"""
        yield self.recv_wait()
        while True:
            try:
                yield self.recv_wait(timeout=timeout)
            except TimeoutException:
                return

@Protocol.register_objtype
class Broker(Capability):
    __objtype__ = cn_pb.CPOBJTYPE_BROKER

    def register(self, service_name, cap):
        """Register the given service and a cap to an RP it will receive caps on,
        on this Broker object.
        
        Returns nothing.
        """
        args = cn_pb.CPInvokeBrokerRegisterArgs()
        args.service_name = service_name
        args.rp_cptr = cap.cptr
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_BROKER_REGISTER,
                                            ("broker_register_args", args))
        assert reply.has_no_items

    def lookup(self, service_name):
        """Lookup @service_name on this Broker, and return a capability to an
        RP to talk to the service, if it was registered at this Broker.

        Returns the received object capability.
        """
        args = cn_pb.CPInvokeBrokerLookupArgs()
        args.service_name = service_name
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_BROKER_LOOKUP,
                                      ("broker_lookup_args", args))
        return reply.item.capability

    def lookup_wait(self, service_name, timeout=None):
        """Same as lookup, but if no service is registered, wait until a service
           has been registered"""
        args = cn_pb.CPInvokeBrokerLookupWaitArgs()
        args.service_name = service_name

        future = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_BROKER_LOOKUP_WAIT,
                                       ("broker_lookup_wait_args", args), 
                                       register_notify=True)
        result = future.recv(timeout=timeout)
        return result.item.capability

@Protocol.register_objtype
class Membrane(Capability):
    __objtype__ = cn_pb.CPOBJTYPE_MEMBRANE

    def send(self, cap):
        """Send the given capability through this membrane.

        Returns nothing.
        """
        args = cn_pb.CPInvokeMembraneSendArgs()
        args.cptr = cap.cptr
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_MEMBRANE_SEND, 
                                            ("membrane_send_args", args))
        assert reply.has_no_items

    def recv(self):
        """Receive a capability from this membrane.

        Returns the next capability in the queue wrapped according to the
        membrane semantics. See the paper or ask someone. Or remind me
        to actually finish this docstring.
        """
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_MEMBRANE_RECV, None)
        return reply.item.capability

    def external(self):
        """Get the external version of this membrane pair.

        Returns a capability to the external membrane. Note: the "type" of
        that capability is still just "Membrane".
        """
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_MEMBRANE_EXTERNAL, None)
        return reply.item.capability

    def clear(self):
        """Clear this membrane. All capabilites sent from the internal -> external
        or external -> internal side that have not crossed back over to the internal
        side will be revoked.

        Returns Nothing.
        """
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_MEMBRANE_CLEAR, None)
        assert reply.has_no_items

    def recv_wait(self, timeout=None):
        """Same as a recv operation, but it will wait until a capability is
        sent on the membrane instead of throwing an error.

        Returns the received object capability.
        """
        future = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_MEMBRANE_RECV_WAIT,
                                       None, register_notify=True)
        # The future could be directly exposed, but for now, we'll just always
        # block
        result = future.recv(timeout=timeout)
        return result.item.capability

@Protocol.register_objtype
class Flow(Capability):
    """Note: Flow Capabilities are not automatically deleted when they go
    out of scope, the .delete() method must be called explicitly."""
    __objtype__ = cn_pb.CPOBJTYPE_FLOW

    def mint(self, *args):
        """Create a new capability from this capability with the given
        restrictions.

        Returns a new flow capability.
        """
        # XXX Note:, I'm just leaving this stub because it may need to do 
        # special stuff in the future
        return super(Flow, self).mint()

    def __del__(self):
        try:
            super(Capability, self).__del__()
        except AttributeError:
            pass

@Protocol.register_objtype
class NodeGrant(Capability):
    __objtype__ = cn_pb.CPOBJTYPE_NODE_GRANT

    def grant(self, cap):
        """Grant the given capability `cap` to the node this node grant was
        obtained from.

        Returns nothing.
        """

        args = cn_pb.CPInvokeNodeGrantGrantArgs()
        args.cptr = cap.cptr
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_NODE_GRANT_GRANT,
                                            ("node_grant_grant_args", args))
        assert reply.has_no_items

    def rp0(self):
        """Get the RP0 of the node this grant was created from.

        Returns a capability to the appropriate RP.
        """

        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_NODE_GRANT_RP0, None)
        return reply.item.capability

    def flow(self, *args):
        """Create a new flow for the node this node grant was obtained from
        with the given restrictions.

        Shorthand for Protocol.create(Flow, <self>, *args)

        Returns a new flow object.
        """
        return self.protocol.create(Flow, self, *args)

@Protocol.register_objtype
class SealerUnsealer(Capability):
    """Object that is capable of sealing and unsealing capabilities. A sealed capability
       can only be unsealed by the same SealerUnsealer it was sealed with (or a
       restricted Sealer/Unsealer derrived from the SealerUnsealer it was sealed
       with)"""

    __objtype__ = cn_pb.CPOBJTYPE_SEALER_UNSEALER

    def seal(self, cap):
        """Seal the given capability with the SealerUnsealer.
        
        Returns a new sealed capability."""
        args = cn_pb.CPInvokeSealerUnsealerSealArgs()
        args.to_seal = cap.cptr
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_SEALER_UNSEALER_SEAL,
                                            ("sealer_unsealer_seal_args", args))
        return reply.item.capability

    def unseal(self, sealed_cap):
        """Unseal the given sealed capability (as returned from the sealer's .seal
           method). 

        Returns the capability that was previously sealed."""
        args = cn_pb.CPInvokeSealerUnsealerUnsealArgs()
        args.sealed = sealed_cap.cptr
        reply = self.protocol._invoke(self, cn_pb.CPOBJMETHOD_SEALER_UNSEALER_UNSEAL,
                                            ("sealer_unsealer_unseal_args", args))
        return reply.item.capability

    def restrict_to_seal(self):
        """Return a sealer/unsealer that is equivalent to this sealer unsealer
           (objects sealed by it can be unsealed by this sealer/unsealer) that only
           supports the "seal" method."""
        reply = self.protocol._invoke(self, 
                    cn_pb.CPOBJMETHOD_SEALER_UNSEALER_RESTRICT_TO_SEAL, None)
        return reply.item.capability

    def restrict_to_unseal(self):
        """Return a sealer/unsealer that is equivalent to this sealer/unsealer
           (it can unseal any objects sealed by this sealer/unsealer) that only
           supports the "unseal" method."""
        reply = self.protocol._invoke(self, 
                    cn_pb.CPOBJMETHOD_SEALER_UNSEALER_RESTRICT_TO_UNSEAL, None)
        return reply.item.capability
