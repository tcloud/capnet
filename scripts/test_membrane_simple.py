import capnet
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = capnet.Protocol(args.iface)

print "getting rp0 and my node cap"
rp0 = p.rp0()
me = rp0.recv()

print "getting my node info"
known = me.info()

print "setting up membrane"
membrane_int = p.create(capnet.Membrane)
membrane_ext = membrane_int.external()
assert membrane_int.cptr != membrane_ext.cptr, "should be different cptrs"

print "sending myself through membrane"
membrane_int.send(me)
wrapped_me = membrane_ext.recv()

print "getting my wrapped node info"
known_wrapped = wrapped_me.info()
assert known_wrapped.name == known.name, "should be same node info"

print "sending wrapped me back through membrane"
membrane_ext.send(wrapped_me)
unwrapped_post_me = membrane_int.recv()

print "testing unwrapped cap"
known_wrapped_post = unwrapped_post_me.info()
assert known_wrapped_post.name == known.name, "should be same node info"

print "clearing membrane"
membrane_int.clear()

print "attempting to get my wrapped node info (should fail)"
try:
    fail = wrapped_me.info()
except Exception, e:
    print "(Good)", e
else:
    assert False, "Getting wrapped cap didn't throw exn after membrane clear!"

print "trying to get unwrapped cap after clear"
known_wrapped_post2 = unwrapped_post_me.info()
assert known_wrapped_post2.name == known.name, "should be same node info"

print "...Success..."
