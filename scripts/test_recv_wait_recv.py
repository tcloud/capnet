import capnet
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = capnet.Protocol(args.iface)

print "getting rp0 and my node cap"
rp0 = p.rp0()
m = rp0.recv_wait()

while True:
    print "Waiting for any cap!"
    got = m.recv_wait(timeout=10)
    print "got:", got
