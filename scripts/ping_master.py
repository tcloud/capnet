import capnet as cn
import capnet.util
import argparse
from pprint import pprint
from collections import namedtuple
import time

PAIRS = [("sw1-port-1", "sw1-port-2")]
#PAIRS = [("sw1-port-2", "sw2-port-2")]
#PAIRS = []
#PAIRS = [("sw1-port-2", "sw1-port-1")]

parser = argparse.ArgumentParser()
parser.add_argument("-D","--direct",action="store_true",default=False,
                    help="Automatically recv flow caps for other nodes, so you don't have to run a capability receiver (like ping_sub.py) on them to realize the granted flow caps.")
parser.add_argument("-R", "--revoke", action="store_true", default=False,
                    help="Revoke the granted capabilities after 10s")
parser.add_argument("iface")
parser.add_argument("pairs",nargs='*',
                    help="Specify node names to connect bidirectionally; i.e., \"sw1p2,sw2p2 sw1p15,sw2p9\"")

args = parser.parse_args()
if args.pairs and len(args.pairs) > 0:
    PAIRS = []
    for ap in args.pairs:
        apa = ap.split(",")
        if len(apa) == 1:
            apa = ap.split(":")
        if len(apa) == 1:
            raise Exception("node pair %s doesn't seem like a pair; aborting!"
                            % (ap,))
        PAIRS.append((apa[0],apa[1]))
        pass
else:
    print "WARNING: using default pair %s" % (str(PAIRS),)
    pass

p = cn.Protocol(args.iface)

nodes = {}
rp0, me, broker = capnet.util.recv_preamble(p)
myname = me.info.name
nodes[myname] = me
nodes.update(capnet.util.recv_nodes(p, rp0.recv_iter_exn()))

print "Network DB:"
pprint(nodes)

granted = []
for (a, b) in PAIRS:
    a = nodes[a]
    b = nodes[b]
    print "Sending flow ->\"{0}\" to \"{1}\"".format(b.info.name, a.info.name)
    if a.info.name != myname:
        f_cap = b.flow.mint()
        granted.append(f_cap)
        if args.direct:
            a.grant.grant(f_cap)
        else:
            a.rp.send(f_cap)
    
    print "Sending flow ->\"{0}\" to \"{1}\"".format(a.info.name, b.info.name)
    if b.info.name != myname:
        f_cap = a.flow.mint()
        granted.append(f_cap)
        if args.direct:
            b.grant.grant(f_cap)
        else:
            b.rp.send(f_cap)
    pass

if args.revoke:
    from time import sleep
    print "Sleeping for 10s..."
    sleep(10)
    print "revoking caps..."
    sleep(0.5)
    for cap in granted:
        cap.revoke()
        cap.delete()
