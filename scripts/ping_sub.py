import capnet
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = capnet.Protocol(args.iface)

print "Receiving Node Capability..."
p.rp0().recv()
