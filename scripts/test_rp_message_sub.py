import capnet
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = capnet.Protocol(args.iface)

print "Receiving Node Capability..."
rp0 = p.rp0()

while True:
    print "Got:", rp0.recv_wait()
