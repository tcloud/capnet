import capnet as cn
import capnet.util
import argparse
from pprint import pprint
from collections import namedtuple
import time

Entry = namedtuple("Entry", ["node", "node_grant", "info", "rp", "flow"])

parser = argparse.ArgumentParser()
parser.add_argument("iface")

args = parser.parse_args()

p = cn.Protocol(args.iface)

nodes = {}
print "getting rp0"
rp0, me, broker = capnet.util.recv_preamble(p)
nodes[me.info.name] = me
nodes.update(capnet.util.recv_nodes(p, rp0.recv_iter_exn()))

print "Network DB:"
pprint(nodes)

raw_input("Press <ENTER> to begin sending...")

c = 1
while True:
    nodes["veth.br0p2.ex"].rp.send(me, msg=str(c))
    c += 1
    time.sleep(0.5)
