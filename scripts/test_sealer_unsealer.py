import capnet
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = capnet.Protocol(args.iface)

print "getting rp0 and my node cap"
rp0 = p.rp0()
me = rp0.recv()

print "getting my node info"
known = me.info()

su1 = p.create(capnet.SealerUnsealer)
su2 = p.create(capnet.SealerUnsealer)

print "sealing"
me_su1_sealed = su1.seal(me)
try:
    me_su1_sealed.info()
except Exception, e:
    pass
else:
    assert False, "Able to retrieve info on sealed cap!"

print "unsealing"
me_unsealed = su1.unseal(me_su1_sealed)
info_unsealed = me_unsealed.info()
assert info_unsealed.name == known.name

me_su2_sealed = su2.seal(me)
try:
    su1.unseal(me_su2_sealed)
except Exception, e:
    pass
else:
    assert False, "Able to unseal with wrong sealer"

su2_sealer_only = su2.restrict_to_seal()
try:
    su2_sealer_only.unseal(me_su2_sealed)
except Exception, e:
    pass
else:
    assert False, "Able to unseal with retricted sealer"

su2_unsealer_only = su2.restrict_to_unseal()

me_su2_unsealed = su2_unsealer_only.unseal(me_su2_sealed)
assert me_su2_unsealed.info().name == known.name

print "[Basic] ...Success..."

membrane = p.create(capnet.Membrane)
membrane_ext = membrane.external()

membrane.send(su1)
membrane.send(me)

su1_after_mem = membrane_ext.recv()
me_after_mem = membrane_ext.recv()

assert me_after_mem.info().name == known.name, "Make sure membranes work"

me_after_mem_sealed = su1_after_mem.seal(me_after_mem)
me_after_mem2 = su1.unseal(me_after_mem_sealed)

assert me_after_mem2.info().name == known.name, \
       "Make sure sealer/unsealer passed through membranes work"

membrane.clear()

try:
    _ = me_after_mem.info()
except Exception, e:
    pass
else:
    assert False, "Able to use node cap after membrane cleared"

me_unsealed_by_su1_after_mem = su1_after_mem.unseal(su1.seal(me))
assert me_unsealed_by_su1_after_mem.info().name == known.name, \
       "Make sure that sealer/unsealer passed through membrane still works"

print "[Membrane] ...Success..."
