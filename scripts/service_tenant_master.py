#!/usr/bin/env python2

import capnet as cn
import capnet.util 
import argparse
from pprint import pprint
from collections import namedtuple
import time
import traceback

import itertools

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = cn.Protocol(args.iface)

rp0, me, broker = capnet.util.recv_preamble(p)
myname = me.info.name

service_rp = p.create(cn.RP)
broker.register("myservice", service_rp)

print "Service 'myservice' registered!"

client_membrane = service_rp.recv_wait()

print "Got client membrane"

trans_rp = client_membrane.recv_wait()

nodes = capnet.util.recv_nodes(p, trans_rp.recv_iter())

for (a, b) in itertools.combinations(nodes.itervalues(), 2):
    if a.info.name != myname:
        a.grant.grant(b.flow)
    if b.info.name != myname:
        b.grant.grant(a.flow)
