#!/usr/bin/env python2

import capnet as cn
import capnet.util
import argparse
from pprint import pprint
from collections import namedtuple
import time

##
## This is a simple toy example that gets its own master wfa cap, the
## broker for the network, and any other nodes belonging to this tenant.
## It also gets the service tenant's RP that that tenant offered through
## the broker.  As it receives nodes, it sends them to the service
## tenant.  This just demonstrates brokers.
##

parser = argparse.ArgumentParser()
parser.add_argument("-D","--direct",action="store_true",default=False,
                    help="Automatically recv flow caps for other nodes, so you don't have to run a capability receiver (like ping_sub.py) on them to realize the granted flow caps.")
parser.add_argument("iface")

args = parser.parse_args()

p = cn.Protocol(args.iface)

rp0, me, broker = capnet.util.recv_preamble(p)
myname = me.info.name

nodes = capnet.util.recv_nodes(p, rp0.recv_iter_exn())

print "Network DB:"
pprint(nodes)

print "waiting on service"
service_rp = broker.lookup_wait("myservice")

m = p.create(cn.Membrane)
m_ext = m.external()
service_rp.send(m_ext)
trans_rp = p.create(cn.RP)
m.send(trans_rp)

with trans_rp.send_iter_context(): 
    for node_p in nodes.itervalues():
        trans_rp.send(node_p.node)

print "...Done..."
