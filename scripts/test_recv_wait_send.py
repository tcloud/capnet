import capnet
import argparse
from pprint import pprint
from collections import namedtuple
import time

Entry = namedtuple("Entry", ["node", "node_grant", "info", "rp", "flow"])

#PAIRS = [("sw1-port-2", "sw2-port-2")]
#PAIRS = []
#PAIRS = [("sw1-port-2", "sw1-port-1")]

parser = argparse.ArgumentParser()
parser.add_argument("iface")
args = parser.parse_args()

p = cn.Protocol(args.iface)

nodes = {}
print "getting rp0"
rp0 = p.rp0()

print "rp0 recv me"
me = rp0.recv()
print "node info"
info = me.info()
print "node flow"
flow = p.create(cn.Flow)
node_rp = p.create(cn.RP)
nodes[info.name] = Entry(me,None,info,node_rp,flow)
myname = info.name

print "rp0 recv broker"
broker = rp0.recv()
if type(broker) != cn.Broker:
    print "Second cap not a broker (was %s)" % (str(type(broker)),)
    pass

try:
    while True:
        print "rp0 recv"
        node = rp0.recv()
        node_rp = p.create(cn.RP)
        node_grant = node.reset(node_rp)
        print "node info"
        info = node.info()
        print "node flow"
        flow = node_grant.flow()
        nodes[info.name] = Entry(node, node_grant, info, node_rp, flow)
except Exception, e:
    #raise
    print "Exn:", e
    pass

print "Network DB:"
pprint(nodes)

p2 = nodes["veth.br0p2.ex"]
membrane = p.create(cn.Membrane)
p2.rp.send(membrane)

while True:
    raw_input("Press enter when ready to send the cap!")
    membrane.send(me)
