 /* file: minunit.h */

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                                 if (message) return message; } while (0)

#include <glib.h>
#include <libcap.h>

#include <obj.h>
#include <dispatch.h>

const void * dummy_owner = (void *) 0xdeadc0deUL;

static unsigned int tests_run = 0;

#define _stringify(x) #x
#define stringify(x) _stringify(x)
#define debug(fmt, ...) printf( "line " stringify(__LINE__) ": " fmt "\n", ## __VA_ARGS__)

static int __cn_dispatch_result_new(cn_dispatch_result_t **result) {
    if (cn_dispatch_result_alloc(result) != 0) {
        return -1;
    }

    cn_dispatch_result_init(*result);
    return 0;
}

static char * __cn_dispatch_result_get_cptr_result(cn_dispatch_result_t *result,
                                                   cn_objtype_t type, cptr_t *cptr) {
    debug("STATUS: %s", result->msg);

    mu_assert("dispatch succeeded", result->status== true);
    mu_assert("One cptr item returned", g_list_length(result->items) == 1);

    cn_dispatch_result_item_t * item = g_list_nth(result->items, 0)->data;

    mu_assert("dispatch result item is cptr", item->type == CN_RESULT_ITEM_CPTR);
    debug("cptr should be: %s, is %s", cn_objtype_name(type), cn_objtype_name(item->cptr.type));
    mu_assert("cptr is appropriate type", item->cptr.type == type);

    *cptr = item->cptr.cptr;

    return NULL;
}

static char * __cn_dispatch_result_empty(cn_dispatch_result_t * result) {
    debug("STATUS: %s", result->msg);
    mu_assert("dispatch succeeded", result->status == true);
    mu_assert("no items returned", result->items == NULL);
    return NULL;
}

static char * __cn_dispatch_create_single(cn_principal_t *p, cn_objtype_t type, cptr_t *cptr, void * args) {
    cn_dispatch_result_t * result;

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    if (cn_dispatch_create(p, result, type, args) != 0) {
        return "Failed dispatch create";
    }

    char * res = __cn_dispatch_result_get_cptr_result(result, type, cptr);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    return NULL;
}

/* Testing helpers */

static char * __test_can_deref(cn_principal_t *p, cptr_t c) {
    struct cnode *cnode;

    if (cn_principal_get(p, c, &cnode) != 0) {
        return "Could not deref cptr";
    }

    cap_cnode_put(cnode);

    return NULL;
}

static char * __test_can_not_deref(cn_principal_t *p, cptr_t c) {
    struct cnode *cnode;

    if (cn_principal_get(p, c, &cnode) == 0) {
        return "Derefed cptr when it should not be possible";
    }

    return NULL;
}

static char * __test_wrapped_by(cn_principal_t *p, cptr_t obj, cptr_t membrane, struct cnode ** cnode_o) {
    struct cnode * cnode;

    if (cn_principal_get(p, obj, &cnode) != 0) {
        return "Failed to get supposedly wrapped object";
    }

    cn_cnode_meta_t * meta = cap_cnode_metadata(cnode);
    mu_assert("OBJ wrapped cnode should have meta", meta != NULL);
    char * meta_s = cn_cnode_meta_string(meta);
    debug("OBJ meta: %s", meta_s);
    free(meta_s);

    mu_assert("OBJ should be wrapped", cn_cnode_is_annotated(cnode));

    struct cnode * membrane_cnode;
    if (cn_principal_get(p, membrane, &membrane_cnode) != 0) {
        return "Couldn't get membrane from principal";
    }
    mu_assert("Membrane cnode is membrane type", cn_cnode_type(membrane_cnode) == CN_MEMBRANE);
    cn_obj_t * membrane_obj = cn_cnode_object(membrane_cnode);

    mu_assert("OBJ annotated by membrane", cn_cnode_meta_is_annotated_by(meta, membrane_obj));

    cap_cnode_put(membrane_cnode);

    *cnode_o = cnode;

    return NULL;
}

static char * __cn_obj_wrap(cn_principal_t *p, cptr_t obj, cn_objtype_t obj_type,
                            cptr_t *mem_in, cptr_t *mem_out, cptr_t *wrapped_o) {

    char * res = NULL;

    cn_dispatch_result_t * result;

    cptr_t membrane_internal;

    debug("create membrane internal");
    res = __cn_dispatch_create_single(p, CN_MEMBRANE, &membrane_internal, NULL);
    if (res != NULL) { return res; }

    /* Create the external membrane from the internal membrane */

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("create membrane external");
    if (cn_dispatch_invoke(p, result, membrane_internal, CN_MEMBRANE_EXTERNAL, NULL) != 0) {
        return "Failed dispatch invoke external on membrane";
    }

    cptr_t membrane_external;

    res = __cn_dispatch_result_get_cptr_result(result, CN_MEMBRANE, &membrane_external);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    cn_dispatch_invoke_membrane_send_args_t membrane_send_args = {
        .cap = obj,
    };

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("do membrane send");
    if (cn_dispatch_invoke(p, result, membrane_internal, 
                           CN_MEMBRANE_SEND, (void *) &membrane_send_args) != 0) {
        return "Sending on the membrane failed";
    }

    res = __cn_dispatch_result_empty(result);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Now we receive off the membrane */

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("do membrane recv");
    if (cn_dispatch_invoke(p, result, membrane_external, CN_MEMBRANE_RECV, NULL) != 0) {
        return "Sending on the membrane failed";
    }

    cptr_t wrapped;

    res = __cn_dispatch_result_get_cptr_result(result, obj_type, &wrapped);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    *mem_in = membrane_internal;
    *mem_out = membrane_external;
    *wrapped_o = wrapped;
    
    return NULL;
}

static char * test_membrane_membrane_grant(void) {

    cn_principal_t * pa;

    char * res = NULL;
    cn_dispatch_result_t * result = NULL;

    /* Create the principal */

    debug("creating principal");
    if (cn_principal_new(&pa) != 0) {
        return "Failed to allocate pa";
    }

    RHOLD_OBJ(pa, dummy_owner);

    debug("create test rp");
    cptr_t rp;
    res = __cn_dispatch_create_single(pa, CN_RP, &rp, NULL);
    if (res != NULL) { return res; }

    /* Wrap the RP with the membrane */

    cptr_t pa_membrane_interal;
    cptr_t pa_membrane_external;
    cptr_t rp_wrapped;

    res = __cn_obj_wrap(pa, rp, CN_RP, &pa_membrane_interal, &pa_membrane_external, &rp_wrapped);
    if (res) { return res; }

    /* now check all the wrapping invariants. */

    struct cnode * cnode;

    debug("do checks...");
    if (cn_principal_get(pa, rp_wrapped, &cnode) != 0) {
        return "Failed to get our wrapped rp";
    }

    mu_assert("RP wrapped cnode must be rp", cn_cnode_type(cnode) == CN_RP);
    mu_assert("RP should be wrapped", cn_cnode_is_annotated(cnode));
    cn_cnode_meta_t * meta = cap_cnode_metadata(cnode);
    mu_assert("Rp wrapped cnode should have meta", meta != NULL);

    struct cnode * membrane_cnode;
    if (cn_principal_get(pa, pa_membrane_external, &membrane_cnode) != 0) {
        return "Couldn't get membrane from principal";
    }
    mu_assert("Membrane cnode is membrane type", cn_cnode_type(membrane_cnode) == CN_MEMBRANE);
    cn_obj_t * membrane_ext = cn_cnode_object(membrane_cnode);

    mu_assert("RP annotated by membrane", cn_cnode_meta_is_annotated_by(meta, membrane_ext));

    cn_annotation_t * annotation;
    if (cn_cnode_meta_get_annotation_by(meta, membrane_ext, &annotation) != 0) {
        return "Couldn't get rp annotation";
    }

    mu_assert("Annotation should be from membrane", annotation->declassifier == membrane_ext);

    method_mask_t unrestricted_mask;
    cn_method_mask_permission_all(&unrestricted_mask);

    mu_assert("Method mask is unrestricted", 
              annotation->method_mask == unrestricted_mask);
    mu_assert("annotation should be external", 
              cn_membrane_annotation_type(annotation) == CN_MEMBRANE_TYPE_EXTERNAL);

    cap_cnode_put(cnode);
    cap_cnode_put(membrane_cnode);

    RPUTs_OBJ(pa, dummy_owner);

    return NULL;
}

static char * test_membrane_rp_grant(void) {

    cn_principal_t * pa;

    char * res = NULL;
    cn_dispatch_result_t * result = NULL;

    /* Create the principal */

    debug("creating principal");
    if (cn_principal_new(&pa) != 0) {
        return "Failed to allocate pa";
    }

    RHOLD_OBJ(pa, dummy_owner);

    /* Create the internal membrane */

    cptr_t pa_membrane_internal;

    debug("create membrane internal");
    res = __cn_dispatch_create_single(pa, CN_MEMBRANE, &pa_membrane_internal, NULL);
    if (res != NULL) { return res; }

    /* Create the external membrane from the internal membrane */

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("create membrane external");
    if (cn_dispatch_invoke(pa, result, pa_membrane_internal, CN_MEMBRANE_EXTERNAL, NULL) != 0) {
        return "Failed dispatch invoke external on membrane";
    }

    cptr_t pa_membrane_external;

    res = __cn_dispatch_result_get_cptr_result(result, CN_MEMBRANE, &pa_membrane_external);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Create a dummy object to send through the membrane */

    debug("create test rp");
    cptr_t rp;
    res = __cn_dispatch_create_single(pa, CN_RP, &rp, NULL);
    if (res != NULL) { return res; }

    /* Send the dummy object through the membrane */

    cn_dispatch_invoke_membrane_send_args_t membrane_send_args = {
        .cap = rp,
    };

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("do membrane send");
    if (cn_dispatch_invoke(pa, result, pa_membrane_internal, 
                           CN_MEMBRANE_SEND, (void *) &membrane_send_args) != 0) {
        return "Sending on the membrane failed";
    }

    res = __cn_dispatch_result_empty(result);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Now we receive off the membrane */

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("do membrane recv");
    if (cn_dispatch_invoke(pa, result, pa_membrane_external, CN_MEMBRANE_RECV, NULL) != 0) {
        return "Sending on the membrane failed";
    }

    cptr_t rp_wrapped;

    res = __cn_dispatch_result_get_cptr_result(result, CN_RP, &rp_wrapped);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Now create another RP and send it through the original RP */

    debug("create sender rp");
    cptr_t sender_rp;
    res = __cn_dispatch_create_single(pa, CN_RP, &sender_rp, NULL);
    if (res != NULL) { return res; }

    /* Send this RP from the unwrapped RP capability */

    cn_dispatch_invoke_rp_send_args_t rp_send_args = {
        .has_cap = true,
        .cap = sender_rp,
        .has_msg = false,
    };

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("send sender rp on orig rp");
    if (cn_dispatch_invoke(pa, result, rp, 
                           CN_RP_SEND, (void *) &rp_send_args) != 0) {
        return "Sending on the rp failed";
    }

    res = __cn_dispatch_result_empty(result);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Receive the sender RP using the *wrapped* RP capability */

    if (__cn_dispatch_result_new(&result) != 0) {
        return "Failed to allocate dispatch result";
    }

    debug("do rp recv");
    if (cn_dispatch_invoke(pa, result, rp_wrapped, CN_RP_RECV, NULL) != 0) {
        return "recving on the wrapped rp failed";
    }

    cptr_t sender_rp_wrapped;

    debug("getting rp recv result");
    res = __cn_dispatch_result_get_cptr_result(result, CN_RP, &sender_rp_wrapped);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    /* Check the invariants. */

    struct cnode * cnode;

    debug("do checks...");

    if (cn_principal_get(pa, sender_rp_wrapped, &cnode) != 0) {
        return "Failed to get our wrapped rp";
    }

    mu_assert("RP wrapped cnode must be rp", cn_cnode_type(cnode) == CN_RP);
    mu_assert("RP should be wrapped", cn_cnode_is_annotated(cnode));
    cn_cnode_meta_t * meta = cap_cnode_metadata(cnode);
    mu_assert("Rp wrapped cnode should have meta", meta != NULL);

    struct cnode * membrane_cnode;
    if (cn_principal_get(pa, pa_membrane_external, &membrane_cnode) != 0) {
        return "Couldn't get membrane from principal";
    }
    mu_assert("Membrane cnode is membrane type", cn_cnode_type(membrane_cnode) == CN_MEMBRANE);
    cn_obj_t * membrane_ext = cn_cnode_object(membrane_cnode);

    mu_assert("RP annotated by membrane", cn_cnode_meta_is_annotated_by(meta, membrane_ext));

    cn_annotation_t * annotation;
    if (cn_cnode_meta_get_annotation_by(meta, membrane_ext, &annotation) != 0) {
        return "Couldn't get rp annotation";
    }

    mu_assert("Annotation should be from membrane", annotation->declassifier == membrane_ext);

    method_mask_t unrestricted_mask;
    cn_method_mask_permission_all(&unrestricted_mask);

    mu_assert("Method mask is unrestricted", 
              annotation->method_mask == unrestricted_mask);
    mu_assert("annotation should be external", 
              cn_membrane_annotation_type(annotation) == CN_MEMBRANE_TYPE_EXTERNAL);

    cap_cnode_put(cnode);
    cap_cnode_put(membrane_cnode);

    RPUTs_OBJ(pa, dummy_owner);

    return NULL;
}

static char * test_annotated_node_reset(void) {
    cn_principal_t *p;

    char * res = NULL;
    cn_dispatch_result_t * result = NULL;

    /* Create the principal */

    debug("creating principal");
    if (cn_principal_new(&p) != 0) {
        return "Failed to allocate p";
    }

    RHOLD_OBJ(p, dummy_owner);

    cn_node_t *node_obj;
    if (cn_node_new(&node_obj) != 0) {
        return "failed to allocate new node";
    }

    RHOLD_OBJ(p, node_obj);

    debug("inserting node cptr");
    cptr_t node_cptr;
    if (cn_principal_insert(p, cn_obj_ref(node_obj), NULL, &node_cptr) != 0) {
        return "couldn't insert node into principal";
    }

    debug("grant orig node grant to test principal");
    cptr_t orig_node_grant;
    cn_grant(node_obj->secret, node_obj->grant, p, NULL, &orig_node_grant);

    debug("creating flow to node using its grant");
    cn_dispatch_create_flow_args_t flow_args = {
        .node_grant = orig_node_grant,
    };

    cptr_t orig_flow;
    res = __cn_dispatch_create_single(p, CN_FLOW, &orig_flow, &flow_args);
    if (res) { return res; }

    debug("make sure we can deref the new flow");
    res = __test_can_deref(p, orig_flow);
    if (res) { return res; }

    cptr_t wrapped_node;
    cptr_t node_wrapping_membrane_in;
    cptr_t node_wrapping_membrane_ext;

    debug("wrapping the node object");
    res = __cn_obj_wrap(p, node_cptr, CN_NODE, &node_wrapping_membrane_in, 
                                               &node_wrapping_membrane_ext,
                                               &wrapped_node);
    if (res) { return res; }

    /* Create a new RP */
    debug("create test rp in node c-space");
    cptr_t test_cptr;
    res = __cn_dispatch_create_single(node_obj->principal, CN_RP, &test_cptr, NULL);
    if (res) { return res; }

    debug("make sure we can deref the new test-cptr");
    res = __test_can_deref(node_obj->principal, test_cptr);
    if (res) { return res; }

    debug("reset the node");
    if (__cn_dispatch_result_new(&result) != 0) {
        return "failed to create dispatch result";
    }

    if (cn_dispatch_invoke(p, result, wrapped_node, CN_NODE_RESET, NULL) != 0) {
        return "node reset failed";
    }

    cptr_t node_grant;
    res = __cn_dispatch_result_get_cptr_result(result, CN_NODE_GRANT, &node_grant);
    if (res != NULL) { return res; }

    cn_dispatch_result_free(result);

    debug("check that the node grant is wrapped by the ext membrane");
    struct cnode * node_grant_cnode;
    res = __test_wrapped_by(p, node_grant, node_wrapping_membrane_ext, &node_grant_cnode);
    if (res) { return res; }
    cap_cnode_put(node_grant_cnode);

    debug("make sure we can't deref the test cptr we put in before");
    res = __test_can_not_deref(node_obj->principal, test_cptr);
    if (res) { return res; }

    debug("make sure we can't deref the original flow we created");
    res = __test_can_not_deref(p, orig_flow);
    if (res) { return res; }

    return NULL;
}

static char * all_tests(void) {
    mu_run_test(test_membrane_membrane_grant);
    mu_run_test(test_membrane_rp_grant);
    mu_run_test(test_annotated_node_reset);
    return NULL;
}

int main(int argc,char **argv) {
    cn_init();
    char * result = all_tests();
    if (result) {
        debug("FAILED: %s", result);
        return 1;
    }
    return 0;
}
