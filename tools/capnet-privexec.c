#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <getopt.h>
#include <sys/capability.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <sys/prctl.h>

#include "capnet_config.h"

/**
 ** A simple program that must be run as root, and takes a list of
 ** capabilities to set on itself, as well as a uid/gid to drop privs
 ** to.  It exec()s the remainder of the args as a argv, with the
 ** reduced privs + extra caps.
 **
 ** (Our purpose is to use it to run programs with CAP_NET_RAW as
 ** nobody/nobody.)
 **/

extern char *optarg;
extern int optind, opterr, optopt;

#ifdef HAVE_PR_CAP_AMBIENT
const char *shortopts = "ac:u:g:";
#else
const char *shortopts = "c:u:g:";
#endif

cap_value_t str2cap(char *capname);

int main(int argc,char **argv) {
    int have_newuid = 0, have_newgid = 0;
    uid_t newuid = 0;
    gid_t newgid = 0;
    int rc;
    char **eargv;
    char *doptarg;
    char *tok = NULL;
    char *endptr = NULL;
    cap_t current;
    int clear_all_caps = 0;
    cap_value_t capv;
    cap_value_t *addcaps = calloc(8,sizeof(*addcaps));
    int addcaps_alen = 8, addcaps_len = 0;
    cap_value_t *delcaps = calloc(8,sizeof(*delcaps));
    int delcaps_alen = 8, delcaps_len = 0;
    struct passwd *passwd;
    struct group *group;
    int i;
    int opt;
#ifdef HAVE_PR_CAP_AMBIENT
    int do_ambient = 0;
#endif

    while ((opt = getopt(argc,argv,shortopts)) != -1) {
	switch (opt) {
#ifdef HAVE_PR_CAP_AMBIENT
	case 'a':
	    do_ambient = 1;
	    break;
#endif
	case 'c':
	    doptarg = strdup(optarg);
	    tok = doptarg;
	    while ((tok = strtok(tok,",")) != NULL) {
		if (*tok == '^' && *(tok+1) == '\0')
		    clear_all_caps = 1;
		else if (*tok == '^') {
		    ++tok;
		    if (delcaps_len == delcaps_alen) {
			delcaps_alen += 8;
			delcaps = realloc(delcaps,delcaps_alen*sizeof(*delcaps));
			if (!delcaps) {
			    fprintf(stderr,"ERROR: realloc: %s; aborting\n",
				    strerror(errno));
			    exit(-1);
			}
		    }
		    capv = str2cap(tok);
		    delcaps[delcaps_len++] = capv;
		    if (!CAP_IS_SUPPORTED(capv)) {
			fprintf(stderr,"cap %s is not supported; aborting!\n",
				tok);
			exit(-1);
		    }
		}
		else {
		    if (addcaps_len == addcaps_alen) {
			addcaps_alen += 8;
			addcaps = realloc(addcaps,addcaps_alen*sizeof(*addcaps));
			if (!addcaps) {
			    fprintf(stderr,"ERROR: realloc: %s; aborting\n",
				    strerror(errno));
			    exit(-1);
			}
		    }
		    capv = str2cap(tok);
		    addcaps[addcaps_len++] = capv;
		    if (!CAP_IS_SUPPORTED(capv)) {
			fprintf(stderr,"cap %s is not supported; aborting!\n",
				tok);
			exit(-1);
		    }
		}

		tok = NULL;
	    }
	    free(doptarg);
	    doptarg = NULL;
	    break;
	case 'u':
	    have_newuid = 1;
	    newuid = (uid_t)strtoul(optarg,&endptr,0);
	    if (endptr == optarg || (newuid == 0 && errno)) {
		/* Try as a name */
		passwd = getpwnam(optarg);
		if (passwd)
		    newuid = passwd->pw_uid;
		else {
		    fprintf(stderr,
			    "ERROR: could not find uid/user %s; aborting\n",
			    optarg);
		    exit(-1);
		}
	    }
	    else if (errno) {
		fprintf(stderr,"ERROR: uid %s over/underflow: %s; aborting\n",
			optarg,strerror(errno));
		exit(-1);
	    }
	    break;
	case 'g':
	    have_newgid = 1;
	    newgid = (gid_t)strtol(optarg,&endptr,0);
	    if (endptr == optarg || (newgid == 0 && errno)) {
		/* Try as a name */
		group = getgrnam(optarg);
		if (group)
		    newgid = group->gr_gid;
		else {
		    fprintf(stderr,
			    "ERROR: could not find gid/group %s; aborting\n",
			    optarg);
		    exit(-1);
		}
	    }
	    else if (errno) {
		fprintf(stderr,"ERROR: gid %s over/underflow: %s; aborting\n",
			optarg,strerror(errno));
		exit(-1);
	    }
	    break;
	default:
	    fprintf(stderr,
		    "Usage: %s [-c CAP_X,^CAP_Y,...] [-u <uid|uname]"
		    " [-g <gid|group>] prog arg1 arg2...\n",argv[0]);
	    exit(-1);
	}
    }

    if (geteuid() != 0) {
	fprintf(stderr,"ERROR: must be run as root!\n");
	exit(-1);
    }

    /* Clean up our extra args into an execv vector. */
    if (optind >= argc) {
	fprintf(stderr,"ERROR: you must specify a program and args to exec!\n");
	exit(-1);
    }
    eargv = calloc(argc - optind + 1,sizeof(char *));
    eargv[0] = argv[optind];
    for (i = 1; i < argc - optind + 1; ++i) {
	eargv[i] = argv[optind + i];
    }

    /* Adjust our caps. */
    if (addcaps_len > 0 || delcaps_len > 0) {
	current = cap_get_proc();

	if (clear_all_caps) {
	    if (cap_clear(current)) {
		fprintf(stderr,"ERROR: cap_clear: %s; aborting\n",
			strerror(errno));
		exit(-1);
	    }
	}

	if (addcaps_len > 0) {
	    rc = cap_set_flag(current,CAP_PERMITTED,addcaps_len,addcaps,CAP_SET);
	    if (rc) {
		fprintf(stderr,"cap_set_flag(add) failed: %s; aborting!\n",
			strerror(errno));
		exit(-1);
	    }
	    rc = cap_set_flag(current,CAP_INHERITABLE,addcaps_len,addcaps,CAP_SET);
	    if (rc) {
		fprintf(stderr,"cap_set_flag(add) failed: %s; aborting!\n",
			strerror(errno));
		exit(-1);
	    }
	    rc = cap_set_flag(current,CAP_EFFECTIVE,addcaps_len,addcaps,CAP_SET);
	    if (rc) {
		fprintf(stderr,"cap_set_flag(add) failed: %s; aborting!\n",
			strerror(errno));
		exit(-1);
	    }

#ifdef HAVE_PR_CAP_AMBIENT
	    if (do_ambient) {
		for (i = 0; i < addcaps_len; ++i) {
		    rc = prctl(PR_CAP_AMBIENT,PR_CAP_AMBIENT_RAISE,addcaps[i],0,0);
		    if (rc) {
			fprintf(stderr,
				"ERROR: prctl(ambient) failed: %s; aborting!\n",
				strerror(errno));
			exit(-1);
		    }
		}
	    }
#endif
	}
	if (delcaps_len > 0) {
	    rc = cap_set_flag(current,CAP_PERMITTED,delcaps_len,delcaps,CAP_CLEAR);
	    if (rc) {
		fprintf(stderr,"cap_set_flag(clear) failed: %s; aborting!\n",
			strerror(errno));
		exit(-1);
	    }
	}

	if (cap_set_proc(current)) {
	    fprintf(stderr,"ERROR: cap_set_proc failed: %s; aborting!\n",
		    strerror(errno));
	    exit(-1);
	}

	cap_free(current);
    }

    /* Jump to our gid, the setuid to blow our saved and root perms away. */
    if (have_newgid) {
	rc = setgid(newgid);
	if (rc < 0) {
	    fprintf(stderr,"ERROR: setgid(%d): %s; aborting\n",
		    newgid,strerror(errno));
	    exit(-1);
	}
    }
    if (have_newuid) {
	rc = setuid(newuid);
	if (rc < 0) {
	    fprintf(stderr,"ERROR: setuid(%d): %s; aborting\n",
		    newuid,strerror(errno));
	    exit(-1);
	}
    }

    /* Ok, exec or die. */
    rc = execv(eargv[0],eargv);

    fprintf(stderr,"ERROR: execv(%s,...): %s\n",eargv[0],strerror(errno));
    exit(-1);
}

cap_value_t str2cap(char *capname) {
    cap_value_t ret;

    if (strncmp(capname,"CAP_AUDIT_CONTROL",strlen(capname)) == 0)
	ret = CAP_AUDIT_CONTROL;
    //else if (strncmp(capname,"CAP_AUDIT_READ",strlen(capname)) == 0)
    //	ret = CAP_AUDIT_READ;
    else if (strncmp(capname,"CAP_AUDIT_WRITE",strlen(capname)) == 0)
	ret = CAP_AUDIT_WRITE;
    else if (strncmp(capname,"CAP_BLOCK_SUSPEND",strlen(capname)) == 0)
	ret = CAP_BLOCK_SUSPEND;
    else if (strncmp(capname,"CAP_CHOWN",strlen(capname)) == 0)
	ret = CAP_CHOWN;
    else if (strncmp(capname,"CAP_DAC_OVERRIDE",strlen(capname)) == 0)
	ret = CAP_DAC_OVERRIDE;
    else if (strncmp(capname,"CAP_DAC_READ_SEARCH",strlen(capname)) == 0)
	ret = CAP_DAC_READ_SEARCH;
    else if (strncmp(capname,"CAP_FOWNER",strlen(capname)) == 0)
	ret = CAP_FOWNER;
    else if (strncmp(capname,"CAP_FSETID",strlen(capname)) == 0)
	ret = CAP_FSETID;
    else if (strncmp(capname,"CAP_IPC_LOCK",strlen(capname)) == 0)
	ret = CAP_IPC_LOCK;
    else if (strncmp(capname,"CAP_IPC_OWNER",strlen(capname)) == 0)
	ret = CAP_IPC_OWNER;
    else if (strncmp(capname,"CAP_KILL",strlen(capname)) == 0)
	ret = CAP_KILL;
    else if (strncmp(capname,"CAP_LEASE",strlen(capname)) == 0)
	ret = CAP_LEASE;
    else if (strncmp(capname,"CAP_LINUX_IMMUTABLE",strlen(capname)) == 0)
	ret = CAP_LINUX_IMMUTABLE;
    else if (strncmp(capname,"CAP_MAC_ADMIN",strlen(capname)) == 0)
	ret = CAP_MAC_ADMIN;
    else if (strncmp(capname,"CAP_MAC_OVERRIDE",strlen(capname)) == 0)
	ret = CAP_MAC_OVERRIDE;
    else if (strncmp(capname,"CAP_MKNOD",strlen(capname)) == 0)
	ret = CAP_MKNOD;
    else if (strncmp(capname,"CAP_NET_ADMIN",strlen(capname)) == 0)
	ret = CAP_NET_ADMIN;
    else if (strncmp(capname,"CAP_NET_BIND_SERVICE",strlen(capname)) == 0)
	ret = CAP_NET_BIND_SERVICE;
    else if (strncmp(capname,"CAP_NET_BROADCAST",strlen(capname)) == 0)
	ret = CAP_NET_BROADCAST;
    else if (strncmp(capname,"CAP_NET_RAW",strlen(capname)) == 0)
	ret = CAP_NET_RAW;
    else if (strncmp(capname,"CAP_SETGID",strlen(capname)) == 0)
	ret = CAP_SETGID;
    else if (strncmp(capname,"CAP_SETFCAP",strlen(capname)) == 0)
	ret = CAP_SETFCAP;
    else if (strncmp(capname,"CAP_SETPCAP",strlen(capname)) == 0)
	ret = CAP_SETPCAP;
    else if (strncmp(capname,"CAP_SETUID",strlen(capname)) == 0)
	ret = CAP_SETUID;
    else if (strncmp(capname,"CAP_SYS_ADMIN",strlen(capname)) == 0)
	ret = CAP_SYS_ADMIN;
    else if (strncmp(capname,"CAP_SYS_BOOT",strlen(capname)) == 0)
	ret = CAP_SYS_BOOT;
    else if (strncmp(capname,"CAP_SYS_CHROOT",strlen(capname)) == 0)
	ret = CAP_SYS_CHROOT;
    else if (strncmp(capname,"CAP_SYS_MODULE",strlen(capname)) == 0)
	ret = CAP_SYS_MODULE;
    else if (strncmp(capname,"CAP_SYS_NICE",strlen(capname)) == 0)
	ret = CAP_SYS_NICE;
    else if (strncmp(capname,"CAP_SYS_PACCT",strlen(capname)) == 0)
	ret = CAP_SYS_PACCT;
    else if (strncmp(capname,"CAP_SYS_PTRACE",strlen(capname)) == 0)
	ret = CAP_SYS_PTRACE;
    else if (strncmp(capname,"CAP_SYS_RAWIO",strlen(capname)) == 0)
	ret = CAP_SYS_RAWIO;
    else if (strncmp(capname,"CAP_SYS_RESOURCE",strlen(capname)) == 0)
	ret = CAP_SYS_RESOURCE;
    else if (strncmp(capname,"CAP_SYS_TIME",strlen(capname)) == 0)
	ret = CAP_SYS_TIME;
    else if (strncmp(capname,"CAP_SYS_TTY_CONFIG",strlen(capname)) == 0)
	ret = CAP_SYS_TTY_CONFIG;
    else if (strncmp(capname,"CAP_SYSLOG",strlen(capname)) == 0)
	ret = CAP_SYSLOG;
    else if (strncmp(capname,"CAP_WAKE_ALARM",strlen(capname)) == 0)
	ret = CAP_WAKE_ALARM;
    //else if (strncmp(capname,"",strlen(capname)) == 0)
    //	ret = ;
    else {
	fprintf(stderr,"ERROR: unknown capability %s; aborting!\n",capname);
	exit(-1);
    }

    return ret;
}
