Prerequisites
-------------

  * a basic Linux development environment, including gcc, glibc,
    automake, autoconf, libtool, et al;

  * glib > 2.10 (https://developer.gnome.org/glib/) (this will already
    be installed on any modern Linux distro; you shouldn't need to
    install it from source, ever);

  * our version of OpenMUL (modified to simplify build/install of MUL
    and our controller application); get it at

      https://gitlab.flux.utah.edu/tcloud/openmul 

  * our capability library, libcap; get it at

      https://gitlab.flux.utah.edu/xcap/libcap

  * protobuf-c, a C implementation of Google's Protocol Buffers, and
    protobuf-c depends on protobuf (the Google native version) itself;
    you want to use either protobuf-c 1.1.1 (master branch in git) or
    the `next` branch.  This means you want to install protobuf-2.6.x!

  On Ubuntu, you should be able to get everything (except possibly
  protobuf-c) you need with this command, or similar:

    $ sudo apt-get install build-essential automake autoconf libtool \
        libglib2.0-0 libglib2.0-dev swig libpython2.7-dev

  On Ubuntu 14.04 LTS, as of Dec 2015, the version of libprotobuf-c is
  too old (it is 0.15) (and to install the latest version, you'll also
  need a newer version of Google's protocol buffers stuff).  Here's what
  I did to get that all working for me:

    ### First grab protobuf itself:
    $ cd /tmp
    $ wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.gz
    $ tar -xzvf protobuf-2.6.1.tar.gz
    $ cd protobuf-2.6.1
    $ ./configure --prefix=/usr/local
    $ make && sudo make install

    ### Now protobuf-c
    $ git clone https://github.com/protobuf-c/protobuf-c.git
    $ cd protobuf-c && ./autogen.sh && cd ..
    $ mkdir protobuf-c.obj && cd protobuf-c.obj
    $ ../protobuf-c/configure --prefix=/opt/protobuf-c
    $ make && sudo make install

  Now you should be ready to go with protobuf-c .

Build & Install
---------------

First, run ./autogen.sh in the top source directory.  This will prepare
the source tree to be built.

  $ cd capnet
  $ ./autogen.sh

Second, create a separate object directory; this holds the compiled
binaries and libraries.  You can also build within the source tree, but
usually it's nice to have the separation so you don't have to clean out
lint in your source tree.

  $ cd ..
  $ mkdir capnet.obj
  $ cd capnet.obj

Third, configure the build.  You can run `../capnet/configure --help` to
see a list of configure options.  --prefix is the installation prefix;
you may want to set it to something out-of-the-way like /opt/capnet, or
you might not.  If your glib is installed in a non-standard place, you
can tell configure by passing the appropriate directory to the
--with-glib option.  You'll also need to tell configure where your
libcap and Utah-customized openmul are installed at, using the
--with-libcap and --with-openmul arguments.

  $ ../capnet/configure --prefix=/opt/capnet
        --with-libcap=</path/to/libcap> \
        --with-mul=</path/to/openmul>

Fourth, make and make install!

  $ make && make install
